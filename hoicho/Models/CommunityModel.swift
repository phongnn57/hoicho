//
//  CommunityModel.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 4/21/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit
import SwiftyJSON

class CommunityModel {
    var id = 0
    var content = ""
    var images = [String]()
    var like_count = 0
    var liked = false
    var share_count = 0
    var comment_count = 0
    
    var shop_id = 0
    var account_image = ""
    var account_name = ""
    var account_id = 0
    
    var created_at: Date?
    
    var product: ProductModel?
    
    var comments = [CommentModel]()
    
    init(_ json: JSON) {
        id = json["id"].intValue
        content = json["content"].stringValue.trimpSpace()
        for subJson in json["images"].arrayValue {
            images.append(subJson.stringValue)
        }
        like_count = json["like_count"].intValue
        comment_count = json["comment_count"].intValue
        share_count = json["share_count"].intValue
        liked = json["liked"].boolValue
        shop_id = json["shop_id"].intValue
        account_id = json["account_id"].intValue
        account_name = json["account_name"].stringValue
        account_image = json["account_image"].stringValue
        
        created_at = json["created_at"].stringValue.toDate("yyyy/MM/dd HH:mm:ss")
        
        if json["product"].type != .null {
            product = ProductModel(json["product"])
        }
    }
    
    static func fromJSON(_ response: JSON) -> [CommunityModel] {
        var temp = [CommunityModel]()
        for json in response.arrayValue {
            temp.append(CommunityModel(json))
        }
        return temp
    }
}
