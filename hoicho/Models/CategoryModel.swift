//
//  CategoryModel.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 4/20/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit
import SwiftyJSON

class CategoryModel {
    
    //local val
    var expand = false
    
    //
    var id = 0
    var name = ""
    var image = ""
    var subCate = [CategoryModel]()
    var breadCrumb = [CategoryModel]()
    var product_count = 0
    
    
    init(_ json: JSON) {
        id = json["id"].intValue
        name = json["name"].stringValue
        image = json["image"].stringValue
        if json["breakCum"].exists() {
            for subJson in json["breakCum"].arrayValue {
                breadCrumb.append(CategoryModel(subJson))
            }
        } else {
            breadCrumb.append(self)
        }
        
        product_count = json["count"].intValue
        
        for subJson in json["subCate"].arrayValue {
            let subCategory = CategoryModel(subJson)
            subCategory.breadCrumb = [self, subCategory]
            subCate.append(subCategory)
        }
    }
    
    static func fromJSON(_ response: JSON) -> [CategoryModel] {
        var temp = [CategoryModel]()
        for json in response.arrayValue {
            temp.append(CategoryModel(json))
        }
        return temp
    }
    
}
