//
//  DataManager.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 6/14/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit
import RealmSwift

class DataManager {
    
    static let shared = DataManager()
    
    let realm = try! Realm()

    init() {
        
    }
    
    func addProduct(product: ProductModel) {
        let objects = realm.objects(ProductRealm.self).filter("id == \(product.id)")
        if objects.count == 0 {
            let newProduct = ProductRealm()
            newProduct.name = product.name
            newProduct.id = product.id
            newProduct.price = product.price.intValue
            newProduct.image = product.image
            try! realm.write {
                realm.add(newProduct)
            }
            
        } else {
            guard let existProduct = objects.first else {return}
            try! realm.write {
                existProduct.name = product.name
                existProduct.id = product.id
                existProduct.price = product.price.intValue
                existProduct.image = product.image
            }
        }
    }
    
    func getProducts() -> [ProductModel] {
        var temp = [ProductModel]()
        let objects = realm.objects(ProductRealm.self)
        for object in objects {
            let product = ProductModel()
            product.name = object.name
            product.id = object.id
            product.price = NSNumber(value: object.price)
            product.image = object.image
            temp.append(product)
        }
        return temp
    }
    
}

class ProductRealm: Object {
    @objc dynamic var name = ""
    @objc dynamic var id = 0
    @objc dynamic var image = ""
    @objc dynamic var price = 0
}
