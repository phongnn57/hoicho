//
//  CommentModel.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 4/26/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit
import SwiftyJSON

class CommentModel {
    
    //local
    var expand = false
    
    //
    var account_name = ""
    var account_id = 0
    var content = ""
    var post_id = 0
    var id = 0
    var status = 0
    var like_count = 0
    var images = [String]()
    var files = [String]()
    var time = ""
    var comment_count = 0
    var share_count = 0
    var account_image = ""
    var updated_at: Date?
    var rate: Double = 0
    
    var comments = [CommentModel]()
    
    init(_ json: JSON) {
        account_name = json["account_name"].stringValue
        account_id = json["account_id"].intValue
        content = json["content"].stringValue
        post_id = json["post_id"].intValue
        id = json["id"].intValue
        status = json["status"].intValue
        like_count = json["like_count"].intValue
        comment_count = json["comment_count"].intValue
        share_count = json["share_count"].intValue
        account_image = json["account_image"].stringValue
        updated_at = json["updated_at"].stringValue.toDate("yyyy/MM/dd HH:mm:ss")
        rate = json["rate"].doubleValue
        if let date = json["time"].stringValue.toDate("yyyy/MM/dd HH:mm:ss") {
            time = date.toDateTimeString()
        } else {
            time = json["time"].stringValue
        }
        if let dict = json["last_comment"].dictionary {
            comments.append(CommentModel(JSON(dict)))
        }
        for subJson in json["images"].arrayValue {
            images.append(subJson.stringValue)
        }
        
        for subJson in json["files"].arrayValue {
            files.append(subJson.stringValue)
        }
    }
    
    static func fromJSON(_ response: JSON) -> [CommentModel] {
        var temp = [CommentModel]()
        for json in response.arrayValue {
            temp.append(CommentModel(json))
        }
        return temp
    }
}
