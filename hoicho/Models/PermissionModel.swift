//
//  PermissionModel.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 6/18/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit
import SwiftyJSON


class PermissionModel: NSObject {
    
    var name = ""
    var value = false
    var key = ""
    var submenu = [PermissionModel]()
    var hasRole = false
    var expand = false
    
    init(_ json: JSON) {
        name = json["name"].stringValue.trimpSpace()
        value = json["value"].boolValue
        key = json["key"].stringValue
        if json["sub_menu"].exists() {
            for subJSON in json["sub_menu"].arrayValue {
                submenu.append(PermissionModel(subJSON))
            }
        } else if json["role"].exists() {
            for subJSON in json["role"].arrayValue {
                submenu.append(PermissionModel(subJSON))
            }
            if submenu.count > 0 {
                self.hasRole = true
            }
        }
        
    }
    
    func hasActiveRole() -> Bool {
        for item in self.submenu {
            if item.value {
                return true
            }
        }
        return false
    }
    
    class func listAll(_ objects: [PermissionModel]) -> [PermissionModel] {
        var temp = [PermissionModel]()
        
        for object in objects {
            if object.hasRole {
                temp.append(object)
            } else {
                temp.append(contentsOf: listAll(object.submenu))
            }
        }
        
        for object in temp {
            for item in object.submenu {
                item.value = false
            }
        }
        
        return temp
    }
    
    class func showAllAvailableMenu(_ objects: [PermissionModel]) -> [SettingModel] {
        var temp = [SettingModel]()
        if let array = NSArray(contentsOfFile:Bundle.main.path(forResource: "ChuHeThong", ofType: "plist")!) {
            for item in array {
                let settingModel = SettingModel(JSON(item))
                if settingModel.enable {
                    temp.append(settingModel)
                }
            }
        }
        
        for object in objects {
            print(object.name)
        }
        

        return temp

    }
    
    func mapSettingItemWithPermission(model: SettingItem) {
        switch self.name {

        default:
            break
        }
        
    }
    
    func mapSettingModelWithPermission(model: SettingModel) {
        switch self.name {
        
        default:
            break
        }
    }
    
}
