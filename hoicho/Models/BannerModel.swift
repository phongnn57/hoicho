//
//  BannerModel.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 4/27/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit
import SwiftyJSON

class BannerModel {

    var image = ""
    var action_url = ""
    
    init(_ json: JSON) {
        image = json["image"].stringValue
        action_url = json["action_url"].stringValue
    }
    
    static func fromJSON(_ response: JSON) -> [BannerModel] {
        var temp = [BannerModel]()
        for json in response.arrayValue {
            temp.append(BannerModel(json))
        }
        return temp
    }
}
