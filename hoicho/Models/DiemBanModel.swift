//
//  DiemBanModel.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 4/27/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit
import SwiftyJSON

class DiemBanModel {
    var id = 0
    var price: NSNumber = 0
    var phone = ""
    var name = ""
    var city = ""
    var district = ""
    var address = ""
    var account_id = 0
    var chat_id = 0
    
    init() {
        
    }
    
    init(_ json: JSON) {
        id = json["id"].intValue
        price = json["price"].numberValue
        phone = json["phone"].stringValue
        name = json["name"].stringValue
        city = json["city"].stringValue
        district = json["district"].stringValue
        address = json["address"].stringValue
        account_id = json["account_id"].intValue
        chat_id = json["chat_id"].intValue
    }
    
    static func fromJSON(_ response: JSON) -> [DiemBanModel] {
        var temp = [DiemBanModel]()
        for json in response.arrayValue {
            temp.append(DiemBanModel(json))
        }
        return temp
    }
}
