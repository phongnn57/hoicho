//
//  UserModel.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 4/18/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit
import SwiftyJSON
import Locksmith

enum FriendStatus: Int {
    case not_friend = -1
    case request_sent = 0
    case friend = 1
    case accept = 2
}

enum UserType: String {
    case chu_he_thong = "Chủ hội chợ"
    case gian_hang = "Chủ gian hàng"
    case thanh_vien = "Thành viên"
    case quan_tri_vien = "Quản trị viên"
}

class UserModel: NSObject, NSCopying {
    
    static var shared: UserModel = {
        
        let instance = UserModel()
        return instance
        
    }()
    
    var id = 0
    var name = ""
    var phone = ""
    var token = ""
    var image = ""
    var birthday: Date?
    var email = ""
    var region = ""
    var district = ""
    var address = ""
    var created_at: Date?
    var type: UserType?
    var type_text_expo = ""
    var company = ""
    var introduction = ""
    var booth_name = ""
    var sale_point_name = ""
    
    func copy(with zone: NSZone? = nil) -> Any {
        let copy = UserModel()
        copy.phone = self.phone
        copy.name = self.name
        return copy
    }
    
    override init() {
        
    }
    
    init(_ phone: String) {
        self.phone = phone
    }
    
    
    init(_ json: JSON) {
        if json["token"].exists() {
            token = json["token"].stringValue
        }
        
        var userjson = json
        if json["user"].exists() {
            userjson = json["user"]
        }
        id = userjson["id"].intValue
        name = userjson["name"].stringValue
        phone = userjson["phone"].stringValue
        image = userjson["image"].stringValue
        birthday = userjson["birthday"].stringValue.toDate("yyyy/MM/dd HH:mm:ss")
        email = userjson["email"].stringValue
        region = userjson["region"].stringValue
        address = userjson["address"].stringValue
        created_at = userjson["created_at"].stringValue.toDate("yyyy/MM/dd HH:mm:ss")
        type = UserType.init(rawValue: userjson["type"].stringValue)
        company = userjson["company"].stringValue
        type_text_expo = userjson["type_text_expo"].stringValue
        introduction = userjson["introduction"].stringValue
        booth_name = json["booth_name"].stringValue
        sale_point_name = json["sale_point_name"].stringValue
        district = userjson["district"].stringValue
        if userjson["friend_id"].exists() {
            id = userjson["friend_id"].intValue
        } else if userjson["account_id"].exists() {
            id = userjson["account_id"].intValue
        }
    }
    
    func save(_ json: JSON, _ tokenString: String? = "") {
        do {
            if let dictionary = json.dictionaryObject {
                var temp = dictionary
                if tokenString! != "" {
                    temp["token"] = tokenString!
                }
                
                try Locksmith.updateData(data: temp, forUserAccount: "hc_user")
            }
            
        } catch _ {}
        
    }
    
    func updateToken(token: String) {
        
        self.token = token
        
        if let dictionary = Locksmith.loadDataForUserAccount(userAccount: "hc_user") {
            save(JSON(dictionary), token)
        }
        
    }

    class func updateUserPhone(_ phone: String) {
        do {
            try Locksmith.updateData(data: ["phone": phone], forUserAccount: "hc_user_phone")
        } catch _ {}
    }
    
    class func currentUserPhone() -> String {
        if let dictionary = Locksmith.loadDataForUserAccount(userAccount: "hc_user_phone") {
            return JSON(dictionary)["phone"].stringValue
        } else {
            return ""
        }
    }
    
    class func deleteCurrentUserPhone() {
        do {
            try Locksmith.deleteDataForUserAccount(userAccount: "hc_user_phone")
        } catch _ {}
    }
    
    func delete() {
        do {
            try Locksmith.deleteDataForUserAccount(userAccount: "hc_user")
            UserModel.shared = UserModel()
        } catch _ {}
    }

    
    class func loadData() {
        if let dictionary = Locksmith.loadDataForUserAccount(userAccount: "hc_user") {
            UserModel.shared = UserModel(JSON(dictionary))
        } else {
            UserModel.shared = UserModel()
        }
    }
    
    //
    func isAdmin() -> Bool {
        if !token.isEmpty && type != nil && (type! == .chu_he_thong || type! == .quan_tri_vien) {
            return true
        }
        return false
    }
}
