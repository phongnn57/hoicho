//
//  ExpoModel.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 6/18/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit
import SwiftyJSON

class ExpoLocationModel: NSObject {
    var id = 0
    var booth_id = 0
    var priority = 0
    var booth: GianHangModel?
    
    init(_ json: JSON) {
        id = json["id"].intValue
        booth_id = json["booth_id"].intValue
        priority = json["priority"].intValue
        booth = GianHangModel(json["booth"])
    }
    
    static func fromJSON(_ response: JSON) -> [ExpoLocationModel] {
        var temp = [ExpoLocationModel]()
        for json in response.arrayValue {
            temp.append(ExpoLocationModel(json))
        }
        return temp
    }
}

class ExpoModel: NSObject {
    var start_time: Date?
    var name = ""
    var id = 0
    var image = ""
    var end_time: Date?
    var address = ""
    var count = 0
    var description_text = ""
    var map = ""
    var qrcode = ""
    
    var locations = [ExpoLocationModel]()
    
    init(_ json: JSON) {
        start_time = json["start_time"].stringValue.toDate("yyyy/MM/dd HH:mm:ss")
        name = json["name"].stringValue
        id = json["id"].intValue
        image = json["image"].stringValue
        end_time = json["end_time"].stringValue.toDate("yyyy/MM/dd HH:mm:ss")
        address = json["address"].stringValue
        count = json["count"].intValue
        description_text = json["description"].stringValue
        map = json["map"].stringValue
        qrcode = json["qrcode"].stringValue
    }
    
    static func fromJSON(_ response: JSON) -> [ExpoModel] {
        var temp = [ExpoModel]()
        for json in response.arrayValue {
            temp.append(ExpoModel(json))
        }
        return temp
    }
}
