//
//  TongHopModel.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 5/14/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit
import SwiftyJSON

class TongHopModel: NSObject {
    var id = 0
    var account_name = ""
    var created_at: Date?
    var category_id = 0
    var category_name = ""
    var status_text = ""
    var name = ""
    var image = ""
    var short_content = ""
    
    init(_ json: JSON) {
        id = json["id"].intValue
        account_name = json["account_name"].stringValue
        created_at = json["created_at"].stringValue.toDate("yyyy/MM/dd HH:mm:ss")
        category_id = json["category_id"].intValue
        category_name = json["category_name"].stringValue
        name = json["name"].stringValue
        status_text = json["status_text"].stringValue
        image = json["image"].stringValue
        short_content = json["short_content"].stringValue.trimpSpace()
    }
    
    static func fromJSON(_ response: JSON) -> [TongHopModel] {
        var temp = [TongHopModel]()
        for json in response.arrayValue {
            temp.append(TongHopModel(json))
        }
        return temp
    }
}

class DanhMucModel: NSObject {
    
    var id = 0
    var style = 0
    var name = ""
    var type = 0
    var title = ""
    var meta_keyword = ""
    var meta_description = ""
    var desc = ""
    
    init(_ json: JSON) {
        id = json["id"].intValue
        style = json["style"].intValue
        name = json["name"].stringValue
        type = json["type"].intValue
        title = json["title"].stringValue
        meta_keyword = json["meta_keyword"].stringValue
        meta_description = json["meta_description"].stringValue
        desc = json["description"].stringValue
    }
    
    static func fromJSON(_ response: JSON) -> [DanhMucModel] {
        var temp = [DanhMucModel]()
        for json in response.arrayValue {
            temp.append(DanhMucModel(json))
        }
        return temp
    }
}
