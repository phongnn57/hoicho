//
//  ContactModel.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 4/22/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit
import SwiftyJSON

class ContactModel {
    var id = 0
    var idConversions = ""
    var content = ""
    var date_care = ""
    var phone = ""
    var name = ""
    var image = ""
    var type_text = ""
    
    init(_ json: JSON) {
        id = json["id"].intValue
        phone = json["phone"].stringValue
        name = json["name"].stringValue.trimpSpace()
        image = json["image"].stringValue
        type_text = json["type_text"].stringValue
        idConversions = json["idConversions"].stringValue
        content = json["content"].stringValue.replacingOccurrences(of: "<br>", with: "\n")
        date_care = json["date_care"].stringValue
    }
    
    static func fromJSON(response: JSON) -> [ContactModel] {
        var temp = [ContactModel]()
        for json in response.arrayValue {
            temp.append(ContactModel(json))
        }
        return temp
    }
}
