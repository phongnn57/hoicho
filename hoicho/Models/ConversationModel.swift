//
//  ConversationModel.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 4/22/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit
import SwiftyJSON

class ConversationInfo {
    var title = ""
    var listMember = [UserModel]()
    var type = 0
    var image = ""
    
    init(_ json: JSON) {
        title = json["title"].stringValue
        for subJson in json["listMember"].arrayValue {
            listMember.append(UserModel(subJson))
        }
        type = json["type"].intValue
        image = json["image"].stringValue
    }
}

class ConversationModel {
    
    var id = 0
    var content = ""
    var time = ""
    var image = ""
    var read = false
    var id_conversations = ""
    var name = ""
    var last_msg_time: Date!
    
    var info: ConversationInfo?
    
    init() {
        
    }
    
    init(_ json: JSON) {
        id = json["id"].intValue
        content = json["content"].stringValue.replacingOccurrences(of: "<br>", with: "\n")
        time = json["time"].stringValue
        image = json["image"].stringValue
        read = json["read"].boolValue
        id_conversations = json["id_conversions"].stringValue
        name = json["name"].stringValue
        
        if json["idConversion"].exists() {
            id_conversations = json["idConversion"].stringValue
        }
        if json["timeRender"].exists() {
            time = json["timeRender"].stringValue
        }
        if json["avatar"].exists() {
            image = json["avatar"].stringValue
        }
        if json["title"].exists() {
            name = json["title"].stringValue
        }
        if json["api_content"].exists() {
            content = json["api_content"].stringValue
        }
        if let date = json["last_msg_time"].stringValue.toDate("dd/MM/yyyy HH:mm:ss") {
            last_msg_time = date
        }
        if json["api_time"].exists(), let date = json["api_time"].stringValue.toDate("HH:mm:ss dd/MM/yyyy") {
            self.last_msg_time = date
        }
        if json["conversation_name"].exists() {
            name = json["conversation_name"].stringValue
        }
    }
    
    static func fromJSON(response: JSON) -> [ConversationModel] {
        var temp = [ConversationModel]()
        for json in response.arrayValue {
            temp.append(ConversationModel(json))
        }
        return temp
    }
    
    func copyTo(_ object: ConversationModel) {
        object.id_conversations = self.id_conversations
        object.time = self.time
        object.content = self.content
        object.last_msg_time = self.last_msg_time
        object.read = self.read
    }
}
