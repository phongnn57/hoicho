//
//  BrandModel.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 4/21/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit
import SwiftyJSON

class BrandModel {

    var id = 0
    var name = ""
    var logo = ""
    var featured = false
    
    init(_ json: JSON) {
        id = json["id"].intValue
        name = json["name"].stringValue
        logo = json["logo"].stringValue
        featured = json["is_featured"].boolValue
    }
    
    static func fromJSON(_ response: JSON) -> [BrandModel] {
        var temp = [BrandModel]()
        for json in response.arrayValue {
            temp.append(BrandModel(json))
        }
        return temp
    }
}
