//
//  TicketModel.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 5/14/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit
import SwiftyJSON

class TicketModel {
    var id = 0
    var code = ""
    var name = ""
    var phone = ""
    var city = ""
    var time: Date?
    var account_id = 0
    
    init(_ json: JSON) {
        id = json["id"].intValue
        code = json["code"].stringValue
        name = json["name"].stringValue
        phone = json["phone"].stringValue
        city = json["city"].stringValue
        time = json["created_at"].stringValue.toDate("yyyy/MM/dd HH:mm:ss")
        account_id = json["account_id"].intValue
    }
    
    static func fromJSON(_ response: JSON) -> [TicketModel] {
        var temp = [TicketModel]()
        for json in response.arrayValue {
            temp.append(TicketModel(json))
        }
        return temp
    }
}
