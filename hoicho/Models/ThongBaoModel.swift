//
//  ThongBaoModel.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 6/6/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit
import SwiftyJSON
import DateToolsSwift

enum PayloadType: String, EnumEnumerable {
    
    case don_hang = "don_hang"
    case tin_tuc = "tin_tuc"
    case phan_hoi = "phan_hoi"
    case cong_dong = "cong_dong"
    case dao_tao = "dao_tao"
    case chinh_sach = "chinh_sach"
    case tuyen_dung = "tuyen_dung"
    case cong_viec = "cong_viec"
    case san_pham = "san_pham"
    case bao_loi = "error_tech"
    case chat = "chat"
    case friend = "ket_ban"
}

class PayloadModel: NSObject {
    var id = 0
    var type: PayloadType!
    var notificationId = 0
    var domain = ""
    var phone = ""
    var idConversion = ""
    
    override init() {
        
    }
    
    init(_ json: JSON) {
        id = json["id"].intValue
        if json["friend_id"].exists() {
            id = json["friend_id"].intValue
        }
        if let _type = PayloadType(rawValue: json["type"].stringValue) {
            self.type = _type
        }
        domain = json["domain"].stringValue
        phone = json["phone"].stringValue
        notificationId = json["notification_id"].intValue
        idConversion = json["idConversion"].stringValue

    }
}

class NotificationModel: NSObject {
    var id = 0
    var title = ""
    var content = ""
    var createdAt: Date?
    var isRead = false
    var payload: PayloadModel!
    var image = ""
    var sender = ""
    var account_name = ""
    var account_image = ""
    var short_description = ""
    
    init(_ json: JSON) {
        id = json["id"].intValue
        title = json["title"].stringValue
        content = json["content"].stringValue
        payload = PayloadModel(json["payload_data"])
        isRead = json["is_read"].boolValue
        short_description = json["short_description"].stringValue

        createdAt = json["created_at"].stringValue.toDate("yyyy/MM/dd HH:mm:ss")
        image = json["image"].stringValue
        sender = json["sender"].stringValue
        account_name = json["account"].stringValue
        account_image = json["account_image"].stringValue
    }
    
}
