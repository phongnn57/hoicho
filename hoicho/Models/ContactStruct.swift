//
//  ContactStruct.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 6/8/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

struct ContactStruct {
    var givenName = ""
    var familyName = ""
    var phoneNumber = ""
    var alphabetName = ""
}
