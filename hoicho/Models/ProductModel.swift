//
//  ProductModel.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 4/20/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit
import SwiftyJSON

open class ProductModel {
    
    var id = 0
    var code = ""
    var name = ""
    var tt_price: NSNumber = 0
    var price: NSNumber = 0
    var description_text = ""
    var images = [String]()
    var image = ""
    var brand: BrandModel?
    var provider_id = 0
    var is_featured = false
    var liked = false
    var likes = 0
    var shares = 0
    var comments = 0
    var link_affiliate = ""
    var wholesale_count_product = 0
    var wholesale_price_from: NSNumber = 0
    var wholesale_price_to: NSNumber = 0
    var view_wholesale = false
    
    var booth: GianHangModel?
    
    init() {
        
    }
    
    init(_ json: JSON) {
        id = json["id"].intValue
        code = json["code"].stringValue
        name = json["name"].stringValue.trimpSpace()
        tt_price = json["tt_price"].numberValue
        price = json["price"].numberValue
        description_text = json["description"].stringValue
        image = json["image"].stringValue
        for subJson in json["images"].arrayValue {
            images.append(subJson.stringValue)
        }
        brand = BrandModel(json["department"])
        provider_id = json["provider_id"].intValue
        booth = GianHangModel(json["booth"])
        is_featured = json["is_featured"].boolValue
        likes = json["likes"].intValue
        comments = json["comments"].intValue
        shares = json["shares"].intValue
        liked = json["liked"].boolValue
        
        link_affiliate = json["link_affiliate"].stringValue
        if json["link"].exists() && !json["link_affiliate"].exists() {
            link_affiliate = json["link"].stringValue
        }
        
        wholesale_count_product = json["wholesale_count_product"].intValue
        wholesale_price_from = json["wholesale_price_from"].numberValue
        wholesale_price_to = json["wholesale_price_to"].numberValue
        view_wholesale = json["view_wholesale"].boolValue
    }
    
    static func fromJSON(_ response: JSON) -> [ProductModel] {
        var temp = [ProductModel]()
        for json in response.arrayValue {
            temp.append(ProductModel(json))
        }
        return temp
    }

}
