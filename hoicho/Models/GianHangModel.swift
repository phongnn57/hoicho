//
//  GianHangModel.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 4/27/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit
import SwiftyJSON

class RateModel {
    var id = 0
    var content = ""
    var rate_point: Double = 0
    var account_name = ""
    var account_image = ""
    
    var time: Date?
    
    init(_ json: JSON) {
        id = json["id"].intValue
        content = json["content"].stringValue
        rate_point = json["rate_point"].doubleValue
        account_name = json["account"]["name"].stringValue
        account_image = json["account"]["image"].stringValue
        
        time = json["time"].stringValue.toDate("yyyy/MM/dd HH:mm:ss")
    }
    
    static func fromJSON(_ response: JSON) -> [RateModel] {
        var temp = [RateModel]()
        for json in response.arrayValue {
            temp.append(RateModel(json))
        }
        return temp
    }
}

class GianHangModel {

    var id = 0
    var info = ""
    var hotline = ""
    var name = ""
    var introduction = ""
    var banner = ""
    var address = ""
    var product_count = 0
    var member_cnt = 0
    var rate_count = 0
    var created_at: Date?
    var company_store = ""
    var qrcode = ""
    var follow = false
    var account_id = 0
    var visit = 0
    var count_follow = 0
    
    init(_ json: JSON) {
        id = json["id"].intValue
        info = json["info"].stringValue
        if json["hotline"].exists() {
            hotline = json["hotline"].stringValue
        } else if json["phone"].exists() {
            hotline = json["phone"].stringValue
        }
        follow = json["follow"].boolValue
        name = json["name"].stringValue
        introduction = json["introduction"].stringValue
        banner = json["banner"].stringValue
        if json["address"].exists() {
            address = json["address"].stringValue.trimpSpace()
        } else if json["region"].exists() {
            address = json["region"].stringValue.trimpSpace()
        } else if json["city"].exists() {
            address = json["city"].stringValue.trimpSpace()
        }
        account_id = json["account_id"].intValue
        if json["count"].exists() {
            product_count = json["count"].intValue
        } else if json["number_product"].exists() {
            product_count = json["number_product"].intValue
        } else if json["product_count"].exists() {
            product_count = json["product_count"].intValue
        }
        created_at = json["created_at"].stringValue.toDate("yyyy/MM/dd HH:mm:ss")
        rate_count = json["rate"].intValue
        company_store = json["company_store"].stringValue
        member_cnt = json["member_cnt"].intValue
        qrcode = json["qrcode"].stringValue
        visit = json["visit"].intValue
        count_follow = json["count_follow"].intValue
    }
    
    static func fromJSON(_ response: JSON) -> [GianHangModel] {
        var temp = [GianHangModel]()
        for json in response.arrayValue {
            temp.append(GianHangModel(json))
        }
        return temp
    }
}
