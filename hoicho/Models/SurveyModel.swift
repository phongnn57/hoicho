//
//  SurveyModel.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 6/11/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit
import SwiftyJSON

class QuestionModel: NSObject {
    var id = 0
    var title = ""
    var answers = [AnswerModel]()
    
    init(_ json: JSON) {
        id = json["id"].intValue
        title = json["title"].stringValue
        for subJSON in json["answers"].arrayValue {
            answers.append(AnswerModel(subJSON))
        }
    }
}

class AnswerModel: NSObject {
    var id = 0
    var content = ""
    var type = 0
    var selected = false
    var answer_content = ""
    
    init(_ json: JSON) {
        id = json["id"].intValue
        content = json["content"].stringValue
        type = json["type"].intValue
    }
}

class SurveyModel: NSObject {
    var status = 0
    var name = ""
    var id = 0
    var questions = [QuestionModel]()
    
    init(_ json: JSON) {
        status = json["status"].intValue
        name = json["name"].stringValue
        id = json["id"].intValue
        for subJSON in json["questions"].arrayValue {
            questions.append(QuestionModel(subJSON))
        }
    }
}
