//
//  ChatModel.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 5/14/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit
import SwiftyJSON
import SDWebImage

class ChatContactModel: NSObject {
    var id = 0
    var idConversions = ""
    var content = ""
    var date_care = ""
    var phone = ""
    var name = ""
    var image = ""
    var type_text = ""
    
    init(_ json: JSON) {
        id = json["id"].intValue
        phone = json["phone"].stringValue
        name = json["name"].stringValue
        image = json["image"].stringValue
        type_text = json["type_text"].stringValue
        idConversions = json["idConversions"].stringValue
        content = json["content"].stringValue.replacingOccurrences(of: "<br>", with: "\n")
        date_care = json["date_care"].stringValue
    }
}

class ChatModel: NSObject {
    
    var from = 0
    var content = ""
    var images = [String]()
    var account_name = ""
    var created_at: Date!
    var id_conversation = ""
    var account_avatar = ""
    var id = 0
    var ui_id = ""
    var image: UIImage?
    var type = 0
    var product: ProductModel?
    
    //
    var singleImageSize: CGSize = CGSize(width: UIScreen.main.bounds.width/2, height: UIScreen.main.bounds.width/2)
    
    init(content: String, ui_id: String, id_conversation: String, image: UIImage?) {
        self.ui_id = ui_id
        self.from = UserModel.shared.id
        self.account_name = UserModel.shared.name
        self.created_at = Date()
        self.id_conversation = id_conversation
        self.account_avatar = UserModel.shared.image
        self.content = content
        self.image = image
    }
    
    init(_ json: JSON) {
        super.init()
        id = json["id"].intValue
        from = json["from"].intValue
        content = json["content"].stringValue.replacingOccurrences(of: "<br>", with: "\n").trimpSpace()
        for subJson in json["images"].arrayValue {
            images.append(subJson.stringValue)
        }
        account_name = json["account_name"].stringValue
        if let date = json["created_at"].stringValue.toDate("yyyy/MM/dd HH:mm:ss") {
            created_at = date
        }
        id_conversation = json["id_conversation"].stringValue
        account_avatar = json["account_avatar"].stringValue
        type = json["type"].intValue
        if json["timeRender"].exists() {
            
        }
        if json["title"].exists() {
            account_name = json["title"].stringValue
        }
        if json["idConversion"].exists() {
            id_conversation = json["idConversion"].stringValue
        }
        if json["avatar"].exists() {
            account_avatar = json["avatar"].stringValue
        }
        if json["image"].exists() {
            account_avatar = json["image"].stringValue
        }
        if json["api_content"].exists() {
            content = json["api_content"].stringValue.replacingOccurrences(of: "<br>", with: "\n")
        }
        if json["name"].exists() {
            account_name = json["name"].stringValue
        }
        ui_id = json["ui_id"].stringValue
        
        self.calculateSingleImageSize()
        
        if json["product"].exists() {
            product = ProductModel(json["product"])
        }
    }
    
    static func fromJSON(_ response: JSON) -> [ChatModel] {
        var temp = [ChatModel]()
        for json in response.arrayValue {
            temp.append(ChatModel(json))
        }
        return temp
    }
    
    func calculateSingleImageSize() {
        if images.count == 1 {
            if let image = SDImageCache.shared().imageFromCache(forKey: images[0]) {
                if image.size.height > image.size.width {
                    
                    let itemHeight = UIScreen.main.bounds.height/2
                    let itemWidth = itemHeight * image.size.width / image.size.height
                    self.singleImageSize = CGSize(width: itemWidth, height: itemHeight)
                } else {
                    let itemWidth = UIScreen.main.bounds.width * 3/4
                    let itemHeight = itemWidth * image.size.height / image.size.width
                    self.singleImageSize = CGSize(width: itemWidth, height: itemHeight)
                }
                
            }
        }
        
    }
    
}
