//
//  RegionModel.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 4/22/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit
import SwiftyJSON
import CoreLocation

class RegionModel {
    
    var id = ""
    var name = ""
    var type = ""
    var coordinate: CLLocationCoordinate2D?
    
    init() {
        
    }
    
    init(name: String) {
        self.name = name
    }
    
    init(_ json: JSON) {
        if json["id"].exists() {
            id = json["id"].stringValue
        } else if json["provinceid"].exists() {
            id = json["provinceid"].stringValue
        }
        coordinate = CLLocationCoordinate2D(latitude: json["lat"].doubleValue, longitude: json["lng"].doubleValue)
        name = json["name"].stringValue
        type = json["type"].stringValue
    }
    
    static func fromJSON(response: JSON) -> [RegionModel] {
        var temp = [RegionModel]()
        for json in response.arrayValue {
            temp.append(RegionModel(json))
        }
        return temp
    }

}

class DistrictModel {
    var name = ""
    var type = ""
    var coordinate: CLLocationCoordinate2D?
    var districtid = ""
    
    init(_ json: JSON) {
        name = json["name"].stringValue
        type = json["type"].stringValue
        districtid = json["districtid"].stringValue
        coordinate = CLLocationCoordinate2D(latitude: json["lat"].doubleValue, longitude: json["lng"].doubleValue)
    }
    
    init(name: String) {
        self.name = name
    }
    
    static func fromJSON(response: JSON) -> [DistrictModel] {
        var temp = [DistrictModel]()
        for json in response.arrayValue {
            temp.append(DistrictModel(json))
        }
        return temp
    }
}
