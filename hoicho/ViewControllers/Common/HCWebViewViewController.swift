//
//  HCWebViewViewController.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 4/19/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit



class HCWebViewViewController: UIViewController {
    
    @IBOutlet weak var webView: UIWebView!
    
    var html: String = ""
    var url: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if !html.isEmpty {
            webView.loadHTMLString(html.addCssStyle(), baseURL: nil)
        } else if !url.isEmpty {
            webView.loadRequest(URLRequest.init(url: URL(string: url)!))
        }
        
    }

    

}
