//
//  KhuVucViewController.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 4/18/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

class KhuVucViewController: UIViewController {

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    var refreshControl: UIRefreshControl!
    
    var provinces = [RegionModel]() {
        didSet {
            tableView?.reloadData()
        }
    }
    var filterProvinces = [RegionModel]() {
        didSet {
            tableView?.reloadData()
        }
    }
    var isSearching = false {
        didSet {
            tableView?.reloadData()
            if !isSearching {
                filterProvinces = []
                filterDistricts = []
            }
        }
    }
    var provinceId: String!
    var provinceName = ""
    var districts = [DistrictModel]() {
        didSet {
            tableView?.reloadData()
        }
    }
    var filterDistricts = [DistrictModel]() {
        didSet {
            tableView?.reloadData()
        }
    }
    var completion: ((RegionModel?) -> Void)?
    var completionDistrict: ((DistrictModel) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if provinceId == nil {
            title = "Chọn khu vực"
        } else {
            title = "Chọn quận huyện"
        }
        
        refreshControl = initRefreshControl()
        if #available(iOS 10, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        tableView.tableFooterView = UIView()
        loadData()
        
    }
    
    func loadData(refresh: Bool = false) {
        if !refresh {
            showProgress()
        }
        if provinceId == nil {
            HCService.getRegions(completion: { (objects) in
                self.dismissProgress()
                self.refreshControl.endRefreshing()
                self.provinces = objects
            }) { (error) in
                self.refreshControl.endRefreshing()
                self.showError(error)
            }
        } else {
            HCService.getDistricts(id: provinceId, name: provinceName, completion: { (objects) in
                self.dismissProgress()
                self.refreshControl.endRefreshing()
                self.districts = objects
            }) { (error) in
                self.refreshControl.endRefreshing()
                self.showError(error)
            }
        }
        
    }
    
    override func doRefresh() {
        loadData(refresh: true)
    }

}

extension KhuVucViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if provinceId == nil {
            if isSearching {
                return filterProvinces.count
            }
            return provinces.count
        }
        if isSearching {
            return filterDistricts.count
        }
        return districts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "KhuVucViewCell")
        if cell == nil {
            cell = UITableViewCell.init(style: .default, reuseIdentifier: "KhuVucViewCell")
        }
        
        if provinceId == nil {
            if isSearching {
                cell?.textLabel?.text = filterProvinces[indexPath.row].type + " " + filterProvinces[indexPath.row].name
            } else {
                cell?.textLabel?.text = provinces[indexPath.row].type + " " +  provinces[indexPath.row].name
            }
        } else {
            if isSearching {
                cell?.textLabel?.text = filterDistricts[indexPath.row].type + " " + filterDistricts[indexPath.row].name
            } else {
                cell?.textLabel?.text = districts[indexPath.row].type + " " + districts[indexPath.row].name
            }
        }

        cell?.textLabel?.font = UIFont.systemFont(ofSize: 14, weight: .semibold)
        cell?.textLabel?.textColor = UIColor(hex: "#333333")
        
        return cell!
    }
}

extension KhuVucViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        searchBar?.resignFirstResponder()
        if provinceId == nil {
            if isSearching {
                completion?(filterProvinces[indexPath.row])
            } else {
                completion?(provinces[indexPath.row])
            }
        } else {
            if isSearching {
                completionDistrict?(filterDistricts[indexPath.row])
            } else {
                completionDistrict?(districts[indexPath.row])
            }
        }
        
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}

extension KhuVucViewController: UISearchBarDelegate {
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        isSearching = true
        searchBar.showsCancelButton = true
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = nil
        searchBar.resignFirstResponder()
        isSearching = false
        
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
        isSearching = true
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        let predicate = NSPredicate(format: "SELF contains[c] %@", searchText)
        if provinceId == nil {
            filterProvinces = provinces.filter {predicate.evaluate(with: $0.name)}
        } else {
            filterDistricts = districts.filter {predicate.evaluate(with: $0.name)}
        }
        
    }
}
