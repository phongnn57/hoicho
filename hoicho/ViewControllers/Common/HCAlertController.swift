//
//  HCAlertController.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 4/18/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit


typealias HCAlertControllerCompletion = (_ alertView: HCAlertController, _ buttonIndex: Int) -> Void
typealias HCAlertControllerShownCompletion = () -> Void
typealias HCAlertControllerRemovedCompletion = () -> Void
typealias HCAlertCompletion = (Int) -> Void

class HCAlertController: NSObject {
    
    var tag: Int = 0
    var title: String?
    var message: String?
    var buttonTitles: Array<String>!
    var cancelButtonIndex: Int = 0
    var isShowing = false
    var autoRemoving = false
    var autoRemovedCompletion: HCAlertControllerRemovedCompletion!
    
    private var hasNotification = false
    var completion: HCAlertControllerCompletion!
    private var alert: AnyObject?
    
    init(title: String?, message: String?, completion: HCAlertControllerCompletion!, cancelButtonTitle: String?, otherButtonTitle: String?) {
        super.init()
        
        DispatchQueue.main.async {
            
            self.title = title
            self.message = message
            
            self.buttonTitles = Array<String>()
            if #available(iOS 8.0, *) {
                
                let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
                self.alert = alertController
                
                if cancelButtonTitle != nil && (cancelButtonTitle?.count)! > 0 {
                    self.cancelButtonIndex = self.buttonTitles.count
                    self.buttonTitles.append(cancelButtonTitle!)
                }
                
                if otherButtonTitle != nil && (otherButtonTitle?.count)! > 0 {
                    self.buttonTitles.append(otherButtonTitle!)
                }
                for (index, element) in self.buttonTitles.enumerated() {
                    
                    alertController.addAction(UIAlertAction(title: element, style: (index == self.cancelButtonIndex) ? .destructive : .default, handler: { (action: UIAlertAction) in
                        self.isShowing = false
                        
                        if completion != nil {
                            
                            completion(self, index)
                        }
                    }))
                }
            } else {
                
                let alt = UIAlertView()
                alt.delegate = self
                alt.title = title!
                alt.message = message
                if cancelButtonTitle != nil && (cancelButtonTitle?.characters.count)! > 0 {
                    self.cancelButtonIndex = 0
                    alt.cancelButtonIndex = 0
                    alt.addButton(withTitle: cancelButtonTitle)
                    self.buttonTitles.append(cancelButtonTitle!)
                }
                
                if otherButtonTitle != nil && (otherButtonTitle?.characters.count)! > 0 {
                    alt.addButton(withTitle: otherButtonTitle!)
                    self.buttonTitles.append(otherButtonTitle!)
                }
                
                if completion != nil {
                    self.completion = completion
                }
                self.alert = alt
            }
        }
        
        autoRemoving = false
    }
    
    static func showAlert(message: String, leftButton: String? = nil, rightButton: String? = "Đóng", viewController: UIViewController, completion:
        ((Int) -> Void)?) {
        let alert = HCAlertController.init(title: nil, message: message, completion: { (_, index) in
            completion?(index)
        }, cancelButtonTitle: leftButton, otherButtonTitle: rightButton)
        alert.show(viewController: viewController)
    }
    
    static func showAlertWithMessage(message: String, viewController: UIViewController) {
        let alert = HCAlertController.init(title: nil, message: message, completion: nil, cancelButtonTitle: nil, otherButtonTitle: "Đóng")
        alert.show(viewController: viewController)
    }
    
    func setAutoRemoving(autoRemove: Bool, autoRemovedCompletion completion: @escaping HCAlertControllerRemovedCompletion) {
        autoRemoving = autoRemove
        if autoRemovedCompletion != nil {
            autoRemovedCompletion = completion
        }
    }
    
    func clear() {
        if isShowing {
            remove(completion: nil)
        }
        
        if hasNotification {
            NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIApplicationWillResignActive, object: nil)
            hasNotification = false
        }
        
        autoRemovedCompletion = nil
        if buttonTitles != nil {
            buttonTitles?.removeAll()
            buttonTitles = nil
        }
        alert = nil
        completion = nil
    }
    
    deinit {
        
        clear()
    }
    
    func show(viewController: UIViewController!) {
        
        DispatchQueue.main.async {
            
            NotificationCenter.default.addObserver(self, selector: #selector(HCAlertController.didEnterBackground(notification:)), name: NSNotification.Name.UIApplicationWillResignActive, object: nil)
            self.hasNotification = true
            self.isShowing = true
            
            let ac = self.alert as! UIAlertController
            
            if viewController != nil {
                viewController.present(ac, animated: true, completion: {
                })
            }
        }
    }
    
    func remove(completion: HCAlertControllerRemovedCompletion!) {
        
        DispatchQueue.main.async {
            
            if self.hasNotification {
                NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIApplicationWillResignActive, object: nil)
                self.hasNotification = false
            }
            
            let ac = self.alert as! UIAlertController
            ac.dismiss(animated: true, completion: completion)
            self.isShowing = false
            
        }
    }
    
    @objc func didEnterBackground(notification: Notification) {
        if autoRemoving && isShowing {
            remove(completion: autoRemovedCompletion)
        }
    }
    
}


