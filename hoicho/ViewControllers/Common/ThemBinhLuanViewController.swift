//
//  ThemBinhLuanViewController.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 4/19/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit
import DKImagePickerController

private let reuseCellIdentifier = "RemoveableMediaCollectionViewCell"

class ThemBinhLuanViewController: UIViewController {

    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var productPriceLabel: UILabel!
    @IBOutlet weak var replyLabel: UILabel!
    @IBOutlet weak var contentTextView: GrowingTextView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var ratingView: CosmosView!
    
    var dismiss:(() -> Void)?
    var completion: ((CommentModel) -> Void)?
    var images = [UIImage]() {
        didSet {
            collectionView?.reloadData()
        }
    }
    var product: ProductModel! {
        didSet {
            productImageView?.sd_setImage(with: URL(string: product.image), placeholderImage: #imageLiteral(resourceName: "icon_no_image"))
            productNameLabel?.text = product.name
            productPriceLabel?.text = product.price.priceString()
        }
    }
    var parent_comment: CommentModel?

    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
    }
    
    func setupUI() {
        title = "Bình luận sản phẩm"
        ratingView.rating = 4.0
        if parent_comment != nil {
            replyLabel.isHidden = false
            replyLabel.text = "Đang trả lời bình luận của \(parent_comment!.account_name)"
        } else {
            replyLabel.isHidden = true
        }
        collectionView.register(UINib(nibName: reuseCellIdentifier, bundle: nil), forCellWithReuseIdentifier: reuseCellIdentifier)
        
        if product != nil {
            productImageView?.sd_setImage(with: URL(string: product.image), placeholderImage: #imageLiteral(resourceName: "icon_no_image"))
            productNameLabel?.text = product.name
            productPriceLabel?.text = product.price.priceString()
        }
    }
    
    @IBAction func dismissView(_ sender: Any) {
        dismiss?()
    }
  
    @IBAction func doAddMedia(_ sender: Any) {
        contentTextView.resignFirstResponder()
        let pickerController = DKImagePickerController()
        pickerController.maxSelectableCount = 5
        pickerController.didSelectAssets = {(assets: [DKAsset]) in
            for asset in assets {
                asset.fetchFullScreenImage(true, completeBlock: { (image: UIImage?, option:[AnyHashable : Any]?) in
                    self.images.append(image!)
                })
                
            }
        }
        
        present(pickerController, animated: true, completion: nil)
    }
    
    @IBAction func doCommentProduct(_ sender: Any) {
        self.view.endEditing(true)
        
        if Utilities.isNilOrEmpty(contentTextView.text) {
            showAlertWithMessage("Vui lòng nhập nội dung bình luận")
            return
        }
        
        showProgress()
        HCService.commentProduct(product_id: product.id, parent_comment_id: parent_comment?.id, content: contentTextView.text, images: images, rate: ratingView.rating, completion: {[weak self](comment) in
            self?.dismissProgress()
            self?.dismiss(animated: true, completion: {
                self?.completion?(comment)
            })
        }) { (error) in
            self.showError(error)
        }
        
    }
    
    @objc func doCloseButton(_ sender: UIButton) {
        self.images.remove(at: sender.tag)
    }

}

extension ThemBinhLuanViewController: GrowingTextViewDelegate {
    func textViewDidChangeHeight(_ textView: GrowingTextView, height: CGFloat) {
        UIView.animate(withDuration: 0.2) {
            self.view.layoutIfNeeded()
        }
    }
}

extension ThemBinhLuanViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseCellIdentifier, for: indexPath) as! RemoveableMediaCollectionViewCell
        
        cell.mediaImageView.image = images[indexPath.row]
        cell.closeButton.tag = indexPath.row
        cell.closeButton.addTarget(self, action: #selector(doCloseButton(_:)), for: .touchUpInside)
        
        
        return cell
    }
}

extension ThemBinhLuanViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: 128, height: 120)
    }
}
