//
//  HCNavigationController.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 4/18/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

class HCNavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        delegate = self
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func doCloseItem() {
        dismiss(animated: true, completion: nil)
    }

}

extension HCNavigationController {
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        if viewController.navigationItem.backBarButtonItem == nil {
            let backBarItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
            viewController.navigationItem.backBarButtonItem = backBarItem
        }
        
        if  viewController.isKind(of: ThemBaiVietViewController.self) ||
            viewController.isKind(of: HCWebViewViewController.self) ||
            viewController.isKind(of: DanhGiaSanPhamViewController.self) ||
            viewController.isKind(of: KhuVucViewController.self) ||
            viewController.isKind(of: ThemDiemBanViewController.self) ||
            viewController.isKind(of: ThemBinhLuanViewController.self) ||
            viewController.isKind(of: ChonNhaCungCapViewController.self) ||
            viewController.isKind(of: ChonThuongHieuViewController.self) ||
            viewController.isKind(of: TongHopDanhMucViewController.self) ||
            viewController.isKind(of: TongHopThemDanhMucViewController.self) ||
            viewController.isKind(of: KhoAnhViewController.self) ||
            viewController.isKind(of: TaoNhomMoiViewController.self) ||
            viewController.isKind(of: InviteViewController.self) ||
            viewController.isKind(of: FriendRequestViewController.self) ||
            viewController.isKind(of: DanhSachCauHoiViewController.self) ||
            viewController.isKind(of: QuyenQuanTriVienViewController.self) ||
            viewController.isKind(of: ThemHoiChoViewController.self) ||
            viewController.isKind(of: CapNhatSoDoHoiChoViewController.self) ||
            viewController.isKind(of: ChonGianHangViewController.self)
        {
            let closeItem = UIBarButtonItem(image: #imageLiteral(resourceName: "icon_close"), style: .plain, target: self, action: #selector(doCloseItem))
            viewController.navigationItem.leftBarButtonItem = closeItem
        }
    }
}
