//
//  HCTabBarViewController.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 4/18/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit
import PusherSwift
import SwiftyJSON

class HCTabBarViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
    }

    func setupUI() {
        setupPusher()
        
        delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(updateUnreadInboxCountListener(_:)), name: Notification.Name.countUnreadInbox, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(updateUnreadNotificationListener(_:)), name: Notification.Name.countUnreadNotification, object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        updateUnreadInboxCount()
        getUnreadNotification()
        if Constant.appDelegate.payload != nil {
            Constant.appDelegate.handlePayload(Constant.appDelegate.payload)
            Constant.appDelegate.payload = nil
        }
    }
    
    func getUnreadNotification() {
        if !UserModel.shared.token.isEmpty && UserModel.shared.id > 0 {
            HCService.countUnreadNotification(completion: { [weak self](count) in
                DispatchQueue.main.async {
                    if count > 0 {
                        if let item = self?.tabBar.items?[1] {
                            item.badgeValue = "\(count)"
                        }
                        
                    } else {
                        if let item = self?.tabBar.items?[1] {
                            item.badgeValue = nil
                        }
                    }
                }
            }) { (_) in
                
            }
        }
    }
    
    @objc func updateUnreadNotificationListener(_ notification: NSNotification) {
        self.updateUnreadNotification()
    }

    
    func updateUnreadNotification() {
        if !UserModel.shared.token.isEmpty {
            HCService.countUnreadPushNotification(completion: { (count) in
                DispatchQueue.main.async {
                    UIApplication.shared.applicationIconBadgeNumber = count
                }
            }) { (error) in
                UIApplication.shared.applicationIconBadgeNumber = 0
            }
        } else {
            UIApplication.shared.applicationIconBadgeNumber = 0
        }
    }
    
    
    @objc func updateUnreadInboxCountListener(_ notification: NSNotification) {
        self.updateUnreadInboxCount()
    }
    
    func updateUnreadInboxCount() {
        if !UserModel.shared.token.isEmpty {
            HCService.countUnreadInbox(completion: { [weak self](count) in
                DispatchQueue.main.async {
                    if count > 0 {
                        if let item = self?.tabBar.items?[3] {
                            item.badgeValue = "\(count)"
                        }
                        
                    } else {
                        if let item = self?.tabBar.items?[3] {
                            item.badgeValue = nil
                        }
                    }
                }
            }) { (error) in
                
            }
        }

    }
    
    func setupPusher() {
        
        let myChannel = Constant.appDelegate.pusher.subscribe("private-new-channel-\(UserModel.shared.id)")
        let _ = myChannel.bind(eventName: Constant.Pusher.event, callback: { (data: Any?) -> Void in
            
            if let data = data as? String {
                self.updateUnreadInboxCount()
                let json = JSON.init(parseJSON: data)
                print(json)
                NotificationCenter.default.post(name: .didReceivedNewChatMessage, object: nil, userInfo: ["value": json])
                if json["from"].intValue != UserModel.shared.id {
                    Constant.appDelegate.playSound()
                }
                
            }
        })
        
    }


}

extension HCTabBarViewController: UITabBarControllerDelegate {
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        if viewController.isKind(of: HCNavigationController.self) {
            if let rootController = (viewController as! HCNavigationController).viewControllers.first {
                if rootController.isKind(of: ChatRootViewController.self) {
                    if UserModel.shared.token.isEmpty {
                        self.requestLoginWithCompletion {
                            self.selectedIndex = 3
                        }
                        return false
                    } else {
                        return true
                    }
                    
                }
            }
        }
        return true
    }
}
