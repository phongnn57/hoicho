//
//  MediaViewController.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 4/19/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit
import iCarousel

class MediaViewController: UIViewController {

    @IBOutlet weak var mediaView: iCarousel!
    var defaultIndex = 0
    
    var images = [String]() {
        didSet {
            mediaView?.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        mediaView.delegate = self
        mediaView.dataSource = self
        mediaView.type = .linear
        mediaView.isPagingEnabled = true
        mediaView.currentItemIndex = defaultIndex
    }

    @IBAction func doClose(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}


extension MediaViewController: iCarouselDataSource {
    func numberOfItems(in carousel: iCarousel) -> Int {
        return images.count
    }
    
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
        if let imageView = view as? UIImageView {
            imageView.sd_setImage(with: URL(string: images[index]))
            return imageView
        } else {
            let imageView = UIImageView(frame: UIScreen.main.bounds)
            imageView.contentMode = .scaleAspectFit
            imageView.clipsToBounds = true
            imageView.sd_setImage(with: URL(string: images[index]))
            
            return imageView
        }
    }
}

extension MediaViewController: iCarouselDelegate {

    func carousel(_ carousel: iCarousel, valueFor option: iCarouselOption, withDefault value: CGFloat) -> CGFloat {
        if option == .wrap {
            return 1
        }
        return value
    }
}
