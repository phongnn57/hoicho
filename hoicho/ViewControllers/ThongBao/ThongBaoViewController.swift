//
//  ThongBaoViewController.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 6/7/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

class ThongBaoViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var refreshControl: UIRefreshControl!
    var loadMoreIndicator: UIActivityIndicatorView!
    
    var datasource = [NotificationModel]() {
        didSet {
            tableView?.reloadData()
        }
    }
    var offset = 0
    var can_load_more = true
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        loadData()
    }
    
    func setupUI() {
        title = "Thông báo"
        
        tableView.tableFooterView = UIView()
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 1000
        
        loadMoreIndicator = initLoadMoreIndicator()
        refreshControl = initRefreshControl()
        if #available(iOS 10, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        
        let readAllItem = UIBarButtonItem(image: #imageLiteral(resourceName: "icon_tick_all"), style: .plain, target: self, action: #selector(doReadAllItem))
        navigationItem.rightBarButtonItem = readAllItem
    }
    
    @objc func doReadAllItem() {
        showAlertTwoButtonWithMessage("Xác nhận đã xem hết các thông báo chưa đọc?") {
            self.showProgress()
            HCService.readNotification(nil, completion: {
                self.dismissProgress()
                for object in self.datasource {
                    object.isRead = true
                }
                self.tableView.reloadData()
            }) { (error) in
                self.showError(error)
            }
        }
    }
    
    func loadData(refresh: Bool = false, next: Bool = false) {
        if !next && !refresh {
            showProgress()
        } else if !refresh && next {
            tableView.tableFooterView = loadMoreIndicator
        }
        
        HCService.getNotifications(offset, completion: { [weak self](objects) in
            if let strongSelf = self {
                strongSelf.refreshControl.endRefreshing()
                strongSelf.dismissProgress()
                strongSelf.tableView.tableFooterView = UIView()
                
                if strongSelf.offset == 0 {
                    strongSelf.datasource = objects
                } else {
                    strongSelf.datasource.append(contentsOf: objects)
                }
                
                if objects.count == Constant.limit {
                    strongSelf.offset += Constant.limit
                    strongSelf.can_load_more = true
                } else {
                    strongSelf.can_load_more = false
                }
            }
        }) { (error) in
            self.refreshControl.endRefreshing()
            self.tableView.tableFooterView = UIView()
            self.showError(error)
        }
        
    }
    
    override func doRefresh() {
        offset = 0
        loadData(refresh: true)
    }


}

extension ThongBaoViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datasource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ThongBaoTableViewCell", for: indexPath) as! ThongBaoTableViewCell
        
        cell.notification = datasource[indexPath.row]
        
        if can_load_more && datasource.count - 1 == indexPath.row {
            loadData(next: true)
        }
        
        return cell
    }
}

extension ThongBaoViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let notification = datasource[indexPath.row]
        
        // đánh dấu thông báo đã đọc khi ấn vào nó
        if !notification.isRead {
            HCService.readNotification(notification.id, completion: {
                self.datasource[indexPath.row].isRead = true
                DispatchQueue.main.async {
                    self.tableView.reloadRows(at: [indexPath], with: .none)
                }
            }, failure: { (error) in
                
            })
        }
        
        guard let payload = notification.payload, payload.type != nil else {
            return
        }
        
        if payload.type == .san_pham {
            guard let chiTietSanPhamViewController = ChiTietSanPhamViewController.viewController(.home) as? ChiTietSanPhamViewController else {return}
            chiTietSanPhamViewController.hidesBottomBarWhenPushed = true
            chiTietSanPhamViewController.product_id = payload.id
            navigationController?.pushViewController(chiTietSanPhamViewController, animated: true)
        } else if payload.type == .cong_dong {
            guard let chiTietBaiVietViewController = ChiTietCongDongViewController.viewController(.congdong) as? ChiTietCongDongViewController else {return}
            chiTietBaiVietViewController.hidesBottomBarWhenPushed = true
            chiTietBaiVietViewController.id = payload.id
            navigationController?.pushViewController(chiTietBaiVietViewController, animated: true)
        } else if payload.type == .friend {
            guard let thongTinCaNhanViewController = ThongTinCaNhanViewController.viewController(.setting) as? ThongTinCaNhanViewController else {return}
            thongTinCaNhanViewController.hidesBottomBarWhenPushed = true
            thongTinCaNhanViewController.user_id = payload.id
            navigationController?.pushViewController(thongTinCaNhanViewController, animated: true)
        }
        
    }
}
