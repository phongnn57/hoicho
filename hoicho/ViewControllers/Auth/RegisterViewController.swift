//
//  RegisterViewController.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 4/18/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController {

    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var typeSegmentControl: UISegmentedControl!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var companyTextField: UITextField!
    @IBOutlet weak var regionTextField: UITextField!
    @IBOutlet weak var addressTextView: GrowingTextView!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    
    var image: UIImage? {
        didSet {
            avatarImageView?.image = image
        }
    }
    var region: RegionModel? {
        didSet {
            regionTextField.text = region?.name
        }
    }
    var completion: ((String) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        
    }

    func setupUI() {
        title = "Đăng ký"
        
        regionTextField.delegate = self
    }

    
    override func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        picker.dismiss(animated: true, completion: nil)
        if let image = info[UIImagePickerControllerEditedImage] as? UIImage {
            self.image = image
        }
    }
    

    @IBAction func doSelectAvatar(_ sender: Any) {
        doSelectPhoto(allowEdit: true)
    }
    
    @IBAction func doRegisterButton(_ sender: Any) {
        self.view.endEditing(true)
        
        if image == nil {
            showAlertWithMessage("Vui lòng chọn ảnh đại diện")
            return
        }
        if Utilities.isNilOrEmpty(phoneTextField.text) {
            showAlertWithMessage("Vui lòng nhập số điện thoại")
            return
        }
        if Utilities.isNilOrEmpty(emailTextField.text) {
            showAlertWithMessage("Vui lòng nhập email")
            return
        }
        if Utilities.isNilOrEmpty(nameTextField.text) {
            showAlertWithMessage("Vui lòng nhập họ tên")
            return
        }
        if Utilities.isNilOrEmpty(addressTextView.text) {
            showAlertWithMessage("Vui lòng nhập địa chỉ")
            return
        }
        if Utilities.isNilOrEmpty(passwordTextField.text) {
            showAlertWithMessage("Vui lòng nhập mật khẩu")
            return
        }
        if Utilities.isNilOrEmpty(confirmPasswordTextField.text) {
            showAlertWithMessage("Vui lòng xác nhận mật khẩu")
            return
        }
        if passwordTextField.text! != confirmPasswordTextField.text! {
            showAlertWithMessage("Mật khẩu không khớp")
            return
        }
        showProgress()
        HCService.register(type: UserType.thanh_vien, image: image!, phone: phoneTextField.text!, name: nameTextField.text!, email: emailTextField.text!, company: companyTextField.text, birthday: nil, region: region, address: addressTextView.text, password: passwordTextField.text!, completion: {[weak self]() in
            self?.dismissProgress()
            self?.showAlertWithMessage("Chúc mừng bạn đã đăng ký thành công tài khoản. Hãy đăng nhập và trải nghiệm ngay các tính năng", completion: {
                self?.completion?(self?.phoneTextField.text ?? "")
                self?.navigationController?.popViewController(animated: true)
            })
        }) { (error) in
            self.showError(error)
        }
        
    }
    
}

extension RegisterViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField.isEqual(regionTextField) {
            guard let khuVucViewController = KhuVucViewController.viewController(.common) as? KhuVucViewController else {return false}
            khuVucViewController.completion = {[weak self](region) in
                if let strongSelf = self {
                    strongSelf.region = region
                }
            }
            self.present(HCNavigationController(rootViewController: khuVucViewController), animated: true, completion: nil)
            return false
        }
        return true
    }
}


