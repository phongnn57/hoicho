//
//  LoginViewController.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 4/18/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    var completion: (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
    }
    
    func setupUI() {
        title = "Đăng nhập"
        
        #if DEBUG
        phoneTextField.text = "0985771135"
        passwordTextField.text = "ledaiduong771133"
        #endif
        if completion != nil {
            let closeItem = UIBarButtonItem.init(image: #imageLiteral(resourceName: "icon_close"), style: .plain, target: self, action: #selector(cancelLogin))
            navigationItem.leftBarButtonItem = closeItem
        }
    }
    
    @objc func cancelLogin() {
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func doLoginButton(_ sender: Any) {
        
        self.view.endEditing(true)
        
        if Utilities.isNilOrEmpty(phoneTextField.text) {
            showAlertWithMessage("Vui lòng nhập số điện thoại")
            return
        }
        if Utilities.isNilOrEmpty(passwordTextField.text) {
            showAlertWithMessage("Vui lòng nhập mật khẩu")
            return
        }
        
        showProgress()
        HCService.login(phone: phoneTextField.text!, password: passwordTextField.text!, completion: {[weak self]() in
            DispatchQueue.main.async {
                self?.dismissProgress()
                if self?.completion != nil {
                    
                    self?.dismiss(animated: true, completion: {
                        self?.completion?()
                    })
                } else {
                    guard let mainViewController = HCTabBarViewController.viewController(.main) as? HCTabBarViewController else {return}
                    Constant.appDelegate.window?.rootViewController = mainViewController
                }
                
            }
            
        }) { (error) in
            self.showError(error)
        }
        
        
    }
    
    @IBAction func doRegisterButton(_ sender: Any) {
        guard let registerViewController = RegisterViewController.viewController(.auth) as? RegisterViewController else {return}
        registerViewController.completion = {[weak self](phone) in
            self?.phoneTextField.text = phone
        }
        navigationController?.pushViewController(registerViewController, animated: true)
    }
    
    @IBAction func doForgotPassword(_ sender: Any) {
        guard let quenMatKhauViewController = QuenMatKhauViewController.viewController(.auth) as? QuenMatKhauViewController else {return}
        quenMatKhauViewController.completion = {[weak self](phone) in
            self?.phoneTextField.text = phone
        }
        navigationController?.pushViewController(quenMatKhauViewController, animated: true)
    }
    
}
