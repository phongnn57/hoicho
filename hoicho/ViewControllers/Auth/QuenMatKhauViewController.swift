//
//  QuenMatKhauViewController.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 4/27/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

class QuenMatKhauViewController: UIViewController {

    @IBOutlet weak var phoneView: UIView!
    @IBOutlet weak var otpView: UIView!
    @IBOutlet weak var passwordView: UIView!
    @IBOutlet weak var actionButton: UIButton!
    
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var otpTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var resendView: UIStackView!
    @IBOutlet weak var resendButton: UIButton!
    @IBOutlet weak var resendLabel: UILabel!
    
    var timer: Timer!
    var isRequestOTP = false {
        didSet {
            if isRequestOTP {
                actionButton?.setTitle("Đặt mật khẩu", for: .normal)
                passwordView.isHidden = false
                otpView.isHidden = false
                resendView.isHidden = false
                resendButton.isHidden = true
                timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(timerUpdated), userInfo: nil, repeats: true)
            } else {
                actionButton?.setTitle("Lấy mã OTP", for: .normal)
                passwordView.isHidden = true
                otpView.isHidden = true
                resendView.isHidden = true
                resendButton.isHidden = true
                if timer != nil {
                    timer.invalidate()
                    timer = nil
                }
            }
        }
    }
    var completion: ((String) -> Void)?
    var waitingTime = 30
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
    }
    
    func setupUI() {
        title = "Quên mật khẩu"

        isRequestOTP = false
        
    }
    
    @objc func timerUpdated() {
        
        if waitingTime > 0 {
            resendLabel.text = "Yêu cầu gửi lại mã OTP sau \(waitingTime)s"
            resendButton.isHidden = true
            waitingTime -= 1
        } else {
            resendLabel.text = "Yêu cầu gửi lại mã OTP"
            resendButton.isHidden = false
            if timer != nil {
                timer.invalidate()
                timer = nil
            }
        }

    }
    
    @IBAction func doResendButton(_ sender: Any) {
        self.view.endEditing(true)
        waitingTime = 30
        if Utilities.isNilOrEmpty(phoneTextField.text) {
            showAlertWithMessage("Vui lòng nhập số điện thoại")
            return
        }
        showProgress()
        HCService.getOTP(phone: phoneTextField.text!, completion: {
            self.dismissProgress()
            self.isRequestOTP = true
        }) { (error) in
            self.showError(error)
        }
    }
    
    @IBAction func doActionButton(_ sender: Any) {
        self.view.endEditing(true)
        if !isRequestOTP {
            if Utilities.isNilOrEmpty(phoneTextField.text) {
                showAlertWithMessage("Vui lòng nhập số điện thoại")
                return
            }
            showProgress()
            HCService.getOTP(phone: phoneTextField.text!, completion: {
                self.dismissProgress()
                self.isRequestOTP = true
            }) { (error) in
                self.showError(error)
            }
        } else {
            if Utilities.isNilOrEmpty(otpTextField.text) {
                showAlertWithMessage("Vui lòng nhập mã số OTP")
                return
            }
            if Utilities.isNilOrEmpty(passwordTextField.text) {
                showAlertWithMessage("Vui lòng nhập mật khẩu")
                return
            }
            showProgress()
            HCService.createNewPassword(phone: phoneTextField.text!, password: passwordTextField.text!, otp: otpTextField.text!, completion: {
                self.showAlertWithMessage("Mật khẩu mới đã được khởi tạo. Hãy đăng nhập ngay", completion: {
                    self.completion?(self.phoneTextField.text!)
                    self.navigationController?.popViewController(animated: true)
                })
            }) { (error) in
                self.showError(error)
            }
        }
    }

}
