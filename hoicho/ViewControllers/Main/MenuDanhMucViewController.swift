//
//  MenuDanhMucViewController.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 4/18/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

private let reuseCellIdentifier = "MenuTableViewCell"
private let reuseHeaderIdentifier = "MenuHeaderView"
private let reuseFooterIdentifier = "MenuFooterView"

class MenuDanhMucViewController: UIViewController {
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var tableView: UITableView!
    var refreshControl: UIRefreshControl!
    
    var isShown = false
    
    var datasource = [CategoryModel]() {
        didSet {
            tableView?.reloadData()
        }
    }
    
    var didSelectCategory: ((CategoryModel) -> Void)?
    var didSelectThongTinHoiCho: ((Int) -> Void)?

    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        loadData()
    }

    func setupUI() {
        tableView.register(UINib(nibName: reuseHeaderIdentifier, bundle: nil), forHeaderFooterViewReuseIdentifier: reuseHeaderIdentifier)
        tableView.register(UINib(nibName: reuseFooterIdentifier, bundle: nil), forHeaderFooterViewReuseIdentifier: reuseFooterIdentifier)
        refreshControl = initRefreshControl()
        if #available(iOS 10, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        tableView.contentInset = .zero
    }
    
    func loadData(refresh: Bool = false) {
        if !refresh {
            showProgress()
        }
        HCService.getCategories(completion: { (objects) in
            self.dismissProgress()
            self.refreshControl.endRefreshing()
            self.datasource = objects
        }) { (error) in
            self.refreshControl.endRefreshing()
            self.showError(error)
        }
    }
    
    override func doRefresh() {
        loadData(refresh: true)
    }
    
    @objc func doActionButton(_ sender: UIButton) {
        for (pos, item) in datasource.enumerated() {
            if pos == sender.tag {
                item.expand = !item.expand
            } else {
                item.expand = false
            }
        }
        
        self.tableView.reloadData()
    }
    
    @objc func tapHeader(_ sender: UITapGestureRecognizer) {
        if let titleLabel = sender.view as? UILabel {
            didSelectCategory?(datasource[titleLabel.tag])
        }
    }
    
    @IBAction func doThongTinHoiChoButton(_ sender: Any) {
        didSelectThongTinHoiCho?((sender as! UIButton).tag)
    }
    
}

extension MenuDanhMucViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return datasource.count + 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == datasource.count {
            return 0
        }
        if datasource[section].expand {
            return datasource[section].subCate.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let item = datasource[indexPath.section].subCate[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseCellIdentifier, for: indexPath) as! MenuTableViewCell
        
        cell.titleLabel.text = item.name
        
        return cell
    }
}

extension MenuDanhMucViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        didSelectCategory?(datasource[indexPath.section].subCate[indexPath.row])
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == datasource.count {
            return UITableViewAutomaticDimension
        }
        return CGFloat.leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForFooterInSection section: Int) -> CGFloat {
        return 500
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == datasource.count {
            return CGFloat.leastNormalMagnitude
        }
        return 44
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == datasource.count {
            return nil
        }
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: reuseHeaderIdentifier) as! MenuHeaderView
        
        let item = datasource[section]
        
        headerView.titleLabel.text = item.name
        if item.subCate.count > 0 {
            if item.expand {
                headerView.arrowImageView.image = #imageLiteral(resourceName: "icon_arrow_up_black")
            } else {
                headerView.arrowImageView.image = #imageLiteral(resourceName: "icon_arrow_down_black")
            }
            headerView.tintColor = UIColor(hex: "#333333")
        } else {
            headerView.arrowImageView.image = nil
        }
        headerView.logoImageView.sd_setImage(with: URL(string: item.image))
        headerView.actionButton.tag = section
        headerView.actionButton.addTarget(self, action: #selector(doActionButton(_:)), for: .touchUpInside)
        headerView.titleLabel.tag = section
        headerView.titleLabel.isUserInteractionEnabled = true
        headerView.titleLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tapHeader(_:))))
        
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if section == datasource.count {
            let footerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: reuseFooterIdentifier) as! MenuFooterView
            
            footerView.newsButton.tag = 0
            footerView.questionButton.tag = 1
            footerView.infoButton.tag = 2
            footerView.mapButton.tag = 3
            
            footerView.newsButton.addTarget(self, action: #selector(doThongTinHoiChoButton(_:)), for: .touchUpInside)
            footerView.questionButton.addTarget(self, action: #selector(doThongTinHoiChoButton(_:)), for: .touchUpInside)
            footerView.infoButton.addTarget(self, action: #selector(doThongTinHoiChoButton(_:)), for: .touchUpInside)
            footerView.mapButton.addTarget(self, action: #selector(doThongTinHoiChoButton(_:)), for: .touchUpInside)
            
            return footerView
        }
        return nil
    }
}
