//
//  ChiTietDiemBanViewController.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 5/30/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

private let reuseCellIdentifier = "ProductCollectionViewCell"
private let reuseHeaderIdentifier = "ThongTinDiemBanCollectionReusableView"

class ChiTietDiemBanViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var chatStackView: UIStackView!
    @IBOutlet weak var bottomView: UIStackView!
    var refreshControl: UIRefreshControl!
    var offset = 0
    var can_load_more = true
    var products = [ProductModel]() {
        didSet {
            collectionView?.reloadData()
        }
    }
    var salePoint: DiemBanModel! {
        didSet {
            if salePoint != nil {
                bottomView?.isHidden = false
                if salePoint != nil && salePoint.chat_id > 0 {
                    chatStackView?.isHidden = false
                } else {
                    chatStackView?.isHidden = true
                }
            } else {
                bottomView?.isHidden = true
            }
        }
    }
    var product: ProductModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        loadData()
    }
    
    func setupUI() {
        title = "Chi tiết điểm bán"
        
        let menuItem = UIBarButtonItem(image: #imageLiteral(resourceName: "icon_more_vertical"), style: .plain, target: self, action: #selector(doMenuItem))
        navigationItem.rightBarButtonItem = menuItem
        
        collectionView.register(UINib(nibName: reuseCellIdentifier, bundle: nil), forCellWithReuseIdentifier: reuseCellIdentifier)
        collectionView.register(UINib(nibName: reuseHeaderIdentifier, bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: reuseHeaderIdentifier)
        
        refreshControl = initRefreshControl()
        if #available(iOS 10, *) {
            collectionView.refreshControl = refreshControl
        } else {
            collectionView.addSubview(refreshControl)
        }
    }
    
    @objc func doMenuItem() {
        
    }

    func loadData(refresh: Bool = false, next: Bool = false) {
        showProgress()
        HCService.getDetailSalePoint(phone: salePoint.phone, product_id: product?.id, offset: offset, completion: {[weak self](objects, object) in
            if let strongSelf = self {
                if strongSelf.offset == 0 {
                    strongSelf.products = objects
                } else {
                    strongSelf.products.append(contentsOf: objects)
                }
                strongSelf.salePoint = object
                if objects.count == Constant.limit {
                    strongSelf.can_load_more = true
                    strongSelf.offset += Constant.limit
                } else {
                    strongSelf.can_load_more = false
                }
                strongSelf.dismissProgress()
            }
            
        }) { (error) in
            self.salePoint = nil
            self.showError(error)
        }
    }
    
    override func doRefresh() {
        offset = 0
        loadData(refresh: true)
    }
    
    @IBAction func doCallButton(_ sender: Any) {
        if salePoint != nil {
            doCall(salePoint.phone)
        }
    }
    
    @IBAction func doMessageButton(_ sender: Any) {
        self.requestLoginWithCompletion {
            if let account_id = self.salePoint?.chat_id, account_id > 0 {
                
                if account_id != UserModel.shared.id {
                    let user = UserModel()
                    user.id = account_id
                    guard let chatViewController = HCMessengerViewController.viewController(.chat) as? HCMessengerViewController else {return}
                    chatViewController.hidesBottomBarWhenPushed = true
                    chatViewController.users = [user]
                    if self.product != nil {
                        chatViewController.product = self.product
                    }
                    
                    self.navigationController?.pushViewController(chatViewController, animated: true)
                } else {
                    self.showAlertWithMessage("Bạn không thể chat với chính mình")
                }
                
            } else {
                self.showAlertWithMessage("Không tìm thấy id thành viên")
            }
        }
        
    }
    
}

extension ChiTietDiemBanViewController: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if salePoint == nil {return 0}
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        } else if section == 1 {
            if product != nil {
                return 1
            }
            return 0
        }
        return products.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.section == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ChiTietDiemBanCollectionViewCell", for: indexPath) as! ChiTietDiemBanCollectionViewCell
            
            cell.nameLabel.text = salePoint.name
            cell.phoneLabel.text = salePoint.phone
            cell.addressLabel.text = salePoint.address
            
            return cell
        } else if indexPath.section == 1 {
            let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: "DiemBanThongTinSanPhamCollectionViewCell", for: indexPath) as! DiemBanThongTinSanPhamCollectionViewCell
            
            cell.avatarImageView.sd_setImage(with: URL(string: product.image))
            cell.nameLabel.text = product.name
            cell.codeLabel.text = product.code
            cell.priceLabel.text = salePoint.price.priceString()
            
            return cell
        }
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseCellIdentifier, for: indexPath) as! ProductCollectionViewCell
        
        cell.product = products[indexPath.row]
        
        if can_load_more && products.count - 1 == indexPath.row {
            loadData(next: true)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
        case UICollectionElementKindSectionHeader:
            let reusableview = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: reuseHeaderIdentifier, for: indexPath) as! ThongTinDiemBanCollectionReusableView
            if indexPath.section == 2 {
                reusableview.isHidden = false
            } else {
                reusableview.isHidden = true
            }
            return reusableview
            
            
        default:  fatalError("Unexpected element kind")
        }
    }
}

extension ChiTietDiemBanViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.section == 1 {
            guard let chiTietSanPhamViewController = ChiTietSanPhamViewController.viewController(.home) as? ChiTietSanPhamViewController else {return}
            chiTietSanPhamViewController.hidesBottomBarWhenPushed = true
            chiTietSanPhamViewController.product_id = product.id
            navigationController?.pushViewController(chiTietSanPhamViewController, animated: true)
        } else if indexPath.section == 2 {
            self.product = products[indexPath.row]
            self.doRefresh()
        }
        
        
    }
}

extension ChiTietDiemBanViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.section == 0 {
            return CGSize(width: UIScreen.main.bounds.width, height: 160)
        } else if indexPath.section == 1 {
            return CGSize(width: UIScreen.main.bounds.width, height: 180)
        } else {
            let itemWidth = UIScreen.main.bounds.width/2
            let imageWidth = itemWidth - 20
            let itemHeight = 6 + 4 + imageWidth + 4 + 33.5 + 4 + 17 + 4 + 6 + 8
            return CGSize.init(width: itemWidth, height: itemHeight)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        if section == 0 || section == 1 {
            return CGSize.zero
        }
        return CGSize(width: UIScreen.main.bounds.width, height: 40)
    }
}
