//
//  DanhSachDiemBanViewController.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 4/28/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

private let reuseCellIdentifier = "DiemBanTableViewCell"
private let reuseHeaderIdentifier = "DanhSachDiemBanHeaderView"

class DanhSachDiemBanViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var loadMoreIndicator: UIActivityIndicatorView!
    var refreshControl: UIRefreshControl!
    
    var datasource = [DiemBanModel]() {
        didSet {
            tableView?.reloadData()
        }
    }
    var product: ProductModel!
    var offset = 0
    var can_load_more = true
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        loadData()
    }

    func setupUI() {
        title = "Điểm bán"
        tableView.register(UINib(nibName: reuseCellIdentifier, bundle: nil), forCellReuseIdentifier: reuseCellIdentifier)
        tableView.register(UINib(nibName: reuseHeaderIdentifier, bundle: nil), forHeaderFooterViewReuseIdentifier: reuseHeaderIdentifier)
        tableView.tableFooterView = UIView()
        tableView.estimatedRowHeight = 1000
        
        refreshControl = initRefreshControl()
        loadMoreIndicator = initLoadMoreIndicator()
        
        if #available(iOS 10, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
    }
    
    func loadData(refresh: Bool = false, next: Bool = false) {
        if !refresh && !next {
            showProgress()
        } else if next && !refresh {
            tableView.tableFooterView = loadMoreIndicator
        }
        
        HCService.getSalePoint(offset: offset, product_id: product.id, completion: { [weak self](objects) in
            if let strongSelf = self {
                strongSelf.refreshControl.endRefreshing()
                strongSelf.dismissProgress()
                strongSelf.tableView.tableFooterView = UIView()
                if strongSelf.offset == 0 {
                    strongSelf.datasource = objects
                } else {
                    strongSelf.datasource.append(contentsOf: objects)
                }
                if objects.count == Constant.limit {
                    strongSelf.can_load_more = true
                    strongSelf.offset += Constant.limit
                } else {
                    strongSelf.can_load_more = false
                }
            }
        }) { (error) in
            self.refreshControl.endRefreshing()
            self.tableView.tableFooterView = UIView()
            self.showError(error)
        }
    }
    
    override func doRefresh() {
        can_load_more = true
        offset = 0
        loadData(refresh: true)
    }

}

extension DanhSachDiemBanViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        if product == nil {
            return 0
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datasource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseCellIdentifier, for: indexPath) as! DiemBanTableViewCell
        
        let item = datasource[indexPath.row]
        cell.nameLabel.text = item.name
        cell.addressLabel.text = "\(item.address), \(item.district), \(item.city)"
        cell.phoneButton.setTitle(item.phone, for: .normal)
        cell.priceLabel.text = item.price.priceString()
        
        cell.topConstraint.constant = 8
        
        if can_load_more && datasource.count - 1 == indexPath.row {
            loadData(next: true)
        }
        return cell
    }
}

extension DanhSachDiemBanViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let chiTietDiemBanViewController = ChiTietDiemBanViewController.viewController(.common) as? ChiTietDiemBanViewController else {return}
        chiTietDiemBanViewController.salePoint = datasource[indexPath.row]
        navigationController?.pushViewController(chiTietDiemBanViewController, animated: true)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: reuseHeaderIdentifier) as! DanhSachDiemBanHeaderView
        
        if product != nil {
            headerView.productImageView.sd_setImage(with: URL(string: product.image))
            headerView.productNameLabel.text = product.name
            headerView.productPriceLabel.text = product.price.priceString()
        }
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return 200
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
}
