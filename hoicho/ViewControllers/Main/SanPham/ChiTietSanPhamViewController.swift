//
//  ChiTietSanPhamViewController.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 4/19/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit
import FBSDKShareKit

private let reuseHeaderIdentifier = "DanhGiaSanPhamHeaderView"
private let reuseCellDanhGiaIdentifier = "DanhGiaTableViewCell"
private let reuseFooterIdentifier = "DanhGiaSanPhamFooterView"

class ChiTietSanPhamViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var favoriteImageView: UIImageView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var favoriteLabel: UILabel!
    
    var refreshControl: UIRefreshControl!

    
    var product_id: Int!
    var product: ProductModel! {
        didSet {
            tableView?.reloadData()
            if product != nil {
                bottomView.isHidden = false
                
                if UserModel.shared.token.isEmpty || (!UserModel.shared.token.isEmpty && !product.liked) {
                    favoriteImageView.tintColor = UIColor(hex: "#333333")
                    favoriteLabel.textColor = UIColor(hex: "#333333")
                    favoriteLabel.text = "Quan tâm"
                } else {
                    favoriteImageView.tintColor = UIColor.HCColor.appColor
                    favoriteLabel.textColor = UIColor.HCColor.appColor
                    favoriteLabel.text = "Đang quan tâm"
                }
                //neu chua login thi luu san pham nay lai de hien thi o danh sach san pham da xem
                if UserModel.shared.token.isEmpty {
                    DataManager.shared.addProduct(product: self.product)
                }
            }
        }
    }
    var salePoints = [DiemBanModel]() {
        didSet {
            tableView?.reloadData()
        }
    }
    var relateProducts = [ProductModel]() {
        didSet {
            tableView?.reloadData()
        }
    }
    var quanTamProducts = [ProductModel]() {
        didSet {
            tableView?.reloadData()
        }
    }
    var daxemProducts = [ProductModel]() {
        didSet {
            tableView?.reloadData()
        }
    }
    var comments = [CommentModel]() {
        didSet {
            tableView?.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        loadData()
        
    }
    
    func setupUI() {
        title = "Chi tiết sản phẩm"
        tableView.estimatedRowHeight = 2000
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.register(UINib(nibName: reuseHeaderIdentifier, bundle: nil), forHeaderFooterViewReuseIdentifier: reuseHeaderIdentifier)
        tableView.register(UINib(nibName: reuseCellDanhGiaIdentifier, bundle: nil), forCellReuseIdentifier: reuseCellDanhGiaIdentifier)
        tableView.register(UINib(nibName: reuseFooterIdentifier, bundle: nil), forHeaderFooterViewReuseIdentifier: reuseFooterIdentifier)
        
        refreshControl = initRefreshControl()
        if #available(iOS 10, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        bottomView.isHidden = true
        
    }
    
    func loadData(refresh: Bool = false) {
        if product_id == nil {return}
        
        if !refresh {
            showProgress()
        }
        
        let dispatchGroup = DispatchGroup()
        var count = 0
        
        dispatchGroup.enter()
        HCService.getProductDetail(id: product_id, completion: { [weak self](object) in
            if let strongSelf = self {
                strongSelf.product = object
                
                
            }
            
            HCService.getSanPhamCungGianHang(product_id: self?.product_id, completion: { [weak self](objects) in
                count += 1
                dispatchGroup.leave()
                if let strongSelf = self {
                    strongSelf.relateProducts = objects
                }
            }, failure: { (error) in
                count += 1
                dispatchGroup.leave()
            })
            
        }) { (error) in
            count += 1
            dispatchGroup.leave()
            
        }
        
        dispatchGroup.enter()
        HCService.getSalePoint(product_id: product_id, completion: { [weak self](objects) in
            count += 1
            dispatchGroup.leave()
            if let strongSelf = self {
                strongSelf.salePoints = objects
            }
        }) { (error) in
            count += 1
            dispatchGroup.leave()
        }
        
        dispatchGroup.enter()
        HCService.getCommentProduct(product_id: product_id, last_id: nil, limit: 3, completion: { [weak self](objects) in
            count += 1
            dispatchGroup.leave()
            if let strongSelf = self {
                strongSelf.comments = objects
            }
        }) { (error) in
            count += 1
            dispatchGroup.leave()
        }
        
        if !UserModel.shared.token.isEmpty {
            dispatchGroup.enter()
            HCService.getViewedProducts(product_id: product_id, completion: { [weak self](objects) in
                count += 1
                dispatchGroup.leave()
                if let strongSelf = self {
                    strongSelf.daxemProducts = objects
                }
            }) { (error) in
                count += 1
                dispatchGroup.leave()
            }
            
            dispatchGroup.enter()
            HCService.getFavoriteProducts(completion: { [weak self](objects) in
                count += 1
                dispatchGroup.leave()
                if let strongSelf = self {
                    strongSelf.quanTamProducts = objects
                }
            }) { (error) in
                count += 1
                dispatchGroup.leave()
            }
        } else {
            daxemProducts = DataManager.shared.getProducts()
        }
        
        
        dispatchGroup.notify(queue: .main) {
            if (count == 3 && UserModel.shared.token.isEmpty) || (count == 5 && !UserModel.shared.token.isEmpty) {
                self.refreshControl.endRefreshing()
                self.dismissProgress()
            }
        }
    }
    
    override func doRefresh() {
        loadData(refresh: true)
    }
    
    func openGianHang() {
        guard let gianHangViewController = GianHangViewController.viewController(.home) as? GianHangViewController else {return}
        gianHangViewController.hidesBottomBarWhenPushed = true
        gianHangViewController.booth_id = product.booth?.id
        navigationController?.pushViewController(gianHangViewController, animated: true)
    }
    
    func openDanhSachSanPham() {
        guard let danhSachSanPhamViewController = DanhSachSanPhamViewController.viewController(.main) as? DanhSachSanPhamViewController else {return}
        danhSachSanPhamViewController.hidesBottomBarWhenPushed = true
        if let brand = product.brand {
            danhSachSanPhamViewController.brand = brand
        }
        navigationController?.pushViewController(danhSachSanPhamViewController, animated: true)
    }
    
    func doCallGianHang() {
        doCall(product.booth?.hotline ?? "")
    }
    
    func doMessageGianHang() {
        
        self.requestLoginWithCompletion {
            if let account_id = self.product.booth?.id, account_id > 0{
                if account_id != UserModel.shared.id {
                    let user = UserModel()
                    user.id = account_id
                    guard let chatViewController = HCMessengerViewController.viewController(.chat) as? HCMessengerViewController else {return}
                    chatViewController.hidesBottomBarWhenPushed = true
                    chatViewController.users = [user]
                    chatViewController.product = self.product
                    self.navigationController?.pushViewController(chatViewController, animated: true)
                } else {
                    self.showAlertWithMessage("Bạn không thể chat với chính mình")
                }
            } else {
                self.showAlertWithMessage("Không tìm thấy id thành viên")
            }
        }

    }
    
    @IBAction func doFavoriteButton(_ sender: Any) {
        followProduct()
    }
    
    @IBAction func doCallButton(_ sender: Any) {
        doCallGianHang()
    }
    
    @IBAction func doMessageButton(_ sender: Any) {
        self.doMessageGianHang()
    }
    
    @objc func followProduct() {
        self.requestLoginWithCompletion {[weak self]() in
            self?.showProgress()
            HCService.followProduct(id: self!.product_id, completion: {
                if let strongSelf = self {
                    strongSelf.dismissProgress()
                    strongSelf.product.liked = !strongSelf.product.liked
                    
                    if strongSelf.product.liked {
                        strongSelf.favoriteImageView.tintColor = UIColor.HCColor.appColor
                        strongSelf.favoriteLabel.textColor = UIColor.HCColor.appColor
                        strongSelf.favoriteLabel.text = "Đang quan tâm"
                    } else {
                        strongSelf.favoriteImageView.tintColor = UIColor(hex: "#333333")
                        strongSelf.favoriteLabel.textColor = UIColor(hex: "#333333")
                        strongSelf.favoriteLabel.text = "Quan tâm"
                    }
                    strongSelf.tableView.reloadData()
                }
                
            }) { (error) in
                self?.showError(error)
            }
        }
        
    }
    
    @objc func shareProduct() {
        if product.link_affiliate.length > 0 {
            
            let height: CGFloat = 180
            

            var items = [CustomizableActionSheetItem]()
            
            let shareView = ShareProductView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width - 30, height: height))

            let shareViewItem = CustomizableActionSheetItem()
            shareViewItem.type = .view
            shareViewItem.view = shareView
            shareViewItem.height = height
            items.append(shareViewItem)
            
            // Setup button
            let closeItem = CustomizableActionSheetItem()
            closeItem.type = .button
            closeItem.label = "Đóng"
            closeItem.selectAction = { (actionSheet: CustomizableActionSheet) -> Void in
                actionSheet.dismiss()
            }
            items.append(closeItem)
            
            // Show
            let actionSheet = CustomizableActionSheet()
            actionSheet.showInView(self.view, items: items)
            
            
            shareView.doShareFacebookProduct = {
                
                actionSheet.dismiss()
                
                let shareContent = FBSDKShareLinkContent()
                shareContent.contentURL = URL(string: self.product.link_affiliate)
                
                
                FBSDKShareDialog.show(from: self, with: shareContent, delegate: nil)
                FBSDKShareDialog.show(from: self, with: shareContent, delegate: self)
            }
            
            shareView.doShareOthersProduct =  {
                
                actionSheet.dismiss()
                
                let url = URL(string: self.product.link_affiliate)
  
                let shareItems = [url!]
                let activityController = UIActivityViewController(activityItems: shareItems, applicationActivities: nil)
                activityController.excludedActivityTypes = [.print, .postToWeibo, .copyToPasteboard, .addToReadingList, .postToVimeo, .postToTwitter, .assignToContact, .saveToCameraRoll, .postToFlickr, .postToTencentWeibo, .airDrop, .openInIBooks]
                activityController.completionWithItemsHandler = {(activityType, completed, returnedItems, error) in
                    if completed {
                        HCService.shareProductCount(id: self.product_id, completion: { [weak self](count) in
                            if let strongSelf = self {
                                strongSelf.product.shares = count
                                strongSelf.tableView.reloadData()
                            }
                        }) { (error) in
                            
                        }
                    }
                }
                
                self.present(activityController, animated: true, completion: nil)
            }
            
        } else {
            showAlertWithMessage("Link sản phẩm bị trống, không thể chia sẻ")
        }
    }
    
    @objc func themDiemBan() {
        self.requestLoginWithCompletion {
            guard let themDiemBanViewController = ThemDiemBanViewController.viewController(.home) as? ThemDiemBanViewController else {return}
            themDiemBanViewController.product = self.product
            themDiemBanViewController.completion = {
                HCService.getSalePoint(product_id: self.product_id, completion: { [weak self](objects) in
                    if let strongSelf = self {
                        strongSelf.salePoints = objects
                    }
                }) { (error) in
                }
            }
            self.present(HCNavigationController(rootViewController: themDiemBanViewController), animated: true, completion: nil)
        }
        
    }
    
    @objc func xemThemDiemBan() {
        guard let danhSachDiemBanViewController = DanhSachDiemBanViewController.viewController(.home) as? DanhSachDiemBanViewController else {return}
        danhSachDiemBanViewController.hidesBottomBarWhenPushed = true
        danhSachDiemBanViewController.product = product
        navigationController?.pushViewController(danhSachDiemBanViewController, animated: true)
    }
    
    @objc func doCommentButton() {
        
        self.requestLoginWithCompletion {[weak self]() in
            
            guard let themBinhLuanViewController = ThemBinhLuanViewController.viewController(.common) as? ThemBinhLuanViewController else {return}
            themBinhLuanViewController.product = self?.product
            themBinhLuanViewController.completion = {[weak self](comment) in
                self?.comments.insert(comment, at: 0)
                self?.product.comments += 1
                self?.tableView.reloadData()
            }
            self?.present(HCNavigationController(rootViewController: themBinhLuanViewController), animated: true, completion: nil)
        }
        
    }
    
    @objc func doReplyButton(_ sender: UIButton) {
        self.requestLoginWithCompletion {[weak self]() in
            
            guard let themBinhLuanViewController = ThemBinhLuanViewController.viewController(.common) as? ThemBinhLuanViewController else {return}
            themBinhLuanViewController.product = self?.product
            themBinhLuanViewController.parent_comment = self?.comments[sender.tag]
            self?.present(HCNavigationController(rootViewController: themBinhLuanViewController), animated: true, completion: nil)
        }
    }
    
    @objc func doViewMoreComment() {
        guard let danhGiaSanPhamViewController = DanhGiaSanPhamViewController.viewController(.home) as? DanhGiaSanPhamViewController else {return}
        danhGiaSanPhamViewController.product = product
        self.present(HCNavigationController(rootViewController: danhGiaSanPhamViewController), animated: true, completion: nil)
    }
    
    @objc func doViewMoreOtherProducts(_ sender: UIButton) {
        guard let danhSachSanPhamKhacViewController = DanhSachSanPhamKhacViewController.viewController(.main) as? DanhSachSanPhamKhacViewController else {return}
        if sender.tag == 3 {
            danhSachSanPhamKhacViewController.type = .cunggianhang
        } else if sender.tag == 4 {
            danhSachSanPhamKhacViewController.type = .yeuthich
        } else if sender.tag == 5 {
            danhSachSanPhamKhacViewController.type = .daxem
        }
        danhSachSanPhamKhacViewController.product_id = product_id
        navigationController?.pushViewController(danhSachSanPhamKhacViewController, animated: true)
    }
    
    func openSalePoint(_ index: Int) {
        guard let chiTietDiemBanViewController = ChiTietDiemBanViewController.viewController(.common) as? ChiTietDiemBanViewController else {return}
        chiTietDiemBanViewController.salePoint = salePoints[index]
        chiTietDiemBanViewController.product = product
        navigationController?.pushViewController(chiTietDiemBanViewController, animated: true)
    }

}

extension ChiTietSanPhamViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        if product == nil {
            return 0
        }
        return 6
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            if product.booth != nil && product.booth!.id != 0 {
                return 2
            } else {
                return 1
            }
        }
        if section == 2 {
            if comments.count > 3 {return 3}
            return comments.count
        } else if section == 1 {
            return 1
        }
        
        if section == 3 {
            if relateProducts.count == 0 {
                return 0
            }
            return 1
        }
        if section == 4 {
            if quanTamProducts.count == 0 {
                return 0
            }
            return 1
        }
        if section == 5 {
            if daxemProducts.count == 0 {
                return 0
            }
            return 1
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ChiTietSanPhamTopTableViewCell", for: indexPath) as! ChiTietSanPhamTopTableViewCell
                
                cell.product = product
                
                cell.didSelectButton = {[weak self](index) in
                    if let strongSelf = self {
                        if index == 0 {
                            strongSelf.openWebViewWith(html: strongSelf.product.description_text, webTitle: "Chi tiết sản phẩm")
                        } else if index == 1 {
                            strongSelf.openGianHang()
                        } else if index == 2 {
                            strongSelf.doCallGianHang()
                        } else if index == 3 {
                            strongSelf.doMessageGianHang()
                        } else {
                            strongSelf.openDanhSachSanPham()
                        }
                    }
                    
                }
                
                cell.didSelectImageAt = {[weak self](index) in
                    if let strongSelf = self {
                        strongSelf.openMedia(images: strongSelf.product.images, index: index)
                    }
                    
                }
                
                cell.shareButton.addTarget(self, action: #selector(shareProduct), for: .touchUpInside)
                
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ThongTinGianHangTableViewCell", for: indexPath) as! ThongTinGianHangTableViewCell
                
                cell.booth = product.booth
                
                cell.didSelectButton = {[weak self](index) in
                    if let strongSelf = self {
                        if index == 0 {
                            strongSelf.openWebViewWith(html: strongSelf.product.description_text, webTitle: "Chi tiết sản phẩm")
                        } else if index == 1 {
                            strongSelf.openGianHang()
                        } else if index == 2 {
                            strongSelf.doCallGianHang()
                        } else if index == 3 {
                            strongSelf.doMessageGianHang()
                        } else {
                            strongSelf.openDanhSachSanPham()
                        }
                    }
                    
                }
                
                return cell
            }
            
        } else if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DanhSachDiemBanTableViewCell", for: indexPath) as! DanhSachDiemBanTableViewCell
            
            cell.themDiemBanButton.addTarget(self, action: #selector(themDiemBan), for: .touchUpInside)
            cell.xemThemDiemBanButton.addTarget(self, action: #selector(xemThemDiemBan), for: .touchUpInside)
            
            cell.datasource = salePoints
            cell.didSelectItemAt = {[weak self](index) in
                self?.openSalePoint(index)
            }
            
            return cell
        } else if indexPath.section == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: reuseCellDanhGiaIdentifier, for: indexPath) as! DanhGiaTableViewCell
            
            cell.comment = comments[indexPath.row]
            cell.replyButton.tag = indexPath.row
            cell.replyButton.addTarget(self, action: #selector(doReplyButton(_:)), for: .touchUpInside)
            
            cell.didSelectAvatar = {
                guard let thongTinCaNhanViewController = ThongTinCaNhanViewController.viewController(.setting) as? ThongTinCaNhanViewController else {return}
                thongTinCaNhanViewController.hidesBottomBarWhenPushed = true
                thongTinCaNhanViewController.user_id = self.comments[indexPath.row].account_id
                self.navigationController?.pushViewController(thongTinCaNhanViewController, animated: true)
            }
            
            return cell
        } else if indexPath.section == 3 || indexPath.section == 4 || indexPath.section == 5 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SanPhamLienQuanTableViewCell", for: indexPath) as! SanPhamLienQuanTableViewCell
            
            if indexPath.section == 3 {
                cell.titleLabel.text = "Sản phẩm cùng gian hàng"
                cell.datasource = relateProducts
                if relateProducts.count < Constant.limit {
                    cell.viewMoreButton.isHidden = true
                } else {
                    cell.viewMoreButton.isHidden = false
                }
            } else if indexPath.section == 4 {
                cell.titleLabel.text = "Sản phẩm quan tâm"
                cell.datasource = quanTamProducts
                if quanTamProducts.count < Constant.limit {
                    cell.viewMoreButton.isHidden = true
                } else {
                    cell.viewMoreButton.isHidden = false
                }
            } else {
                cell.titleLabel.text = "Sản phẩm đã xem"
                cell.datasource = daxemProducts
                if daxemProducts.count < Constant.limit {
                    cell.viewMoreButton.isHidden = true
                } else {
                    cell.viewMoreButton.isHidden = false
                }
            }
            
            cell.viewMoreButton.tag = indexPath.section
            cell.viewMoreButton.addTarget(self, action: #selector(doViewMoreOtherProducts(_:)), for: .touchUpInside)
            
            cell.didSelectAtIndex = {[weak self](index) in
                guard let chiTietSanPhamViewController = ChiTietSanPhamViewController.viewController(.home) as? ChiTietSanPhamViewController else {return}
                chiTietSanPhamViewController.hidesBottomBarWhenPushed = true
                if indexPath.section == 3 {
                    chiTietSanPhamViewController.product_id = self?.relateProducts[index].id
                } else if indexPath.section == 4 {
                    chiTietSanPhamViewController.product_id = self?.quanTamProducts[index].id
                } else {
                    chiTietSanPhamViewController.product_id = self?.daxemProducts[index].id
                }
                self?.navigationController?.pushViewController(chiTietSanPhamViewController, animated: true)
            }
            
            return cell
        }
        return UITableViewCell()
    }
}

extension ChiTietSanPhamViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 || indexPath.section == 1 || indexPath.section == 2 {
            return UITableViewAutomaticDimension
        }
        
        let itemWidth = UIScreen.main.bounds.width/3
        let imageWidth = itemWidth - 20
        let itemHeight = 6 + 4 + imageWidth + 4 + 33.5 + 4 + 17 + 4 + 6
        return itemHeight + 16 + 17 + 16
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 2 {
            let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: reuseHeaderIdentifier) as! DanhGiaSanPhamHeaderView
            
            headerView.likeCountLabel.text = "\(product.likes) thích"
            headerView.commentCountLabel.text = "\(product.comments) bình luận"
            headerView.shareCountLabel.text = "\(product.shares) chia sẻ"
            
            return headerView
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 2 {
            return UITableViewAutomaticDimension
        }
        return CGFloat.leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return 200
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if section == 2 {
            let footerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: reuseFooterIdentifier) as! DanhGiaSanPhamFooterView
            
            footerView.commentButton.addTarget(self, action: #selector(doCommentButton), for: .touchUpInside)
            footerView.viewMoreButton.addTarget(self, action: #selector(doViewMoreComment), for: .touchUpInside)
            
            return footerView
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == 2 {
            return UITableViewAutomaticDimension
        }
        return CGFloat.leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForFooterInSection section: Int) -> CGFloat {
        return 500
    }
}

extension ChiTietSanPhamViewController: FBSDKSharingDelegate {
    func sharer(_ sharer: FBSDKSharing!, didCompleteWithResults results: [AnyHashable : Any]!) {
        HCService.shareProductCount(id: product_id, completion: { [weak self](count) in
            if let strongSelf = self {
                strongSelf.product.shares = count
                strongSelf.tableView.reloadData()
            }
        }) { (error) in
            
        }
    }
    
    func sharer(_ sharer: FBSDKSharing!, didFailWithError error: Error!) {
        
    }
    
    func sharerDidCancel(_ sharer: FBSDKSharing!) {
        
    }
}
