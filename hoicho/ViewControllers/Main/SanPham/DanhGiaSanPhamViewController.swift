//
//  DanhGiaSanPhamViewController.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 4/19/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

private let reuseHeaderIdentifier = "DanhGiaTableHeaderView"
private let reuseCellIdentifier = "DanhGiaTableViewCell"

class DanhGiaSanPhamViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var loadMoreIndicator: UIActivityIndicatorView!
    var refreshControl: UIRefreshControl!
    
    var product: ProductModel!
    var datasource = [CommentModel]() {
        didSet {
            tableView?.reloadData()
        }
    }
    var last_id: Int!
    var can_load_more = true
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        loadData()
    }

    func setupUI() {
        title = "Đánh giá sản phẩm"
        tableView.register(UINib(nibName: reuseCellIdentifier, bundle: nil), forCellReuseIdentifier: reuseCellIdentifier)
        tableView.register(UINib(nibName: reuseHeaderIdentifier, bundle: nil), forHeaderFooterViewReuseIdentifier: reuseHeaderIdentifier)
        loadMoreIndicator = initLoadMoreIndicator()
        refreshControl = initRefreshControl()
        if #available(iOS 10, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
    }
    
    func loadData(refresh: Bool = false, next: Bool = false) {
        if !refresh && !next {
            showProgress()
        } else if !refresh && next {
            tableView.tableFooterView = loadMoreIndicator
        }
        
        HCService.getCommentProduct(product_id: product.id, last_id: last_id, completion: { [weak self](objects) in
            if let strongSelf = self {
                strongSelf.dismissProgress()
                strongSelf.refreshControl.endRefreshing()
                strongSelf.tableView.tableFooterView = UIView()
                if strongSelf.last_id == nil {
                    strongSelf.datasource = objects
                } else {
                    strongSelf.datasource.append(contentsOf: objects)
                }
                if objects.count == Constant.limit {
                    strongSelf.can_load_more = true
                    strongSelf.last_id = objects.last?.id
                } else {
                    strongSelf.can_load_more = false
                }
            }
        }) { (error) in
            self.refreshControl.endRefreshing()
            self.tableView.tableFooterView = UIView()
            self.showError(error)
        }
    }
    
    override func doRefresh() {
        last_id = nil
        loadData()
    }
    

    @objc func replyComment(_ sender: UIButton) {
        
        self.requestLoginWithCompletion {[weak self]() in
            
            guard let themBinhLuanViewController = ThemBinhLuanViewController.viewController(.common) as? ThemBinhLuanViewController else {return}
            themBinhLuanViewController.product = self?.product
            themBinhLuanViewController.parent_comment = self?.datasource[sender.tag]
            themBinhLuanViewController.completion = {[weak self](comment) in
                self?.datasource[sender.tag].comments.append(comment)
                self?.tableView.reloadData()
            }
            self?.present(HCNavigationController(rootViewController: themBinhLuanViewController), animated: true, completion: nil)
        }
    }

    @IBAction func openThemBinhLuan(_ sender: Any) {
        self.requestLoginWithCompletion {[weak self]() in
            
            guard let themBinhLuanViewController = ThemBinhLuanViewController.viewController(.common) as? ThemBinhLuanViewController else {return}
            themBinhLuanViewController.product = self?.product
            themBinhLuanViewController.completion = {[weak self](comment) in
                self?.datasource.insert(comment, at: 0)
                self?.tableView?.reloadData()
            }
            self?.present(HCNavigationController(rootViewController: themBinhLuanViewController), animated: true, completion: nil)
        }
    }
}

extension DanhGiaSanPhamViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return datasource.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datasource[section].comments.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseCellIdentifier, for: indexPath) as! DanhGiaTableViewCell
        
        cell.mediaLeftConstraint.constant = 46
        cell.comment = datasource[indexPath.section].comments[indexPath.row]
        
        cell.replyButton.tag = indexPath.section
        cell.replyButton.addTarget(self, action: #selector(replyComment(_:)), for: .touchUpInside)
        
        
        cell.didSelectAvatar = {
            guard let thongTinCaNhanViewController = ThongTinCaNhanViewController.viewController(.setting) as? ThongTinCaNhanViewController else {return}
            thongTinCaNhanViewController.hidesBottomBarWhenPushed = true
            thongTinCaNhanViewController.user_id = self.datasource[indexPath.section].comments[indexPath.row].account_id
            self.navigationController?.pushViewController(thongTinCaNhanViewController, animated: true)
        }
        
        if can_load_more && datasource.count - 1 == indexPath.row {
            loadData(next: true)
        }
        
        return cell
    }
}

extension DanhGiaSanPhamViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 1000
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return 1000
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: reuseHeaderIdentifier) as! DanhGiaTableHeaderView
        
        headerView.comment = datasource[section]
        
        headerView.replyButton.tag = section
        headerView.replyButton.addTarget(self, action: #selector(replyComment(_:)), for: .touchUpInside)
        headerView.didSelectAvatar = {
            guard let thongTinCaNhanViewController = ThongTinCaNhanViewController.viewController(.setting) as? ThongTinCaNhanViewController else {return}
            thongTinCaNhanViewController.hidesBottomBarWhenPushed = true
            thongTinCaNhanViewController.user_id = self.datasource[section].account_id
            self.navigationController?.pushViewController(thongTinCaNhanViewController, animated: true)
        }
        
        
        return headerView
    }
}
