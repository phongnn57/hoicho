//
//  ThemDiemBanViewController.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 4/27/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit
import CoreLocation

class ThemDiemBanViewController: UIViewController {

    @IBOutlet weak var infoLabelTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var noteLabel: UILabel!
    @IBOutlet weak var priceView: UIView!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var productViewHeight: NSLayoutConstraint!
    @IBOutlet weak var productView: UIView!
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var productPriceLabel: UILabel!
    
    @IBOutlet weak var priceTextField: UITextField!
    
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var provinceTextField: UITextField!
    @IBOutlet weak var districtTextField: UITextField!
    @IBOutlet weak var addressTextField: UITextField!
//    @IBOutlet weak var coordinateTextField: UITextField!
    
    var product: ProductModel!
    
    var city: RegionModel? {
        didSet {
            provinceTextField?.text = city?.name
            district = nil
        }
    }
    var district: DistrictModel? {
        didSet {
            if district != nil {
                districtTextField?.text = district!.type + " " + district!.name
            } else {
                districtTextField?.text = nil
            }
            
        }
    }
//    var location: CLLocationCoordinate2D? {
//        didSet {
//            if location != nil {
//                coordinateTextField?.text = "\(location!.latitude), \(location!.longitude)"
//            } else {
//                coordinateTextField?.text = nil
//            }
//
//        }
//    }
    
    var completion: (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
    }
    
    func setupUI() {
        title = "Thêm điểm bán"
        
        if product != nil {
            productImageView.sd_setImage(with: URL(string: product.image), placeholderImage: #imageLiteral(resourceName: "icon_no_image"))
            productNameLabel.text = product.name
            productPriceLabel.text = product.price.priceString()
            
            priceTextField.addTarget(self, action: #selector(textChanged(_:)), for: .editingChanged)
        } else {
            productView.isHidden = true
            productViewHeight.constant = 0
            priceLabel.isHidden = true
            priceLabel.text = nil
            priceView.isHidden = true
            noteLabel.isHidden = true
            infoLabelTopConstraint.constant = 30
        }
        
        
        phoneTextField.addTarget(self, action: #selector(getSalePointInfo), for: .editingChanged)
    }
    
    @objc func textChanged(_ sender: UITextField) {
        if let amountString = sender.text?.currencyInputFormatting() {
            sender.text = amountString
        }
    }
    
    @objc func getSalePointInfo() {
        
        HCService.findUser(phoneTextField.text ?? "", completion: { [weak self](object) in
            if let strongSelf = self {
                strongSelf.nameTextField.text = object.name
                strongSelf.provinceTextField.text = object.region
                strongSelf.districtTextField.text = object.district
                strongSelf.addressTextField.text = object.address
                
                if !object.region.isEmpty {
                    let selectedCity = RegionModel(name: object.region)
                    strongSelf.city = selectedCity
                }
                if !object.district.isEmpty {
                    let selectedDistrict = DistrictModel(name: object.district)
                    strongSelf.district = selectedDistrict
                }
                
            }
        }) { (error) in
            
        }

    }
    
    @IBAction func doAddButton(_ sender: Any) {
        self.view.endEditing(true)
        
        
        if product != nil {
            if Utilities.isNilOrEmpty(priceTextField.text) {
                showAlertWithMessage("Vui lòng nhập giá sản phẩm")
                return
            }
            if priceTextField.text!.amountNumber().intValue <= 0 {
                showAlertWithMessage("Giá sản phẩm phải lớn hơn 0đ")
                return
            }
        }
        
        
        if Utilities.isNilOrEmpty(phoneTextField.text) {
            showAlertWithMessage("Vui lòng nhập số điện thoại")
            return
        }
        if Utilities.isNilOrEmpty(nameTextField.text) {
            showAlertWithMessage("Vui lòng nhập tên điểm bán")
            return
        }
        if Utilities.isNilOrEmpty(districtTextField.text) {
            showAlertWithMessage("Vui lòng nhập quận, huyện")
            return
        }
        if Utilities.isNilOrEmpty(addressTextField.text) {
            showAlertWithMessage("Vui lòng nhập địa chỉ chi tiết")
            return
        }
        if city == nil {
            showAlertWithMessage("Vui lòng chọn tỉnh/thành phố")
            return
        }
        if district == nil {
            showAlertWithMessage("Vui lòng chọn quận/huyện")
            return
        }
        
        showProgress()
        if product != nil {
            HCService.themDiemBan(id: product.id, price: priceTextField.text!.amountNumber().intValue, phone: phoneTextField.text!, name: nameTextField.text!, city: city!.name, district: district!.name, address: addressTextField.text!, location: nil, completion: {[weak self]() in
                self?.dismissProgress()
                self?.showAlertWithMessage("Thêm điểm bán thành công", completion: {
                    self?.dismiss(animated: true, completion: {
                        self?.completion?()
                    })
                })
            }) { (error) in
                self.showError(error)
            }
        } else {
            HCService.themDiemBanGianHang(phone: phoneTextField.text!, name: nameTextField.text!, city: city!.name, district: district!.name, address: addressTextField.text!, location: nil, completion: {[weak self]() in
                self?.dismissProgress()
                self?.showAlertWithMessage("Thêm điểm bán thành công", completion: {
                    self?.dismiss(animated: true, completion: {
                        self?.completion?()
                    })
                })
            }) { (error) in
                self.showError(error)
            }
        }
        
    }
    
}

extension ThemDiemBanViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField.isEqual(provinceTextField) {
            guard let khuVucViewController = KhuVucViewController.viewController(.common) as? KhuVucViewController else {return false}
            khuVucViewController.completion = {[weak self](region) in
                if let strongSelf = self {
                    strongSelf.city = region
                }
            }
            self.present(HCNavigationController(rootViewController: khuVucViewController), animated: true, completion: nil)
            return false
        } else if textField.isEqual(districtTextField) {
            if city == nil {
                showAlertWithMessage("Vui lòng chọn tỉnh/thành phố trước")
                return false
            } else {
                guard let khuVucViewController = KhuVucViewController.viewController(.common) as? KhuVucViewController else {return false}
                khuVucViewController.provinceId = city!.id
                khuVucViewController.provinceName = provinceTextField.text ?? ""
                khuVucViewController.completionDistrict = {[weak self](district) in
                    if let strongSelf = self {
                        strongSelf.district = district
                    }
                }
                self.present(HCNavigationController(rootViewController: khuVucViewController), animated: true, completion: nil)
                return false
            }
            
        }
//        else if textField.isEqual(coordinateTextField) {
//            guard let chonViTriViewController = ChonViTriViewController.viewController(.home) as? ChonViTriViewController else {return false}
//            chonViTriViewController.completion = {[weak self](coordinate) in
//                if let strongSelf = self {
//                    strongSelf.location = coordinate
//                }
//            }
//            self.navigationController?.pushViewController(chonViTriViewController, animated: true)
//            return false
//        }
        return true
    }
    
    
}
