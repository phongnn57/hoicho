//
//  DanhSachSanPhamKhacViewController.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 5/8/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

enum DanhSachSanPhamKhacType {
    case noibat
    case yeuthich
    case daxem
    case cunggianhang
}

class DanhSachSanPhamKhacViewController: DanhSachSanPhamViewController {
    
    var type: DanhSachSanPhamKhacType = .noibat
    var product_id: Int!

    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        loadData()
    }
    
    override func setupUI() {
        super.setupUI()
        
        if type == .noibat {
            title = "Sản phẩm nổi bật"
        } else if type == .yeuthich {
            title = "Sản phẩm quan tâm"
        } else if type == .daxem {
            title = "Sản phẩm đã xem"
        } else if type == .cunggianhang {
            title = "Sản phẩm cùng gian hàng"
        }
    }

    
    override func loadData(refresh: Bool = false, next: Bool = false) {
        if !refresh && !next {
            showProgress()
        }
        
        if type == .noibat {
            HCService.getHighlightProducts(offset: offset, completion: { [weak self](objects) in
                if let strongSelf = self {
                    strongSelf.refreshControl.endRefreshing()
                    strongSelf.dismissProgress()
                    
                
                    if strongSelf.offset == 0 {
                        strongSelf.datasource = objects
                    } else {
                        strongSelf.datasource.append(contentsOf: objects)
                    }
                    
                    if objects.count == Constant.limit {
                        strongSelf.can_load_more = true
                        strongSelf.offset += Constant.limit
                    } else {
                        strongSelf.can_load_more = false
                    }
                }
            }) { (error) in
                self.refreshControl.endRefreshing()
                self.showError(error)
            }
        } else if type == .yeuthich {
            HCService.getFavoriteProducts(offset: offset, completion: { [weak self](objects) in
                if let strongSelf = self {
                    strongSelf.refreshControl.endRefreshing()
                    strongSelf.dismissProgress()
                    
                    if strongSelf.offset == 0 {
                        strongSelf.datasource = objects
                    } else {
                        strongSelf.datasource.append(contentsOf: objects)
                    }
                    
                    if objects.count == Constant.limit {
                        strongSelf.can_load_more = true
                        strongSelf.offset += Constant.limit
                    } else {
                        strongSelf.can_load_more = false
                    }
                }
            }) { (error) in
                self.refreshControl.endRefreshing()
                self.showError(error)
            }
        } else if type == .daxem {
            HCService.getViewedProducts(product_id: product_id, offset: offset, completion: { [weak self](objects) in
                if let strongSelf = self {
                    strongSelf.refreshControl.endRefreshing()
                    strongSelf.dismissProgress()
                    
                    
                    if strongSelf.offset == 0 {
                        strongSelf.datasource = objects
                    } else {
                        strongSelf.datasource.append(contentsOf: objects)
                    }
                    
                    if objects.count == Constant.limit {
                        strongSelf.can_load_more = true
                        strongSelf.offset += Constant.limit
                    } else {
                        strongSelf.can_load_more = false
                    }
                }
            }) { (error) in
                self.refreshControl.endRefreshing()
                self.showError(error)
            }
        } else if type == .cunggianhang {
            HCService.getSanPhamCungGianHang(offset: offset, product_id: product_id, completion: { [weak self](objects) in
                if let strongSelf = self {
                    strongSelf.refreshControl.endRefreshing()
                    strongSelf.dismissProgress()

                    if strongSelf.offset == 0 {
                        strongSelf.datasource = objects
                    } else {
                        strongSelf.datasource.append(contentsOf: objects)
                    }
                    
                    if objects.count == Constant.limit {
                        strongSelf.can_load_more = true
                        strongSelf.offset += Constant.limit
                    } else {
                        strongSelf.can_load_more = false
                    }
                }
            }) { (error) in
                self.refreshControl.endRefreshing()
                self.showError(error)
            }
        }
    }
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0 {
            return 0
        }
        return datasource.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = super.collectionView(collectionView, cellForItemAt: indexPath)

        return cell
    }

}
