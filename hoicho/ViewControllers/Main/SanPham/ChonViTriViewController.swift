//
//  ChonViTriViewController.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 5/24/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation

class ChonViTriViewController: UIViewController {

    @IBOutlet weak var mapView: GMSMapView!
    let locationManager = CLLocationManager()
    let zoomLevel: Float = 15.0
    
    var completion: ((CLLocationCoordinate2D) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Chọn vị trí"
        
        
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.startUpdatingLocation()
        
        let camera = GMSCameraPosition.camera(withLatitude: 21.0277644,
                                              longitude: 105.8341598,
                                              zoom: zoomLevel)
        self.mapView.animate(to: camera)
        
        let doneItem = UIBarButtonItem(title: "Xong", style: .plain, target: self, action: #selector(doneItemTapped))
        navigationItem.rightBarButtonItem = doneItem
    }
    
    @objc func doneItemTapped() {
        completion?(mapView.projection.coordinate(for: mapView.center))
        navigationController?.popViewController(animated: true)
    }



}

extension ChonViTriViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location: CLLocation = locations.last else {return}
        
        let camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude,
                                              longitude: location.coordinate.longitude,
                                              zoom: zoomLevel)
        
        if mapView.isHidden {
            mapView.isHidden = false
            mapView.camera = camera
        } else {
            mapView.animate(to: camera)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        
    }
}
