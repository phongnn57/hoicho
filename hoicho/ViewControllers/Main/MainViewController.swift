//
//  MainViewController.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 4/18/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit
private let reuseCellProductIdentifier = "ProductCollectionViewCell"
private let reuseHeaderIdentifier = "MainHeaderCollectionReusableView"

enum MainViewProductType: Int {
    case goiy = 0
    case yeuthich = 1
    case daxem = 2
}

class MainViewProduct {
    var type: MainViewProductType = .goiy
    var offset = 0
    var can_load_more = true
    var products = [ProductModel]()
    
    init(_ type: MainViewProductType) {
        self.type = type
    }
    
    static func getLists() -> [MainViewProduct] {
        var temp = [MainViewProduct.init(.goiy)]
        if !UserModel.shared.token.isEmpty {
            temp.append(MainViewProduct.init(.yeuthich))
            temp.append(MainViewProduct.init(.daxem))
        }
        return temp
    }
}

class MainViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    var refreshControl: UIRefreshControl!
    var searchBar: UISearchBar!
    
    lazy var menuViewController: MenuDanhMucViewController = {
        let menuViewController = MenuDanhMucViewController.viewController(.main) as! MenuDanhMucViewController
        return menuViewController
    }()
    var opacityView: UIView!
    
    var banners = [BannerModel]() {
        didSet {
            collectionView?.reloadData()
        }
    }
    var brands = [BrandModel]() {
        didSet {
            collectionView?.reloadData()
        }
    }
    var categories = [CategoryModel]() {
        didSet {
            collectionView?.reloadData()
        }
    }
    var highlightProducts = [ProductModel]() {
        didSet {
            collectionView?.reloadData()
        }
    }
    
    //
    var current_type: MainViewProductType = .goiy {
        didSet {
            collectionView?.reloadData()
        }
    }
    var yeuThichProduct: MainViewProduct = MainViewProduct.init(.yeuthich)
    var daXemProduct: MainViewProduct = MainViewProduct.init(.daxem)
    var news = [TongHopModel]() {
        didSet {
            collectionView?.reloadData()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        loadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if UserModel.shared.token.isEmpty || UserModel.shared.id == 0 {
            daXemProduct.products = DataManager.shared.getProducts()
            self.collectionView?.reloadData()
        }
    }
    
    func setupUI() {
        let menuItem = UIBarButtonItem.init(image: #imageLiteral(resourceName: "tabbar_5"), style: .plain, target: self, action: #selector(doMenuItem))
        navigationItem.leftBarButtonItem = menuItem
        
        NotificationCenter.default.addObserver(self, selector: #selector(userLoggedIn(_:)), name: NSNotification.Name.UserLoggedIn, object: nil)
        
        opacityView = UIView(frame: navigationController!.view.bounds)
        opacityView.backgroundColor = .black
        opacityView.autoresizingMask = [UIViewAutoresizing.flexibleHeight, UIViewAutoresizing.flexibleWidth]
        opacityView.layer.opacity = 0.0
        opacityView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(closeMenu)))
        
        searchBar = UISearchBar()
        searchBar.sizeToFit()
        searchBar.searchBarStyle = .minimal
        searchBar.delegate = self
        searchBar.placeholder = "Tìm kiếm"
        searchBar.setTextColor(color: UIColor(hex: "#333333"), font: UIFont.systemFont(ofSize: 14, weight: .medium))
        navigationItem.titleView = searchBar
        
        
        collectionView.register(UINib(nibName: reuseCellProductIdentifier, bundle: nil), forCellWithReuseIdentifier: reuseCellProductIdentifier)
        collectionView.register(UINib(nibName: reuseHeaderIdentifier, bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: reuseHeaderIdentifier)
        refreshControl = initRefreshControl()
        if #available(iOS 10.0, *) {
            collectionView.refreshControl = refreshControl
        } else {
            collectionView.addSubview(refreshControl)
        }
        
        menuViewController.didSelectCategory = {[weak self](category) in
            if let strongSelf = self {
                strongSelf.dismissMenu(completion: {
                    strongSelf.openCategory(category)
                })
            }
        }
        menuViewController.didSelectThongTinHoiCho = {[weak self](index) in
            if let strongSelf = self {
                strongSelf.dismissMenu(completion: {
                    strongSelf.openThongTinHoiCho(index)
                })
            }
        }
    }
    
    func openThongTinHoiCho(_ index: Int) {
        if index == 0  || index == 1 || index == 2 {
            guard let quanLyTongHopViewController = QuanLyTongHopTableViewController.viewController(.setting) as? QuanLyTongHopTableViewController else {return}
            quanLyTongHopViewController.hidesBottomBarWhenPushed = true
            if index == 0 {
                quanLyTongHopViewController.type = .tintuc
            } else if index == 1 {
                quanLyTongHopViewController.type = .hoidap
            } else if index == 2 {
                quanLyTongHopViewController.type = .thongtinchung
            }
            
            navigationController?.pushViewController(quanLyTongHopViewController, animated: true)
        } else {
            guard let soDoGianHangViewController = SoDoGianHangViewController.viewController(.home) as? SoDoGianHangViewController else {return}
            soDoGianHangViewController.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(soDoGianHangViewController, animated: true)
        }
    }
    
    func openCategory(_ category: CategoryModel) {
        DispatchQueue.main.async {
            guard let danhSachSanPhamViewController = DanhSachSanPhamViewController.viewController(.main) as? DanhSachSanPhamViewController else {return}
            danhSachSanPhamViewController.hidesBottomBarWhenPushed = true
            danhSachSanPhamViewController.category = category
            self.navigationController?.pushViewController(danhSachSanPhamViewController, animated: true)
        }
    }
    
    @objc func userLoggedIn(_ notification: NSNotification) {
        doRefresh()
    }
    
    @objc func doMenuItem() {
        
        let menuWidth: CGFloat = 270
        
        navigationController!.view.addSubview(opacityView)
        
        menuViewController.view.frame = CGRect(x: -menuWidth, y: 0, width: menuWidth, height: navigationController!.view.bounds.height)
        navigationController?.addChildViewController(menuViewController)
        navigationController!.view.addSubview(menuViewController.view)
        
        
        DispatchQueue.main.async(execute: {
            if let window = UIApplication.shared.keyWindow {
                window.windowLevel = UIWindowLevelStatusBar + 1
            }
        })
        
        menuViewController.beginAppearanceTransition(true, animated: true)

        
        menuViewController.view.layer.masksToBounds = false
        menuViewController.view.layer.shadowOffset = CGSize(width: 0,height: 0)
        menuViewController.view.layer.shadowOpacity = 0
        menuViewController.view.layer.shadowRadius = 0
        menuViewController.view.layer.shadowPath = UIBezierPath(rect: menuViewController.view.bounds).cgPath
        
        UIView.animate(withDuration: 0.4, delay: 0.0, options: [], animations: { [weak self]() -> Void in
            if let strongSelf = self {
                strongSelf.menuViewController.view.frame = CGRect.init(x: 0, y: 0, width: menuWidth, height: UIScreen.main.bounds.height - 49)
                strongSelf.opacityView.layer.opacity = 0.5
                
            }
        }) { [weak self](Bool) -> Void in
            if let strongSelf = self {
                strongSelf.view.isUserInteractionEnabled = false
                strongSelf.menuViewController.endAppearanceTransition()
            }
        }
        
    }
    
    @objc func closeMenu() {
        dismissMenu()
        
    }
    
    func dismissMenu(completion: (() -> Void)? = nil) {
        let menuWidth: CGFloat = 270
        menuViewController.beginAppearanceTransition(false, animated: true)
        UIView.animate(withDuration: 0.4, delay: 0.0, options: [], animations: { [weak self]() -> Void in
            if let strongSelf = self {
                strongSelf.menuViewController.view.frame = CGRect.init(x: -menuWidth, y: 0, width: menuWidth, height: UIScreen.main.bounds.height - 49)
                strongSelf.opacityView.layer.opacity = 0.0
                strongSelf.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            }
        }) { [weak self](Bool) -> Void in
            if let strongSelf = self {
                strongSelf.menuViewController.view.layer.masksToBounds = true
                strongSelf.menuViewController.view.layer.opacity = 1.0
                strongSelf.view.isUserInteractionEnabled = true
                strongSelf.menuViewController.endAppearanceTransition()
                strongSelf.opacityView.removeFromSuperview()
                strongSelf.menuViewController.willMove(toParentViewController: nil)
                strongSelf.menuViewController.view.removeFromSuperview()
                strongSelf.menuViewController.removeFromParentViewController()
            }
            completion?()
        }
        DispatchQueue.main.async(execute: {
            if let window = UIApplication.shared.keyWindow {
                window.windowLevel = UIWindowLevelNormal
            }
        })
    }
    
    func openDanhSachSanPham(_ index: Int) {
        guard let danhSachSanPhamViewController = DanhSachSanPhamViewController.viewController(.main) as? DanhSachSanPhamViewController else {return}
        danhSachSanPhamViewController.hidesBottomBarWhenPushed = true
        danhSachSanPhamViewController.brand = brands[index]
        navigationController?.pushViewController(danhSachSanPhamViewController, animated: true)
    }
    
    func openBannerAction(_ index: Int) {
        if !banners[index].action_url.isEmpty {
            openWebViewWith(url: banners[index].action_url, webTitle: "")
        }
    }
    
    func openProduct(_ id: Int) {
        guard let chiTietSanPhamViewController = ChiTietSanPhamViewController.viewController(.home) as? ChiTietSanPhamViewController else {return}
        chiTietSanPhamViewController.hidesBottomBarWhenPushed = true
        chiTietSanPhamViewController.product_id = id
        navigationController?.pushViewController(chiTietSanPhamViewController, animated: true)
    }
    
    func loadData(refresh: Bool = false) {
        if !refresh {
            showProgress()
        }
        
        var count = 0
        
        let dispatchGroup = DispatchGroup()
        dispatchGroup.enter()
        HCService.getBanners(completion: { [weak self](objects) in
            count += 1
            dispatchGroup.leave()
            if let strongSelf = self {
                strongSelf.banners = objects
            }
            
        }) { (error) in
            count += 1
            dispatchGroup.leave()
        }
        
        dispatchGroup.enter()
        HCService.getCategories(completion: { [weak self](objects) in
            count += 1
            dispatchGroup.leave()
            if let strongSelf = self {
                strongSelf.categories = objects
            }
        }) { (error) in
            count += 1
            dispatchGroup.leave()
        }
        
        dispatchGroup.enter()
        HCService.getHighlightBrands(completion: { [weak self](objects) in
            count += 1
            dispatchGroup.leave()
            if let strongSelf = self {
                strongSelf.brands = objects
            }
        }) { (error) in
            count += 1
            dispatchGroup.leave()
        }
        
        dispatchGroup.enter()
        HCService.getHighlightProducts(completion: { [weak self](objects) in
            count += 1
            dispatchGroup.leave()
            if let strongSelf = self {
                strongSelf.highlightProducts = objects
            }
        }) { (error) in
            count += 1
            dispatchGroup.leave()
        }

        if !UserModel.shared.token.isEmpty && UserModel.shared.id != 0 {
            dispatchGroup.enter()
            HCService.getFavoriteProducts(completion: { [weak self](objects) in
                count += 1
                dispatchGroup.leave()
                if let strongSelf = self {
                    strongSelf.yeuThichProduct.products = objects
                    strongSelf.collectionView.reloadData()

                }
            }) { (error) in
                count += 1
                dispatchGroup.leave()
            }
            
            self.daXemProduct.offset = 0
            dispatchGroup.enter()
            HCService.getViewedProducts(offset: self.daXemProduct.offset, completion: { [weak self](objects) in
                count += 1
                dispatchGroup.leave()
                if let strongSelf = self {
                    if strongSelf.daXemProduct.offset == 0 {
                        strongSelf.daXemProduct.products = objects
                    } else {
                        strongSelf.daXemProduct.products.append(contentsOf: objects)
                    }
                    strongSelf.collectionView.reloadData()
                    
                    if objects.count == Constant.limit {
                        strongSelf.daXemProduct.can_load_more = true
                        strongSelf.daXemProduct.offset += Constant.limit
                    } else {
                        strongSelf.daXemProduct.can_load_more = false
                    }
                }
            }) { (error) in
                count += 1
                dispatchGroup.leave()
            }
        } else {
            self.daXemProduct.products = DataManager.shared.getProducts()
        }
        
        
        dispatchGroup.enter()
        HCService.getBaiVietTongHop(type: .tintuc, offset: 0, category_id: nil, name: "", status: true, completion: {[weak self](objects, _, _) in
            if let strongSelf = self {
                strongSelf.news = objects
                count += 1
                dispatchGroup.leave()
            }

        }) { (error) in
            count += 1
            dispatchGroup.leave()
        }
        
        dispatchGroup.notify(queue: .main) {
            if (UserModel.shared.token.isEmpty && count == 5) || (!UserModel.shared.token.isEmpty && count == 7) {
                self.dismissProgress()
                self.refreshControl.endRefreshing()
            }
        }
    }
    
    func loadMoreViewedProducts() {
        HCService.getViewedProducts(offset: self.daXemProduct.offset, completion: { [weak self](objects) in
            if let strongSelf = self {
                if strongSelf.daXemProduct.offset == 0 {
                    strongSelf.daXemProduct.products = objects
                } else {
                    strongSelf.daXemProduct.products.append(contentsOf: objects)
                }
                strongSelf.collectionView.reloadData()
                
                if objects.count == Constant.limit {
                    strongSelf.daXemProduct.can_load_more = true
                    strongSelf.daXemProduct.offset += Constant.limit
                } else {
                    strongSelf.daXemProduct.can_load_more = false
                }
            }
        }) { (error) in
        }
    }
    
    override func doRefresh() {
        loadData(refresh: true)
    }
    
    @objc func openViewMoreProducts(_ sender: UIButton) {
        guard let danhSachSanPhamKhacViewController = DanhSachSanPhamKhacViewController.viewController(.main) as? DanhSachSanPhamKhacViewController else {return}
        if sender.tag == 3 {
            danhSachSanPhamKhacViewController.type = .noibat
        } else if sender.tag == 4 {
            danhSachSanPhamKhacViewController.type = .yeuthich
        } else if sender.tag == 5 {
            danhSachSanPhamKhacViewController.type = .daxem
        }
        navigationController?.pushViewController(danhSachSanPhamKhacViewController, animated: true)
    }
    
    @objc func openTinTucHoiCho(_ index: Int) {
        showProgress()
        HCService.getChiTietBaiViet(type: .tintuc, id: news[index].id, completion: { (object) in
            self.openWebViewWith(html: object, webTitle: self.news[index].name)
            self.dismissProgress()
        }) { (error) in
            self.showError(error)
        }
    }

}

extension MainViewController: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 7
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0 {
            if banners.count == 0 {
                return 0
            }
            return 1
        } else if section == 1 {
            if brands.count == 0 {
                return 0
            }
            return 1
        } else if section == 2 {
            if categories.count == 0 {
                return 0
            }
            return 1
        } else if section == 3 {
            if highlightProducts.count == 0 {return 0}
            return 1
        } else if section == 4 {
            if !UserModel.shared.token.isEmpty && yeuThichProduct.products.count > 0 {
                return 1
            }
        } else if section == 5 {
            if daXemProduct.products.count > 0 {
                return 1
            }
        } else if section == 6 {
            if news.count > 0 {
                return 1
            }
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.section == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BannerCollectionViewCell", for: indexPath) as! BannerCollectionViewCell
            cell.banners = banners
            cell.completion = {[weak self](index) in
                self?.openBannerAction(index)
            }
            
            return cell
        } else if indexPath.section == 1 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DanhSachThuongHieuCollectionViewCell", for: indexPath) as! DanhSachThuongHieuCollectionViewCell
            cell.brands = brands
            cell.didSelectItemAt = {[weak self](index) in
                self?.openDanhSachSanPham(index)
            }
            
            return cell
        } else if indexPath.section == 2 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DanhMucCollectionViewCell", for: indexPath) as! DanhMucCollectionViewCell
            
            cell.categories = categories
            cell.didSelectItemAt = {[weak self](index) in
                if let strongSelf = self {
                    strongSelf.openCategory(strongSelf.categories[index])
                }
            }
            
            return cell
        } else if indexPath.section == 3 || indexPath.section == 4 || indexPath.section == 5 || indexPath.section == 6 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SanPhamNoiBatCollectionViewCell", for: indexPath) as! SanPhamNoiBatCollectionViewCell
            
            cell.showNews = false
            
            if indexPath.section == 3 {
                cell.datasource = highlightProducts
                cell.titleLabel.text = "Sản phẩm nổi bật"
                if highlightProducts.count >= Constant.limit {
                    cell.viewMoreButton.isHidden = false
                } else {
                    cell.viewMoreButton.isHidden = true
                }
            } else if indexPath.section == 4 {
                cell.datasource = yeuThichProduct.products
                cell.titleLabel.text = "Sản phẩm quan tâm"
                if yeuThichProduct.products.count >= Constant.limit {
                    cell.viewMoreButton.isHidden = false
                } else {
                    cell.viewMoreButton.isHidden = true
                }
            } else if indexPath.section == 5 {
                cell.datasource = daXemProduct.products
                cell.titleLabel.text = "Sản phẩm đã xem"
                if daXemProduct.products.count >= Constant.limit {
                    cell.viewMoreButton.isHidden = false
                } else {
                    cell.viewMoreButton.isHidden = true
                }
            } else if indexPath.section == 6 {
                cell.showNews = true
                cell.news = self.news
                cell.titleLabel.text = "Tin tức hội chợ"
                if news.count >= Constant.limit {
                    cell.viewMoreButton.isHidden = false
                } else {
                    cell.viewMoreButton.isHidden = true
                }
            }
            cell.viewMoreButton.tag = indexPath.section
            cell.viewMoreButton.addTarget(self, action: #selector(openViewMoreProducts(_:)), for: .touchUpInside)
            
            cell.didSelectAtIndex = {[weak self](index) in
                if let strongSelf = self {
                    if indexPath.section == 3 {
                        strongSelf.openProduct(strongSelf.highlightProducts[index].id)
                    } else if indexPath.section == 4 {
                        strongSelf.openProduct(strongSelf.yeuThichProduct.products[index].id)
                    } else if indexPath.section == 5 {
                        strongSelf.openProduct(strongSelf.daXemProduct.products[index].id)
                    } else if indexPath.section == 6 {
                        strongSelf.openTinTucHoiCho(index)
                    }
                }
                
            }
            
            return cell
        }
//        else if indexPath.section == 5 {
//            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseCellProductIdentifier, for: indexPath) as! ProductCollectionViewCell
//
//            cell.product = daXemProduct.products[indexPath.row]
//
//            if indexPath.row % 2 == 0 {
//                cell.leftConstraint.constant = 8
//                cell.rightConstraint.constant = 4
//            } else {
//                cell.leftConstraint.constant = 4
//                cell.rightConstraint.constant = 8
//            }
//
//            if UserModel.shared.token.isEmpty || UserModel.shared.id == 0 {
//
//            } else {
//                if daXemProduct.can_load_more && daXemProduct.products.count - 1 == indexPath.row {
//                    loadMoreViewedProducts()
//                }
//            }
//
//            return cell
//        }
        
        return UICollectionViewCell()
    }
}

extension MainViewController: UICollectionViewDelegate {
    


}

extension MainViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.section == 0 {
            return CGSize.init(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.width/3)
        } else if indexPath.section == 1 {
            return CGSize.init(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.width/2)
        } else if indexPath.section == 2 {
            return CGSize.init(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.width * 3 / 4 + 40)
        } else if indexPath.section == 3 || indexPath.section == 4 || indexPath.section == 5 {
            let itemWidth = UIScreen.main.bounds.width/3
            let imageWidth = itemWidth - 20
            let itemHeight = 6 + 4 + imageWidth + 4 + 33.5 + 4 + 17 + 4 + 6
            if indexPath.section == 3 {
                return CGSize(width: UIScreen.main.bounds.width, height: itemHeight * 2 + 17 + 16)
            } else {
                return CGSize(width: UIScreen.main.bounds.width, height: itemHeight + 17 + 16)
            }
            
        } else if indexPath.section == 6 {
            let itemWidth = UIScreen.main.bounds.width/2
            let imageWidth = itemWidth - 20
            let itemHeight = imageWidth * 2/3 + 125
            return CGSize(width: UIScreen.main.bounds.width, height: itemHeight + 17 + 16)
        }

        return CGSize.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        if section == 0 {
            return CGSize.zero
        } else {
            if section == 1 && brands.count == 0 {
                return CGSize.zero
            }
            if section == 2 && categories.count == 0 {
                return CGSize.zero
            }
            if section == 3 && highlightProducts.count == 0 {
                return CGSize.zero
            }
            if section == 4 && yeuThichProduct.products.count == 0 {
                return CGSize.zero
            }
            if section == 5 && daXemProduct.products.count == 0 {
                return CGSize.zero
            }
            if section == 6 && news.count == 0 {
                return CGSize.zero
            }
            return CGSize(width: UIScreen.main.bounds.width, height: 16)
        }
    }
}

extension MainViewController: UISearchBarDelegate {
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        guard let lichSuTimKiemViewController = LichSuTimKiemViewController.viewController(.home) as? LichSuTimKiemViewController else {return false}
        self.present(HCNavigationController(rootViewController: lichSuTimKiemViewController), animated: false, completion: nil)
        return false
    }
}
