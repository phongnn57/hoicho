//
//  GianHangViewController.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 4/19/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

enum GianHangViewType: Int {
    case thong_tin = 0
    case san_pham = 1
    case danh_muc = 2
    case danh_gia = 3
}

private let reuseCellProductIdentifier = "ProductCollectionViewCell"
private let reuseCellReviewIdentifier = "DanhGiaCollectionViewCell"

class GianHangViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    var refreshControl: UIRefreshControl!

    var viewType: GianHangViewType = .thong_tin {
        didSet {
            doRefresh()
        }
    }
    
    var booth_id: Int!
    var booth: GianHangModel! {
        didSet {
            title = booth?.name
            setFollowItem()
            DispatchQueue.main.async {
                self.collectionView?.reloadData()
            }
        }
    }
    var can_load_more_rate = true
    var rate_offset = 0
    var rates = [RateModel]() {
        didSet {
            DispatchQueue.main.async {
                self.collectionView?.reloadData()
            }
        }
    }
    var categories = [CategoryModel]() {
        didSet {
            DispatchQueue.main.async {
                self.collectionView?.reloadData()
            }
        }
    }

    var can_load_more_product = true
    var product_offset = 0
    var products = [ProductModel]() {
        didSet {
            DispatchQueue.main.async {
                self.collectionView?.reloadData()
            }
        }
    }
    var followImageView: UIImageView!
    
    var followImediately = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        loadData()
    }
    
    func setupUI() {
        collectionView.register(UINib(nibName: reuseCellProductIdentifier, bundle: nil), forCellWithReuseIdentifier: reuseCellProductIdentifier)
        collectionView.register(UINib(nibName: reuseCellReviewIdentifier, bundle: nil), forCellWithReuseIdentifier: reuseCellReviewIdentifier)
        
        refreshControl = initRefreshControl()
        if #available(iOS 10, *) {
            collectionView.refreshControl = refreshControl
        } else {
            collectionView.addSubview(refreshControl)
        }
        if UserModel.shared.id == booth_id {
            let editItem = UIBarButtonItem(image: #imageLiteral(resourceName: "icon_more_vertical"), style: .plain, target: self, action: #selector(openOption))
            navigationItem.rightBarButtonItem = editItem
        } else {
            followImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
            followImageView.image = #imageLiteral(resourceName: "icon_product_like")
            followImageView.clipsToBounds = true
            followImageView.contentMode = .scaleAspectFit
            followImageView.tintColor = UIColor(hex: "#666666")
            followImageView.isUserInteractionEnabled = true
            followImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(followGianHang)))
            let followItem = UIBarButtonItem(customView: followImageView)
            navigationItem.rightBarButtonItem = followItem
        }

    }
    
    @objc func openOption() {
        let actionSheetController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        actionSheetController.addAction(UIAlertAction(title: "Cập nhật thông tin", style: .default, handler: { (_) in
            self.updateBooth()
        }))
        actionSheetController.addAction(UIAlertAction(title: "Huỷ", style: .cancel, handler: { (_) in
            
        }))
        if let popoverController = actionSheetController.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.height, width: 0, height: 0)
        }
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    func updateBooth() {
        guard let cauHinhGianHangViewController = CauHinhGianHangViewController.viewController(.setting) as? CauHinhGianHangViewController else {return}
        cauHinhGianHangViewController.hidesBottomBarWhenPushed = true
        cauHinhGianHangViewController.completion = {
            self.doRefresh()
        }
        navigationController?.pushViewController(cauHinhGianHangViewController, animated: true)
    }
    
    @objc func followGianHang() {
        self.requestLoginWithCompletion {
            self.showProgress()
            HCService.followGianHang(self.booth_id, completion: {
                self.booth.follow = !self.booth.follow
                self.setFollowItem()
                self.dismissProgress()
//                self.showAlertWithMessage("Thành công")
            }) { (error) in
                self.showError(error)
            }
        }
        
    }
    
    func setFollowItem() {
        if booth == nil || followImageView == nil {return}
        if booth.follow {
            followImageView.tintColor = UIColor.red
        } else {
            followImageView.tintColor = UIColor(hex: "#666666")
        }
    }
    
    func openAddRating(value: Double) {
        if let themDanhGiaView = ThemDanhGiaView.loadFromNibNamed() as? ThemDanhGiaView {
            themDanhGiaView.frame = UIScreen.main.bounds
            navigationController?.view.addSubview(themDanhGiaView)
            themDanhGiaView.ratingView.rating = value
            themDanhGiaView.openPopup()
            themDanhGiaView.completion = {[weak self](content, rate) in
                guard let strongSelf = self else {return}
                strongSelf.showProgress()
                HCService.danhGiaGianHang(id: strongSelf.booth_id, content: content, rate: rate, completion: {(rate) in
                    strongSelf.dismissProgress()
                    strongSelf.rates.insert(rate, at: 0)
                }, failure: { (error) in
                    strongSelf.showError(error)
                })
            }
        }
    }
    
    func loadBooth(completion: @escaping () -> Void) {
        HCService.getBoothDetail(id: booth_id, completion: { [weak self](object) in
            if let strongSelf = self {
                strongSelf.booth = object
            }
            completion()
        }) { (error) in

        }
    }
    
    func loadData(refresh: Bool = false, next: Bool = false) {
        
        if !refresh && !next {
            showProgress()
        }
        
        if viewType == .thong_tin {
            
            HCService.getBoothDetail(id: booth_id, completion: { [weak self](object) in
                if let strongSelf = self {
                    strongSelf.booth = object
                    strongSelf.refreshControl.endRefreshing()
                    strongSelf.dismissProgress()
                    
                    if !refresh && !next && !strongSelf.booth.follow {
                        HCService.followGianHang(strongSelf.booth_id, completion: {
                            strongSelf.booth.follow = !strongSelf.booth.follow
                            strongSelf.setFollowItem()
                        }, failure: { (_) in
                            
                        })
                    }
                }
                
            }) { (error) in
                self.refreshControl.endRefreshing()
                self.dismissProgress()
                self.showError(error)
            }
        } else if viewType == .san_pham {
            HCService.getProductBooth(booth_id: self.booth_id, category_id: nil, offset: self.product_offset, completion: { [weak self](objects) in
                if let strongSelf = self {
                    if strongSelf.product_offset == 0 {
                        strongSelf.products = objects
                    } else {
                        strongSelf.products.append(contentsOf: objects)
                    }
                    if objects.count == Constant.limit {
                        strongSelf.can_load_more_product = true
                        strongSelf.product_offset += Constant.limit
                    } else {
                        strongSelf.can_load_more_product = false
                    }
                    strongSelf.refreshControl.endRefreshing()
                    strongSelf.dismissProgress()
                }
               
            }) { (error) in
                self.refreshControl.endRefreshing()
                self.dismissProgress()
                self.showError(error)
            }
        } else if viewType == .danh_muc {
            HCService.getDanhMucGianHang(id: self.booth_id, completion: { [weak self](objects) in
                if let strongSelf = self {
                    strongSelf.categories = objects
                    strongSelf.refreshControl.endRefreshing()
                    strongSelf.dismissProgress()
                }
            }) { (error) in
                self.refreshControl.endRefreshing()
                self.dismissProgress()
                self.showError(error)
            }
        } else if viewType == .danh_gia {
            HCService.getBoothRates(id: self.booth_id, offset: self.rate_offset, completion: { [weak self](objects) in
                if let strongSelf = self {
                    if strongSelf.rate_offset == 0 {
                        strongSelf.rates = objects
                    } else {
                        strongSelf.rates.append(contentsOf: objects)
                    }
                    if objects.count == Constant.limit {
                        strongSelf.can_load_more_rate = true
                        strongSelf.rate_offset += Constant.limit
                    } else {
                        strongSelf.can_load_more_rate = false
                    }
                    strongSelf.refreshControl.endRefreshing()
                    strongSelf.dismissProgress()
                }

            }) { (error) in
                self.refreshControl.endRefreshing()
                self.dismissProgress()
                self.showError(error)
            }
        }
        
    }
    
    
    override func doRefresh() {
        rate_offset = 0
        product_offset = 0
        loadData(refresh: true)
    }
    
    func viewTypeChanged(index: Int) {
        if index == 0 {
            viewType = .thong_tin
        } else if index == 1 {
            viewType = .san_pham
        } else if index == 2 {
            viewType = .danh_muc
        } else {
            viewType = .danh_gia
        }
    }
    
    @objc func doCallHotline() {
        doCall(booth.hotline)
    }

}

extension GianHangViewController: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if booth == nil {
            return 0
            
        }
        
        if viewType == .danh_gia {
            return 3
        }
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }
        if viewType == .thong_tin {
            if section == 1 {return 1}
        } else if viewType == .danh_gia {
            if section == 1 {return 1}
            return rates.count
        } else if viewType == .danh_muc {
            if section == 1 {
                return categories.count
            }
        } else if viewType == .san_pham {
            return products.count
        }
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.section == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GianHangTopCollectionViewCell", for: indexPath) as! GianHangTopCollectionViewCell
            
            cell.didSelectButtonAt = {(index) in
                self.viewTypeChanged(index: index)
            }

            cell.setIndicatorAtIndex(viewType.rawValue, animated: true)
            
            cell.gianHangImageView.sd_setImage(with: URL(string: booth.banner))

            
            return cell
        }
        
        if viewType == .thong_tin {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ThongTinGianHangCollectionViewCell", for: indexPath) as! ThongTinGianHangCollectionViewCell
            
            cell.nameLabel.text = booth.name
            cell.productCountLabel.text = "\(booth.product_count)"
            cell.addressLabel.text = booth.address
            cell.rateLabel.text = "\(booth.rate_count)"
            cell.clickLabel.text = "\(booth.count_follow)"
            cell.shareLabel.text = "\(booth.visit)"
            cell.introductionLabel.attributedText = booth.introduction.html2AttributedString
            cell.phoneButton.setTitle(booth.hotline, for: .normal)
            cell.phoneButton.addTarget(self, action: #selector(doCallHotline), for: .touchUpInside)
            
            return cell
        } else if viewType == .danh_muc {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DanhMucGianHangCollectionViewCell", for: indexPath) as! DanhMucGianHangCollectionViewCell
            
            let item = categories[indexPath.row]
            
            cell.nameLabel.text = item.name
            cell.countLabel.text = "\(item.product_count) sản phẩm"
            
            return cell
        } else if viewType == .danh_gia {
            if indexPath.section == 1 {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ThemDanhGiaGianHangCollectionViewCell", for: indexPath) as! ThemDanhGiaGianHangCollectionViewCell
                cell.ratingView.didFinishTouchingCosmos = {[weak self](value)in
                    if let strongSelf = self {
                        strongSelf.openAddRating(value: value)
                    }
                }
                
                return cell
            }
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseCellReviewIdentifier, for: indexPath) as! DanhGiaCollectionViewCell
            
            let item = rates[indexPath.row]
            
            cell.avatarImageView.sd_setImage(with: URL(string: item.account_image), placeholderImage: #imageLiteral(resourceName: "icon_default_avatar"))
            cell.nameLabel.text = item.account_name
            cell.contentLabel.text = item.content
            cell.timeLabel.text = item.time?.toString()
            cell.ratingView.rating = item.rate_point
            
            if can_load_more_rate && rates.count - 1 == indexPath.row {
                loadData(next: true)
            }
            
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseCellProductIdentifier, for: indexPath) as! ProductCollectionViewCell
            
            if indexPath.row % 2 == 0 {
                cell.leftConstraint.constant = 8
                cell.rightConstraint.constant = 4
            } else {
                cell.leftConstraint.constant = 4
                cell.rightConstraint.constant = 8
            }

            cell.product = products[indexPath.row]
            
            if can_load_more_product && products.count - 1 == indexPath.row {
                loadData(next: true)
            }
            
            return cell
        }
        
    }
}

extension GianHangViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if viewType == .san_pham {
            guard let chiTietSanPhamViewController = ChiTietSanPhamViewController.viewController(.home) as? ChiTietSanPhamViewController else {return}
            chiTietSanPhamViewController.hidesBottomBarWhenPushed = true
            chiTietSanPhamViewController.product_id = products[indexPath.row].id
            navigationController?.pushViewController(chiTietSanPhamViewController, animated: true)
        } else if viewType == .danh_muc {
            guard let sanPhamGianHangViewController = SanPhamGianHangViewController.viewController(.home) as? SanPhamGianHangViewController else {return}
            sanPhamGianHangViewController.category = categories[indexPath.row]
            sanPhamGianHangViewController.booth_id = booth_id
            navigationController?.pushViewController(sanPhamGianHangViewController, animated: true)
        }
    }
}

extension GianHangViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if indexPath.section == 0 {
            let itemHeight = UIScreen.main.bounds.width/2 + 44
            return CGSize(width: UIScreen.main.bounds.width, height: itemHeight)
        }
        
        if viewType == .thong_tin {
            if indexPath.section == 1 {
                let contentHeight = booth.introduction.height(withConstrainedWidth: UIScreen.main.bounds.width - 30, font: UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.semibold))
                return CGSize.init(width: UIScreen.main.bounds.width, height: contentHeight + 320)
            }
            return CGSize.init(width: UIScreen.main.bounds.width, height: 48)
        } else if viewType == .danh_muc {
            return CGSize.init(width: UIScreen.main.bounds.width, height: 60)
        } else if viewType == .danh_gia {
            if indexPath.section == 1 {
                return CGSize.init(width: UIScreen.main.bounds.width, height: 60)
            }
            
            let contentHeight = rates[indexPath.row].content.height(withConstrainedWidth: UIScreen.main.bounds.width - 30 - 24, font: UIFont.systemFont(ofSize: 14))
            return CGSize.init(width: UIScreen.main.bounds.width, height: 67 + contentHeight)
        }
        
        let itemWidth = UIScreen.main.bounds.width/2
        let imageWidth = itemWidth - 20
        let itemHeight = 6 + 4 + imageWidth + 4 + 33.5 + 4 + 17 + 4 + 6 + 8
        return CGSize.init(width: itemWidth, height: itemHeight)
    }
}
