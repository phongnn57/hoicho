//
//  SanPhamGianHangViewController.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 6/2/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

class SanPhamGianHangViewController: DanhSachSanPhamViewController {
    
    var booth_id: Int!

    override func viewDidLoad() {
        super.viewDidLoad()

        title = category.name
        loadData()
    }
    
    override func loadData(refresh: Bool = false, next: Bool = false) {
        if !refresh && !next {
            showProgress()
        }
        
        HCService.getProductBooth(booth_id: booth_id, category_id: category.id, offset: offset, completion: { [weak self](objects) in
            if let strongSelf = self {
                if strongSelf.offset == 0 {
                    strongSelf.datasource = objects
                } else {
                    strongSelf.datasource.append(contentsOf: objects)
                }
                if objects.count == Constant.limit {
                    strongSelf.can_load_more = true
                    strongSelf.offset += Constant.limit
                } else {
                    strongSelf.can_load_more = false
                }
                strongSelf.dismissProgress()
                strongSelf.refreshControl.endRefreshing()
            }
        }) { (error) in
            self.showError(error)
            self.refreshControl.endRefreshing()
        }
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0 {
            return 0
        }
        
        return datasource.count
    }

}
