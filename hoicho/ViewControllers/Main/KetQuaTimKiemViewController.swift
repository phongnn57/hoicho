//
//  KetQuaTimKiemViewController.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 4/20/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

private let reuseCellIdentifier = "ProductTableViewCell"
private let reuseShopIdentifier = "ShopTableViewCell"
private let reuseSalePointIdentifier = "SearchSalePointTableViewCell"

enum SearchType: Int, EnumEnumerable {
    case product
    case booth
    case sale_point
}

class KetQuaTimKiemViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var refreshControl: UIRefreshControl!
    var loadMoreIndicator: UIActivityIndicatorView!
    
    var searchType: SearchType = .product
    var bottomInset: CGFloat = 0 {
        didSet {
            tableView?.contentInset = UIEdgeInsets.init(top: 0, left: 0, bottom: bottomInset, right: 0)
            tableView?.scrollIndicatorInsets = UIEdgeInsets.init(top: 0, left: 0, bottom: bottomInset, right: 0)
        }
    }
    
    var offset = 0
    var can_load_more = true
    var products = [ProductModel]() {
        didSet {
            tableView?.reloadData()
        }
    }
    var booths = [GianHangModel]() {
        didSet {
            tableView?.reloadData()
        }
    }
    var salepoints = [DiemBanModel]() {
        didSet {
            tableView?.reloadData()
        }
    }
    var text = "" {
        didSet {
            if viewIsShown {
                offset = 0
                loadData()
            }
            
        }
    }
    
    var viewIsShown = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        loadData()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        viewIsShown = true
    }
    
    func setupUI() {
        tableView.tableFooterView = UIView()
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 500
        tableView.register(UINib(nibName: reuseCellIdentifier, bundle: nil), forCellReuseIdentifier: reuseCellIdentifier)
        tableView.register(UINib(nibName: reuseShopIdentifier, bundle: nil), forCellReuseIdentifier: reuseShopIdentifier)
        tableView.register(UINib(nibName: reuseSalePointIdentifier, bundle: nil), forCellReuseIdentifier: reuseSalePointIdentifier)
        tableView.contentInset = UIEdgeInsets.init(top: 0, left: 0, bottom: bottomInset, right: 0)
        tableView.scrollIndicatorInsets = UIEdgeInsets.init(top: 0, left: 0, bottom: bottomInset, right: 0)
        
        refreshControl = initRefreshControl()
        loadMoreIndicator = initLoadMoreIndicator()
        if #available(iOS 10, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
    }
    
    func loadData(refresh: Bool = false, next: Bool = false) {
        if !refresh && !next {
            showProgress()
        } else if !refresh && next {
            tableView.tableFooterView = loadMoreIndicator
        }
        if searchType == .booth {
            HCService.searchBooths(text: text, offset: offset, completion: { [weak self](objects) in

                if let strongSelf = self {
                    strongSelf.tableView.tableFooterView = UIView()
                    strongSelf.refreshControl.endRefreshing()
                    strongSelf.dismissProgress()
                    if strongSelf.offset == 0 {
                        strongSelf.booths = objects
                    } else {
                        strongSelf.booths.append(contentsOf: objects)
                    }
                    if objects.count == Constant.limit {
                        strongSelf.can_load_more = true
                        strongSelf.offset += Constant.limit
                    } else {
                        strongSelf.can_load_more = false
                    }
                }
            }) { (error) in
                self.showError(error)
                self.tableView?.tableFooterView = UIView()
                self.refreshControl?.endRefreshing()
            }
        } else if searchType == .product {
            HCService.searchProducts(text: text, offset: offset, completion: { [weak self](objects) in
                
                if let strongSelf = self {
                    strongSelf.tableView.tableFooterView = UIView()
                    strongSelf.refreshControl.endRefreshing()
                    strongSelf.dismissProgress()
                    if strongSelf.offset == 0 {
                        strongSelf.products = objects
                    } else {
                        strongSelf.products.append(contentsOf: objects)
                    }
                    if objects.count == Constant.limit {
                        strongSelf.can_load_more = true
                        strongSelf.offset += Constant.limit
                    } else {
                        strongSelf.can_load_more = false
                    }
                }
            }) { (error) in
                self.showError(error)
                self.tableView.tableFooterView = UIView()
                self.refreshControl.endRefreshing()
            }
        } else if searchType == .sale_point {
            HCService.searchSalePoint(text: text, offset: offset, completion: { [weak self](objects) in
                if let strongSelf = self {
                    strongSelf.tableView.tableFooterView = UIView()
                    strongSelf.refreshControl.endRefreshing()
                    strongSelf.dismissProgress()
                    if strongSelf.offset == 0 {
                        strongSelf.salepoints = objects
                    } else {
                        strongSelf.salepoints.append(contentsOf: objects)
                    }
                    if objects.count == Constant.limit {
                        strongSelf.can_load_more = true
                        strongSelf.offset += Constant.limit
                    } else {
                        strongSelf.can_load_more = false
                    }
                }
            }) { (error) in
                self.showError(error)
                self.tableView.tableFooterView = UIView()
                self.refreshControl.endRefreshing()
            }
        }
        
    }
    
    override func doRefresh() {
        offset = 0
        can_load_more = true
        loadData(refresh: true)
    }
}

extension KetQuaTimKiemViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchType == .product {
            return products.count
        } else if searchType == .sale_point {
            return salepoints.count
        }
        return booths.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if searchType == .booth {
            let cell = tableView.dequeueReusableCell(withIdentifier: reuseShopIdentifier, for: indexPath) as! ShopTableViewCell
            
            let item = booths[indexPath.row]
            
            let attributedString = NSMutableAttributedString()
            attributedString.append(NSAttributedString(string: "\(item.product_count)", attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 12), NSAttributedStringKey.foregroundColor: UIColor.HCColor.appColor]))
            attributedString.append(NSAttributedString(string: " sản phẩm", attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 12), NSAttributedStringKey.foregroundColor: UIColor(hex: "#333333")]))
            cell.productCountLabel.attributedText = attributedString
            
            let attributedString1 = NSMutableAttributedString()
            
            attributedString1.append(NSAttributedString(string: "Tham gia ", attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 12), NSAttributedStringKey.foregroundColor: UIColor(hex: "#333333")]))
            attributedString1.append(NSAttributedString(string: "\(item.created_at!.toString())", attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 12), NSAttributedStringKey.foregroundColor: UIColor.HCColor.appColor]))
            cell.joinDateLabel.attributedText = attributedString1
            
            cell.shopNameLabel.text = item.name
            
            if can_load_more && booths.count == indexPath.row + 1 {
                loadData(next: true)
            }
            
            
            return cell
        } else if searchType == .product {
            let cell = tableView.dequeueReusableCell(withIdentifier: reuseCellIdentifier, for: indexPath) as! ProductTableViewCell
            
            let item = products[indexPath.row]
            
            cell.productImageView.sd_setImage(with: URL(string: item.image))
            cell.productNameLabel.text = item.name
            cell.productCodeLabel.text = item.code
            cell.productPriceLabel.text = item.price.priceString()
            
            if can_load_more && products.count == indexPath.row + 1 {
                loadData(next: true)
            }
            
            return cell
        } else if searchType == .sale_point {
            let cell = tableView.dequeueReusableCell(withIdentifier: reuseSalePointIdentifier, for: indexPath) as! SearchSalePointTableViewCell
        
            let item = salepoints[indexPath.row]
            cell.nameLabel.text = item.name
            cell.addressLabel.text = item.address
            
            return cell
        }
        
        return UITableViewCell()
        
    }
}

extension KetQuaTimKiemViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if searchType == .product {
            guard let chiTietSanPhamViewController = ChiTietSanPhamViewController.viewController(.home) as? ChiTietSanPhamViewController else {return}
            chiTietSanPhamViewController.hidesBottomBarWhenPushed = true
            chiTietSanPhamViewController.product_id = products[indexPath.row].id
            navigationController?.pushViewController(chiTietSanPhamViewController, animated: true)
        } else if searchType == .sale_point {
            
            guard let chiTietDiemBanViewController = ChiTietDiemBanViewController.viewController(.common) as? ChiTietDiemBanViewController else {return}
            chiTietDiemBanViewController.salePoint = salepoints[indexPath.row]
            navigationController?.pushViewController(chiTietDiemBanViewController, animated: true)
            
        } else if searchType == .booth {
            guard let gianHangViewController = GianHangViewController.viewController(.home) as? GianHangViewController else {return}
            gianHangViewController.hidesBottomBarWhenPushed = true
            gianHangViewController.booth_id = booths[indexPath.row].id
            navigationController?.pushViewController(gianHangViewController, animated: true)
        }
    }
}

