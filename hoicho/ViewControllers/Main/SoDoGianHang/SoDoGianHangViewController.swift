//
//  SoDoGianHangViewController.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 6/18/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

class SoDoGianHangViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var refreshControl: UIRefreshControl!
    var loadMoreIndicatorView: UIActivityIndicatorView!
    
    var datasource = [ExpoModel](){
        didSet {
            tableView?.reloadData()
        }
    }
    var offset = 0
    var can_load_more = true
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        loadData()
    }

    func setupUI() {
        title = "Lịch hội chợ"
        
        if UserModel.shared.isAdmin() {
            navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(doAddItem))
        }
        
        
        tableView.tableFooterView = UIView()
        
        loadMoreIndicatorView = initLoadMoreIndicator()
        refreshControl = initRefreshControl()
        if #available(iOS 10, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
    }
    
    @objc func doAddItem() {
        guard let themHoiChoViewController = ThemHoiChoViewController.viewController(.setting) as? ThemHoiChoViewController else {return}
        themHoiChoViewController.completion = {
            self.doRefresh()
        }
        self.present(HCNavigationController(rootViewController: themHoiChoViewController), animated: true, completion: nil)
    }
    
    func loadData(refresh: Bool = false, next: Bool = false) {
        if !refresh && !next {
            showProgress()
        } else if !refresh && next {
            tableView.tableFooterView = loadMoreIndicatorView
        }
        
        HCService.getFair(offset: offset, completion: { [weak self](objects) in
            if let strongSelf = self {
                strongSelf.refreshControl.endRefreshing()
                strongSelf.tableView.tableFooterView = UIView()
                strongSelf.dismissProgress()
                if strongSelf.offset == 0 {
                    strongSelf.datasource = objects
                } else {
                    strongSelf.datasource.append(contentsOf: objects)
                }
                
                if objects.count == Constant.limit {
                    strongSelf.can_load_more = true
                    strongSelf.offset += Constant.limit
                } else {
                    strongSelf.can_load_more = false
                }
            }
        }) { (error) in
            self.refreshControl.endRefreshing()
            self.tableView.tableFooterView = UIView()
            self.showError(error)
        }
    }
    
    override func doRefresh() {
        offset = 0
        loadData(refresh: true)
    }
    
    @objc func doMoreButton(_ sender: UIButton) {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Sửa", style: .default, handler: { (action: UIAlertAction) in
            self.doEditExpo(sender.tag)
        }))
        actionSheet.addAction(UIAlertAction(title: "Cài đặt sơ đồ", style: .default, handler: { (action: UIAlertAction) in
            self.updateMapExpo(sender.tag)
        }))
        actionSheet.addAction(UIAlertAction(title: "Xoá", style: .default, handler: { (action: UIAlertAction) in
            self.deleteExpo(sender.tag)
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Huỷ", style: .destructive, handler: nil))
        if let popoverController = actionSheet.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.height, width: 0, height: 0)
        }
        present(actionSheet, animated: true, completion: nil)
    }
    
    func doEditExpo(_ index: Int) {
        guard let themHoiChoViewController = ThemHoiChoViewController.viewController(.setting) as? ThemHoiChoViewController else {return}
        themHoiChoViewController.expo = datasource[index]
        themHoiChoViewController.completion = {
            self.doRefresh()
        }
        self.present(HCNavigationController(rootViewController: themHoiChoViewController), animated: true, completion: nil)
    }
    
    func updateMapExpo(_ index: Int) {
        guard let capNhatSoDoViewController = CapNhatSoDoHoiChoViewController.viewController(.setting) as? CapNhatSoDoHoiChoViewController else {return}
        capNhatSoDoViewController.expo = datasource[index]
        capNhatSoDoViewController.completion = {
            self.doRefresh()
        }
        self.present(HCNavigationController(rootViewController: capNhatSoDoViewController), animated: true, completion: nil)
    }
    
    func deleteExpo(_ index: Int) {
        showAlertTwoButtonWithMessage("Bạn có chắc chắn muốn xoá") {
            self.showProgress()
            HCService.deleteExpo(id: self.datasource[index].id, completion: {
                self.datasource.remove(at: index)
                self.tableView.reloadData()
                self.dismissProgress()
            }, failure: { (error) in
                self.showError(error)
            })
        }
    }
    
}

extension SoDoGianHangViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datasource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SoDoGianHangTableViewCell", for: indexPath) as! SoDoGianHangTableViewCell
        
        cell.expo = datasource[indexPath.row]
        
        cell.moreButton.tag = indexPath.row
        cell.moreButton.addTarget(self, action: #selector(doMoreButton(_:)), for: .touchUpInside)
        
        if  can_load_more && datasource.count - 1 == indexPath.row {
            loadData(next: true)
        }
        
        return cell
    }
}

extension SoDoGianHangViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UIScreen.main.bounds.width * 2/3
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let chiTietSoDoViewController = SoDoChiTietViewController.viewController(.home) as? SoDoChiTietViewController else {return}
        chiTietSoDoViewController.expo = datasource[indexPath.row]
        navigationController?.pushViewController(chiTietSoDoViewController, animated: true)
    }
}
