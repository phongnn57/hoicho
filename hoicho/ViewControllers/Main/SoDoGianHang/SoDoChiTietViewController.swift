//
//  SoDoChiTietViewController.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 6/18/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit
import QRCode

private let reuseHeaderIdentifier = "SoDoChiTietHeaderView"

class SoDoChiTietViewController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contentViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var expandButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var arrowImageView: UIImageView!
    var refreshControl: UIRefreshControl!
    var loadMoreIndicator: UIActivityIndicatorView!
    var mapImageView: UIImageView!
    var searchBar: UISearchBar!
    var closeItem: UIBarButtonItem!
    var searchItem: UIBarButtonItem!
    
    var expo: ExpoModel!
    var offset = 0
    var can_load_more = true
    var name = "" {
        didSet {
            doRefresh()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        loadData()
    }
    
    func setupUI() {
        title = "Chi tiết sơ đồ"

        mapImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height - 64))
        mapImageView.clipsToBounds = false
        mapImageView.sd_setImage(with: URL(string: expo.map))
        mapImageView.contentMode = .scaleAspectFit
        scrollView.addSubview(mapImageView)
        scrollView.delegate = self
        
        tableView.tableFooterView = UIView()
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 500
        tableView.register(UINib(nibName: reuseHeaderIdentifier, bundle: nil), forHeaderFooterViewReuseIdentifier: reuseHeaderIdentifier)
        
        refreshControl = initRefreshControl()
        if #available(iOS 10, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        loadMoreIndicator = initLoadMoreIndicator()
        self.doExpandButton("")
        
        searchBar = UISearchBar()
        searchBar.placeholder = "Tìm theo tên gian hàng"
        searchBar.searchBarStyle = .minimal
        searchBar.delegate = self
        
        searchItem = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(doSearchItem))
        navigationItem.rightBarButtonItem = searchItem
        closeItem = UIBarButtonItem(title: "Đóng", style: .plain, target: self, action: #selector(doCloseItem))
        
    }
    
    @objc func doSearchItem() {
        navigationItem.titleView = searchBar
        navigationItem.rightBarButtonItem = closeItem
    }
    
    @objc func doCloseItem() {
        name = ""
        searchBar.text = nil
        title = "Chi tiết sơ đồ"
        navigationItem.rightBarButtonItem = searchItem
        navigationItem.titleView = nil
    }
    
    func loadData(refresh: Bool = false, next: Bool = false) {
        if !next && !refresh {
            showProgress()
        } else if !refresh && next {
            tableView.tableFooterView = loadMoreIndicator
        }
        
        HCService.mapList(id: expo.id, offset: offset, name: name, completion: { [weak self](objects) in
            if let strongSelf = self {
                if strongSelf.offset == 0 {
                    strongSelf.expo.locations = objects
                } else {
                    strongSelf.expo.locations.append(contentsOf: objects)
                }
                strongSelf.tableView.reloadData()
                
                if objects.count == Constant.limit {
                    strongSelf.offset += Constant.limit
                    strongSelf.can_load_more = true
                } else {
                    strongSelf.can_load_more = false
                }
                strongSelf.dismissProgress()
                strongSelf.refreshControl.endRefreshing()
                strongSelf.tableView.tableFooterView = UIView()
            }
        }) { (error) in
            self.refreshControl.endRefreshing()
            self.tableView.tableFooterView = UIView()
            self.showError(error)
        }
    }
    
    override func doRefresh() {
        offset = 0
        loadData(next: true)
    }

    @IBAction func doExpandButton(_ sender: Any) {
        if self.contentViewTopConstraint.constant == 0 {
            self.contentViewTopConstraint.constant = UIScreen.main.bounds.height - 60
            self.expandButton.setTitle("Mở rộng", for: .normal)
            self.arrowImageView.image = #imageLiteral(resourceName: "icon_arrow_up_black")
        } else {
            self.contentViewTopConstraint.constant = 0
            self.expandButton.setTitle("Thu nhỏ", for: .normal)
            self.arrowImageView.image = #imageLiteral(resourceName: "icon_arrow_down_black")
        }
        self.arrowImageView.tintColor = UIColor.HCColor.appColor
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    @objc func doCallButton(_ sender: UIButton) {
        doCall(expo.locations[sender.tag].booth?.hotline ?? "")
    }
    
    @objc func doSelectBooth(_ sender: UIButton) {
        if !UserModel.shared.isAdmin() {
            return
        }
        guard let chonGianHangViewController = ChonGianHangViewController.viewController(.setting) as? ChonGianHangViewController else {return}
        chonGianHangViewController.location = expo.locations[sender.tag]
        chonGianHangViewController.completion = {[weak self](booth) in
            if let strongSelf = self {
                strongSelf.expo.locations[sender.tag].booth_id = booth.id
                strongSelf.expo.locations[sender.tag].booth = booth
                strongSelf.tableView.reloadData()
            }
        }
        self.present(HCNavigationController(rootViewController: chonGianHangViewController), animated: true, completion: nil)
    }
    
    @objc func doTrashButton(_ sender: UIButton) {
        if !UserModel.shared.isAdmin() {
            return
        }
        let item = expo.locations[sender.tag]
        guard let booth = item.booth else {return}
        showAlertTwoButtonWithMessage("Bạn có chắc muốn xoá gian hàng \(booth.name) ra khỏi vị trí \(item.priority)") {
            self.showProgress()
            HCService.removeBoothFromMap(id: item.id, id_booth: item.booth_id, completion: {
                self.dismissProgress()
                self.expo.locations[sender.tag].booth_id = 0
                self.expo.locations[sender.tag].booth = nil
                self.tableView.reloadData()
            }, failure: { (error) in
                self.showError(error)
            })
        }
    }
    
}

extension SoDoChiTietViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if expo == nil {return 0}
        return expo.locations.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SoDoChiTietTableViewCell", for: indexPath) as! SoDoChiTietTableViewCell
        
        
        let item = expo.locations[indexPath.row]
        
        cell.positionLabel.text = "\(item.priority)"
        if let booth = item.booth, item.booth_id > 0 {
            cell.nameLabel.text = booth.name
            cell.addressLabel.text = booth.address
            cell.phoneButton.setTitle(booth.hotline, for: .normal)
            cell.selectButton.isHidden = true
            cell.boothStackView.isHidden = false
            if !UserModel.shared.isAdmin() {
                cell.trashButton.isHidden = true
            } else {
                cell.trashButton.isHidden = false
            }
            
        } else {
            cell.nameLabel.text = ""
            cell.addressLabel.text = ""
            cell.phoneButton.setTitle("", for: .normal)
            cell.selectButton.isHidden = false
            cell.boothStackView.isHidden = true
            cell.trashButton.isHidden = true
            if UserModel.shared.isAdmin() {
                cell.selectButton.setTitle("Chọn gian hàng", for: .normal)
            } else {
                cell.selectButton.setTitle("Chưa có gian hàng nào tại vị trí này", for: .normal)
            }

        }
        
        cell.phoneButton.tag = indexPath.row
        cell.selectButton.tag = indexPath.row
        cell.trashButton.tag = indexPath.row
        cell.phoneButton.addTarget(self, action: #selector(doCallButton(_:)), for: .touchUpInside)
        cell.selectButton.addTarget(self, action: #selector(doSelectBooth(_:)), for: .touchUpInside)
        cell.trashButton.addTarget(self, action: #selector(doTrashButton(_:)), for: .touchUpInside)
        if can_load_more && expo.locations.count - 1 == indexPath.row {
            loadData(next: true)
        }
        
        return cell
    }
}

extension SoDoChiTietViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: reuseHeaderIdentifier) as! SoDoChiTietHeaderView
        
        headerView.bgImageView.sd_setImage(with: URL(string: expo.image))
        headerView.nameLabel.text = expo.name.uppercased()
        headerView.timeLabel.text = "Thời gian: Từ \(expo.start_time?.toString(format: "HH:mm dd/MM/yyyy") ?? "...") đến \(expo.end_time?.toString(format: "HH:mm dd/MM/yyyy") ?? "...")"
        headerView.addressLabel.text = "Địa chỉ: \(expo.address)"
        headerView.qrcodeImageView.sd_setImage(with: URL(string: expo.qrcode))
        
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return 1000
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if expo.locations[indexPath.row].booth_id > 0 {
            guard let gianHangViewController = GianHangViewController.viewController(.home) as? GianHangViewController else {return}
            gianHangViewController.hidesBottomBarWhenPushed = true
            gianHangViewController.booth_id = expo.locations[indexPath.row].booth_id
            navigationController?.pushViewController(gianHangViewController, animated: true)
        }
    }
}

extension SoDoChiTietViewController: UIScrollViewDelegate {
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return mapImageView
    }
}

extension SoDoChiTietViewController: UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        self.name = ""
        title = "Chi tiết sơ đồ"
        navigationItem.rightBarButtonItem = searchItem
        navigationItem.titleView = nil
        self.dismiss(animated: false, completion: nil)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.name = searchBar.text ?? ""
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.name = searchBar.text ?? ""
    }
}
