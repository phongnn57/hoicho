//
//  DanhSachSanPhamViewController.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 4/19/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit



class DanhSachSanPhamViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    var refreshControl: UIRefreshControl!
    
    let reuseCellIdentifier = "ProductCollectionViewCell"
    
    var brand: BrandModel!
    var category: CategoryModel!
    var offset = 0
    var can_load_more = true
    var sort: ProductSortType = .lowestPrice
    var datasource = [ProductModel]() {
        didSet {
            collectionView?.reloadData()
            collectionView.emptyDataSetSource = self
            collectionView.emptyDataSetDelegate = self
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        loadData()
    }
    
    func setupUI() {
        if brand != nil {
            title = brand.name
        } else {
            title = "Danh sách sản phẩm"
        }
        
        collectionView.register(UINib(nibName: reuseCellIdentifier, bundle: nil), forCellWithReuseIdentifier: reuseCellIdentifier)
        refreshControl = initRefreshControl()
        if #available(iOS 10, *) {
            collectionView.refreshControl = refreshControl
        } else {
            collectionView.addSubview(refreshControl)
        }
    }
    
    func loadData(refresh: Bool = false, next: Bool = false) {
        if category == nil && brand == nil {return}
        if !refresh && !next {
            showProgress()
        }
        if category != nil {
            
            let dispatchGroup = DispatchGroup()
            var count = 0
            
            dispatchGroup.enter()
            HCService.getSubCategory(category_id: category.id, completion: {[weak self](categories) in
                if let strongSelf = self {
                    strongSelf.category.subCate = categories
                }
                count += 1
                dispatchGroup.leave()
            }) { (error) in
                count += 1
                dispatchGroup.leave()
            }
            
            dispatchGroup.enter()
            HCService.getProductCategory(category_id: category.id, offset: offset, sort: sort, completion: { [weak self](products) in
                if let strongSelf = self {
                    if strongSelf.offset == 0 {
                        strongSelf.datasource = products
                    } else {
                        strongSelf.datasource.append(contentsOf: products)
                    }
                    if products.count == Constant.limit {
                        strongSelf.can_load_more = true
                        strongSelf.offset += Constant.limit
                    } else {
                        strongSelf.can_load_more = false
                    }
                }
                count += 1
                dispatchGroup.leave()
                
            }) { (error) in
                count += 1
                dispatchGroup.leave()
            }
            
            dispatchGroup.notify(queue: .main) {
                if count == 2 {
                    self.dismissProgress()
                    self.refreshControl.endRefreshing()
                    self.collectionView?.reloadData()
                }
            }
            
        } else if brand != nil {
            HCService.getBrandProducts(brand_id: brand.id, offset: offset, sort: sort, completion: { [weak self](products) in
                if let strongSelf = self {
                    strongSelf.refreshControl.endRefreshing()
                    strongSelf.dismissProgress()
                    if strongSelf.offset == 0 {
                        strongSelf.datasource = products
                    } else {
                        strongSelf.datasource.append(contentsOf: products)
                    }
                    if products.count == Constant.limit {
                        strongSelf.can_load_more = true
                        strongSelf.offset += Constant.limit
                    } else {
                        strongSelf.can_load_more = false
                    }
                }
            }) { (error) in
                self.refreshControl.endRefreshing()
                self.showError(error)
            }
        }
        
    }
    
    override func doRefresh() {
        offset = 0
        can_load_more = true
        loadData(refresh: true)
    }

}

extension DanhSachSanPhamViewController: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if category == nil && brand == nil {
            return 0
        }
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0 {
            if category != nil {
                return 1
            }
            return 0
        }
        
        return datasource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.section == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BreadcrumbCollectionViewCell", for: indexPath) as! BreadcrumbCollectionViewCell
            
            cell.category = category
            cell.didSelectCategoryAtIndex = {[weak self](index, isParent) in
                if let strongSelf = self {
                    if isParent {
                        if index < strongSelf.category.breadCrumb.count {
                            let temp = strongSelf.category.breadCrumb[index]
                            strongSelf.category = temp
                            strongSelf.showProgress()
                            strongSelf.doRefresh()
                        }
                        
                    } else {
                        if index < strongSelf.category.subCate.count {
                            var newBreadCrumb = strongSelf.category.breadCrumb
                            newBreadCrumb.append(strongSelf.category.subCate[index])
                            strongSelf.category = strongSelf.category.subCate[index]
                            strongSelf.category.breadCrumb = newBreadCrumb
                            strongSelf.showProgress()
                            strongSelf.doRefresh()
                        }
                    }
                    
                }
            }
            
            return cell
        }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseCellIdentifier, for: indexPath) as! ProductCollectionViewCell
        
        if indexPath.row % 2 == 0 {
            cell.leftConstraint.constant = 8
            cell.rightConstraint.constant = 4
        } else {
            cell.leftConstraint.constant = 4
            cell.rightConstraint.constant = 8
        }
        
        cell.product = datasource[indexPath.row]
        
        if can_load_more && indexPath.row == datasource.count - 1 {
            loadData(next: true)
        }
        
        return cell
    }
}

extension DanhSachSanPhamViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let chiTietSanPhamViewController = ChiTietSanPhamViewController.viewController(.home) as? ChiTietSanPhamViewController else {return}
        chiTietSanPhamViewController.hidesBottomBarWhenPushed = true
        chiTietSanPhamViewController.product_id = datasource[indexPath.row].id
        navigationController?.pushViewController(chiTietSanPhamViewController, animated: true)
    }
}

extension DanhSachSanPhamViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.section == 0 {
            if category == nil || category.breadCrumb.count == 0 {
                return CGSize.zero
            }
            if category.subCate.count == 0 {
                return CGSize(width: UIScreen.main.bounds.width, height: 45)
            }
            return CGSize(width: UIScreen.main.bounds.width, height: 100)
        }
        let itemWidth = UIScreen.main.bounds.width/2
        let imageWidth = itemWidth - 20
        let itemHeight = 6 + 4 + imageWidth + 4 + 34 + 4 + 17 + 4 + 6
        return CGSize.init(width: itemWidth, height: itemHeight)
    }
}

extension DanhSachSanPhamViewController: DZNEmptyDataSetSource {
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        return NSAttributedString.init(string: "Không có sản phẩm nào", attributes: [NSAttributedStringKey.foregroundColor: UIColor(hex: "#999999"), NSAttributedStringKey.font: UIFont.systemFont(ofSize: 14)])
    }
    
    func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage! {
        return #imageLiteral(resourceName: "icon_empty_cart")
    }
}

extension DanhSachSanPhamViewController: DZNEmptyDataSetDelegate {
    func emptyDataSetShouldDisplay(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
    
    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
}
