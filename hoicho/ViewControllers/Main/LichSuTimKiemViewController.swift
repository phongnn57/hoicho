//
//  LichSuTimKiemViewController.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 4/20/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit
import CarbonKit

class LichSuTimKiemViewController: UIViewController {
    
    lazy var timKiemSanPhamViewController: KetQuaTimKiemViewController = {
        let timKiemSanPhamViewController = KetQuaTimKiemViewController.viewController(.home) as! KetQuaTimKiemViewController
        timKiemSanPhamViewController.searchType = .product
        return timKiemSanPhamViewController
    }()
    lazy var timKiemGianHangViewController: KetQuaTimKiemViewController = {
        let timKiemSanPhamViewController = KetQuaTimKiemViewController.viewController(.home) as! KetQuaTimKiemViewController
        timKiemSanPhamViewController.searchType = .booth
        return timKiemSanPhamViewController
    }()
    lazy var timKiemDiemBanViewController: KetQuaTimKiemViewController = {
        let timKiemDiemBanViewController = KetQuaTimKiemViewController.viewController(.home) as! KetQuaTimKiemViewController
        timKiemDiemBanViewController.searchType = .sale_point
        return timKiemDiemBanViewController
    }()
    
    var searchBar: UISearchBar!
    var carbonTabSwipeNavigation: CarbonTabSwipeNavigation!

    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
    }

    func setupUI() {
        searchBar = UISearchBar()
        searchBar.sizeToFit()
        searchBar.searchBarStyle = .minimal
        searchBar.delegate = self
        searchBar.placeholder = "Tìm kiếm"
        searchBar.setTextColor(color: UIColor(hex: "#333333"), font: UIFont.systemFont(ofSize: 14, weight: .medium))
        navigationItem.titleView = searchBar
        
        let closeItem = UIBarButtonItem(title: "Đóng", style: .plain, target: self, action: #selector(doCloseItem))
        navigationItem.rightBarButtonItem = closeItem
        
        searchBar.becomeFirstResponder()
        
        carbonTabSwipeNavigation = CarbonTabSwipeNavigation.init(items: ["Sản phẩm", "Gian hàng", "Điểm bán"], delegate: self)
        carbonTabSwipeNavigation.setNormalColor(UIColor(hex: "#333333"), font: UIFont.boldSystemFont(ofSize: 14))
        carbonTabSwipeNavigation.setSelectedColor(UIColor.HCColor.appColor, font: UIFont.boldSystemFont(ofSize: 14))
        carbonTabSwipeNavigation.setIndicatorColor(UIColor.HCColor.appColor)
        carbonTabSwipeNavigation.carbonSegmentedControl?.setWidth(UIScreen.main.bounds.width/3, forSegmentAt: 0)
        carbonTabSwipeNavigation.carbonSegmentedControl?.setWidth(UIScreen.main.bounds.width/3, forSegmentAt: 1)
        carbonTabSwipeNavigation.carbonSegmentedControl?.setWidth(UIScreen.main.bounds.width/3, forSegmentAt: 2)
        carbonTabSwipeNavigation.insert(intoRootViewController: self)
        
        timKiemSanPhamViewController.searchType = .product
        timKiemGianHangViewController.searchType = .booth
        timKiemDiemBanViewController.searchType = .sale_point
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: .UIKeyboardWillHide, object: nil)
        
    }
    
    @objc func doCloseItem() {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if searchBar != nil {
            searchBar.becomeFirstResponder()
        }
    }
    
    
    @objc func keyboardWillShow(_ notification: NSNotification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            timKiemSanPhamViewController.bottomInset = keyboardHeight
            timKiemGianHangViewController.bottomInset = keyboardHeight
        }
    }
    
    @objc func keyboardWillHide(_ notification: NSNotification){
        timKiemSanPhamViewController.bottomInset = 0
        timKiemGianHangViewController.bottomInset = 0
    }

}


extension LichSuTimKiemViewController: UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        self.dismiss(animated: false, completion: nil)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        timKiemSanPhamViewController.text = searchBar.text ?? ""
        timKiemGianHangViewController.text = searchBar.text ?? ""
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        timKiemSanPhamViewController.text = searchBar.text ?? ""
        timKiemGianHangViewController.text = searchBar.text ?? ""
    }
}

extension LichSuTimKiemViewController: CarbonTabSwipeNavigationDelegate {
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
        if index == 0 {
            return timKiemSanPhamViewController
        } else if index == 1 {
            return timKiemGianHangViewController
        }
        return timKiemDiemBanViewController
    }
}
