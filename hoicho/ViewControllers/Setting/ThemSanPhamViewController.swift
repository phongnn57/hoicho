//
//  ThemSanPhamViewController.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 5/14/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit
import DKImagePickerController

private let reuseCellIdentifier = "RemoveableMediaCollectionViewCell"
private let reuseCellTopIdentifier = "BreadcrumbTableViewCell"
private let reuseCellBottomIdentifier = "BreadcrumbColorTableViewCell"

class ThemSanPhamViewController: UIViewController {

    @IBOutlet weak var topImageViewHeight: NSLayoutConstraint!
    @IBOutlet weak var collectionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var mainImageView: UIImageView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var codeTextField: UITextField!
    @IBOutlet weak var giaBanLeTextField: UITextField!
    @IBOutlet weak var donViTinhTextField: UITextField!
    @IBOutlet weak var xoaDanhMucButton: UIButton!
    @IBOutlet weak var topStackView: UIStackView!
    @IBOutlet weak var topStackViewHeight: NSLayoutConstraint!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var bottomStackView: UIStackView!
    @IBOutlet weak var bottomStackViewHeight: NSLayoutConstraint!
    @IBOutlet weak var providerTextField: UITextField!
    @IBOutlet weak var brandTextField: UITextField!
    @IBOutlet weak var xuatXuTextField: UITextField!
    @IBOutlet weak var descriptionTextView: GrowingTextView!
    @IBOutlet weak var noiBatSwitch: UISwitch!
    @IBOutlet weak var statusSwitch: UISwitch!
    
    
    ///////////////
    @IBOutlet weak var banSiSwitch: UISwitch!
    @IBOutlet weak var banSiTuTextField: UITextField!
    @IBOutlet weak var banSiDenTextField: UITextField!
    @IBOutlet weak var soSanPhamToiThieu: UITextField!
    @IBOutlet weak var banSiStackView: UIStackView!
    @IBOutlet weak var banSiStackViewheight: NSLayoutConstraint!
    
    var topTableView: UITableView!
    var bottomTableView: UITableView!
    
    var pickerView: UIPickerView!
    
    var image: UIImage? {
        didSet {
            mainImageView.image = image
            if image == nil {
                mainImageView?.isHidden = true
                topImageViewHeight?.constant = 0
            } else {
                mainImageView?.isHidden = false
                topImageViewHeight?.constant = UIScreen.main.bounds.width * 2/3
            }
        }
    }
    
    var images = [UIImage]() {
        didSet {
            collectionView?.reloadData()
            if images.count == 0 {
                collectionView?.isHidden = true
                collectionViewHeight?.constant = 0
            } else {
                collectionView?.isHidden = false
                collectionViewHeight?.constant = 100
            }
        }
    }
    
    
    var category: CategoryModel? {
        didSet {
            loadCategory()
        }
    }
    var categories = [CategoryModel]() {
        didSet {
            bottomTableView?.reloadData()
            if categories.count == 0 {
                bottomStackView.isHidden = true
                bottomStackViewHeight.constant = 0
            } else {
                bottomStackView.isHidden = false
                bottomStackViewHeight.constant = 80
            }
        }
    }
    var breadcrumb = [CategoryModel]() {
        didSet {
            if breadcrumb.count == 0 {
                xoaDanhMucButton?.isHidden = true
                topStackView.isHidden = true
                topStackViewHeight.constant = 0
            } else {
                xoaDanhMucButton?.isHidden = false
                topStackView.isHidden = false
                topStackViewHeight.constant = 82
            }
            topTableView?.reloadData()
        }
    }
    
    var provider: GianHangModel? {
        didSet {
            providerTextField?.text = provider?.name
        }
    }
    var brand: BrandModel? {
        didSet {
            brandTextField?.text = brand?.name
        }
    }
    var relateProducts = [ProductModel]() {
        didSet {
            
        }
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        loadData()
    }
    
    func setupUI() {
        title = "Thêm sản phẩm"
        collectionView.register(UINib(nibName: reuseCellIdentifier, bundle: nil), forCellWithReuseIdentifier: reuseCellIdentifier)
        
        let topFrame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 44)
        topTableView = UITableView(frame: topFrame)
        topTableView.transform = CGAffineTransform.init(rotationAngle: -CGFloat.pi/2)
        topTableView.frame = topFrame
        topTableView.separatorStyle = .none
        topTableView.showsVerticalScrollIndicator = false
        topTableView.showsHorizontalScrollIndicator = false
        topTableView.tableFooterView = UIView()
        topTableView.register(UINib(nibName: reuseCellTopIdentifier, bundle: nil), forCellReuseIdentifier: reuseCellTopIdentifier)
        topTableView.delegate = self
        topTableView.dataSource = self
        
        
        let bottomFrame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 55)
        bottomTableView = UITableView(frame: bottomFrame)
        bottomTableView.transform = CGAffineTransform.init(rotationAngle: -CGFloat.pi/2)
        bottomTableView.frame = bottomFrame
        bottomTableView.separatorStyle = .none
        bottomTableView.showsVerticalScrollIndicator = false
        bottomTableView.showsHorizontalScrollIndicator = false
        bottomTableView.tableFooterView = UIView()
        bottomTableView.register(UINib(nibName: reuseCellBottomIdentifier, bundle: nil), forCellReuseIdentifier: reuseCellBottomIdentifier)
        bottomTableView.delegate = self
        bottomTableView.dataSource = self
        
        topView.addSubview(topTableView)
        bottomView.addSubview(bottomTableView)
        
        providerTextField.delegate = self
        brandTextField.delegate = self
        
        banSiTuTextField.addTarget(self, action: #selector(currencyTextFieldTextChanged(_:)), for: .editingChanged)
        giaBanLeTextField.addTarget(self, action: #selector(currencyTextFieldTextChanged(_:)), for: .editingChanged)
        banSiDenTextField.addTarget(self, action: #selector(currencyTextFieldTextChanged(_:)), for: .editingChanged)
        
        images.removeAll()
        image = nil

    }

    
    @objc func currencyTextFieldTextChanged(_ sender: UITextField) {
        if let amountString = sender.text?.currencyInputFormatting() {
            sender.text = amountString
        }
    }
    
    func loadData() {
        showProgress()
        HCService.getCategories(completion: { (objects) in
            self.dismissProgress()
            self.categories = objects
            self.breadcrumb.removeAll()
        }) { (error) in
            self.showError(error)
        }
    }
    
    func loadCategory() {
        if category == nil {
            showProgress()
            HCService.getCategories(completion: { (objects) in
                self.dismissProgress()
                self.categories = objects
                self.breadcrumb.removeAll()
            }) { (error) in
                self.showError(error)
            }
        } else {
            showProgress()
            HCService.getSubCateAndProducts(category_id: category!.id, offset: 0, sort: .aName, completion: { (_, objects, breadcrumb) in
                self.dismissProgress()
                self.categories = objects
                self.breadcrumb = breadcrumb
            }) { (error) in
                self.showError(error)
            }
        }
    }

    @objc func doCloseButton(_ sender: UIButton) {
        self.images.remove(at: sender.tag)
    }

    @IBAction func doBanSiSwitch(_ sender: Any) {
        if banSiSwitch.isOn {
            banSiStackView.isHidden = false
            banSiStackViewheight.constant = 211
        } else {
            banSiStackView.isHidden = true
            banSiStackViewheight.constant = 0
        }
    }
    
    @IBAction func doSelectMainImage(_ sender: Any) {
        self.doSelectPhoto()
    }
    
    @IBAction func doSelectImages(_ sender: Any) {
        self.view.endEditing(true)
        let pickerController = DKImagePickerController()
        pickerController.maxSelectableCount = 5
        pickerController.didSelectAssets = {(assets: [DKAsset]) in
            for asset in assets {
                asset.fetchFullScreenImage(true, completeBlock: { (image: UIImage?, option:[AnyHashable : Any]?) in
                    self.images.append(image!)
                })
                
            }
        }
        
        present(pickerController, animated: true, completion: nil)
    }
    
    @IBAction func doXoaDanhMuc(_ sender: Any) {
        if self.breadcrumb.count > 0 {
            if self.breadcrumb.count > 1 {
                self.category = self.breadcrumb[self.breadcrumb.count - 2]
            } else {
                self.category = nil
                self.breadcrumb.removeAll()
            }
        }
    }
    
    @IBAction func doSubmitButton(_ sender: Any) {
        self.view.endEditing(true)
        
        if Utilities.isNilOrEmpty(nameTextField.text) {
            showAlertWithMessage("Không được bỏ trống tên sản phẩm")
            return
        }
        if Utilities.isNilOrEmpty(codeTextField.text) {
            showAlertWithMessage("Không được bỏ trống mã sản phẩm")
            return
        }
        if Utilities.isNilOrEmpty(giaBanLeTextField.text) {
            showAlertWithMessage("Không được bỏ trống giá bán lẻ")
            return
        }
        if giaBanLeTextField.text!.amountNumber().intValue <= 0 {
            showAlertWithMessage("Giá bán lẻ phải lớn hơn 0 VND")
            return
        }
        
        if image == nil {
            showAlertWithMessage("Không được bỏ trống ảnh chính sản phẩm")
            return
        }


        showProgress()
        HCService.themSanPham(mainImage: image, images: images, name: nameTextField.text ?? "", code: codeTextField.text ?? "",price: giaBanLeTextField.text!.amountNumber().intValue, showBanSi: banSiSwitch.isOn, banSiTu: banSiTuTextField.text?.amountNumber().intValue ?? 0, banSiDen: banSiDenTextField.text?.amountNumber().intValue ?? 0, soSanPhamToiThieu: soSanPhamToiThieu.text?.amountNumber().intValue ?? 0, dvt: donViTinhTextField.text ?? "", provider_id: provider?.id, department_id: brand?.id, madeIn: xuatXuTextField.text ?? "", description_text: descriptionTextView.text ?? "", status: statusSwitch.isOn ? 2 : 1, featured: noiBatSwitch.isOn ? 1 : 0, categories: breadcrumb, completion: {
            self.dismissProgress()
            self.showAlertWithMessage("Thêm mới sản phẩm thành công", completion: {
                self.navigationController?.popViewController(animated: true)
            })

            
        }) { (error) in
            self.showError(error)
        }
    }
    
    override func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            self.image = image
        }
        picker.dismiss(animated: true, completion: nil)
    }
}


extension ThemSanPhamViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseCellIdentifier, for: indexPath) as! RemoveableMediaCollectionViewCell
        
        cell.mediaImageView.image = images[indexPath.row]
        cell.closeButton.tag = indexPath.row
        cell.closeButton.addTarget(self, action: #selector(doCloseButton(_:)), for: .touchUpInside)
        
        
        return cell
    }
}

extension ThemSanPhamViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: 150, height: 100)
    }
}

extension ThemSanPhamViewController: GrowingTextViewDelegate {
    func textViewDidChangeHeight(_ textView: GrowingTextView, height: CGFloat) {
        UIView.animate(withDuration: 0.2) {
            self.view.layoutIfNeeded()
        }
    }
}

extension ThemSanPhamViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView.isEqual(topTableView) {
            return breadcrumb.count
        }
        return categories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView.isEqual(topTableView) {
            let cell = tableView.dequeueReusableCell(withIdentifier: reuseCellTopIdentifier, for: indexPath) as! BreadcrumbTableViewCell
            cell.transform = CGAffineTransform.init(rotationAngle: CGFloat.pi/2)
            var font: UIFont!
            var textValue = ""
            
            if indexPath.row == breadcrumb.count - 1 {
                textValue = breadcrumb[indexPath.row].name
                font = UIFont.boldSystemFont(ofSize: 12)
            } else {
                textValue = breadcrumb[indexPath.row].name + "  " + Constant.arrowCharacter + "  "
                font = UIFont.systemFont(ofSize: 12, weight: .regular)
            }
            cell.titleLabel.font = font
            cell.titleLabel.text = textValue
            
            if indexPath.row > 0 {
                cell.leftConstraint.constant = 0
            } else {
                cell.leftConstraint.constant = 8
            }
            
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseCellBottomIdentifier, for: indexPath) as! BreadcrumbColorTableViewCell
        cell.transform = CGAffineTransform.init(rotationAngle: CGFloat.pi/2)
        cell.titleLabel.text = categories[indexPath.row].name
        
        cell.colorView.backgroundColor = UIColor(hex: UIColor.HCColor.randomColors[indexPath.row % UIColor.HCColor.randomColors.count])
        
        return cell
        
        
    }
}

extension ThemSanPhamViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView.isEqual(topTableView) {
            if indexPath.row < breadcrumb.count - 1 {
            }
        } else {
            category = categories[indexPath.row]
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView.isEqual(topTableView) {
            var font: UIFont!
            var textValue = ""
            
            if indexPath.row == breadcrumb.count - 1 {
                textValue = breadcrumb[indexPath.row].name
                font = UIFont.boldSystemFont(ofSize: 12)
            } else {
                textValue = breadcrumb[indexPath.row].name + "  " + Constant.arrowCharacter + "  "
                font = UIFont.systemFont(ofSize: 12, weight: .regular)
            }
            if indexPath.row == 0 {
                return textValue.width(withConstrainedHeight: 15, font: font) + 8
            }
            return textValue.width(withConstrainedHeight: 15, font: font)
        }
        
        return categories[indexPath.row].name.width(withConstrainedHeight: 15, font: UIFont.systemFont(ofSize: 12)) + 32
        
    }
    
    
}

extension ThemSanPhamViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField.isEqual(providerTextField) {
            guard let chonNhaCungCapViewController = ChonNhaCungCapViewController.viewController(.setting) as? ChonNhaCungCapViewController else {return false}
            chonNhaCungCapViewController.completion = {[weak self](object) in
                self?.provider = object
            }
            self.present(HCNavigationController(rootViewController: chonNhaCungCapViewController), animated: true, completion: nil)
            return false
        } else if textField.isEqual(brandTextField) {
            guard let chonThuongHieuViewController = ChonThuongHieuViewController.viewController(.setting) as? ChonThuongHieuViewController else {return false}
            chonThuongHieuViewController.completion = {[weak self](object) in
                self?.brand = object
            }
            self.present(HCNavigationController(rootViewController: chonThuongHieuViewController), animated: true, completion: nil)
            return false
        }
        return true
    }
}

