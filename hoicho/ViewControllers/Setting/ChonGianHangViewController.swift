//
//  ChonGianHangViewController.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 6/18/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit
import QRCode

class ChonGianHangViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var refreshControl: UIRefreshControl!
    var loadMoreIndicator: UIActivityIndicatorView!
    
    var searchBar: UISearchBar!
    var closeItem: UIBarButtonItem!
    var searchItem: UIBarButtonItem!
    
    var offset = 0
    var can_load_more = true
    var datasource = [GianHangModel]() {
        didSet {
            tableView?.reloadData()
        }
    }
    var name = "" {
        didSet {
            doRefresh()
        }
    }
    var location: ExpoLocationModel!
    var completion: ((GianHangModel) -> Void)?

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        loadData()
    }
    
    func setupUI() {
        title = "Chọn gian hàng"
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 200
        tableView.tableFooterView = UIView()
        
        refreshControl = initRefreshControl()
        loadMoreIndicator = initLoadMoreIndicator()
        if #available(iOS 10, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }

        searchBar = UISearchBar()
        searchBar.placeholder = "Tìm theo tên gian hàng"
        searchBar.searchBarStyle = .minimal
        searchBar.delegate = self
        
        searchItem = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(doSearchItem))
        navigationItem.rightBarButtonItem = searchItem
        closeItem = UIBarButtonItem(title: "Đóng", style: .plain, target: self, action: #selector(doCloseItem))
    }
    
    
    @objc func doSearchItem() {
        navigationItem.titleView = searchBar
        navigationItem.rightBarButtonItem = closeItem
    }
    
    @objc func doCloseItem() {
        name = ""
        searchBar.text = nil
        title = "Chọn gian hàng"
        navigationItem.rightBarButtonItem = searchItem
        navigationItem.titleView = nil
    }

    func loadData(refresh: Bool = false, next: Bool = false) {
        if !refresh && !next {
            showProgress()
        } else if !refresh && next {
            tableView.tableFooterView = loadMoreIndicator
        }
        HCService.getBooths(offset: offset, is_follow: false, name: self.name, completion: { [weak self](objects) in
            if let strongSelf = self {
                strongSelf.dismissProgress()
                strongSelf.refreshControl.endRefreshing()
                strongSelf.tableView.tableFooterView = UIView()
                
                if strongSelf.offset == 0 {
                    strongSelf.datasource = objects
                } else {
                    strongSelf.datasource.append(contentsOf: objects)
                }
                if objects.count == Constant.limit {
                    strongSelf.offset += Constant.limit
                    strongSelf.can_load_more = true
                } else {
                    strongSelf.can_load_more = false
                }
            }
        }) { (error) in
            self.refreshControl.endRefreshing()
            self.tableView.tableFooterView = UIView()
            self.showError(error)
        }
    }
    
    override func doRefresh() {
        offset = 0
        loadData(refresh: true)
    }
    
    @objc func doCallButton(_ sender: UIButton) {
        doCall(datasource[sender.tag].hotline)
    }
    
    @objc func doSaveQRCode(_ sender: UIButton) {
        
        let image = UIImage(view: createQRCodeView(name: datasource[sender.tag].name, qrcode: datasource[sender.tag].qrcode))
        UIImageWriteToSavedPhotosAlbum(image, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
        
    }
    
    func createQRCodeView(name: String, qrcode: String) -> UIView {
        let viewWidth = UIScreen.main.bounds.width
        let viewHeight: CGFloat = 380
        
        let aView = UIView(frame: CGRect(x: 0, y: 0, width: viewWidth, height: viewHeight))
        
        let dash1 = UIView(frame: CGRect(x: 20, y: 20, width: viewWidth - 40, height: 1))
        let dash2 = UIView(frame: CGRect(x: 20, y: viewHeight - 21, width: viewWidth - 40, height: 1))
        let dash3 = UIView(frame: CGRect(x: 20, y: 21, width: 1, height: viewHeight - 42))
        let dash4 = UIView(frame: CGRect(x: viewWidth - 21, y: 21, width: 1, height: viewHeight - 42))
        
        dash1.addDashedLine()
        dash2.addDashedLine()
        dash3.addDashedLine(vertical: true)
        dash4.addDashedLine(vertical: true)
        
        aView.addSubview(dash1)
        aView.addSubview(dash2)
        aView.addSubview(dash3)
        aView.addSubview(dash4)
        
        let qrcodeImageView = UIImageView(frame: CGRect(x: (viewWidth - 240)/2, y: 40, width: 240, height: 240))
        qrcodeImageView.contentMode = .scaleAspectFit
        qrcodeImageView.clipsToBounds = true
        qrcodeImageView.sd_setImage(with: URL(string: qrcode))
        aView.addSubview(qrcodeImageView)
        
        let nameLabel = UILabel(frame: CGRect(x: 40, y: 300, width: viewWidth - 80, height: 40))
        nameLabel.textAlignment = .center
        nameLabel.textColor = UIColor.HCColor.appColor
        nameLabel.numberOfLines = 0
        nameLabel.text = "Gian hàng: " + name
        nameLabel.font = UIFont.systemFont(ofSize: 14, weight: .semibold)
        aView.addSubview(nameLabel)
        
        return aView
    }
    
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let _ = error {
            self.showAlertWithMessage("Không lưu được ảnh. Hãy đảm bảo bạn đã cấp quyền cho ứng dụng lưu ảnh vào điện thoại của bạn")
        } else {
            self.showAlertWithMessage("Lưu ảnh thành công")
        }
    }
}

extension ChonGianHangViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datasource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChonGianHangTableViewCell", for: indexPath) as! ChonGianHangTableViewCell
        
        let item = datasource[indexPath.row]
        
        cell.nameLabel.text = item.name
        cell.phoneButton.setTitle(item.hotline, for: .normal)
        cell.addressLabel.text = item.address

        cell.phoneButton.tag = indexPath.row
        cell.phoneButton.addTarget(self, action: #selector(doCallButton(_:)), for: .touchUpInside)
        
        if can_load_more && datasource.count - 1 == indexPath.row {
            loadData(next: true)
        }
        
        return cell
    }
}

extension ChonGianHangViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        showAlertTwoButtonWithMessage("Xác nhận thêm gian hàng \(datasource[indexPath.row].name) vào sơ đồ hội chợ tại vị trí \(location.priority)") {
            self.showProgress()
            HCService.addBoothToMap(id: self.location.id, id_booth: self.datasource[indexPath.row].id, completion: {
                self.dismissProgress()
                self.showAlertWithMessage("Thành công", completion: {
                    self.dismiss(animated: true, completion: {
                        self.completion?(self.datasource[indexPath.row])
                    })
                })
            }, failure: { (error) in
                self.showError(error)
            })
        }
    }
}

extension ChonGianHangViewController: UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        self.name = ""
        title = "Chọn gian hàng"
        navigationItem.rightBarButtonItem = searchItem
        navigationItem.titleView = nil
        self.dismiss(animated: false, completion: nil)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.name = searchBar.text ?? ""
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.name = searchBar.text ?? ""
    }
}
