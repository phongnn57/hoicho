//
//  QuanLySanPhamViewController.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 6/6/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

private let reuseCellIdentifier = "ProductTableViewCell"

class QuanLySanPhamViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var refreshControl: UIRefreshControl!
    var loadMoreIndicator: UIActivityIndicatorView!
    var searchBar: UISearchBar!
    var addItem: UIBarButtonItem!
    
    var offset = 0
    var can_load_more = true
    var products = [ProductModel]() {
        didSet {
            tableView?.reloadData()
        }
    }
    var text = "" {
        didSet {
            offset = 0
            loadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        loadData()
    }
    
    func setupUI() {
        
        searchBar = UISearchBar()
        searchBar.searchBarStyle = .minimal
        searchBar.sizeToFit()
        searchBar.delegate = self
        searchBar.placeholder = "Tìm kiếm"
        navigationItem.titleView = searchBar
        
        addItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(doAddItem))
        navigationItem.rightBarButtonItem = addItem
        
        tableView.register(UINib(nibName: reuseCellIdentifier, bundle: nil), forCellReuseIdentifier: reuseCellIdentifier)
        tableView.tableFooterView = UIView()
        refreshControl = initRefreshControl()
        loadMoreIndicator = initLoadMoreIndicator()
        if #available(iOS 10, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 500
    }
    
    @objc func doAddItem() {
        guard let themSanPhamViewController = ThemSanPhamViewController.viewController(.setting) as? ThemSanPhamViewController else {return}
        themSanPhamViewController.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(themSanPhamViewController, animated: true)
    }
    
    func loadData(refresh: Bool = false, next: Bool = false) {
        if !refresh && !next {
            showProgress()
        } else if !refresh && next {
            tableView.tableFooterView = loadMoreIndicator
        }
        
        if UserModel.shared.type != nil && UserModel.shared.type! == .gian_hang {
            HCService.getProductBooth(booth_id: UserModel.shared.id, category_id: nil, offset: offset, completion: { [weak self](objects) in
                if let strongSelf = self {
                    strongSelf.tableView.tableFooterView = UIView()
                    strongSelf.refreshControl.endRefreshing()
                    strongSelf.dismissProgress()
                    if strongSelf.offset == 0 {
                        strongSelf.products = objects
                    } else {
                        strongSelf.products.append(contentsOf: objects)
                    }
                    if objects.count == Constant.limit {
                        strongSelf.can_load_more = true
                        strongSelf.offset += Constant.limit
                    } else {
                        strongSelf.can_load_more = false
                    }
                }
            }) { (error) in
                self.showError(error)
                self.tableView.tableFooterView = UIView()
                self.refreshControl.endRefreshing()
            }
        } else {
            HCService.searchProducts(text: text, offset: offset, completion: { [weak self](objects) in
                
                if let strongSelf = self {
                    strongSelf.tableView.tableFooterView = UIView()
                    strongSelf.refreshControl.endRefreshing()
                    strongSelf.dismissProgress()
                    if strongSelf.offset == 0 {
                        strongSelf.products = objects
                    } else {
                        strongSelf.products.append(contentsOf: objects)
                    }
                    if objects.count == Constant.limit {
                        strongSelf.can_load_more = true
                        strongSelf.offset += Constant.limit
                    } else {
                        strongSelf.can_load_more = false
                    }
                }
            }) { (error) in
                self.showError(error)
                self.tableView.tableFooterView = UIView()
                self.refreshControl.endRefreshing()
            }
        }
        
    }
    
    override func doRefresh() {
        offset = 0
        loadData(refresh: true)
    }

}

extension QuanLySanPhamViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return products.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseCellIdentifier, for: indexPath) as! ProductTableViewCell
        
        let item = products[indexPath.row]
        
        cell.productImageView.sd_setImage(with: URL(string: item.image))
        cell.productNameLabel.text = item.name
        cell.productCodeLabel.text = item.code
        cell.productPriceLabel.text = item.price.priceString()
        
        if can_load_more && products.count == indexPath.row + 1 {
            loadData(next: true)
        }
        
        return cell
    }
    
}

extension QuanLySanPhamViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let chiTietSanPhamViewController = ChiTietSanPhamViewController.viewController(.home) as? ChiTietSanPhamViewController else {return}
        chiTietSanPhamViewController.hidesBottomBarWhenPushed = true
        chiTietSanPhamViewController.product_id = products[indexPath.row].id
        navigationController?.pushViewController(chiTietSanPhamViewController, animated: true)
    }
}

extension QuanLySanPhamViewController: UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = true
        navigationItem.rightBarButtonItem = nil
        navigationItem.leftBarButtonItem = nil
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        self.text = ""
        navigationItem.rightBarButtonItem = addItem
        self.dismiss(animated: false, completion: nil)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.text = searchBar.text ?? ""
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.text = searchBar.text ?? ""
    }
}
