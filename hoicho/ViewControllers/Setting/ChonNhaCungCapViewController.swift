//
//  ChonNhaCungCapViewController.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 5/14/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

class ChonNhaCungCapViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var datasource = [GianHangModel]() {
        didSet {
            tableView?.reloadData()
        }
    }
    var completion: ((GianHangModel) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        loadData()
    }

    func setupUI() {
        title = "Chọn nhà cung cấp"
        tableView.tableFooterView = UIView()
    }
    
    func loadData() {
        showProgress()
        
        HCService.getBooths(offset: 0, limit: 10000, completion: { (objects) in
            self.dismissProgress()
            self.datasource = objects
        }) { (error) in
            self.showError(error)
        }
    }

}

extension ChonNhaCungCapViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datasource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "ChonNhaCungCapCell")
        if cell == nil {
            cell = UITableViewCell.init(style: .default, reuseIdentifier: "ChonNhaCungCapCell")
        }
        
        cell?.textLabel?.text = datasource[indexPath.row].name
        
        return cell!
    }
}

extension ChonNhaCungCapViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        dismiss(animated: true) {
            self.completion?(self.datasource[indexPath.row])
        }
    }
}
