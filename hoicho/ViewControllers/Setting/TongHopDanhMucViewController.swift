//
//  TongHopDanhMucViewController.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 5/14/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

private let reuseCellIdentifier = "TongHopDanhMucTableViewCell"

class TongHopDanhMucViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var loadMoreIndicator: UIActivityIndicatorView!
    let refreshControl = UIRefreshControl()
    
    var type: TongHopType = .tintuc
    
    var offset = 0
    var canLoadMore = true
    var datasource = [DanhMucModel]() {
        didSet {
            tableView?.reloadData()
        }
    }
    var completion: ((DanhMucModel) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        loadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupUI() {
        title = "Danh mục"
        tableView.tableFooterView = UIView()
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 500
        
        loadMoreIndicator = initLoadMoreIndicator()
        refreshControl.addTarget(self, action: #selector(doRefresh), for: .valueChanged)
        tableView.backgroundView = refreshControl
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(doAddDanhMuc))
    }
    
    @objc func doAddDanhMuc() {
        guard let themDanhMucViewController = TongHopThemDanhMucViewController.viewController(.setting) as? TongHopThemDanhMucViewController else {return}
        themDanhMucViewController.type = self.type
        themDanhMucViewController.completion = {
            self.doRefresh()
        }
        self.present(HCNavigationController(rootViewController: themDanhMucViewController), animated: true, completion: nil)
    }
    
    func loadData(loadNext: Bool = false) {
        if !loadNext && !refreshControl.isRefreshing {
            showProgress()
        } else if loadNext {
            tableView.tableFooterView = loadMoreIndicator
        }
        HCService.getDanhMucTongHop(type: type, offset: offset, completion: { (objects) in
            self.dismissProgress()
            self.tableView.tableFooterView = UIView()
            self.refreshControl.endRefreshing()
            if self.offset == 0 {
                self.datasource = objects
            } else {
                self.datasource.append(contentsOf: objects)
            }
            
            if objects.count == Constant.limit {
                self.canLoadMore = true
                self.offset += Constant.limit
            } else {
                self.canLoadMore = false
            }
            
        }) { (error) in
            self.tableView.tableFooterView = UIView()
            self.refreshControl.endRefreshing()
            self.showError(error)
        }
    }
    
    override func doRefresh() {
        offset = 0
        loadData()
    }
    
    @objc func doDeleteButton(_ sender: UIButton) {
        let alert = HCAlertController.init(title: nil, message: "Bạn có chắc chắn muốn xoá danh mục này?", completion: { (_, index) in
            
            if index == 1 {
                self.showProgress()
                HCService.xoaDanhMuc(type: self.type, id: self.datasource[sender.tag].id, completion: {
                    self.dismissProgress()
                    self.showAlertWithMessage("Bạn đã xoá thành công danh mục", completion: {
                        self.datasource.remove(at: sender.tag)
                        self.tableView.reloadData()
                    })
                }, failure: { (error) in
                    self.showError(error)
                })
            }
            
        }, cancelButtonTitle: "Huỷ", otherButtonTitle: "Xoá")
        alert.show(viewController: self)
    }
    
    @objc func doEditButton(_ sender: UIButton) {
        guard let themDanhMucViewController = TongHopThemDanhMucViewController.viewController(.setting) as? TongHopThemDanhMucViewController else {return}
        themDanhMucViewController.type = self.type
        themDanhMucViewController.datasource = datasource[sender.tag]
        themDanhMucViewController.completion = {
            self.doRefresh()
        }
        self.present(HCNavigationController(rootViewController: themDanhMucViewController), animated: true, completion: nil)
    }
    
}

extension TongHopDanhMucViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datasource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseCellIdentifier, for: indexPath) as! TongHopDanhMucTableViewCell
        
        let item = datasource[indexPath.row]
        cell.titleLabel.text = item.name
        
        cell.deleteButton.tag = indexPath.row
        cell.editButton.tag = indexPath.row
        
        cell.deleteButton.addTarget(self, action: #selector(doDeleteButton(_:)), for: .touchUpInside)
        cell.editButton.addTarget(self, action: #selector(doEditButton(_:)), for: .touchUpInside)
        
        cell.deleteButton.isHidden = true
        cell.editButton.isHidden = true
        
        if canLoadMore && datasource.count - 1 == indexPath.row {
            loadData(loadNext: true)
        }
        return cell
    }
}

extension TongHopDanhMucViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.completion?(datasource[indexPath.row])
        self.dismiss(animated: true, completion: nil)
    }
}
