//
//  DangKyGianHangViewController.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 6/11/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

class DangKyGianHangViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
    }

    func setupUI() {
        title = "Đăng ký gian hàng"
    }
}
