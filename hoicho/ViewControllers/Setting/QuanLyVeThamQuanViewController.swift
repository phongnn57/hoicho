//
//  QuanLyVeThamQuanViewController.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 5/14/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit
import QRCode

class QuanLyVeThamQuanViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var refreshControl: UIRefreshControl!
    var loadMoreIndicator: UIActivityIndicatorView!
    
    var searchItem: UIBarButtonItem!
    var cancelItem: UIBarButtonItem!
    var searchBar: UISearchBar!
    
    var offset = 0
    var can_load_more = true
    var datasource = [TicketModel]() {
        didSet {
            tableView?.reloadData()
        }
    }
    
    var name = "" {
        didSet {
            doRefresh()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        loadData()
    }
    
    func setupUI() {
        title = "Quản lý vé tham quan"
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 1000
        tableView.tableFooterView = UIView()
        
        refreshControl = initRefreshControl()
        if #available(iOS 10, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        
       
        searchItem = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(doSearchItem))
        navigationItem.rightBarButtonItem = searchItem
        searchBar = UISearchBar()
        searchBar.placeholder = "Tìm theo tên"
        searchBar.sizeToFit()
        searchBar.searchBarStyle = .minimal
        searchBar.delegate = self
        cancelItem = UIBarButtonItem(title: "Huỷ", style: .plain, target: self, action: #selector(doCancelButtonItem))
    }
    
    @objc func doCancelButtonItem() {
        searchBar.text = nil
        searchBar.resignFirstResponder()
        navigationItem.titleView = nil
        navigationItem.rightBarButtonItem = searchItem
    }
    
    @objc func doSearchItem() {
        navigationItem.titleView = searchBar
        navigationItem.rightBarButtonItem = cancelItem
        searchBar.becomeFirstResponder()
    }
    
    func loadData(refresh: Bool = false, next: Bool = false) {
        if !refresh && !next {
            showProgress()
        } else if !refresh && next {
            tableView.tableFooterView = loadMoreIndicator
        }
        HCService.getTickets(offset: offset, name: self.name, completion: { [weak self](objects) in
            if let strongSelf = self {
                strongSelf.dismissProgress()
                strongSelf.refreshControl.endRefreshing()
                strongSelf.tableView.tableFooterView = UIView()
                if strongSelf.offset == 0 {
                    strongSelf.datasource = objects
                } else {
                    strongSelf.datasource.append(contentsOf: objects)
                }
                if objects.count == Constant.limit {
                    strongSelf.can_load_more = true
                    strongSelf.offset += Constant.limit
                } else {
                    strongSelf.can_load_more = false
                }
            }
        }) { (error) in
            self.showError(error)
            self.refreshControl.endRefreshing()
            self.tableView.tableFooterView = UIView()
        }
        
    }
    
    override func doRefresh() {
        offset = 0
        can_load_more = true
        loadData(refresh: true)
    }
    
    @objc func doCallButton(_ sender: UIButton) {
        doCall(datasource[sender.tag].phone)
    }
    
    @objc func doSaveButton(_ sender: UIButton) {
        
        
        let saveImage = UIImage(view: createQRCodeView(name: datasource[sender.tag].name, qrcode: datasource[sender.tag].code))
        UIImageWriteToSavedPhotosAlbum(saveImage, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
        

    }
    
    func createQRCodeView(name: String, qrcode: String) -> UIView {
        let viewWidth = UIScreen.main.bounds.width
        let viewHeight: CGFloat = 380
        
        let aView = UIView(frame: CGRect(x: 0, y: 0, width: viewWidth, height: viewHeight))
        
        let dash1 = UIView(frame: CGRect(x: 20, y: 20, width: viewWidth - 40, height: 1))
        let dash2 = UIView(frame: CGRect(x: 20, y: viewHeight - 21, width: viewWidth - 40, height: 1))
        let dash3 = UIView(frame: CGRect(x: 20, y: 21, width: 1, height: viewHeight - 42))
        let dash4 = UIView(frame: CGRect(x: viewWidth - 21, y: 21, width: 1, height: viewHeight - 42))
        
        dash1.addDashedLine()
        dash2.addDashedLine()
        dash3.addDashedLine(vertical: true)
        dash4.addDashedLine(vertical: true)
        
        aView.addSubview(dash1)
        aView.addSubview(dash2)
        aView.addSubview(dash3)
        aView.addSubview(dash4)
        
        let qrcodeImageView = UIImageView(frame: CGRect(x: (viewWidth - 240)/2, y: 40, width: 240, height: 240))
        qrcodeImageView.contentMode = .scaleAspectFit
        qrcodeImageView.clipsToBounds = true
        if var _qrcode = QRCode(qrcode) {
            _qrcode.size = CGSize(width: 240, height: 240)
            qrcodeImageView.image = _qrcode.image
        }
        aView.addSubview(qrcodeImageView)
        
        let nameLabel = UILabel(frame: CGRect(x: 40, y: 300, width: viewWidth - 80, height: 40))
        nameLabel.textAlignment = .center
        nameLabel.textColor = UIColor.HCColor.appColor
        nameLabel.numberOfLines = 0
        nameLabel.text = name
        nameLabel.font = UIFont.systemFont(ofSize: 14, weight: .semibold)
        aView.addSubview(nameLabel)
        
        return aView
    }
    
    @objc func doNameButton(_ sender: UIButton) {
        guard let thongTinCaNhanViewController = ThongTinCaNhanViewController.viewController(.setting) as? ThongTinCaNhanViewController else {return}
        thongTinCaNhanViewController.hidesBottomBarWhenPushed = true
        thongTinCaNhanViewController.user_id = datasource[sender.tag].account_id
        navigationController?.pushViewController(thongTinCaNhanViewController, animated: true)
    }
    
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let _ = error {
            self.showAlertWithMessage("Không lưu được ảnh. Hãy đảm bảo bạn đã cấp quyền cho ứng dụng lưu ảnh vào điện thoại của bạn")
        } else {
            self.showAlertWithMessage("Lưu ảnh thành công")
        }
    }

}

extension QuanLyVeThamQuanViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datasource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "QuanLyVeThamQuanTableViewCell", for: indexPath) as! QuanLyVeThamQuanTableViewCell
        
        let item = datasource[indexPath.row]
        
        if var qrcode = QRCode(item.code) {
            
            qrcode.size = CGSize(width: 100, height: 100)
            cell.codeImageView.image = qrcode.image
            cell.codeImageView.isHidden = false
            cell.saveButton.isHidden = false
        } else {
            cell.codeImageView.isHidden = true
            cell.saveButton.isHidden = true
        }
        
        cell.nameButton.setTitle(item.name, for: .normal)
        cell.phoneButton.setTitle(item.phone, for: .normal)
        cell.regionLabel.text = "Khu vực: \(item.city)"
        cell.timeLabel.text = "Ngày quét: \(item.time?.toString() ?? "")"
  
        cell.phoneButton.tag = indexPath.row
        cell.saveButton.tag = indexPath.row
        cell.nameButton.tag = indexPath.row
        cell.phoneButton.addTarget(self, action: #selector(doCallButton(_:)), for: .touchUpInside)
        cell.saveButton.addTarget(self, action: #selector(doSaveButton(_:)), for: .touchUpInside)
        cell.nameButton.addTarget(self, action: #selector(doNameButton(_:)), for: .touchUpInside)
        if can_load_more && datasource.count - 1 == indexPath.row {
            loadData(next: true)
        }
        
        return cell
    }
}

extension QuanLyVeThamQuanViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        name = searchText
    }
}
