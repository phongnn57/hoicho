//
//  ChonThuongHieuViewController.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 5/14/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

class ChonThuongHieuViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var datasource = [BrandModel]() {
        didSet {
            tableView?.reloadData()
        }
    }
    var completion: ((BrandModel) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        loadData()
    }
    
    func setupUI() {
        title = "Chọn thương hiệu"
        tableView.tableFooterView = UIView()
    }
    
    func loadData() {
        showProgress()
        
        HCService.getBrands(completion: { [weak self](objects) in
            self?.dismissProgress()
            self?.datasource = objects
        }) { (error) in
            self.showError(error)
        }
    }
    
}

extension ChonThuongHieuViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datasource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "ChonThuongHieuCell")
        if cell == nil {
            cell = UITableViewCell.init(style: .default, reuseIdentifier: "ChonThuongHieuCell")
        }
        
        cell?.textLabel?.text = datasource[indexPath.row].name
        
        return cell!
    }
}

extension ChonThuongHieuViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        dismiss(animated: true) {
            self.completion?(self.datasource[indexPath.row])
        }
    }
}
