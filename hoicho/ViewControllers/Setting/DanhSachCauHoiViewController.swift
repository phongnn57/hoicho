//
//  DanhSachCauHoiViewController.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 6/11/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

class DanhSachCauHoiViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var survey: SurveyModel! {
        didSet {
            tableView?.reloadData()
        }
    }
    
    var shouldShowQuestionAtIndex: ((Int) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
    }

    func setupUI() {
        
        if survey != nil {
            title = survey.name
        } else {
            title = "Kết quả khảo sát"
        }
        tableView.tableFooterView = UIView()
    }

}

extension DanhSachCauHoiViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if survey == nil {return 0}
        return survey.questions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "DanhSachCauHoiTableViewCell")
        if cell == nil {
            cell = UITableViewCell(style: .default, reuseIdentifier: "DanhSachCauHoiTableViewCell")
        }
        
        let item = survey.questions[indexPath.row]
        
        if let answer = (item.answers.filter({ (answer) -> Bool in
            return answer.selected == true
        })).first {
            if answer.type == 2 && answer.answer_content.isEmpty {
                cell?.accessoryType = .none
                cell?.textLabel?.textColor = UIColor(hex: "#333333")
            } else {
                cell?.accessoryType = .checkmark
                cell?.textLabel?.textColor = UIColor.HCColor.appColor
            }
        } else {
            cell?.accessoryType = .none
            cell?.textLabel?.textColor = UIColor(hex: "#333333")
        }
        
        cell?.textLabel?.text = "Câu \(indexPath.row + 1)"
        
        cell?.tintColor = UIColor.HCColor.appColor
        
        return cell!
    }
}

extension DanhSachCauHoiViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        dismiss(animated: true) {
            self.shouldShowQuestionAtIndex?(indexPath.row)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
}
