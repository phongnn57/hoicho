//
//  DanhSachQuanTriVienViewController.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 6/12/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

class DanhSachQuanTriVienViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var refreshControl: UIRefreshControl!
    var loadMoreIndicator: UIActivityIndicatorView!
    
    var datasource = [UserModel]() {
        didSet {
            tableView?.reloadData()
        }
    }
    var offset = 0
    var can_load_more = true
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        loadData()
    }

    func setupUI() {
        title = "Quản trị viên"
        
        tableView.tableFooterView = UIView()
        
        refreshControl = initRefreshControl()
        loadMoreIndicator = initLoadMoreIndicator()
        if #available(iOS 10, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        
        let addItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(doAddItem))
        navigationItem.rightBarButtonItem = addItem
    }
    
    @objc func doAddItem() {
        guard let quyenQuanTriVienViewController = QuyenQuanTriVienViewController.viewController(.setting) as? QuyenQuanTriVienViewController else {return}
        quyenQuanTriVienViewController.completion = {
            self.doRefresh()
        }
        self.present(HCNavigationController(rootViewController: quyenQuanTriVienViewController), animated: true, completion: nil)
    }

    func loadData(refresh: Bool = false, next: Bool = false) {
        if !refresh && !next {
            showProgress()
        } else if !refresh && next {
            tableView.tableFooterView = loadMoreIndicator
        }
        
        HCService.getAllAdministrator(offset: offset, completion: { [weak self](objects) in
            if let strongSelf = self {
                if strongSelf.offset == 0 {
                    strongSelf.datasource = objects
                } else {
                    strongSelf.datasource.append(contentsOf: objects)
                }
                
                if objects.count == Constant.limit {
                    strongSelf.can_load_more = true
                    strongSelf.offset += Constant.limit
                } else {
                    strongSelf.can_load_more = false
                }
                
                strongSelf.tableView.tableFooterView = UIView()
                strongSelf.refreshControl.endRefreshing()
                strongSelf.dismissProgress()
            }
        }) { (error) in
            self.tableView.tableFooterView = UIView()
            self.refreshControl.endRefreshing()
            self.showError(error)
        }
        
    }
    
    override func doRefresh() {
        offset = 0
        loadData(refresh: true)
    }
    
    @objc func doCallButton(_ sender: UIButton) {
        doCall(datasource[sender.tag].phone)
    }
    
    @objc func doMoreButton(_ sender: UIButton) {
        let actionSheetController = UIAlertController(title: "Xử lý", message: nil, preferredStyle: .actionSheet)
        
        actionSheetController.addAction(UIAlertAction(title: "Sửa", style: .default, handler: { (action: UIAlertAction) in
            self.editAdmin(sender.tag)
        }))
        actionSheetController.addAction(UIAlertAction(title: "Xoá", style: .default, handler: { (action: UIAlertAction) in
            self.showAlertTwoButtonWithMessage("Bạn có chắc muốn xoá quản trị viên này?", completion: {
                self.deleteAdmin(sender.tag)
            })
            
        }))
        actionSheetController.addAction(UIAlertAction(title: "Huỷ", style: .destructive, handler: nil))
        
        
        if let popoverController = actionSheetController.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.height, width: 0, height: 0)
        }
        
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    func deleteAdmin(_ index: Int) {
        showProgress()
        
        HCService.deleteAdmin(id: self.datasource[index].id, completion: {
            self.dismissProgress()
            self.datasource.remove(at: index)
            self.tableView.reloadData()
        }) { (error) in
            self.showError(error)
        }
    }
    
    func editAdmin(_ index: Int) {
        guard let quyenQuanTriVienViewController = QuyenQuanTriVienViewController.viewController(.setting) as? QuyenQuanTriVienViewController else {return}
        quyenQuanTriVienViewController.admin = self.datasource[index]
        quyenQuanTriVienViewController.completion = {
            self.doRefresh()
        }
        self.present(HCNavigationController(rootViewController: quyenQuanTriVienViewController), animated: true, completion: nil)
    }

}

extension DanhSachQuanTriVienViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datasource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DanhSachQuanTriVienTableViewCell", for: indexPath) as! DanhSachQuanTriVienTableViewCell
        
        let item = datasource[indexPath.row]
        
        cell.avatarImageView.sd_setImage(with: URL(string: item.image), placeholderImage: #imageLiteral(resourceName: "icon_default_avatar"))
        cell.nameLabel.text = item.name
        cell.phoneLabel.setTitle(item.phone, for: .normal)
        
        cell.phoneLabel.tag = indexPath.row
        cell.phoneLabel.addTarget(self, action: #selector(doCallButton(_:)), for: .touchUpInside)
        
        cell.moreButton.tag = indexPath.row
        cell.moreButton.addTarget(self, action: #selector(doMoreButton(_:)), for: .touchUpInside)
        
        if can_load_more && datasource.count - 1 == indexPath.row {
            loadData(next: true)
        }
        
        return cell
    }
}

extension DanhSachQuanTriVienViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
}
