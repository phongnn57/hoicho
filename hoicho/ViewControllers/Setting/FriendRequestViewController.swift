//
//  FriendRequestViewController.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 6/9/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

class FriendRequestViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var refreshControl: UIRefreshControl!
    var loadMoreIndicator: UIActivityIndicatorView!
    
    var datasource = [UserModel]() {
        didSet {
            tableView?.reloadData()
        }
    }
    var offset = 0
    var can_load_more = true
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        loadData()
    }
    
    func setupUI() {
        title = "Danh sách yêu cầu kết bạn"
        
        tableView.tableFooterView = UIView()
        refreshControl = initRefreshControl()
        if #available(iOS 10, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        loadMoreIndicator = initLoadMoreIndicator()
    }
    
    func loadData(refresh: Bool = false, next: Bool = false) {
        if !refresh && !next {
            showProgress()
        } else if !refresh && next {
            tableView.tableFooterView = UIView()
        }
        
        HCService.getFriendRequest(offset: offset, completion: { [weak self](objects) in
            if let strongSelf = self {
                strongSelf.dismissProgress()
                strongSelf.tableView.tableFooterView = UIView()
                strongSelf.refreshControl.endRefreshing()
                
                
                if strongSelf.offset == 0 {
                    strongSelf.datasource = objects
                } else {
                    strongSelf.datasource.append(contentsOf: objects)
                }
                
                if objects.count == Constant.limit {
                    strongSelf.can_load_more = true
                    strongSelf.offset += Constant.limit
                } else {
                    strongSelf.can_load_more = false
                }
            }
        }) { (error) in
            self.refreshControl.endRefreshing()
            self.tableView.tableFooterView = UIView()
            self.showError(error)
        }
    }
    
    override func doRefresh() {
        offset = 0
        loadData(refresh: true)
    }
    
    @objc func doAcceptButton(_ sender: UIButton) {
        self.acceptFriend(accept: true, index: sender.tag)
    }
    
    @objc func doRejectButton(_ sender: UIButton) {
        self.acceptFriend(accept: false, index: sender.tag)
    }
    
    func acceptFriend(accept: Bool, index: Int) {
        showProgress()
        HCService.responseFriendRequest(id: self.datasource[index].id, accept: accept, completion: {
            self.dismissProgress()
            DispatchQueue.main.async {
                self.datasource.remove(at: index)
                self.tableView.reloadData()
            }
        }) { (error) in
            self.showError(error)
        }
    }


}

extension FriendRequestViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datasource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FriendRequestTableViewCell", for: indexPath) as! FriendRequestTableViewCell
        
        let item = datasource[indexPath.row]
        
        cell.accountImageView.sd_setImage(with: URL(string: item.image), placeholderImage: #imageLiteral(resourceName: "icon_default_avatar"))
        cell.nameLabel.text = item.name
        cell.acceptButton.tag = indexPath.row
        cell.rejectButton.tag = indexPath.row
        
        cell.acceptButton.addTarget(self, action: #selector(doAcceptButton(_:)), for: .touchUpInside)
        cell.rejectButton.addTarget(self, action: #selector(doRejectButton(_:)), for: .touchUpInside)
        
        
        if can_load_more && datasource.count - 1 == indexPath.row {
            loadData(next: true)
        }
        
        return cell
    }
}

extension FriendRequestViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let thongTinCaNhanViewController = ThongTinCaNhanViewController.viewController(.setting) as? ThongTinCaNhanViewController else {return}
        thongTinCaNhanViewController.hidesBottomBarWhenPushed = true
        thongTinCaNhanViewController.user_id = datasource[indexPath.row].id
        navigationController?.pushViewController(thongTinCaNhanViewController, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
}
