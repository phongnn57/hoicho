//
//  BangKhaoSatViewController.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 6/11/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit
import CarbonKit

class BangKhaoSatViewController: UIViewController {

    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var prevButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var listButton: UIButton!
    
    lazy var questionViewController: CauHoiViewController = {
        let cauHoiViewController = CauHoiViewController.viewController(.setting) as! CauHoiViewController
        return cauHoiViewController
    }()
    
    var carbonTabSwipeNavigation: CarbonTabSwipeNavigation!
    
    var survey: SurveyModel! {
        didSet {
            fillData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        loadData()
    }

    func setupUI() {
        title = "Bảng khảo sát"
        
        
        fillData()
        
    }
    
    func fillData() {
        if survey == nil {
            contentView.isHidden = true
            bottomView.isHidden = true
            navigationItem.rightBarButtonItem = nil
            return
        }
        
        //
        contentView.isHidden = false
        bottomView.isHidden = false
        
        let items = Array(repeating: "", count: survey.questions.count)
        carbonTabSwipeNavigation = CarbonTabSwipeNavigation(items: items, delegate: self)
        carbonTabSwipeNavigation.toolbarHeight.constant = CGFloat.leastNormalMagnitude
        carbonTabSwipeNavigation.insert(intoRootViewController: self, andTargetView: self.contentView)
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Nộp", style: .plain, target: self, action: #selector(doCompleteItem))
    }
    
    func isSurveyComplete() -> Bool {
        if survey == nil {return false}
        
        var isComplete = true
        for question in survey.questions {
            if let answer = (question.answers.filter { (answer) -> Bool in
                return answer.selected == true
                }).first {
                if answer.type == 2 && answer.answer_content.isEmpty {
                    isComplete = false
                    break
                }
            } else {
                isComplete = false
                break
            }
        }
        return isComplete
    }
    
    @objc func doCompleteItem() {
        if !isSurveyComplete() {
            showAlertWithMessage("Bạn chưa trả lời hết tất cả câu hỏi trong bảng khảo sát. Hãy vui lòng kiểm tra lại")
            return
        }
        
        showProgress()
        HCService.finishSurvey(survey: survey, completion: {
            self.dismissProgress()
            self.showAlertWithMessage("Cảm ơn bạn đã làm bài khảo sát", completion: {
                self.navigationController?.popViewController(animated: true)
            })
        }) { (error) in
            self.showError(error)
        }
        
    }
    
    func loadData() {
        showProgress()
        HCService.getSurvey(completion: { [weak self](object) in
            if let strongSelf = self {
                strongSelf.survey = object
                strongSelf.dismissProgress()
            }
        }) { (error) in
            self.showError(error)
        }
    }
    
    @IBAction func doPrevButton(_ sender: Any) {
        if carbonTabSwipeNavigation.currentTabIndex > 0 {
            carbonTabSwipeNavigation.setCurrentTabIndex(carbonTabSwipeNavigation.currentTabIndex - 1, withAnimation: true)
        }
    }
    
    @IBAction func doListButton(_ sender: Any) {
        guard let danhSachCauHoiViewController = DanhSachCauHoiViewController.viewController(.setting) as? DanhSachCauHoiViewController else {return}
        danhSachCauHoiViewController.survey = survey
        danhSachCauHoiViewController.shouldShowQuestionAtIndex = {[weak self](index) in
            if let strongSelf = self {
                strongSelf.carbonTabSwipeNavigation.setCurrentTabIndex(UInt(index), withAnimation: true)
            }
        }
        self.present(HCNavigationController(rootViewController: danhSachCauHoiViewController), animated: true, completion: nil)
    }
    
    @IBAction func doNextButton(_ sender: Any) {
        if carbonTabSwipeNavigation.currentTabIndex < survey.questions.count - 1 {
            carbonTabSwipeNavigation.setCurrentTabIndex(carbonTabSwipeNavigation.currentTabIndex + 1, withAnimation: true)
        }
    }
    

}

extension BangKhaoSatViewController: CarbonTabSwipeNavigationDelegate {
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
        let cauHoiViewController = CauHoiViewController.viewController(.setting) as! CauHoiViewController
        cauHoiViewController.question = survey.questions[Int(index)]
        cauHoiViewController.index = Int(index)
        cauHoiViewController.completion = {[weak self] (object) in
            if let strongSelf = self {
                strongSelf.survey.questions[Int(index)] = object
            }
        }
        return cauHoiViewController
    }
    
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, didMoveAt index: UInt) {
        if index == 0 {
            prevButton.isEnabled = false
            nextButton.isEnabled = true
        } else if index == survey.questions.count - 1 {
            prevButton.isEnabled = true
            nextButton.isEnabled = false
        } else {
            prevButton.isEnabled = true
            nextButton.isEnabled = true
        }
    }
}
