//
//  CapNhatSoDoHoiChoViewController.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 6/18/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

class CapNhatSoDoHoiChoViewController: UIViewController {
    
    @IBOutlet weak var qrcodeImageView: UIImageView!
    @IBOutlet weak var bgImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    
    @IBOutlet weak var countLabel: UITextField!
    @IBOutlet weak var mapImageView: UIImageView!
    
    
    var expo: ExpoModel!
    var image: UIImage? {
        didSet {
            mapImageView.image = image
        }
    }
    var completion: (() -> Void)?
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
    }
    
    func setupUI() {
        title = "Cập nhật sơ đồ"
        
        if expo != nil {
            bgImageView.sd_setImage(with: URL(string: expo.image))
            nameLabel.text = expo.name.uppercased()
            timeLabel.text = "Thời gian: Từ \(expo.start_time?.toString(format: "HH:mm dd/MM/yyyy") ?? "...") đến \(expo.end_time?.toString(format: "HH:mm dd/MM/yyyy") ?? "...")"
            addressLabel.text = "Địa chỉ: \(expo.address)"
            qrcodeImageView.sd_setImage(with: URL(string: expo.qrcode))
            
            countLabel.text = "\(expo.count)"
            mapImageView.sd_setImage(with: URL(string: expo.map))
        }
        
    }

    @IBAction func doSelectMapImage(_ sender: Any) {
        doSelectPhoto()
    }
    
    @IBAction func doSaveButton(_ sender: Any) {
        self.view.endEditing(true)
        
        showProgress()
        HCService.setupMapExpo(id: expo.id, count: Int(countLabel.text ?? "0") ?? 0, image: image, completion: {
            self.dismissProgress()
            self.showAlertWithMessage("Thành công")
            self.completion?()
        }) { (error) in
            self.showError(error)
        }
    }
    
    override func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        picker.dismiss(animated: true) {
            if let _image = info[UIImagePickerControllerOriginalImage] as? UIImage {
                self.image = _image
            }
        }
    }
    
}
