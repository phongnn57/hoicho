//
//  TongHopThemDanhMucViewController.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 5/14/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit


class TongHopThemDanhMucViewController: UIViewController {
    
    @IBOutlet weak var tenDanhMucTextField: UITextField!
    @IBOutlet weak var tieuDeDanhMucTextField: UITextField!
    @IBOutlet weak var keywordTextView: GrowingTextView!
    @IBOutlet weak var descriptionTextView: GrowingTextView!
    @IBOutlet weak var motaTextView: GrowingTextView!
    @IBOutlet weak var stackView: UIStackView!
    
    var type: TongHopType = .tintuc
    var datasource: DanhMucModel!
    var completion: (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupUI() {
        if datasource != nil {
            title = "Cập nhật danh mục"
            
            tenDanhMucTextField.text = datasource.name
            tieuDeDanhMucTextField.text = datasource.title
            keywordTextView.text = datasource.meta_keyword
            descriptionTextView.text = datasource.meta_description
            motaTextView.text = datasource.desc
        } else {
            title = "Thêm mới danh mục"
        }
        
        if type == .hoidap {
            stackView.isHidden = true
        } else {
            stackView.isHidden = false
        }
        
    }
    
    @IBAction func doSaveButton(_ sender: Any) {
        self.view.endEditing(true)
        
        if Utilities.isNilOrEmpty(tenDanhMucTextField.text) {
            showAlertWithMessage("Hãy nhập tên danh mục")
            return
        }
        
        showProgress()
        if datasource == nil {
            HCService.taoDanhMuc(id: datasource?.id, type: type, name: tenDanhMucTextField.text!, title: tieuDeDanhMucTextField.text ?? "", desc: descriptionTextView.text ?? "", meta_keyword: keywordTextView.text ?? "", meta_description: motaTextView.text ?? "", completion: {
                self.dismissProgress()
                self.completion?()
                self.showAlertWithMessage("Thành công", completion: {
                    self.dismiss(animated: true, completion: nil)
                })
                
            }) { (error) in
                self.showError(error)
            }
        }
        
    }
    
    
    
}

