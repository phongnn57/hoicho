//
//  BrandViewController.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 4/22/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

class BrandViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var refreshControl: UIRefreshControl!
    
    var datasource = [BrandModel]() {
        didSet {
            tableView?.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        loadData()
        
    }

    func setupUI() {
        title = "Quản lý thương hiệu"
        tableView.tableFooterView = UIView()
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 1000
        
        refreshControl = initRefreshControl()
        if #available(iOS 10, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
    }
    
    func loadData(refresh: Bool = false) {
        if !refresh {
            showProgress()
        }
        HCService.getBrands(completion: { [weak self](objects) in
            self?.dismissProgress()
            self?.refreshControl.endRefreshing()
            self?.datasource = objects
        }) { (error) in
            self.showError(error)
        }
    }
    
    override func doRefresh() {
        loadData(refresh: true)
    }
    
    @objc func highlightBrand(_ sender: UISwitch) {
        let item = datasource[sender.tag]
        showProgress()
        HCService.updateBrand(id: item.id, feature: !item.featured, completion: {
            self.dismissProgress()
            self.datasource[sender.tag].featured = !self.datasource[sender.tag].featured
            self.tableView.reloadData()
        }) { (error) in
            self.showError(error)
        }
    }

}

extension BrandViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datasource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BrandTableViewCell", for: indexPath) as! BrandTableViewCell
        
        let item = datasource[indexPath.row]
        
        cell.titleLabel.text = item.name
        cell.iconImageView.sd_setImage(with: URL(string: item.logo), placeholderImage: #imageLiteral(resourceName: "icon_no_image"))
        cell.highlightSwitch.tag = indexPath.row
        cell.highlightSwitch.addTarget(self, action: #selector(highlightBrand(_:)), for: .valueChanged)
        cell.highlightSwitch.isOn = item.featured
        
        return cell
    }
}

extension BrandViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let danhSachSanPhamViewController = DanhSachSanPhamViewController.viewController(.main) as? DanhSachSanPhamViewController else {return}
        danhSachSanPhamViewController.hidesBottomBarWhenPushed = true
        danhSachSanPhamViewController.brand = datasource[indexPath.row]
        navigationController?.pushViewController(danhSachSanPhamViewController, animated: true)
    }
}
