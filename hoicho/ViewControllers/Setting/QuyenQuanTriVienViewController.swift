//
//  QuyenQuanTriVienViewController.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 6/12/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

private let reuseCellIdentifier = "MenuConfigViewCell"
private let reuseHeaderIdentifier = "MenuConfigHeaderView"

class QuyenQuanTriVienViewController: UIViewController {

    @IBOutlet weak var roleLabel: UILabel!
    @IBOutlet weak var contentViewHeight: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var saveButton: UIButton!
    
    var datasource = [PermissionModel]() {
        didSet {
            if tableView != nil {
                tableView.reloadData()
            }
            
            self.recalculateContentHeight()
            
        }
    }
    
    var staff: UserModel! {
        didSet {
            if staff != nil {
                nameTextField?.text = staff.name
                phoneTextField?.text = staff.phone
            }
        }
    }
    
    var completion: (() -> Void)?
    var isEditPermission = false
    var activePermissions = [String]()
    var admin: UserModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        loadData()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupUI() {
        if admin != nil {
            title = "Sửa quản trị viên"
            saveButton.setTitle("LƯU THAY ĐỔI", for: .normal)
        } else {
            title = "Thêm quản trị viên"
            saveButton.setTitle("THÊM QUẢN TRỊ VIÊN", for: .normal)
        }
        
        
        fillData()
        
        tableView.register(UINib(nibName: reuseCellIdentifier, bundle: nil), forCellReuseIdentifier: reuseCellIdentifier)
        tableView.register(UINib(nibName: reuseHeaderIdentifier, bundle: nil), forHeaderFooterViewReuseIdentifier: reuseHeaderIdentifier)
        
        contentViewHeight.constant = 280
        tableView.isScrollEnabled = false
        
        nameTextField.delegate = self
        phoneTextField.delegate = self
        
    }
    
    func fillData() {
        if admin == nil {
            return
        }
        
        nameTextField.text = admin.name
        phoneTextField.text = admin.phone
        
        nameTextField.isUserInteractionEnabled = false
        phoneTextField.isUserInteractionEnabled = false
        
    }
    
    func openSelectNhanvien() {
        
        guard let quanLyThanhVienViewController = QuanLyThanhVienViewController.viewController(.setting) as? QuanLyThanhVienViewController else {return}
        quanLyThanhVienViewController.hidesBottomBarWhenPushed = true
        quanLyThanhVienViewController.isSelectAdmin = true
        quanLyThanhVienViewController.didSelectUser = {[weak self](user) in
            if let strongSelf = self {
                strongSelf.staff = user
            }
        }
        navigationController?.pushViewController(quanLyThanhVienViewController, animated: true)
    }
    
    func recalculateContentHeight() {
        var temp: CGFloat = 0
        for object in datasource {
            if object.expand {
                temp += CGFloat(object.submenu.count * 60) + 60
            } else {
                temp += 60
            }
        }
        
        if contentViewHeight != nil {
            contentViewHeight.constant = temp + 280
        }
    }
    
    func loadData() {
        
        if self.datasource.count == 0 {
            showProgress()
            HCService.getAllPermission(completion: { (objects) in
                self.datasource = PermissionModel.listAll(objects)
                if self.admin != nil {
                    HCService.getMemberPermission(id: self.admin.id, completion: { (permissions) in
                        self.dismissProgress()
                        self.activePermissions = permissions
                        for object in self.datasource {
                            for item in object.submenu {
                                if permissions.contains(item.key) {
                                    item.value = true
                                } else {
                                    item.value = false
                                }
                            }
                        }
                        
                        self.tableView.reloadData()
                        
                    }, failure: { (error) in
                        self.showError(error)
                    })
                } else {
                    self.dismissProgress()
                }
                
            }, failure: { (error) in
                self.showError(error)
            })
        }
        
        
    }
    
    
    @IBAction func doAddButton(_ sender: Any) {
        
        self.view.endEditing(true)
        
        if (staff == nil && admin == nil) {
            showAlertWithMessage("Hãy chọn nhân viên bạn muốn phân quyền")
            return
        }
        
        var temp = [String]()
        for object in datasource {
            for item in object.submenu {
                if item.value && !item.key.isEmpty {
                    temp.append(item.key)
                }
            }
        }
        
        if admin != nil && datasource.count == 0 {
            temp = activePermissions
        }
        
        if temp.count == 0 {
            showAlertWithMessage("Bạn chưa chọn quyền nào cho quản trị viên")
            return
        }
        
        showProgress()
        
        HCService.addQuanTriVien(id: admin?.id, phone: phoneTextField.text!, permissions: temp,completion: {
            self.dismissProgress()
            
            self.showAlertWithMessage("\(self.admin != nil ? "Sửa" : "Tạo") thành công quản trị viên", completion: {
                self.completion?()
                self.dismiss(animated: true, completion: nil)
            })
            
        }) { (error) in
            self.showError(error)
        }
        
        
    }
    
    @objc func doHeaderButton(_ sender: UIButton) {
        for (index, object) in datasource.enumerated() {
            if index == sender.tag {
                object.expand = !object.expand
            } else {
                object.expand = false
            }
        }
        
        self.tableView.reloadData()
        
        self.recalculateContentHeight()
    }
    
}

extension QuyenQuanTriVienViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return datasource.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if datasource[section].expand {
            return datasource[section].submenu.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseCellIdentifier, for: indexPath) as! MenuConfigViewCell
        
        let item = datasource[indexPath.section].submenu[indexPath.row]
        
        cell.titleLabel.text = item.name
        cell.switchControl.isOn = item.value
        
        return cell
    }
}

extension QuyenQuanTriVienViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        datasource[indexPath.section].submenu[indexPath.row].value = !datasource[indexPath.section].submenu[indexPath.row].value
        if let cell = tableView.cellForRow(at: indexPath) as? MenuConfigViewCell {
            cell.switchControl.setOn(datasource[indexPath.section].submenu[indexPath.row].value, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: reuseHeaderIdentifier) as! MenuConfigHeaderView
        
        let item = datasource[section]
        
        headerView.titleLabel.text = item.name
        
        if !item.expand {
            headerView.iconImageView.image = #imageLiteral(resourceName: "icon_expand")
        } else {
            headerView.iconImageView.image = #imageLiteral(resourceName: "icon_colapse")
        }
        headerView.actionButton.tag = section
        headerView.actionButton.addTarget(self, action: #selector(doHeaderButton(_:)), for: .touchUpInside)
        
        if item.submenu.filter({ (object) -> Bool in
            return object.value
        }).count > 0 {
            headerView.titleLabel.textColor = UIColor.HCColor.appColor
        } else {
            headerView.titleLabel.textColor = UIColor(hex: "#333333")
        }
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.000001
    }
}

extension QuyenQuanTriVienViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        openSelectNhanvien()
        return false
    }
}


