//
//  CauHinhGianHangViewController.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 4/27/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

class CauHinhGianHangViewController: UIViewController {

    @IBOutlet weak var nameLabel: UITextField!
    @IBOutlet weak var phoneLabel: UITextField!
    @IBOutlet weak var introTextView: GrowingTextView!
    @IBOutlet weak var bannerImageView: UIImageView!
    @IBOutlet weak var addressTextField: UITextField!
    
    var datasource: GianHangModel? {
        didSet {
            nameLabel?.text = datasource?.name
            phoneLabel?.text = datasource?.hotline
            introTextView?.text = datasource?.introduction
            bannerImageView?.sd_setImage(with: URL(string: datasource?.banner ?? ""))
            addressTextField?.text = datasource?.address
        }
    }
    
    var image: UIImage? {
        didSet {
            bannerImageView.image = image
        }
    }
    
    var address: RegionModel? {
        didSet {
            addressTextField?.text = address?.name
        }
    }
    var completion: (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        loadData()
    }
    
    func setupUI() {
        title = "Cấu hình gian hàng"
    }
    
    
    func loadData() {
        showProgress()
        HCService.getCauHinhGianHang(completion: { [weak self](objects) in
            self?.dismissProgress()
            self?.datasource = objects
        }) { (error) in
            self.showError(error)
        }
    }
    @IBAction func selectBanner(_ sender: Any) {
        doSelectPhoto()
    }
    
    @IBAction func doUpdateButton(_ sender: Any) {
        self.view.endEditing(true)
        
        showProgress()
        HCService.updateCauHinhGianHang(name: nameLabel.text ?? "", hotline: phoneLabel.text ?? "", info: "", intro: introTextView.text ?? "", banner: image, address: address, completion: {[weak self]() in
            self?.dismissProgress()
            self?.showAlertWithMessage("Thành công")
            self?.completion?()
        }) { (error) in
            self.showError(error)
        }
        
    }
    
    override func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        picker.dismiss(animated: true, completion: nil)
        if let _image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            self.image = _image
        }
    }
    
}

extension CauHinhGianHangViewController: GrowingTextViewDelegate {
    func textViewDidChangeHeight(_ textView: GrowingTextView, height: CGFloat) {
        UIView.animate(withDuration: 0.2) {
            self.view.layoutIfNeeded()
        }
    }
}

extension CauHinhGianHangViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField.isEqual(addressTextField) {
            guard let khuVucViewController = KhuVucViewController.viewController(.common) as? KhuVucViewController else {return false}
            khuVucViewController.completion = {[weak self](region) in
                if let strongSelf = self {
                    strongSelf.address = region
                }
            }
            self.present(HCNavigationController(rootViewController: khuVucViewController), animated: true, completion: nil)
            return false
        }
        return true
    }
}
