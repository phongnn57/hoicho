//
//  ProfileViewController.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 4/21/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {

    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var birthdayTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var companyTextField: UITextField!
    @IBOutlet weak var regionTextField: UITextField!
    @IBOutlet weak var addressTextView: GrowingTextView!
    @IBOutlet weak var typeTextField: UITextField!
    @IBOutlet weak var dateJoinedTextField: UITextField!
    @IBOutlet weak var updateButton: UIButton!
    @IBOutlet weak var introTextView: GrowingTextView!
    var datePicker: UIDatePicker!
    
    var user_id: Int = UserModel.shared.id
    var user: UserModel! {
        didSet {
            fillData()
        }
    }
    var image: UIImage? {
        didSet {
            avatarImageView?.image = image
        }
    }
    var birthday: Date? {
        didSet {
            birthdayTextField?.text = birthday?.toString()
        }
    }
    var region: RegionModel? {
        didSet {
            regionTextField?.text = region?.name
        }
    }
    var didUpdateProfile: (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        loadData()
    }

    func setupUI() {
        title = "Thông tin cá nhân"
        avatarImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(changeAvatar)))
        
        datePicker = initDatePicker()
        datePicker.maximumDate = Date()
        let toolbar = initToolbar()
        birthdayTextField.inputView = datePicker
        birthdayTextField.inputAccessoryView = toolbar
        
        regionTextField.delegate = self
    }
    
    override func doDoneItem() {
        birthday = datePicker.date
        birthdayTextField.resignFirstResponder()
    }
    
    func loadData() {
        self.contentView.isHidden = true
        showProgress()
        HCService.getUserInfomation(user_id: user_id, completion: { [weak self](object) in
            if let strongSelf = self {
                strongSelf.dismissProgress()
                strongSelf.contentView.isHidden = false
                strongSelf.user = object
            }
        }) { (error) in
            self.showError(error)
        }
    }
    
    func fillData() {
        if user == nil {
            return
        }
        
        birthday = user.birthday
        region = RegionModel()
        region?.name = user.region
        
        avatarImageView.sd_setImage(with: URL(string: user.image), placeholderImage: #imageLiteral(resourceName: "icon_default_avatar"))
        phoneTextField.text = user.phone
        nameTextField.text = user.name
        emailTextField.text = user.email
        companyTextField.text = user.company
        regionTextField.text = user.region
        addressTextView.text = user.address
        dateJoinedTextField.text = user.created_at?.toString()
        typeTextField.text = user.type_text_expo
        introTextView.text = user.introduction

    }
    
    @objc func changeAvatar() {
        doSelectPhoto(allowEdit: true)
    }
    
    override func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerEditedImage] as? UIImage {
            self.image = image
        }
        picker.dismiss(animated: true, completion: nil)
    }

    @IBAction func doUpdateButton(_ sender: Any) {
        self.view.endEditing(true)
        
        if Utilities.isNilOrEmpty(nameTextField.text) {
            showAlertWithMessage("Vui lòng nhập họ tên")
            return
        }
        if Utilities.isNilOrEmpty(emailTextField.text) {
            showAlertWithMessage("Vui lòng nhập email")
            return
        }
        if region == nil {
            showAlertWithMessage("Vui lòng chọn khu vực")
            return
        }
        if Utilities.isNilOrEmpty(addressTextView.text) {
            showAlertWithMessage("Vui lòng nhập địa chỉ")
            return
        }
        
        showProgress()
        HCService.updateProfile(id: user.id, image: image, name: nameTextField.text!, email: emailTextField.text!, birthday: birthday, company: companyTextField.text ?? "", region: region, address: addressTextView.text, intro: introTextView.text ?? "", completion: {[weak self]() in
            self?.dismissProgress()
            self?.showAlertWithMessage("Thông tin của bạn đã được cập nhật thành công")
            self?.didUpdateProfile?()
        }) { (error) in
            self.showError(error)
        }
    }
}

extension ProfileViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField.isEqual(regionTextField) {
            guard let khuVucViewController = KhuVucViewController.viewController(.common) as? KhuVucViewController else {return false}
            khuVucViewController.completion = {[weak self](region) in
                if let strongSelf = self {
                    strongSelf.region = region
                }
            }
            self.present(HCNavigationController(rootViewController: khuVucViewController), animated: true, completion: nil)
            return false
        }
        return true
    }
}
