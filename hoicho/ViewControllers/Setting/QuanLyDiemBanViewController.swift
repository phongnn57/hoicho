//
//  QuanLyDiemBanViewController.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 6/6/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

private let reuseCellIdentifier = "DiemBanTableViewCell"

class QuanLyDiemBanViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var refreshControl: UIRefreshControl!
    var loadMoreIndicator: UIActivityIndicatorView!
    
    var datasource = [DiemBanModel]() {
        didSet {
            tableView?.reloadData()
        }
    }
    var can_load_more = true
    var offset = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        loadData()
    }

    func setupUI() {
        title = "Quản lý điểm bán"
        
        tableView.register(UINib(nibName: reuseCellIdentifier, bundle: nil), forCellReuseIdentifier: reuseCellIdentifier)
        tableView.tableFooterView = UIView()
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 1000
        
        refreshControl = initRefreshControl()
        if #available(iOS 10, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        loadMoreIndicator = initLoadMoreIndicator()
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(doAddItem))
    }
    
    @objc func doAddItem() {
        guard let themDiemBanViewController = ThemDiemBanViewController.viewController(.home) as? ThemDiemBanViewController else {return}
        themDiemBanViewController.completion = {
            self.doRefresh()
        }
        self.present(HCNavigationController(rootViewController: themDiemBanViewController), animated: true, completion: nil)
    }
    
    func loadData(refresh: Bool = false, next: Bool = false) {
        if !refresh && !next {
            showProgress()
        } else if !refresh && next {
            tableView.tableFooterView = loadMoreIndicator
        }
        
        if UserModel.shared.type != nil && UserModel.shared.type! == .gian_hang {
            HCService.getSalePointOfBooth(offset: offset, completion: { [weak self](objects) in
                if let strongSelf = self {
                    strongSelf.dismissProgress()
                    strongSelf.refreshControl.endRefreshing()
                    strongSelf.tableView.tableFooterView = UIView()
                    
                    if strongSelf.offset == 0 {
                        strongSelf.datasource = objects
                    } else {
                        strongSelf.datasource.append(contentsOf: objects)
                    }
                    
                    if objects.count == Constant.limit {
                        strongSelf.can_load_more = true
                        strongSelf.offset += Constant.limit
                    } else {
                        strongSelf.can_load_more = false
                    }
                }
            }) { [weak self](error) in
                if let strongSelf = self {
                    strongSelf.dismissProgress()
                    strongSelf.refreshControl.endRefreshing()
                    strongSelf.tableView.tableFooterView = UIView()
                    strongSelf.showError(error)
                }
            }
        } else {
            self.dismissProgress()
            self.refreshControl.endRefreshing()
            self.tableView.tableFooterView = UIView()
            showAlertWithMessage("Chưa có api lấy danh sách điểm bán của 1 thành viên")
        }
        
        
        
    }
    
    override func doRefresh() {
        offset = 0
        loadData(refresh: true)
    }
}

extension QuanLyDiemBanViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datasource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseCellIdentifier, for: indexPath) as! DiemBanTableViewCell
        
        let item = datasource[indexPath.row]
        cell.nameLabel.text = item.name
        cell.addressLabel.text = "\(item.address), \(item.district), \(item.city)"
        cell.phoneButton.setTitle(item.phone, for: .normal)
        cell.priceLabel.text = item.price.priceString()
        
        cell.topConstraint.constant = 8
        
        if can_load_more && datasource.count - 1 == indexPath.row {
            loadData(next: true)
        }
        return cell
    }
}
