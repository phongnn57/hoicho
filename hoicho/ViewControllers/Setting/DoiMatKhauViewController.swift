//
//  DoiMatKhauViewController.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 4/27/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

class DoiMatKhauViewController: UIViewController {

    @IBOutlet weak var currentPasswordTextField: UITextField!
    @IBOutlet weak var newPasswordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
    }

    func setupUI() {
        title = "Đổi mật khẩu"
    }

    @IBAction func doUpdateButton(_ sender: Any) {
        self.view.endEditing(true)
        
        if Utilities.isNilOrEmpty(currentPasswordTextField.text) {
            showAlertWithMessage("Vui lòng nhập mật khẩu hiện tại")
            return
        }
        if Utilities.isNilOrEmpty(newPasswordTextField.text) {
            showAlertWithMessage("Vui lòng nhập mật khẩu mới")
            return
        }
        if Utilities.isNilOrEmpty(confirmPasswordTextField.text) {
            showAlertWithMessage("Vui lòng xác nhận mật khẩu mới")
            return
        }
        if newPasswordTextField.text! != confirmPasswordTextField.text! {
            showAlertWithMessage("Mật khẩu mới không khớp")
            return
        }
        showProgress()
        HCService.changePassword(current: currentPasswordTextField.text!, new: newPasswordTextField.text!, completion: {[weak self]() in
            self?.dismissProgress()
            self?.showAlertWithMessage("Thay đổi mật khẩu thành công. Vui lòng đăng nhập lại", completion: {
                UserModel.shared.delete()
                Constant.appDelegate.window?.rootViewController = HCNavigationController(rootViewController: LoginViewController.viewController(.auth)!)
            })
        }) { (error) in
            self.showError(error)
        }
    }
    
}
