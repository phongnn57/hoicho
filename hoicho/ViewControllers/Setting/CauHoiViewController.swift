//
//  CauHoiViewController.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 6/11/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

private let reuseHeaderIdentifier = "CauHoiHeaderView"

class CauHoiViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var question: QuestionModel! {
        didSet {
            tableView?.reloadData()
        }
    }
    var index = 0
    var completion: ((QuestionModel) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
    }

    func setupUI() {
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 500
        tableView.tableFooterView = UIView()
        
        tableView.register(UINib(nibName: reuseHeaderIdentifier, bundle: nil), forHeaderFooterViewReuseIdentifier: reuseHeaderIdentifier)
    }
    
    @objc func answerChanged(_ sender: UITextField) {
        self.question.answers[sender.tag].answer_content = sender.text ?? ""
        completion?(self.question)
    }
}

extension CauHoiViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if question == nil {return 0}
        return question.answers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CauHoiTableViewCell", for: indexPath) as! CauHoiTableViewCell
        
        let item = question.answers[indexPath.row]
        
        cell.titleLabel.text = item.content
        
        if item.type == 2 {
            cell.answerView.isHidden = false
            cell.answerViewHeight.constant = 40
        } else {
            cell.answerView.isHidden = true
            cell.answerViewHeight.constant = 0
        }
        
        if item.selected {
            cell.selectImageView.image = #imageLiteral(resourceName: "icon_radio_on")
        } else {
            cell.selectImageView.image = #imageLiteral(resourceName: "icon_radio_off")
        }
        
        if indexPath.row != 0 {
            cell.topLineView.isHidden = true
        } else {
            cell.topLineView.isHidden = false
        }
        
        cell.answerTextField.text = item.answer_content
        
        cell.answerTextField.tag = indexPath.row
        cell.answerTextField.addTarget(self, action: #selector(answerChanged(_:)), for: .editingChanged)
        
        return cell
    }
}

extension CauHoiViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        for (pos,item) in question.answers.enumerated() {
            if pos == indexPath.row {
                item.selected = true
            } else {
                item.selected = false
            }
        }
        completion?(self.question)
        self.tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return 1000
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: reuseHeaderIdentifier) as! CauHoiHeaderView
        
        headerView.indexLabel.text = "Câu \(index + 1):"
        headerView.questionLabel.text = question.title
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
}
