//
//  QRCodeGianHangViewController.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 6/11/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit
import QRCode

class QRCodeGianHangViewController: UIViewController {
    
    @IBOutlet weak var qrcodeView: UIView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var qrcodeImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var dash1View: UIView!
    @IBOutlet weak var dash2View: UIView!
    @IBOutlet weak var dash3View: UIView!
    @IBOutlet weak var dash4View: UIView!
    
    var datasource: GianHangModel! {
        didSet {
            if datasource == nil {
                self.contentView?.isHidden = true
            } else {
                self.contentView?.isHidden = false
                
                qrcodeImageView.sd_setImage(with: URL(string: datasource.qrcode))
                nameLabel.text = "Gian hàng: \(datasource.name)"
                
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        loadData()
    }

    func setupUI() {
        title = "Mã QR Code gian hàng"
        
        dash1View.addDashedLine()
        dash2View.addDashedLine()
        dash3View.addDashedLine(vertical: true)
        dash4View.addDashedLine(vertical: true)
        
        nameLabel.textColor = UIColor.HCColor.appColor
        
        self.contentView.isHidden = true
    }
    
    func loadData() {
        showProgress()
        HCService.getCauHinhGianHang(completion: { [weak self](objects) in
            self?.dismissProgress()
            self?.datasource = objects
        }) { (error) in
            self.showError(error)
        }
    }
    
    @IBAction func doSaveButton(_ sender: Any) {
        let saveImage = UIImage(view: self.qrcodeView)
        UIImageWriteToSavedPhotosAlbum(saveImage, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
    }
    
    
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let _ = error {
            self.showAlertWithMessage("Không lưu được ảnh. Hãy đảm bảo bạn đã cấp quyền cho ứng dụng lưu ảnh vào điện thoại của bạn")
        } else {
            self.showAlertWithMessage("Lưu ảnh thành công")
        }
    }
}
