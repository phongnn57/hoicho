//
//  VeCuaToiViewController.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 6/14/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit
import QRCode

class VeCuaToiViewController: UIViewController {
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var ngayLayVeLabel: UILabel!
    
    @IBOutlet weak var qrcodeImageView: UIImageView!
    
    @IBOutlet weak var dashLineView1: UIView!
    @IBOutlet weak var dashLineView2: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
    }

    func setupUI() {
        title = "Vé của tôi"
        
        dashLineView1.addDashedLine()
        dashLineView2.addDashedLine()
        
        if var qrcode = QRCode("0100336546") {
            qrcode.size = CGSize(width: 170, height: 170)
            qrcodeImageView.image = qrcode.image
        }
    }

}
