//
//  SettingViewController.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 4/18/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit
import SwiftyJSON

enum SettingAction: Int {
    case none = 0
    case profile = 1
    case quan_ly_thanh_vien = 2
    case quan_ly_gian_hang = 3
    case quan_ly_thuong_hieu = 4
    case quan_ly_san_pham = 5
    case quan_ly_ve_tham_quan = 6
    case quan_ly_thong_bao = 7
    case quan_ly_tin_tuc = 8
    case phan_quyen_quan_tri_vien = 9
    case bao_loi_dong_gop_cong_dong = 10
    case dang_xuat = 11
    case doi_mat_khau = 12
    case cau_hinh_gian_hang = 13
    case quan_ly_thong_tin_chung = 14
    case quan_ly_hoi_dap = 15
    case khach_hang_tham_quan = 16
    case gian_hang_quan_tam = 17
    case quan_ly_diem_ban = 18
    case san_pham_quan_tam = 19
    case diem_ban_cua_toi = 20
    case ve_tham_quan_cua_toi = 21
    case dang_ky_gian_hang = 22
    case bang_khao_sat = 23
    case qrcode_gian_hang = 24
    case quan_ly_hoi_cho = 25
}

class SettingItem {
    
    var action: SettingAction = .none
    var icon = ""
    var title = ""
    var selected: Bool = false
    var enable = true

    init(_ title: String, icon: String, action: SettingAction) {
        self.title = title
        self.icon = icon
        self.action = action
    }
    
    init(_ object: JSON) {
        
        title = object["title"].stringValue
        icon = object["icon"].stringValue
        if object["enable"].exists() {
            enable = object["enable"].boolValue
        }
        
        if let action = SettingAction.init(rawValue: object["action"].intValue) {
            self.action = action
        }
        
    }
    
}

class SettingModel {
    var title = ""
    var items: [SettingItem] = [SettingItem]()
    var icon = ""
    var expand = false
    var enable = true
    var action: SettingAction = .none
    var permissions = [String]()
    
    init(_ object: JSON) {
        title = object["title"].stringValue
        icon = object["icon"].stringValue
        items = [SettingItem]()
        if object["enable"].exists() {
            enable = object["enable"].boolValue
        }
        if let action = SettingAction.init(rawValue: object["action"].intValue) {
            self.action = action
        }
        for permission in object["permissions"].arrayValue {
            permissions.append(permission.stringValue)
        }
        if let itemJson = object["items"].array {
            for eachJson in itemJson {
                
                let item = SettingItem(eachJson)
                if item.enable {
                    items.append(item)
                }
                
            }
        }
    }
    
    class func getSetting() -> [SettingModel] {

        guard let type = UserModel.shared.type else {
            return [SettingModel]()
        }
        var fileName = ""
        if type == .chu_he_thong {
            fileName = "ChuHeThong"
        } else if type == .gian_hang {
            fileName = "ChuGianHang"
        } else if type == .thanh_vien || type == .quan_tri_vien {
            fileName = "ThanhVien"
        }
        if fileName.isEmpty {
            return [SettingModel]()
        }
        
        var temp = [SettingModel]()
        if let array = NSArray(contentsOfFile:Bundle.main.path(forResource: fileName, ofType: "plist")!) {
            for item in array {
                let settingModel = SettingModel(JSON(item))
                if settingModel.enable {
                    temp.append(settingModel)
                }
            }
        }
        return temp
    }

}

class SettingViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var refreshControl: UIRefreshControl!
    
    var datasource = [SettingModel]() {
        didSet {
            tableView?.emptyDataSetSource = self
            tableView?.emptyDataSetDelegate = self
            tableView?.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        loadData()
        
    }

    func setupUI() {
        title = "Tài khoản"
        
        refreshControl = initRefreshControl()
        if #available(iOS 10, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        NotificationCenter.default.addObserver(self, selector: #selector(userLoggedIn(_:)), name: NSNotification.Name.UserLoggedIn, object: nil)
        
    }
    
    override func doRefresh() {
        loadData()
    }
    
    @objc func userLoggedIn(_ notification: NSNotification) {
        self.loadData()
    }
    
    func loadData() {
        self.datasource = SettingModel.getSetting()
        if !UserModel.shared.token.isEmpty && UserModel.shared.type != nil && UserModel.shared.type! == .quan_tri_vien {

            showProgress()
            HCService.quyenQuanTriVien(completion: { (objects) in
                self.dismissProgress()
                self.refreshControl.endRefreshing()
                if self.datasource.count > 2 {
                    self.datasource.insert(contentsOf: objects, at: self.datasource.count - 2)
                } else {
                    self.datasource.append(contentsOf: objects)
                }
                
            }) { (error) in
                self.refreshControl.endRefreshing()
                self.showError(error)
            }
            
        } else {
            self.refreshControl.endRefreshing()
        }
    }
    
    /////////////////////////////////////////////
    
    func openProfile() {
        
        guard let thongTinCaNhanViewController = ThongTinCaNhanViewController.viewController(.setting) as? ThongTinCaNhanViewController else {return}
        thongTinCaNhanViewController.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(thongTinCaNhanViewController, animated: true)
        
    }
    
    func openBrands() {
        guard let brandViewController = BrandViewController.viewController(.setting) as? BrandViewController else {return}
        brandViewController.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(brandViewController, animated: true)
    }
    
    func openQuanLyThanhVien() {
        guard let quanLyThanhVienViewController = QuanLyThanhVienViewController.viewController(.setting) as? QuanLyThanhVienViewController else {return}
        quanLyThanhVienViewController.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(quanLyThanhVienViewController, animated: true)
    }
    
    func openQuanLyGianHang(is_follow: Bool = false) {
        guard let quanLyGianHangViewController = QuanLyGianHangViewController.viewController(.setting) as? QuanLyGianHangViewController else {return}
        quanLyGianHangViewController.hidesBottomBarWhenPushed = true
        quanLyGianHangViewController.is_follow = is_follow
        navigationController?.pushViewController(quanLyGianHangViewController, animated: true)
    }
    
    func openDoiMatKhau() {
        guard let doiMatKhauViewController = DoiMatKhauViewController.viewController(.setting) as? DoiMatKhauViewController else {return}
        doiMatKhauViewController.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(doiMatKhauViewController, animated: true)
    }
    
    func openCauHinhGianHang() {
        
        guard let gianHangViewController = GianHangViewController.viewController(.home) as? GianHangViewController else {return}
        gianHangViewController.hidesBottomBarWhenPushed = true
        gianHangViewController.booth_id = UserModel.shared.id
        navigationController?.pushViewController(gianHangViewController, animated: true)
        
    }
    
    func openThemSanPham() {
        
        guard let quanLySanPhamViewController = QuanLySanPhamViewController.viewController(.setting) as? QuanLySanPhamViewController else {return}
        quanLySanPhamViewController.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(quanLySanPhamViewController, animated: true)

    }
    
    func openQuanLyVe() {
        guard let quanLyVeThamQuanViewController = QuanLyVeThamQuanViewController.viewController(.setting) as? QuanLyVeThamQuanViewController else {return}
        quanLyVeThamQuanViewController.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(quanLyVeThamQuanViewController, animated: true)
    }
    
    func openTongHopType(_ type: TongHopType) {
        guard let quanLyTongHopViewController = QuanLyTongHopTableViewController.viewController(.setting) as? QuanLyTongHopTableViewController else {return}
        quanLyTongHopViewController.hidesBottomBarWhenPushed = true
        quanLyTongHopViewController.type = type
        navigationController?.pushViewController(quanLyTongHopViewController, animated: true)
    }
    
    func openSanPhamQuanTam() {
        guard let danhSachSanPhamKhacViewController = DanhSachSanPhamKhacViewController.viewController(.main) as? DanhSachSanPhamKhacViewController else {return}
        danhSachSanPhamKhacViewController.hidesBottomBarWhenPushed = true
        danhSachSanPhamKhacViewController.type = .yeuthich
        navigationController?.pushViewController(danhSachSanPhamKhacViewController, animated: true)
    }
    
    func openQuanLyDiemBan() {
        guard let quanLyDiemBanViewController = QuanLyDiemBanViewController.viewController(.setting) as? QuanLyDiemBanViewController else {return}
        quanLyDiemBanViewController.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(quanLyDiemBanViewController, animated: true)
    }
    
    func openKhachHangThamQuan() {
        guard let khachThamQuanViewController = KhachThamQuanViewController.viewController(.setting) as? KhachThamQuanViewController else {return}
        khachThamQuanViewController.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(khachThamQuanViewController, animated: true)
    }
    
    func openDiemBanCuaToi() {
        guard let chiTietDiemBanViewController = ChiTietDiemBanViewController.viewController(.common) as? ChiTietDiemBanViewController else {return}
        chiTietDiemBanViewController.hidesBottomBarWhenPushed = true
        let salePoint = DiemBanModel()
        salePoint.phone = UserModel.shared.phone
        chiTietDiemBanViewController.salePoint = salePoint
        navigationController?.pushViewController(chiTietDiemBanViewController, animated: true)
    }
    
    func openDangKyGianHang() {
        guard let dangKyGianHangViewController = DangKyGianHangViewController.viewController(.setting) else {return}
        dangKyGianHangViewController.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(dangKyGianHangViewController, animated: true)
    }
    
    func openBangKhaoSat() {
        guard let bangKhaoSatViewController = BangKhaoSatViewController.viewController(.setting) as? BangKhaoSatViewController else {return}
        bangKhaoSatViewController.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(bangKhaoSatViewController, animated: true)
    }
    
    func openQRCodeGianHang() {
        guard let qrcodeGianHangViewController = QRCodeGianHangViewController.viewController(.setting) as? QRCodeGianHangViewController else {return}
        qrcodeGianHangViewController.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(qrcodeGianHangViewController, animated: true)
    }
    
    func openQuanTriVien() {
        guard let quanTriVienViewController = DanhSachQuanTriVienViewController.viewController(.setting) as? DanhSachQuanTriVienViewController else {return}
        quanTriVienViewController.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(quanTriVienViewController, animated: true)
    }
    
    func openVeCuaToi() {
        guard let veCuaToiViewController = VeCuaToiViewController.viewController(.setting) as? VeCuaToiViewController else {return}
        veCuaToiViewController.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(veCuaToiViewController, animated: true)
    }
    
    func openQuanLyHoiCho() {
        guard let soDoGianHangViewController = SoDoGianHangViewController.viewController(.home) as? SoDoGianHangViewController else {return}
        soDoGianHangViewController.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(soDoGianHangViewController, animated: true)
    }
    
    func logout() {
        showProgress()
        HCService.logout(completion: {
            self.dismissProgress()
            UserModel.shared.delete()
            self.datasource = SettingModel.getSetting()
        }) { (error) in
            self.dismissProgress()
            UserModel.shared.delete()
            self.datasource = SettingModel.getSetting()
        }
    }
    
}

extension SettingViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datasource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingTableViewCell", for: indexPath) as! SettingTableViewCell
        
        let item = datasource[indexPath.row]
        cell.titleLabel.text = item.title
        cell.iconImageView.image = UIImage(named: item.icon)
        
        return cell
    }
}

extension SettingViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = datasource[indexPath.row]
        
        if item.action == .profile {
            openProfile()
        } else if item.action == .dang_xuat {
            logout()
        } else if item.action == .quan_ly_thuong_hieu {
            openBrands()
        } else if item.action == .bao_loi_dong_gop_cong_dong {
            guard let url = URL(string: "fb://profile/1991353967749861") else {
                return
            }
            if UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.openURL(url)
            }
        } else if item.action == .quan_ly_thanh_vien {
            openQuanLyThanhVien()
        } else if item.action == .quan_ly_gian_hang {
            openQuanLyGianHang()
        } else if item.action == .doi_mat_khau {
            openDoiMatKhau()
        } else if item.action == .cau_hinh_gian_hang {
            openCauHinhGianHang()
        } else if item.action == .quan_ly_san_pham {
            openThemSanPham()
        } else if item.action == .quan_ly_ve_tham_quan {
            openQuanLyVe()
        } else if item.action == .quan_ly_thong_tin_chung {
            openTongHopType(.thongtinchung)
        } else if item.action == .quan_ly_hoi_dap {
            openTongHopType(.hoidap)
        } else if item.action == .quan_ly_tin_tuc {
            openTongHopType(.tintuc)
        } else if item.action == .san_pham_quan_tam {
            openSanPhamQuanTam()
        } else if item.action == .gian_hang_quan_tam {
            openQuanLyGianHang(is_follow: true)
        } else if item.action == .quan_ly_diem_ban {
            openQuanLyDiemBan()
        } else if item.action == .khach_hang_tham_quan {
            openKhachHangThamQuan()
        } else if item.action == .diem_ban_cua_toi {
            openDiemBanCuaToi()
        } else if item.action == .dang_ky_gian_hang {
            openDangKyGianHang()
        } else if item.action == .bang_khao_sat {
            openBangKhaoSat()
        } else if item.action == .qrcode_gian_hang {
            openQRCodeGianHang()
        } else if item.action == .phan_quyen_quan_tri_vien {
            openQuanTriVien()
        } else if item.action == .ve_tham_quan_cua_toi {
            openVeCuaToi()
        } else if item.action == .quan_ly_hoi_cho {
            openQuanLyHoiCho()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
}

extension SettingViewController: DZNEmptyDataSetSource {
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        return NSAttributedString.init(string: "Bạn chưa đăng nhập hoặc loại tài khoản này chưa được định nghĩa danh mục quản trị", attributes: [NSAttributedStringKey.foregroundColor: UIColor(hex: "#999999"), NSAttributedStringKey.font: UIFont.systemFont(ofSize: 14)])
    }
    
    func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage! {
        return #imageLiteral(resourceName: "setting_empty")
    }
    
    func buttonTitle(forEmptyDataSet scrollView: UIScrollView!, for state: UIControlState) -> NSAttributedString! {
        return NSAttributedString.init(string: "Đăng nhập tài khoản", attributes: [NSAttributedStringKey.foregroundColor: UIColor.HCColor.appColor, NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 14)])
    }
    

}

extension SettingViewController: DZNEmptyDataSetDelegate {
    func emptyDataSetShouldDisplay(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
    
    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
    
    func emptyDataSet(_ scrollView: UIScrollView!, didTap button: UIButton!) {
        self.requestLoginWithCompletion(true) {
            
//            self.loadData()
        }

    }
}
