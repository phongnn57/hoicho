//
//  ThemHoiChoViewController.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 6/18/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

class ThemHoiChoViewController: UIViewController {

    @IBOutlet weak var bgImageView: UIImageView!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var startTimeTextField: UITextField!
    @IBOutlet weak var endTimeTextField: UITextField!
    @IBOutlet weak var addressTextField: UITextField!
    @IBOutlet weak var descriptionTextView: GrowingTextView!
    
    var datePicker: UIDatePicker!
    
    var image: UIImage? {
        didSet {
            bgImageView.image = image
        }
    }
    var start_time: Date? {
        didSet {
            startTimeTextField?.text = start_time?.toString(format: "HH:mm dd/MM/yyyy")
        }
    }
    var end_time: Date? {
        didSet {
            endTimeTextField?.text = end_time?.toString(format: "HH:mm dd/MM/yyyy")
        }
    }
    
    var completion: (() -> Void)?
    var expo: ExpoModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
    }
    
    func setupUI() {
        if expo == nil {
            title = "Thêm hội chợ"
        } else {
            title = "Sửa hội chợ"
            fillData()
        }
        
        datePicker = initDatePicker()
        datePicker.datePickerMode = .dateAndTime
        let toolbar = initToolbar()
        
        startTimeTextField.inputView = datePicker
        startTimeTextField.inputAccessoryView = toolbar
        endTimeTextField.inputView = datePicker
        endTimeTextField.inputAccessoryView = toolbar
    }
    
    func fillData() {
        start_time = expo.start_time
        end_time = expo.end_time
        
        bgImageView.sd_setImage(with: URL(string: expo.image))
        nameTextField.text = expo.name
        addressTextField.text = expo.address
        descriptionTextView.text = expo.description_text
        startTimeTextField.text = start_time?.toString(format: "HH:mm dd/MM/yyyy")
        endTimeTextField.text = end_time?.toString(format: "HH:mm dd/MM/yyyy")
    }
    
    override func doDoneItem() {
        if startTimeTextField.isEditing {
            start_time = datePicker.date
        } else if endTimeTextField.isEditing {
            end_time = datePicker.date
        }
        
        self.view.endEditing(true)
    }

    @IBAction func doSelectImage(_ sender: Any) {
        doSelectPhoto()
    }
    
    @IBAction func doSaveButton(_ sender: Any) {
        self.view.endEditing(true)
        
        if image == nil && expo == nil {
            showAlertWithMessage("Hãy chọn ảnh hội chợ")
            return
        }
        if start_time == nil {
            showAlertWithMessage("Hãy chọn ngày bắt đầu")
            return
        }
        if end_time == nil {
            showAlertWithMessage("Hãy chọn ngày kết thúc")
            return
        }
        if Utilities.isNilOrEmpty(nameTextField.text) {
            showAlertWithMessage("Hãy nhập tên hội chợ")
            return
        }
        if Utilities.isNilOrEmpty(addressTextField.text) {
            showAlertWithMessage("Hãy nhập địa chỉ")
            return
        }
        if Utilities.isNilOrEmpty(descriptionTextView.text) {
            showAlertWithMessage("Hãy nhập mô tả")
            return
        }
        
        showProgress()
        
        HCService.addOrUpdateExpo(id: expo?.id, image: image, name: nameTextField.text ?? "", start_time: start_time!, end_time: end_time!, address: addressTextField.text ?? "", description: descriptionTextView.text ?? "", completion: {
            self.dismissProgress()
            self.showAlertWithMessage("Thành công", completion: {
                self.dismiss(animated: true, completion: {
                    self.completion?()
                })
            })
        }) { (error) in
            self.showError(error)
        }
    }
    
    override func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        picker.dismiss(animated: true, completion: nil)
        if let _image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            image = _image
        }
    }
}
