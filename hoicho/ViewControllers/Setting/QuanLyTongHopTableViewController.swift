//
//  QuanLyTongHopTableViewController.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 5/14/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

enum TongHopType: Int, EnumEnumerable {
    case daotao = 3
    case thongtinchung = 7
    case tintuc = 1
    case hoidap = 0
}


private let reuseCellIdentifier = "QuanLyTongHopTableViewCell"
private let reuseHeaderIdentifier = "QuanLyTongHopTableHeaderView"

class QuanLyTongHopTableViewController: UITableViewController {

    let searchController = UISearchController(searchResultsController: nil)
    var loadMoreIndicator: UIActivityIndicatorView!
    
    
    var offset = 0
    var type: TongHopType = .tintuc
    var category_id: Int?
    var name: String?
    var status = true
    var canLoadMore = true
    
    var datasource = [TongHopModel]() {
        didSet {
            tableView?.reloadData()
        }
    }
    var total = 0
    var subcribed = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        loadData()
    }
    
    func setupUI() {
        
        if type == .daotao {
            title = "Đào tạo & hướng dẫn"
        } else if  type == .tintuc {
            title = "Tin tức hội chợ"
        } else if  type == .thongtinchung {
            title = "Thông tin chung"
        } else if  type == .hoidap {
            title = "Hỏi đáp"
        }
        
        tableView.tableFooterView = UIView()
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 1000
        tableView.register(UINib(nibName: reuseHeaderIdentifier, bundle: nil), forHeaderFooterViewReuseIdentifier: reuseHeaderIdentifier)
        
        loadMoreIndicator = initLoadMoreIndicator()
        
        if #available(iOS 9.1, *) {
            searchController.obscuresBackgroundDuringPresentation = false
        } else {
            // Fallback on earlier versions
        }
        searchController.searchBar.showsScopeBar = false
        
        if type == .daotao {
            searchController.searchBar.placeholder = "Tìm bài viết"
        } else if  type == .tintuc {
            searchController.searchBar.placeholder = "Tìm tin tức"
        } else if  type == .thongtinchung {
            searchController.searchBar.placeholder = "Tìm bài viết"
        } else if  type == .hoidap {
            searchController.searchBar.placeholder = "Tìm câu hỏi"
        }
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.dimsBackgroundDuringPresentation = false
        definesPresentationContext = true
        searchController.searchBar.delegate = self
        
        if #available(iOS 11.0, *) {
            navigationItem.searchController = searchController
            navigationItem.hidesSearchBarWhenScrolling = false
        } else {
            tableView.tableHeaderView = searchController.searchBar
        }
        
        let filterItem = UIBarButtonItem(image: #imageLiteral(resourceName: "icon_filter"), style: .plain, target: self, action: #selector(openListDanhMuc))
//        let addItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(doAddButton))
        if UserModel.shared.type != nil && UserModel.shared.type! == .chu_he_thong {
            navigationItem.rightBarButtonItems = [filterItem]
        } else {
            navigationItem.rightBarButtonItems = [filterItem]
        }
        
    }
    
    @objc func doAddButton() {
        let actionSheetController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        actionSheetController.addAction(UIAlertAction(title: "Thêm danh mục", style: .default, handler: { (_) in
            self.doThemDanhMuc()
        }))
        actionSheetController.addAction(UIAlertAction(title: "Thêm bài viết", style: .default, handler: { (_) in
            
        }))
        actionSheetController.addAction(UIAlertAction(title: "Huỷ", style: .cancel, handler: { (_) in
            
        }))
        if let popoverController = actionSheetController.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.height, width: 0, height: 0)
        }
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    @objc func openListDanhMuc() {
        guard let danhMucViewController = TongHopDanhMucViewController.viewController(.setting) as? TongHopDanhMucViewController else {return}
        danhMucViewController.type = self.type
        danhMucViewController.completion = {[weak self](object) in
            self?.category_id = object.id
            self?.doRefresh()
        }
        self.present(HCNavigationController(rootViewController: danhMucViewController), animated: true, completion: nil)
    }
    
    func doThemDanhMuc() {
        guard let themDanhMucViewController = TongHopThemDanhMucViewController.viewController(.setting) as? TongHopThemDanhMucViewController else {return}
        themDanhMucViewController.type = self.type
        self.present(HCNavigationController(rootViewController: themDanhMucViewController), animated: true, completion: nil)
    }
    
    func loadData(loadNext: Bool = false) {
        if !loadNext && !refreshControl!.isRefreshing {
            showProgress()
        } else if loadNext {
            tableView.tableFooterView = loadMoreIndicator
        }
        
        HCService.getBaiVietTongHop(type: type, offset: offset, category_id: category_id, name: name, status: status, completion: {(objects, total_post, subcribeISG) in
            self.refreshControl?.endRefreshing()
            self.tableView.tableFooterView = UIView()
            self.dismissProgress()
            self.total = total_post
            self.subcribed = subcribeISG
            if self.offset == 0 {
                self.datasource = objects
            } else {
                self.datasource.append(contentsOf: objects)
            }
            if objects.count == Constant.limit {
                self.offset += Constant.limit
                self.canLoadMore = true
            } else {
                self.canLoadMore = false
            }
        }) { (error) in
            self.refreshControl?.endRefreshing()
            self.tableView.tableFooterView = UIView()
            self.showError(error)
        }
    }
    
    @IBAction func refreshChanged(_ sender: Any) {
        self.offset = 0
        self.loadData()
    }
    
    override func doRefresh() {
        self.offset = 0
        self.loadData()
    }
    
    @objc func subcribeISG(_ sender: UISwitch) {
        showProgress()
//        ISGTongHopService.subcribeISG(subcribe: sender.isOn, completion: {
//            self.dismissProgress()
//        }) { (error) in
//            self.dismissProgress()
//            sender.isOn = !sender.isOn
//        }
    }
    
    @objc func statusChanged(_ sender: UISegmentedControl) {
        self.status = sender.selectedSegmentIndex == 0 ? true : false
        self.doRefresh()
    }
    
    func filterContentForSearchText(_ searchText: String) {
        
    }
    
    func searchBarIsEmpty() -> Bool {
        return searchController.searchBar.text?.isEmpty ?? true
    }
    
    func isFiltering() -> Bool {
        let searchBarScopeIsFiltering = searchController.searchBar.selectedScopeButtonIndex != 0
        return searchController.isActive && (!searchBarIsEmpty() || searchBarScopeIsFiltering)
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return datasource.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if type == .hoidap {
            let cell = tableView.dequeueReusableCell(withIdentifier: reuseCellIdentifier, for: indexPath) as! QuanLyTongHopTableViewCell
            
            cell.post = datasource[indexPath.row]
            
            if canLoadMore && datasource.count - 1 == indexPath.row {
                loadData(loadNext: true)
            }
            
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TinTucTableViewCell", for: indexPath) as! TinTucTableViewCell
            
            cell.model = datasource[indexPath.row]
            
            if canLoadMore && datasource.count - 1 == indexPath.row {
                loadData(loadNext: true)
            }
            
            return cell
        }
        
        
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: reuseHeaderIdentifier) as! QuanLyTongHopTableHeaderView
        
        headerView.countLabel.text = "\(total)"
        headerView.subcribeSwitch.isOn = subcribed
        
        if self.type == .hoidap {
            headerView.stackView.isHidden = true
            headerView.stackViewHeight.constant = 0
        } else {
            headerView.stackView.isHidden = true
            headerView.stackViewHeight.constant = 0
        }
        headerView.segmentControl.addTarget(self, action: #selector(statusChanged(_:)), for: .valueChanged)
        if type == .daotao {
            headerView.subcribeLabel.isHidden = false
            headerView.subcribeSwitch.isHidden = false
            headerView.subcribeSwitch.addTarget(self, action: #selector(subcribeISG(_:)), for: .valueChanged)
        } else {
            headerView.subcribeLabel.isHidden = true
            headerView.subcribeSwitch.isHidden = true
        }
        
        return headerView
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return 100
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        showProgress()
        HCService.getChiTietBaiViet(type: self.type, id: datasource[indexPath.row].id, completion: { (object) in
            self.openWebViewWith(html: object, webTitle: self.datasource[indexPath.row].name)
            self.dismissProgress()
        }) { (error) in
            self.showError(error)
        }
    }


}

extension QuanLyTongHopTableViewController: UISearchBarDelegate {
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        searchBar.showsScopeBar = true
        searchBar.sizeToFit()
        return true
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsScopeBar = true
        searchBar.sizeToFit()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        self.name = nil
        self.doRefresh()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        self.name = searchBar.text
        self.doRefresh()
    }
    
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        if selectedScope == 0 {
            status = true
        } else {
            status = false
        }
        self.doRefresh()
    }
    
}


