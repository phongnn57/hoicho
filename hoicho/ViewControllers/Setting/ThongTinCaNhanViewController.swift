//
//  ThongTinCaNhanViewController.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 5/28/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit
import FBSDKShareKit

private let reuseCellIdentifier = "CongDongTableViewCell"

class ThongTinCaNhanViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var refreshControl: UIRefreshControl!
    var loadMoreIndicator: UIActivityIndicatorView!
    
    var user_id: Int = UserModel.shared.id
    var user: UserModel! {
        didSet {
            tableView?.reloadData()
        }
    }
    var friend_status: FriendStatus = .not_friend {
        didSet {
            tableView?.reloadData()
        }
    }
    var posts = [CommunityModel]() {
        didSet {
            tableView?.reloadData()
        }
    }
    var can_load_more = true
    var offset = 0
    var productSharingIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        loadData()
    }

    func setupUI() {
        title = "Thông tin cá nhân"
        
        tableView.tableFooterView = UIView()
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 1000
        tableView.register(UINib(nibName: reuseCellIdentifier, bundle: nil), forCellReuseIdentifier: reuseCellIdentifier)
        refreshControl = initRefreshControl()
        loadMoreIndicator = initLoadMoreIndicator()
        if #available(iOS 10, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        if user_id != UserModel.shared.id {
            let messageItem = UIBarButtonItem(image: #imageLiteral(resourceName: "tabbar_4"), style: .plain, target: self, action: #selector(doMessageButton))
            navigationItem.rightBarButtonItem = messageItem
        } else {
            let friendRequestItem = UIBarButtonItem(image: #imageLiteral(resourceName: "icon_user"), style: .plain, target: self, action: #selector(doFriendRequestItem))
            navigationItem.rightBarButtonItem = friendRequestItem
        }
    }
    
    @objc func doFriendRequestItem() {
        guard let friendRequestViewController = FriendRequestViewController.viewController(.setting) as? FriendRequestViewController else {return}
        self.present(HCNavigationController(rootViewController: friendRequestViewController), animated: true, completion: nil)
    }
    
    @objc func doMessageButton() {
        if user == nil {return}
        guard let chatViewController = HCMessengerViewController.viewController(.chat) as? HCMessengerViewController else {return}
        chatViewController.hidesBottomBarWhenPushed = true
        chatViewController.users = [user]
        navigationController?.pushViewController(chatViewController, animated: true)
    }
    
    func doContactButton() {
        
    }
    
    func loadData(refresh: Bool = false, next: Bool = false) {
        
        let dispatchGroup = DispatchGroup()
        var count = 0
        
        if !refresh && !next {
            showProgress()
        } else if !refresh && next {
            tableView.tableFooterView = UIView()
        }
        
        if !next {
            dispatchGroup.enter()
            HCService.getUserInfomation(user_id: user_id, completion: { [weak self](object) in
                if let strongSelf = self {
                    strongSelf.user = object
                    count += 1
                    dispatchGroup.leave()
                }
            }) { (error) in
                count += 1
                dispatchGroup.leave()
            }
            
            if !UserModel.shared.token.isEmpty && UserModel.shared.id != user_id {
                dispatchGroup.enter()
                HCService.checkFriendStatus(id: user_id, completion: {[weak self](status) in
                    if let strongSelf = self {
                        strongSelf.friend_status = status
                    }
                    count += 1
                    dispatchGroup.leave()
                }) { (error) in
                    count += 1
                    dispatchGroup.leave()
                }
            }
 
        }
        
        dispatchGroup.enter()
        HCService.getCommunities(account_id: user_id, content: "", offset: offset, completion: { [weak self](objects) in
            if let strongSelf = self {
                if strongSelf.offset == 0 {
                    strongSelf.posts = objects
                } else {
                    strongSelf.posts.append(contentsOf: objects)
                }
                if objects.count == Constant.limit {
                    strongSelf.offset += Constant.limit
                    strongSelf.can_load_more = true
                } else {
                    strongSelf.can_load_more = false
                }
                count += 1
                dispatchGroup.leave()
            }
        }) { (error) in
            count += 1
            dispatchGroup.leave()
        }
        
        dispatchGroup.notify(queue: .main) {
            if (count == 3 && !next && !UserModel.shared.token.isEmpty && UserModel.shared.id != self.user_id) ||
                (next && count == 1) ||
                (count == 2 && !next && (UserModel.shared.token.isEmpty || UserModel.shared.id == self.user_id)) {
                self.dismissProgress()
                self.refreshControl.endRefreshing()
                self.tableView.tableFooterView = UIView()
            }
        }
    }
    
    override func doRefresh() {
        offset = 0
        loadData(refresh: true)
    }

    func openNewCommunity() {
        guard let themBaiVietViewController = ThemBaiVietViewController.viewController(.congdong) as? ThemBaiVietViewController else {return}
        themBaiVietViewController.completion = {[weak self](newPost) in
            if let strongSelf = self {
                strongSelf.posts.insert(newPost, at: 0)
                strongSelf.tableView.reloadData()
            }
        }
        self.present(HCNavigationController(rootViewController: themBaiVietViewController), animated: true, completion: nil)
    }
    
    func openSetting() {
        guard let profileViewController = ProfileViewController.viewController(.setting) as? ProfileViewController else {return}
        profileViewController.hidesBottomBarWhenPushed = true
        profileViewController.user_id = user.id
        profileViewController.didUpdateProfile = {
            self.loadData()
        }
        navigationController?.pushViewController(profileViewController, animated: true)
    }
    
    @objc func openProduct(_ sender: UITapGestureRecognizer) {
        if let senderView = sender.view, let product = posts[senderView.tag].product {
            guard let chiTietSanPhamViewController = ChiTietSanPhamViewController.viewController(.home) as? ChiTietSanPhamViewController else {return}
            chiTietSanPhamViewController.hidesBottomBarWhenPushed = true
            chiTietSanPhamViewController.product_id = product.id
            navigationController?.pushViewController(chiTietSanPhamViewController, animated: true)
        }
    }
    
    @objc func openProfile(_ sender: UIButton) {
        guard let thongTinCaNhanViewController = ThongTinCaNhanViewController.viewController(.setting) as? ThongTinCaNhanViewController else {return}
        thongTinCaNhanViewController.hidesBottomBarWhenPushed = true
        thongTinCaNhanViewController.user_id = posts[sender.tag].account_id
        navigationController?.pushViewController(thongTinCaNhanViewController, animated: true)
    }
    
    @objc func doShareButton(_ sender: UIButton) {
        productSharingIndex = sender.tag
        guard let product = posts[sender.tag].product else {return}
        
        if product.link_affiliate.length > 0 {
            
            let height: CGFloat = 180
            
            
            var items = [CustomizableActionSheetItem]()
            
            let shareView = ShareProductView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width - 30, height: height))
            
            let shareViewItem = CustomizableActionSheetItem()
            shareViewItem.type = .view
            shareViewItem.view = shareView
            shareViewItem.height = height
            items.append(shareViewItem)
            
            // Setup button
            let closeItem = CustomizableActionSheetItem()
            closeItem.type = .button
            closeItem.label = "Đóng"
            closeItem.selectAction = { (actionSheet: CustomizableActionSheet) -> Void in
                actionSheet.dismiss()
            }
            items.append(closeItem)
            
            // Show
            let actionSheet = CustomizableActionSheet()
            actionSheet.showInView(self.view, items: items)
            
            
            shareView.doShareFacebookProduct = {
                
                actionSheet.dismiss()
                
                let shareContent = FBSDKShareLinkContent()
                shareContent.contentURL = URL(string: product.link_affiliate)
                
                
                FBSDKShareDialog.show(from: self, with: shareContent, delegate: nil)
                FBSDKShareDialog.show(from: self, with: shareContent, delegate: self)
            }
            
            shareView.doShareOthersProduct =  {
                
                actionSheet.dismiss()
                
                let url = URL(string: product.link_affiliate)
                
                let shareItems = [url!]
                let activityController = UIActivityViewController(activityItems: shareItems, applicationActivities: nil)
                activityController.excludedActivityTypes = [.print, .postToWeibo, .copyToPasteboard, .addToReadingList, .postToVimeo, .postToTwitter, .assignToContact, .saveToCameraRoll, .postToFlickr, .postToTencentWeibo, .airDrop, .openInIBooks]
                activityController.completionWithItemsHandler = {(activityType, completed, returnedItems, error) in
                    if completed {
                        HCService.shareProductCount(id: product.id, completion: { [weak self](count) in
                            if let strongSelf = self {
                                strongSelf.posts[strongSelf.productSharingIndex].share_count += 1
                                strongSelf.tableView.reloadData()
                            }
                        }) { (error) in
                            
                        }
                    }
                }
                
                self.present(activityController, animated: true, completion: nil)
            }
            
        } else {
            showAlertWithMessage("Link sản phẩm bị trống, không thể chia sẻ")
        }
    }
    
    @objc func doGianHangDiemBanButton() {
        if user == nil {return}
        if user.type_text_expo.lowercased() == UserType.gian_hang.rawValue.lowercased() {
            guard let gianHangViewController = GianHangViewController.viewController(.home) as? GianHangViewController else {return}
            gianHangViewController.hidesBottomBarWhenPushed = true
            gianHangViewController.booth_id = user.id
            navigationController?.pushViewController(gianHangViewController, animated: true)
        } else if user.type_text_expo.lowercased() == UserType.thanh_vien.rawValue.lowercased() && !user.sale_point_name.isEmpty {
            guard let chiTietDiemBanViewController = ChiTietDiemBanViewController.viewController(.common) as? ChiTietDiemBanViewController else {return}
            let salePoint = DiemBanModel()
            salePoint.phone = user.phone
            chiTietDiemBanViewController.salePoint = salePoint
            navigationController?.pushViewController(chiTietDiemBanViewController, animated: true)
        }
    }

    @objc func doFriendButton() {
        let isLoggedIn = !UserModel.shared.token.isEmpty
        self.requestLoginWithCompletion {
            if UserModel.shared.id != self.user_id {
                if self.friend_status == .not_friend {
                    self.requestFriend()
                } else if self.friend_status == .accept {
                    self.openResponseFriendAction()
                } else if self.friend_status == .friend || self.friend_status == .request_sent {
                    self.removeFriend()
                }
            } else {
                if !isLoggedIn {
                    self.showAlertWithMessage("Bạn không thể thao tác bạn bè với chính bạn", completion: {
                        self.doRefresh()
                    })
                } else {
                    self.doRefresh()
                    Constant.appDelegate.openDanhBa()
                }
            }
        }
    }
    
    func requestFriend() {
        self.showProgress()
        HCService.addFriend(id: self.user_id, completion: {
            self.dismissProgress()
            self.friend_status = .request_sent
        }, failure: { (error) in
            self.showError(error)
        })
    }
    
    func openResponseFriendAction() {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Chấp nhận", style: .default, handler: { (action: UIAlertAction) in
            self.acceptFriend(accept: true)
        }))
        actionSheet.addAction(UIAlertAction(title: "Huỷ", style: .destructive, handler: { (action: UIAlertAction) in
            self.acceptFriend(accept: false)
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Quyết định sau", style: .cancel, handler: nil))
        if let popoverController = actionSheet.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.height, width: 0, height: 0)
        }
        present(actionSheet, animated: true, completion: nil)
    }
    
    func acceptFriend(accept: Bool) {
        showProgress()
        HCService.responseFriendRequest(id: user_id, accept: accept, completion: {
            self.dismissProgress()
            if accept {
                self.friend_status = .friend
            } else {
                self.friend_status = .not_friend
            }
        }) { (error) in
            self.showError(error)
        }
    }
    
    func removeFriend() {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Huỷ kết bạn", style: .default, handler: { (action: UIAlertAction) in
            self.showProgress()
            HCService.removeFriend(id: self.user_id, completion: {
                self.dismissProgress()
                self.friend_status = .not_friend
            }, failure: { (error) in
                self.showError(error)
            })
        }))
        actionSheet.addAction(UIAlertAction(title: "Huỷ", style: .destructive, handler: { (action: UIAlertAction) in
            
        }))
        
        if let popoverController = actionSheet.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.height, width: 0, height: 0)
        }
        present(actionSheet, animated: true, completion: nil)
    }
}

extension ThongTinCaNhanViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if user == nil {
            return 0
        }
        if section == 0 {return 1}
        return posts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ThongTinCaNhanTopTableViewCell", for: indexPath) as! ThongTinCaNhanTopTableViewCell
            
            cell.user = user
            cell.status = friend_status
            
            cell.didSelectButtonAtIndex = {[weak self](index) in
                if let strongSelf = self {
                    if index == 0 {
                        strongSelf.openNewCommunity()
                    } else if index == 1 {
                        strongSelf.doFriendButton()
                    } else if index == 2 {
                        strongSelf.doCall(strongSelf.user.phone)
                    } else if index == 3 {
                        strongSelf.openSetting()
                    } else if index == 4 {
                        strongSelf.doMessageButton()
                    }
                }
            }
            cell.doCallUser = {
                self.doCall(self.user.phone)
            }
            cell.gianHangDiemBanButton.addTarget(self, action: #selector(doGianHangDiemBanButton), for: .touchUpInside)
            
            return cell
        }
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseCellIdentifier, for: indexPath) as! CongDongTableViewCell
        
        cell.community = posts[indexPath.row]
        cell.didSelectImageAt = {[weak self](index) in
            if let strongSelf = self {
                strongSelf.openMedia(images: strongSelf.posts[indexPath.row].images, index: index)
            }
        }
        
        cell.productView.tag = indexPath.row
        cell.profileButton.tag = indexPath.row
//        cell.productView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openProduct(_:))))
//        cell.profileButton.addTarget(self, action: #selector(openProfile(_:)), for: .touchUpInside)
        cell.likeButton.isHidden = true
        cell.shareButton.tag = indexPath.row
        cell.shareButton.addTarget(self, action: #selector(doShareButton(_:)), for: .touchUpInside)
        
        if can_load_more && posts.count - 1 == indexPath.row {
            loadData(next: true)
        }
        
        return cell
        
    }
}

extension ThongTinCaNhanViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1 {
            let item = posts[indexPath.row]
            if item.product != nil {
                guard let chiTietSanPhamViewController = ChiTietSanPhamViewController.viewController(.home) as? ChiTietSanPhamViewController else {return}
                chiTietSanPhamViewController.hidesBottomBarWhenPushed = true
                chiTietSanPhamViewController.product_id = item.product!.id
                navigationController?.pushViewController(chiTietSanPhamViewController, animated: true)
            } else {
                guard let chiTietBaiVietViewController = ChiTietCongDongViewController.viewController(.congdong) as? ChiTietCongDongViewController else {return}
                chiTietBaiVietViewController.hidesBottomBarWhenPushed = true
                chiTietBaiVietViewController.id = posts[indexPath.row].id
                chiTietBaiVietViewController.completion = {(liked, like_count, comment_count, share_count) in
                    self.posts[indexPath.row].liked = liked
                    self.posts[indexPath.row].like_count = like_count
                    self.posts[indexPath.row].comment_count = comment_count
                    self.posts[indexPath.row].share_count = share_count
                    self.tableView.reloadRows(at: [indexPath], with: .none)
                }
                navigationController?.pushViewController(chiTietBaiVietViewController, animated: true)
            }
        }
    }
}

extension ThongTinCaNhanViewController: FBSDKSharingDelegate {
    func sharer(_ sharer: FBSDKSharing!, didCompleteWithResults results: [AnyHashable : Any]!) {
        HCService.shareProductCount(id: self.posts[productSharingIndex].product!.id, completion: { [weak self](count) in
            if let strongSelf = self {
                strongSelf.posts[strongSelf.productSharingIndex].share_count += 1
                strongSelf.tableView.reloadData()
            }
        }) { (error) in
            
        }
    }
    
    func sharer(_ sharer: FBSDKSharing!, didFailWithError error: Error!) {
        
    }
    
    func sharerDidCancel(_ sharer: FBSDKSharing!) {
        
    }
}
