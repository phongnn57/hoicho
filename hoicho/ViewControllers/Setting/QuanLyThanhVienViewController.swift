//
//  QuanLyThanhVienViewController.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 4/26/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

class QuanLyThanhVienViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var refreshControl: UIRefreshControl!
    var loadMoreIndicator: UIActivityIndicatorView!
    
    var offset = 0
    var can_load_more = true
    var datasource = [UserModel]() {
        didSet {
            tableView?.reloadData()
        }
    }
    var sort: (Date?, Date?, String, String, RegionModel)?
    var searchItem: UIBarButtonItem!
    var searchBar: UISearchBar!
    var cancelItem: UIBarButtonItem!
    
    var name = "" {
        didSet {
            doRefresh()
        }
    }
    var isSelectAdmin = false
    var didSelectUser: ((UserModel) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        loadData()
    }

    func setupUI() {
        if isSelectAdmin {
            title = "Chọn nhân viên"
        } else {
            title = "Quản lý thành viên"
        }
        
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 200
        tableView.tableFooterView = UIView()
        
        refreshControl = initRefreshControl()
        loadMoreIndicator = initLoadMoreIndicator()
        
        if #available(iOS 10, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        
        searchItem = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(doSearchItem))
        navigationItem.rightBarButtonItem = searchItem
        searchBar = UISearchBar()
        searchBar.placeholder = "Tìm theo tên"
        searchBar.sizeToFit()
        searchBar.searchBarStyle = .minimal
        searchBar.delegate = self
        cancelItem = UIBarButtonItem(title: "Huỷ", style: .plain, target: self, action: #selector(doCancelButtonItem))
    }
    
    @objc func doCancelButtonItem() {
        searchBar.text = nil
        searchBar.resignFirstResponder()
        navigationItem.titleView = nil
        navigationItem.rightBarButtonItem = searchItem
    }
    
    @objc func doSearchItem() {
        navigationItem.titleView = searchBar
        navigationItem.rightBarButtonItem = cancelItem
        searchBar.becomeFirstResponder()
    }
    
    func loadData(refresh: Bool = false, next: Bool = false) {
        if !refresh && !next {
            showProgress()
        } else if next {
            tableView.tableFooterView = loadMoreIndicator
        }
        
        HCService.getMembers(offset, start_time: sort?.0, end_time: sort?.1, phone: sort?.2, name: name, region: sort?.4, completion: { [weak self](objects, total) in
            if let strongSelf = self {
                strongSelf.dismissProgress()
                strongSelf.tableView.tableFooterView = UIView()
                strongSelf.refreshControl.endRefreshing()
                if strongSelf.offset == 0 {
                    strongSelf.datasource = objects
                } else {
                    strongSelf.datasource.append(contentsOf: objects)
                }
                if objects.count == Constant.limit {
                    strongSelf.can_load_more = true
                    strongSelf.offset += Constant.limit
                } else {
                    strongSelf.can_load_more = false
                }
            }
            
        }) { (error) in
            self.refreshControl.endRefreshing()
            self.tableView.tableFooterView = UIView()
            self.showError(error)
        }
    }
    
    override func doRefresh() {
        offset = 0
        loadData(refresh: true)
    }
    

}

extension QuanLyThanhVienViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datasource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "QuanLyThanhVienTableViewCell", for: indexPath) as! QuanLyThanhVienTableViewCell
        
        let item = datasource[indexPath.row]
        
        cell.avatarImageView.sd_setImage(with: URL(string: item.image), placeholderImage: #imageLiteral(resourceName: "icon_default_avatar"))
        cell.nameLabel.text = item.name
        cell.phoneLabel.text = item.phone
        cell.regionLabel.text = item.region
        
        if indexPath.row == datasource.count - 1 && can_load_more {
            loadData(next: true)
        }
        
        return cell
    }
}

extension QuanLyThanhVienViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isSelectAdmin {
            didSelectUser?(datasource[indexPath.row])
            self.navigationController?.popViewController(animated: true)
        } else {
            guard let thongTinCaNhanViewController = ThongTinCaNhanViewController.viewController(.setting) as? ThongTinCaNhanViewController else {return}
            thongTinCaNhanViewController.hidesBottomBarWhenPushed = true
            thongTinCaNhanViewController.user_id = datasource[indexPath.row].id
            navigationController?.pushViewController(thongTinCaNhanViewController, animated: true)
        }
        
    }
}

extension QuanLyThanhVienViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        name = searchText
    }
}
