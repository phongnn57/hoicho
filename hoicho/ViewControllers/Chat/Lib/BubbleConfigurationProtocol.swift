//
//  BubbleConfigurationProtocol.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 6/5/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import Foundation
import UIKit

/** Configures a bubble for a ContentNode. Implement this to create your own bubble configuration */
public protocol BubbleConfigurationProtocol {
    var isMasked: Bool {get set}
    
    /** Create and return a UI color representing an incoming message */
    func getIncomingColor() -> UIColor
    
    /** Create and return a UI color representing an outgoing message */
    func getOutgoingColor() -> UIColor
    
    /** Create and return a bubble for the ContentNode */
    func getBubble() -> Bubble
    
    /** Create and return a bubble that is used by the Message group for Message nodes after the first. This is typically used to "stack" messages */
    func getSecondaryBubble() -> Bubble
}

/** Uses a simple bubble as primary and a simple bubble as secondary. Incoming color is pale grey and outgoing is mid grey */
open class ImageBubbleConfiguration: BubbleConfigurationProtocol {
    
    open var isMasked = false
    
    public init() {}
    
    open func getIncomingColor() -> UIColor
    {
        return UIColor.n1PaleGreyColor()
    }
    
    open func getOutgoingColor() -> UIColor
    {
        return UIColor.n1ActionBlueColor()
    }
    
    open func getBubble() -> Bubble
    {
        let newBubble = ImageBubble()
        newBubble.bubbleImage = #imageLiteral(resourceName: "icon_message_bubble")
        newBubble.cutInsets = UIEdgeInsetsMake(29, 32, 25, 43)
        newBubble.hasLayerMask = isMasked
        return newBubble
    }
    
    open func getSecondaryBubble() -> Bubble
    {
        let newBubble = ImageBubble()
        newBubble.bubbleImage = #imageLiteral(resourceName: "icon_message_bubble")
        newBubble.cutInsets = UIEdgeInsetsMake(29, 32, 25, 43)
        newBubble.hasLayerMask = isMasked
        return newBubble
    }
}

/** Uses a simple bubble as primary and a simple bubble as secondary. Incoming color is pale grey and outgoing is mid grey */
open class SimpleBubbleConfiguration: BubbleConfigurationProtocol {
    
    open var isMasked = false
    
    public init() {}
    
    open func getIncomingColor() -> UIColor
    {
        return UIColor.n1PaleGreyColor()
    }
    
    open func getOutgoingColor() -> UIColor
    {
        return UIColor.n1MidGreyColor()
    }
    
    open func getBubble() -> Bubble
    {
        let newBubble = SimpleBubble()
        newBubble.hasLayerMask = isMasked
        return newBubble
    }
    
    open func getSecondaryBubble() -> Bubble
    {
        let newBubble = SimpleBubble()
        newBubble.hasLayerMask = isMasked
        return newBubble
    }
}

/** Uses a default bubble as primary and a stacked bubble as secondary. Incoming color is pale grey and outgoing is blue */
open class StandardBubbleConfiguration: BubbleConfigurationProtocol {
    
    open var isMasked = false
    
    public init() {}
    
    open func getIncomingColor() -> UIColor
    {
        return UIColor.n1PaleGreyColor()
    }
    
    open func getOutgoingColor() -> UIColor
    {
        return UIColor.n1ActionBlueColor()
    }
    
    open func getBubble() -> Bubble
    {
        let newBubble = DefaultBubble()
        newBubble.hasLayerMask = isMasked
        return newBubble
    }
    
    open func getSecondaryBubble() -> Bubble
    {
        let newBubble = StackedBubble()
        newBubble.hasLayerMask = isMasked
        return newBubble
    }
}





