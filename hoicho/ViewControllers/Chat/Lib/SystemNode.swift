//
//  SystemNode.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 6/6/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

import UIKit
import AsyncDisplayKit

//MARK: MessageNode
/**
 Base message class for N Messenger. Extends GeneralMessengerCell. Holds one message
 */
open class SystemNode: GeneralMessengerCell {
    
    // MARK: Public Variables
    open weak var delegate: MessageCellProtocol?
    
    /** ASDisplayNode as the content of the cell*/
    open var contentNode: ContentNode? {
        didSet {
            self.setNeedsLayout()
        }
    }

    /** Message offset from edge (edge->offset->message content). Defaults to 10*/
    open var messageOffset: CGFloat = 10 {
        didSet {
            self.setNeedsLayout()
        }
    }

    /** Max message height. Defaults to 100000.0 */
    open var maxHeight: CGFloat = 10000.0 {
        didSet {
            self.setNeedsLayout()
        }
    }

    // MARK: Initializers
    
    /**
     Initialiser for the cell
     */
    public init(content: ContentNode) {
        super.init()
        self.setupMessageNode(withContent: content)
    }
    
    
    // MARK: Initializer helper method
    
    /**
     Creates background node and avatar node if they do not exist
     */
    fileprivate func setupMessageNode(withContent content: ContentNode)
    {
        self.contentNode = content
        self.automaticallyManagesSubnodes = true
    }
    
    // MARK: Override AsycDisaplyKit Methods
    
    func createSpacer() -> ASLayoutSpec{
        let spacer = ASLayoutSpec()
        spacer.style.flexGrow = 0
        spacer.style.flexShrink = 0
        spacer.style.minHeight = ASDimension(unit: .points, value: 0)
        return spacer
    }
    
    /**
     Overriding layoutSpecThatFits to specifiy relatiohsips between elements in the cell
     */
    override open func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        var layoutSpecs: ASLayoutSpec!
        
        //location dependent on sender
        let justifyLocation = isIncomingMessage ? ASStackLayoutJustifyContent.start : ASStackLayoutJustifyContent.end
        
        let width = constrainedSize.max.width - self.cellPadding.left - self.cellPadding.right - self.messageOffset
        
        contentNode?.style.maxWidth = ASDimension(unit: .points, value: width)
        contentNode?.style.maxHeight = ASDimension(unit: .points, value: self.maxHeight)
        
        contentNode?.style.flexGrow = 1
        
        let contentSizeLayout = ASAbsoluteLayoutSpec()
        contentSizeLayout.sizing = .sizeToFit
        contentSizeLayout.children = [self.contentNode!]
        
        layoutSpecs = ASStackLayoutSpec(direction: .horizontal, spacing: 0, justifyContent: justifyLocation, alignItems: .end, children: [createSpacer(), contentSizeLayout])
        contentSizeLayout.style.flexShrink = 1
        

        let cellOrientation = isIncomingMessage ? [createSpacer(), layoutSpecs!] : [layoutSpecs!, createSpacer()]
        let layoutSpecs2 = ASStackLayoutSpec(direction: .horizontal, spacing: self.messageOffset, justifyContent: justifyLocation, alignItems: .end, children: cellOrientation)
        return ASInsetLayoutSpec(insets: self.cellPadding, child: layoutSpecs2)
    }
    
    
    // MARK: UILongPressGestureRecognizer Override Methods
    
    /**
     Overriding didLoad to add UILongPressGestureRecognizer to view
     */
    override open func didLoad() {
        super.didLoad()
        
        let longPressRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(messageNodeLongPressSelector(_:)))
        view.addGestureRecognizer(longPressRecognizer)
        if contentNode != nil && contentNode!.isKind(of: CollectionViewContentNode.self) {
            // do nothing
        } else {
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(messageNodeTapSelector(_:)))
            view.addGestureRecognizer(tapGesture)
        }
    }
    
    @objc open func messageNodeTapSelector(_ sender: UITapGestureRecognizer) {
        //contentNode?.messageNodeTapSelector(sender)
        
        if let urlString = (self.contentNode as? NetworkImageContentNode)?.url?.absoluteString {
            guard let imagesViewController = MediaViewController.viewController(.common) as? MediaViewController else {
                return
            }
            
            imagesViewController.images = [urlString]
            self.currentViewController?.present(imagesViewController, animated: true, completion: nil)
        } else {
            //contentNode?.messageNodeTapSelector(sender)
        }
    }
    
    /**
     Overriding canBecomeFirstResponder to make cell first responder
     */
    override open func canBecomeFirstResponder() -> Bool {
        return true
    }
    
    /**
     Overriding touchesBegan to make to close UIMenuController
     */
    override open func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        if UIMenuController.shared.isMenuVisible == true {
            UIMenuController.shared.setMenuVisible(false, animated: true)
        }
    }
    
    /**
     Overriding touchesEnded to make to handle events with UIMenuController
     */
    override open func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        if UIMenuController.shared.isMenuVisible == false {
            
        }
    }
    /**
     Overriding touchesCancelled to make to handle events with UIMenuController
     */
    override open func touchesCancelled(_ touches: Set<UITouch>?, with event: UIEvent?) {
        super.touchesCancelled(touches, with: event)
        
        if UIMenuController.shared.isMenuVisible == false {
            
        }
    }
    
    
    // MARK: UILongPressGestureRecognizer Selector Methods
    
    /**
     UILongPressGestureRecognizer selector
     - parameter recognizer: Must be an UITapGestureRecognizer.
     Can be be overritten when subclassed
     */
    @objc open func messageNodeLongPressSelector(_ recognizer: UITapGestureRecognizer) {
        contentNode?.messageNodeLongPressSelector(recognizer)
    }
}



