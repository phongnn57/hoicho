//
//  Bubble.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 6/5/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit
import AsyncDisplayKit

open class Bubble {
    
    // MARK: Public Parameters
    open var bubbleColor : UIColor = UIColor.n1PaleGreyColor()
    
    /** When this is set, the layer mask will mask the ContentNode.*/
    open var hasLayerMask = false
    
    /**
     A layer for the bubble. Make sure this property is first accessed on the main thread.
     */
    open lazy var layer: CAShapeLayer = CAShapeLayer()
    /**
     A layer that holds a mask which is the same shape as the bubble. This can be used to mask anything in the ContentNode to the same shape as the bubble.
     */
    open lazy var maskLayer: CAShapeLayer = CAShapeLayer()
    
    /** Bounds of the bubble*/
    open var calculatedBounds = CGRect.zero
    
    // MARK: Initialisers
    public init() {}
    
    // MARK: Class methods
    /**
     Sizes the layer accordingly. This function should **always** be thread safe.
     -parameter bounds: The bounds of the content
     */
    open func sizeToBounds(_ bounds: CGRect) {
        self.calculatedBounds = bounds
    }
    
    
    /**
     This function should be called on the  main thread. It makes creates the layer with the calculated values from *sizeToBounds*
     */
    open func createLayer() {
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
        
        self.maskLayer.shouldRasterize = true
        self.maskLayer.rasterizationScale = UIScreen.main.scale
    }
    
}

open class DefaultBubble: Bubble {
    
    //MARK: Public Variables
    
    /** Radius of the corners for the bubble. When this is set, you will need to call setNeedsLayout on your message for changes to take effect if the bubble has already been drawn*/
    open var radius : CGFloat = 16
    /** Should be less or equal to the *radius* property. When this is set, you will need to call setNeedsLayout on your message for changes to take effect if the bubble has already been drawn*/
    open var borderWidth : CGFloat = 0 //TODO:
    /** The color of the border around the bubble. When this is set, you will need to call setNeedsLayout on your message for changes to take effect if the bubble has already been drawn*/
    open var bubbleBorderColor : UIColor = UIColor.clear
    /** Path used to cutout the bubble*/
    open fileprivate(set) var path: CGMutablePath = CGMutablePath()
    
    // MARK: Initialisers
    
    /**
     Initialiser class.
     */
    public override init() {
        super.init()
    }
    
    // MARK: Class methods
    
    /**
     Overriding sizeToBounds from super class
     -parameter bounds: The bounds of the content
     */
    open override func sizeToBounds(_ bounds: CGRect) {
        super.sizeToBounds(bounds)
        
        var rect = CGRect.zero
        var radius2: CGFloat = 0
        
        if bounds.width < 2*radius || bounds.height < 2*radius { //if the rect calculation yeilds a negative result
            let newRadiusW = bounds.width/2
            let newRadiusH = bounds.height/2
            
            let newRadius = newRadiusW>newRadiusH ? newRadiusH : newRadiusW
            
            rect = CGRect(x: newRadius, y: newRadius, width: bounds.width - 2*newRadius, height: bounds.height - 2*newRadius)
            radius2 = newRadius - borderWidth / 2
        } else {
            rect = CGRect(x: radius, y: radius, width: bounds.width - 2*radius, height: bounds.height - 2*radius)
            radius2 = radius - borderWidth / 2
        }
        
        path = CGMutablePath()
        
        path.addArc(center: CGPoint(x: rect.maxX, y: rect.minY), radius: radius2, startAngle: CGFloat(-Double.pi/2), endAngle: 0, clockwise: false)
        path.addLine(to: CGPoint(x: rect.maxX + radius2, y: rect.maxY + radius2))
        path.addArc(center: CGPoint(x: rect.minX, y: rect.maxY), radius: radius2, startAngle: CGFloat(Double.pi/2), endAngle: CGFloat(Double.pi), clockwise: false)
        path.addArc(center: CGPoint(x: rect.minX, y: rect.minY), radius: radius2, startAngle: CGFloat(Double.pi), endAngle: CGFloat(-Double.pi/2), clockwise: false)
        
        //CGPathAddArc(path, nil, rect.maxX, rect.minY, radius2, CGFloat(-M_PI_2), 0, false)
        //CGPathAddLineToPoint(path, nil, rect.maxX + radius2, rect.maxY + radius2)
        //CGPathAddArc(path, nil, rect.minX, rect.maxY, radius2, CGFloat(M_PI_2), CGFloat(M_PI), false)
        //CGPathAddArc(path, nil, rect.minX, rect.minY, radius2, CGFloat(M_PI), CGFloat(-M_PI_2), false)
        path.closeSubpath()
    }
    
    /**
     Overriding createLayer from super class
     */
    open override func createLayer() {
        super.createLayer()
        
        CATransaction.begin()
        CATransaction.setDisableActions(true)
        self.layer.path = path
        self.layer.fillColor = self.bubbleColor.cgColor
        self.layer.strokeColor = self.bubbleBorderColor.cgColor
        self.layer.lineWidth = self.borderWidth
        self.layer.position = CGPoint.zero
        
        self.maskLayer.fillColor = UIColor.black.cgColor
        self.maskLayer.path = path
        self.maskLayer.position = CGPoint.zero
        CATransaction.commit()
        
    }
}

open class ImageBubble : Bubble {
    
    // MARK: Public variables
    /** Image 9-Patch used for bubble. When this is set, you will need to call setNeedsLayout on your message for changes to take effect if the bubble has already been drawn*/
    open var bubbleImage: UIImage?
    /** Image 9-Patch cut insets. When this is set, you will need to call setNeedsLayout on your message for changes to take effect if the bubble has already been drawn*/
    open var cutInsets: UIEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0)
    
    // MARK: Initialisers
    
    /**
     Initialiser class.
     Sets hasLayerMask to true
     */
    public override init() {
        super.init()
        self.hasLayerMask = true
    }
    
    // MARK: Class methods
    
    /**
     Overriding sizeToBounds from super class
     -parameter bounds: The bounds of the content
     */
    open override func sizeToBounds(_ bounds: CGRect) {
        super.sizeToBounds(bounds)
        
    }
    
    /**
     Overriding createLayer from super class
     */
    open override func createLayer() {
        super.createLayer()
        
        if let image = bubbleImage {
            UIGraphicsBeginImageContextWithOptions(image.size, false, image.scale);
            let context = UIGraphicsGetCurrentContext();
            self.bubbleColor.setFill()
            context?.translateBy(x: 0, y: image.size.height);
            context?.scaleBy(x: 1.0, y: -1.0);
            context?.clip(to: CGRect(x: 0, y: 0, width: image.size.width, height: image.size.height), mask: image.cgImage!);
            context?.fill(CGRect(x: 0, y: 0, width: image.size.width, height: image.size.height));
            
            var coloredImg = UIGraphicsGetImageFromCurrentImageContext();
            
            UIGraphicsEndImageContext();
            
            
            let insets = self.cutInsets
            coloredImg = coloredImg?.resizableImage(withCapInsets: insets, resizingMode: .stretch)
            
            self.layer.contents = coloredImg?.cgImage
            self.layer.position = CGPoint.zero
            self.layer.frame = CGRect(x: 0, y: 0, width: self.calculatedBounds.width, height: self.calculatedBounds.height)
            self.layer.contentsCenter = CGRect(x: insets.left/(coloredImg?.size.width)!,
                                               y: insets.top/(coloredImg?.size.height)!,
                                               width: 1.0/(coloredImg?.size.width)!,
                                               height: 1.0/(coloredImg?.size.height)!);
            
            self.maskLayer.contents = coloredImg?.cgImage
            self.maskLayer.position = CGPoint.zero
            self.maskLayer.frame = CGRect(x: 0, y: 0, width: self.calculatedBounds.width, height: self.calculatedBounds.height)
            self.maskLayer.contentsCenter = CGRect(x: insets.left/(coloredImg?.size.width)!,
                                                   y: insets.top/(coloredImg?.size.height)!,
                                                   width: 1.0/(coloredImg?.size.width)!,
                                                   height: 1.0/(coloredImg?.size.height)!);
        }
    }
    
    
}

open class SimpleBubble: Bubble {
    
    //MARK: Public Variables
    /** The color of the border around the bubble. When this is set, you will need to call setNeedsLayout on your message for changes to take effect if the bubble has already been drawn*/
    open var bubbleBorderColor : UIColor = UIColor.clear
    /** Path used to cutout the bubble*/
    open fileprivate(set) var path: CGMutablePath = CGMutablePath()
    
    public override init() {
        super.init()
    }
    
    // MARK: Class methods
    
    /**
     Overriding sizeToBounds from super class
     -parameter bounds: The bounds of the content
     */
    open override func sizeToBounds(_ bounds: CGRect) {
        super.sizeToBounds(bounds)
        
        let rect = CGRect(x: 0, y: 0, width: bounds.width, height: bounds.height)
        
        path = CGMutablePath()
        
        path.move(to: CGPoint(x: rect.minX, y: rect.minY))
        path.addLine(to: CGPoint(x: rect.maxX, y: rect.minY))
        path.addLine(to: CGPoint(x: rect.maxX, y: rect.maxY))
        path.addLine(to: CGPoint(x: rect.minX, y: rect.maxY))
        
        // CGPathMoveToPoint(path, nil, rect.minX, rect.minY)
        // CGPathAddLineToPoint(path, nil, rect.maxX, rect.minY)
        // CGPathAddLineToPoint(path, nil, rect.maxX, rect.maxY)
        // CGPathAddLineToPoint(path, nil, rect.minX, rect.maxY)
        
        path.closeSubpath()
    }
    
    /**
     Overriding createLayer from super class
     */
    open override func createLayer() {
        super.createLayer()
        
        CATransaction.begin()
        CATransaction.setDisableActions(true)
        self.layer.path = path
        self.layer.fillColor = self.bubbleColor.cgColor
        self.layer.strokeColor = self.bubbleBorderColor.cgColor
        self.layer.position = CGPoint.zero
        
        self.maskLayer.fillColor = UIColor.black.cgColor
        self.maskLayer.path = path
        self.maskLayer.position = CGPoint.zero
        CATransaction.commit()
        
    }
}

open class StackedBubble: Bubble {
    
    //MARK: Public Variables
    
    /** Radius of the corners for the bubble. When this is set, you will need to call setNeedsLayout on your message for changes to take effect if the bubble has already been drawn*/
    open var radius : CGFloat = 16
    /** Should be less or equal to the *radius* property. When this is set, you will need to call setNeedsLayout on your message for changes to take effect if the bubble has already been drawn*/
    open var borderWidth : CGFloat = 0
    /** The color of the border around the bubble. When this is set, you will need to call setNeedsLayout on your message for changes to take effect if the bubble has already been drawn*/
    open var bubbleBorderColor : UIColor = UIColor.clear
    /** Path used to cutout the bubble*/
    open fileprivate(set) var path: CGMutablePath = CGMutablePath()
    
    public override init() {
        super.init()
    }
    
    // MARK: Class methods
    
    /**
     Overriding sizeToBounds from super class
     -parameter bounds: The bounds of the content
     */
    open override func sizeToBounds(_ bounds: CGRect) {
        super.sizeToBounds(bounds)
        var rect = CGRect.zero
        var radius2: CGFloat = 0
        
        if bounds.width < 2*radius || bounds.height < 2*radius { //if the rect calculation yeilds a negative result
            let newRadiusW = bounds.width/2
            let newRadiusH = bounds.height/2
            
            let newRadius = newRadiusW>newRadiusH ? newRadiusH : newRadiusW
            
            rect = CGRect(x: newRadius, y: newRadius, width: bounds.width - 2*newRadius, height: bounds.height - 2*newRadius)
            radius2 = newRadius - borderWidth / 2
        } else {
            rect = CGRect(x: radius, y: radius, width: bounds.width - 2*radius, height: bounds.height - 2*radius)
            radius2 = radius - borderWidth / 2
        }
        
        
        self.path = CGMutablePath();
        
        
        path.move(to: CGPoint(x: rect.minX, y: rect.minY - radius2))
        path.addLine(to: CGPoint(x: rect.maxX + radius2, y: rect.minY - radius2))
        path.addLine(to: CGPoint(x: rect.maxX + radius2, y: rect.maxY + radius2))
        path.addArc(center: CGPoint(x: rect.minX, y: rect.maxY), radius: radius2, startAngle: CGFloat(Double.pi/2), endAngle: CGFloat(Double.pi), clockwise: false)
        path.addArc(center: CGPoint(x: rect.minX, y: rect.minY), radius: radius2, startAngle: CGFloat(Double.pi), endAngle: CGFloat(-(Double.pi/2)), clockwise: false)
        
        //CGPathMoveToPoint(path, nil, rect.minX, rect.minY - radius2)
        //CGPathAddLineToPoint(path, nil, rect.maxX + radius2, rect.minY - radius2)
        //CGPathAddLineToPoint(path, nil, rect.maxX + radius2, rect.maxY + radius2)
        //CGPathAddArc(path, nil, rect.minX, rect.maxY, radius2, CGFloat(M_PI_2), CGFloat(M_PI), false)
        //CGPathAddArc(path, nil, rect.minX, rect.minY, radius2, CGFloat(M_PI), CGFloat(-M_PI_2), false)
        path.closeSubpath()
        
    }
    
    /**
     Overriding createLayer from super class
     */
    open override func createLayer() {
        super.createLayer()
        
        CATransaction.begin()
        CATransaction.setDisableActions(true)
        self.layer.path = path
        self.layer.fillColor = self.bubbleColor.cgColor
        self.layer.strokeColor = self.bubbleBorderColor.cgColor
        self.layer.lineWidth = self.borderWidth
        self.layer.position = CGPoint.zero
        
        self.maskLayer.fillColor = UIColor.black.cgColor
        self.maskLayer.path = path
        self.maskLayer.position = CGPoint.zero
        CATransaction.commit()
    }
}



