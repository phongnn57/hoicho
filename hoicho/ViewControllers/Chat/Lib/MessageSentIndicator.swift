//
//  MessageSentIndicator.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 6/5/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit
import AsyncDisplayKit

//MARK: HeadLoadingIndicator class
/**
 Spinning loading indicator class. Used by the NMessenger prefetch.
 */
open class MessageSentIndicator: GeneralMessengerCell {
    /** Horizontal spacing between text and spinner. Defaults to 20.*/
    open var contentPadding:CGFloat = 20 {
        didSet {
            self.setNeedsLayout()
        }
    }
    /** Loading text node*/
    open let text = ASTextNode()
    /** Sets the loading attributed text for the spinner. Defaults to *"Loading..."* */
    open var messageSentAttributedText:NSAttributedString? {
        set {
            text.attributedText = newValue
            self.setNeedsLayout()
        } get {
            return text.attributedText
        }
    }
    open var messageSentText: String? {
        set {
            text.attributedText = NSAttributedString(
                string: newValue != nil ? newValue! : "",
                attributes: [
                    NSAttributedStringKey.font: UIFont.systemFont(ofSize: 14),
                    NSAttributedStringKey.foregroundColor: UIColor.lightGray,
                    NSAttributedStringKey.kern: -0.3
                ])
            self.setNeedsLayout()
        } get {
            return text.attributedText?.string
        }
    }
    
    public override init() {
        super.init()
        addSubnode(text)
    }
    
    override open func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        let stackLayout = ASStackLayoutSpec(
            direction: .horizontal,
            spacing: contentPadding,
            justifyContent: .center,
            alignItems: .center,
            children: [ text ])
        let paddingLayout = ASInsetLayoutSpec(insets: cellPadding, child: stackLayout)
        return paddingLayout
    }
}

