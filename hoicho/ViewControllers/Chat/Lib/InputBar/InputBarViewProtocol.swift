//
//  InputBarViewProtocol.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 6/5/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import Foundation
import UIKit

@objc public protocol InputBarViewProtocol
{
    /* Superview of textInputView - can hold send button and/or attachment button*/
    var textInputAreaView: UIView! {get set}
    /* UITextView where the user will input the text*/
    var textInputView: UITextView! {get set}
    //NMessengerViewController where to input is sent to
    var controller:MessengerViewController! {get set}
}
