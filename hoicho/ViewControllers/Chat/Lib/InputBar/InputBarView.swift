//
//  InputBarView.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 6/5/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

//MARK: InputBarView
/**
 InputBarView class for NMessenger.
 Define the input bar for NMessenger. This is where the user would type text and open the camera or photo library.
 */
open class InputBarView: UIView, InputBarViewProtocol {
    
    //MARK: IBOutlets
    //@IBOutlets for input area view
    @IBOutlet open weak var textInputAreaView: UIView!
    //@IBOutlets for input view
    @IBOutlet open weak var textInputView: UITextView!
    
    //MARK: Public Parameters
    
    //MARK: Private Parameters
    //NMessengerViewController where to input is sent to
    open weak var controller:MessengerViewController!
    
    // MARK: Initialisers
    /**
     Initialiser the view.
     - parameter controller: Must be NMessengerViewController. Sets controller for the view.
     Calls helper method to setup the view
     */
    public required init()
    {
        super.init(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
    }
    
    public required init(controller:MessengerViewController) {
        super.init(frame: CGRect.zero)
        self.controller = controller
    }
    /**
     Initialiser the view.
     - parameter controller: Must be NMessengerViewController. Sets controller for the view.
     - parameter controller: Must be CGRect. Sets frame for the view.
     Calls helper method to setup the view
     */
    public required init(controller:MessengerViewController,frame: CGRect) {
        super.init(frame: frame)
        self.controller = controller
    }
    /**
     - parameter aDecoder: Must be NSCoder
     Calls helper method to setup the view
     */
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
}
