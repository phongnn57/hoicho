//
//  HeadLoadingIndicator.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 6/5/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit
import AsyncDisplayKit
//MARK: HeadLoadingIndicator class
/**
 Spinning loading indicator class. Used by the NMessenger prefetch.
 */
open class HeadLoadingIndicator: GeneralMessengerCell {
    /** Horizontal spacing between text and spinner. Defaults to 20.*/
    open var contentPadding:CGFloat = 20 {
        didSet {
            self.setNeedsLayout()
        }
    }
    /** Animated spinner node*/
    open let spinner = SpinnerNode()
    /** Loading text node*/
    open let text = ASTextNode()
    /** Sets the loading attributed text for the spinner. Defaults to *"Loading..."* */
    open var loadingAttributedText:NSAttributedString? {
        set {
            text.attributedText = newValue
            self.setNeedsLayout()
        } get {
            return text.attributedText
        }
    }
    
    public override init() {
        super.init()
        addSubnode(text)
        text.attributedText = NSAttributedString(
            string: "Loading…",
            attributes: [
                NSAttributedStringKey.font: UIFont.systemFont(ofSize: 12),
                NSAttributedStringKey.foregroundColor: UIColor.lightGray,
                NSAttributedStringKey.kern: -0.3
            ])
        addSubnode(spinner)
    }
    
    override open func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        let stackLayout = ASStackLayoutSpec(
            direction: .horizontal,
            spacing: contentPadding,
            justifyContent: .center,
            alignItems: .center,
            children: [ text, spinner ])
        let paddingLayout = ASInsetLayoutSpec(insets: cellPadding, child: stackLayout)
        return paddingLayout
    }
}

//MARK: SpinnerNode class
/**
 Animated spinner. Used by HeadLoadingIndicator. Defaults to *preferredFrameSize.height=32*
 */
open class SpinnerNode: ASDisplayNode {
    open var activityIndicatorView: UIActivityIndicatorView {
        return view as! UIActivityIndicatorView
    }
    
    public override init() {
        super.init()
        self.setViewBlock({ UIActivityIndicatorView(activityIndicatorStyle: .gray) })
        self.style.preferredSize.height = 32
    }
    
    override open func didLoad() {
        super.didLoad()
        activityIndicatorView.startAnimating()
    }
}
