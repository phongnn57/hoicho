//
//  ProductContentNode.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 6/6/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit
import AsyncDisplayKit

open class ProductContentNode: ContentNode {
    
    open var product: ProductModel = ProductModel()

    
    open var insets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5) {
        didSet {
            setNeedsLayout()
        }
    }
    // MARK: Private Variables
    /** ASImageNode as the content of the cell*/
    open fileprivate(set) var productImageNode: ASNetworkImageNode = ASNetworkImageNode()
    open fileprivate(set) var productNameNode: ASTextNode = ASTextNode()
    open fileprivate(set) var productPriceNode: ASTextNode = ASTextNode()
    
    /** UIFont for incoming text messages*/
    open var incomingTextFont = UIFont.systemFont(ofSize: 14.0, weight: UIFont.Weight.regular) {
        didSet {
            self.updateAttributedText()
        }
    }
    /** UIFont for outgoinf text messages*/
    open var outgoingTextFont = UIFont.systemFont(ofSize: 14.0, weight: UIFont.Weight.regular) {
        didSet {
            self.updateAttributedText()
        }
    }
    /** UIColor for incoming text messages*/
    open var incomingTextColor = UIColor(hex: "#333333") {
        didSet {
            self.updateAttributedText()
        }
    }
    /** UIColor for outgoinf text messages*/
    open var outgoingTextColor = UIColor.n1WhiteColor() {
        didSet {
            self.updateAttributedText()
        }
    }

    /** Overriding from super class
     Set backgroundBubble.bubbleColor and the text color when valus is set
     */
    open override var isIncomingMessage: Bool
        {
        didSet {
            if isIncomingMessage
            {
                self.backgroundBubble?.bubbleColor = self.bubbleConfiguration.getIncomingColor()
                self.updateAttributedText()
            } else {
                self.backgroundBubble?.bubbleColor = self.bubbleConfiguration.getOutgoingColor()
                self.updateAttributedText()
            }
        }
    }
    
    open override var isSystemMessage: Bool {
        didSet {
            if isSystemMessage {
                self.backgroundBubble?.bubbleColor = UIColor.clear
                self.updateAttributedText()
            } else if isIncomingMessage {
                self.backgroundBubble?.bubbleColor = self.bubbleConfiguration.getIncomingColor()
                self.updateAttributedText()
            } else {
                self.backgroundBubble?.bubbleColor = self.bubbleConfiguration.getOutgoingColor()
                self.updateAttributedText()
            }
        }
    }
    
    /**
     Initialiser for the cell.
     - parameter image: Must be UIImage. Sets image for cell.
     Calls helper method to setup cell
     */
    public init(product: ProductModel, isIncomming: Bool, bubbleConfiguration: BubbleConfigurationProtocol? = nil) {
        
        super.init(bubbleConfiguration: bubbleConfiguration)
        self.product = product
        self.isIncomingMessage = isIncomming
        self.setupProductNode(product)
    }
    
    /**
     Sets the image to be display in the cell. Clips and rounds the corners.
     - parameter image: Must be UIImage. Sets image for cell.
     */
    fileprivate func setupProductNode(_ product: ProductModel)
    {
        productImageNode.url = URL(string: product.image)
        productImageNode.defaultImage = #imageLiteral(resourceName: "icon_no_image")
        productImageNode.shouldCacheImage = true
        productImageNode.delegate = self
        
        self.backgroundBubble = self.bubbleConfiguration.getBubble()
        
        let fontAndSizeAndTextColor = [ NSAttributedStringKey.font: self.isIncomingMessage ? incomingTextFont : outgoingTextFont, NSAttributedStringKey.foregroundColor: self.isIncomingMessage ? incomingTextColor : outgoingTextColor]
        let outputString = NSMutableAttributedString(string: product.name, attributes: fontAndSizeAndTextColor )

        self.productNameNode.attributedText = outputString
        self.productNameNode.accessibilityIdentifier = "labelMessage"
        self.productNameNode.isAccessibilityElement = true
        
        let fontAndSizeAndTextColor1 = [ NSAttributedStringKey.font: UIFont.systemFont(ofSize: 14.0, weight: UIFont.Weight.semibold), NSAttributedStringKey.foregroundColor: self.isIncomingMessage ? incomingTextColor : outgoingTextColor]
        let outputString1 = NSMutableAttributedString(string: product.price.priceString(), attributes: fontAndSizeAndTextColor1 )
        
        self.productPriceNode.attributedText = outputString1
        self.productPriceNode.accessibilityIdentifier = "labelMessage"
        self.productPriceNode.isAccessibilityElement = true
    
        self.addSubnode(productImageNode)
        self.addSubnode(productNameNode)
        self.addSubnode(productPriceNode)
    }
    
    open override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {

        let productImageWidth: CGFloat = 80
        
        self.productImageNode.style.width = ASDimension(unit: .points, value: productImageWidth)
        self.productImageNode.style.height = ASDimension(unit: .points, value: productImageWidth)
        
        let imageSizeLayout = ASInsetLayoutSpec(insets: insets, child: productImageNode)
        
        let contentWidth = constrainedSize.max.width - insets.left - insets.right - 8 - productImageWidth
        
        productNameNode.style.maxWidth = ASDimension(unit: .points, value: contentWidth)
        let contentSizeLayout = ASInsetLayoutSpec(insets: UIEdgeInsets(top: 5, left: 0, bottom: 5, right: 5), child: productNameNode)
        
        productPriceNode.style.maxWidth = ASDimension(unit: .points, value: contentWidth)
        let priceSizeLayout = ASInsetLayoutSpec(insets: UIEdgeInsets(top: 0, left: 0, bottom: 5, right: 5), child: productPriceNode)
        
        let contentlayoutSpecs = ASStackLayoutSpec(direction: .vertical, spacing: 0, justifyContent: .start, alignItems: .start, children: [contentSizeLayout, priceSizeLayout])

        let justifyLocation = ASStackLayoutJustifyContent.start
        let cellOrientation = [imageSizeLayout, contentlayoutSpecs]
        let layoutSpecs = ASStackLayoutSpec(direction: .horizontal, spacing: 0, justifyContent: justifyLocation, alignItems: .start, children: cellOrientation)
        
        return layoutSpecs
    }
    
    //MARK: Helper Methods
    /** Updates the attributed string to the correct incoming/outgoing settings and lays out the component again*/
    fileprivate func updateAttributedText() {
 
        setNeedsLayout()
    }
    
}

extension ProductContentNode: ASNetworkImageNodeDelegate {
    public func imageNode(_ imageNode: ASNetworkImageNode, didLoad image: UIImage) {
        self.setNeedsLayout()
    }
}
