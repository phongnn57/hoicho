//
//  KhoAnhViewController.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 6/5/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit
import SwiftyJSON

class KhoAnhModel: NSObject {
    var id = 0
    var link = ""
    var selected = false
    
    init(_ json: JSON) {
        id = json["id"].intValue
        link = json["link"].stringValue
    }
}

private let reuseCellIdentifier = "KhoAnhCollectionViewCell"

class KhoAnhViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var sendButton: UIButton!
    var completion: (([String]) -> Void)?
    
    var datasource = [KhoAnhModel]() {
        didSet {
            collectionView?.reloadData()
            if sendButton != nil {
                sendButton.isHidden = datasource.count == 0
            }
        }
    }
    var offset = 0
    var can_load_more = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        loadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupUI() {
        title = "Kho ảnh trên web"
    }
    
    func loadData(loadNext: Bool? = false) {
        if !loadNext! {
            showProgress()
        }
        HCService.getKhoAnh(offset: offset, completion: { (objects) in
            self.dismissProgress()
            if self.offset == 0 {
                self.datasource = objects
            } else {
                self.datasource.append(contentsOf: objects)
            }
            if objects.count == Constant.limit {
                self.can_load_more = true
                self.offset += Constant.limit
            } else {
                self.can_load_more = false
            }
            
        }) { (error) in
            self.showError(error)
        }
        
    }
    
    @IBAction func doSendButton(_ sender: Any) {
        let objects = self.datasource.filter { (object) -> Bool in
            return object.selected
        }
        if objects.count > 0 {
            var temp = [String]()
            for object in objects {
                temp.append(object.link)
            }
            completion?(temp)
            self.dismiss(animated: true, completion: nil)
        } else {
            showAlertWithMessage("Hãy chọn ít nhất 1 ảnh")
        }
    }
    
    @objc func doDeleteButton(_ sender: UIButton) {
        showProgress()
        HCService.xoaKhoAnh(id: self.datasource[sender.tag].id, completion: {
            self.dismissProgress()
            self.datasource.remove(at: sender.tag)
            
        }) { (error) in
            self.showError(error)
        }
    }
    
}

extension KhoAnhViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return datasource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseCellIdentifier, for: indexPath) as! KhoAnhCollectionViewCell
        
        let item = datasource[indexPath.row]
        
        cell.mediaImageView.sd_setImage(with: URL(string: item.link), placeholderImage: #imageLiteral(resourceName: "icon_no_image"))
        cell.checkBoxImageView.image = item.selected == true ? #imageLiteral(resourceName: "icon_checkbox_selected") : #imageLiteral(resourceName: "icon_checkbox_normal")
        
        cell.deleteButton.tag = indexPath.row
        cell.deleteButton.addTarget(self, action: #selector(doDeleteButton(_:)), for: .touchUpInside)
        
        return cell
    }
}

extension KhoAnhViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        datasource[indexPath.row].selected = !datasource[indexPath.row].selected
        collectionView.reloadItems(at: [indexPath])
    }
}

extension KhoAnhViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = ceil(UIScreen.main.bounds.width/3)
        return CGSize(width: width, height: width)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}
