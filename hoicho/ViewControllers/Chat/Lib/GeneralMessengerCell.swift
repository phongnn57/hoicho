//
//  GeneralMessengerCell.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 6/5/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit
import AsyncDisplayKit

/** Protocol used by a message cell to handle various message related actions */
@objc public protocol MessageCellProtocol {
    /**
     Called when the avatar is clicked. Notifies the delegate of the message cell whose avatar is clicked
     */
    @objc optional func avatarClicked(_ messageCell: GeneralMessengerCell)
}

//MARK: GeneralMessengerCell
/**
 GeneralMessengerCell class for NMessenger. Extends ASCellNode.
 Defines the base class for all messages in NMessenger.
 */
open class GeneralMessengerCell: ASCellNode {
    
    // MARK: Public Variables
    /** UIEdgeInsets for cell*/
    open var cellPadding: UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0)
    /** UIViewController that holds the cell. Allows the cell the present View Controllers*/
    open weak var currentViewController: UIViewController?
    /** The current table in which this node resides*/
    open weak var currentTableNode: ASTableNode?
    /** message incoming/outgoing */
    open var isIncomingMessage:Bool = true
    
    // MARK: Initialisers
    /**
     Default Init
     Sets cellPadding to 0 and selectionStyle to None
     */
    public override init() {
        super.init()
        selectionStyle = .none
        cellPadding = UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0)
    }
    /**
     Initialiser for the cell
     - parameter cellPadding: Can be UIEdgeInsets. Sets padding for cell.
     - parameter currentViewController: Can be an UIViewController. Set current view controller holding the cell.
     */
    public convenience init(cellPadding: UIEdgeInsets?, currentViewController: UIViewController?) {
        self.init()
        
        //set cell padding
        if let cellPadding = cellPadding {
            self.cellPadding = cellPadding
        }
        
        //set current view controller
        self.currentViewController = currentViewController
    }
    
}

