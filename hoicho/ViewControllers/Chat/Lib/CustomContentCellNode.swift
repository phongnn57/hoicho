//
//  CustomContentCellNode.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 6/5/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit
import AsyncDisplayKit

//MARK: CustomContentCellNode
/**
 CustomContentCellNode class for N Messenger.
 Define the cell for CollectionViewContentNode
 */
open class CustomContentCellNode: ASCellNode {
    
    // MARK: Public Variables
    /** ASDisplayNode as the content of the cell*/
    open var customContent:ASDisplayNode = ASDisplayNode()
    
    // MARK: Initialisers
    
    /**
     Initialiser for the cell.
     - parameter withCustomNode: Must be ASDisplayNode. Sets content for the cell.
     */
    public init(withCustomNode node:ASDisplayNode)
    {
        super.init()
        self.customContent = node
        self.addSubnode(self.customContent)
    }
    
    // MARK: Override AsycDisaplyKit Methods
    
    /**
     Overriding layoutSpecThatFits to specifiy relatiohsips between elements in the cell
     */
    override open func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        let customContentSpec = ASAbsoluteLayoutSpec()
        customContentSpec.sizing = .sizeToFit
        customContentSpec.children = [customContent]
        return customContentSpec
    }
}

