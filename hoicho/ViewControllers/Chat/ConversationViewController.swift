//
//  ConversationViewController.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 4/18/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit
import DateToolsSwift
import SwiftyJSON

class ConversationViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var refreshControl: UIRefreshControl!
    var loadMoreIndicator: UIActivityIndicatorView!
    
    var offset = 0
    var can_load_more = true
    var datasource = [ConversationModel]() {
        didSet {
            tableView?.reloadData()
        }
    }
    var didSelectConversation: ((ConversationModel) -> Void)?
    var name = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        loadData()
    }
    
    func setupUI() {
        tableView.tableFooterView = UIView()
        
        refreshControl = initRefreshControl()
        loadMoreIndicator = initLoadMoreIndicator()
        if #available(iOS 10, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        NotificationCenter.default.addObserver(self, selector: #selector(didReceivedNewChatMessage(_:)), name: .didReceivedNewChatMessage, object: nil)
    }
    
    @objc func didReceivedNewChatMessage(_ notification: NSNotification) {
        if let object = notification.userInfo as? [String: JSON], let json = object["value"] {
            let receivedConversation = ConversationModel(json)
            if json["from"].intValue != UserModel.shared.id {
                receivedConversation.read = false
            } else {
                receivedConversation.read = true
            }
            
            if json["type"].intValue == 1 {
                self.doRefresh()
            } else {
                let objects = self.datasource.filter({ (object) -> Bool in
                    return object.id_conversations == receivedConversation.id_conversations
                })
                
                if let conversation = objects.first {
                    receivedConversation.copyTo(conversation)
                    var index = 0
                    for (pos, obj) in self.datasource.enumerated() {
                        if obj.id_conversations == conversation.id_conversations {
                            index = pos
                            break
                        }
                    }
                    
                    self.datasource[index] = conversation
                    self.datasource.sort(by: { (object1, object2) -> Bool in
                        if object1.last_msg_time != nil && object2.last_msg_time != nil {
                            return object1.last_msg_time.isAfter(date: object2.last_msg_time)
                        }
                        return true
                    })
                    self.tableView.reloadData()
                } else {
                    self.datasource.insert(receivedConversation, at: 0)
                }
            }

        }
    }
    
    func loadData(refresh: Bool = false, next: Bool = false) {
        if !refresh && !next {
            showProgress()
        } else if !refresh && next {
            tableView.tableFooterView = loadMoreIndicator
        }
        
        HCService.getChatConversations(offset: offset, completion: { [weak self](objects) in
            self?.dismissProgress()
            self?.refreshControl.endRefreshing()
            self?.tableView.tableFooterView = UIView()
            
            if self?.offset == 0 {
                self?.datasource = objects
            } else {
                self?.datasource.append(contentsOf: objects)
            }
            
            if objects.count == Constant.limit {
                self?.can_load_more = true
                self?.offset += Constant.limit
            } else {
                self?.can_load_more = false
            }
        }) { (error) in
            self.dismissProgress()
            self.refreshControl.endRefreshing()
            self.tableView.tableFooterView = UIView()
            self.showError(error)
        }
    }
    
    override func doRefresh() {
        offset = 0
        loadData(refresh: true)
    }

}

extension ConversationViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datasource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ConversationTableViewCell", for: indexPath) as! ConversationTableViewCell
        
        let item = datasource[indexPath.row]
        
        cell.avatarImageView.sd_setImage(with: URL(string: item.image), placeholderImage: #imageLiteral(resourceName: "icon_default_avatar"))
        cell.nameLabel.text = item.name
        cell.lastMessageLabel.text = item.content
        cell.timeLabel.text = Date.timeAgo(since: item.last_msg_time)
        
        if item.read {
            cell.nameLabel.font = UIFont.systemFont(ofSize: 14)
            cell.lastMessageLabel.font = UIFont.systemFont(ofSize: 11)
        } else {
            cell.nameLabel.font = UIFont.boldSystemFont(ofSize: 14)
            cell.lastMessageLabel.font = UIFont.boldSystemFont(ofSize: 11)
        }
        
        if can_load_more && datasource.count - 1 == indexPath.row {
            loadData(next: true)
        }
        
        return cell
    }
}

extension ConversationViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        datasource[indexPath.row].read = true
        didSelectConversation?(datasource[indexPath.row])
    }
}
