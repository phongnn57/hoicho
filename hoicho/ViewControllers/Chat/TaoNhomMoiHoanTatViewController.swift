//
//  TaoNhomMoiHoanTatViewController.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 6/6/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

private let reuseCellIdentifier = "ContactTableViewCell"

class TaoNhomMoiHoanTatViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    
    var users = [ContactModel]() {
        didSet {
            tableView?.reloadData()
        }
    }
    var image: UIImage? {
        didSet {
            tableView?.reloadData()
        }
    }
    var completion: ((String) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
    }

    func setupUI() {
        title = "Tạo nhóm mới"
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "icon_send"), style: .plain, target: self, action: #selector(doCompleteItem))
        
        tableView.register(UINib(nibName: reuseCellIdentifier, bundle: nil), forCellReuseIdentifier: reuseCellIdentifier)
    }
    
    @objc func doCompleteItem() {
        self.view.endEditing(true)
        if let cell = tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? TaoNhomHoanTatTableViewCell {
            let groupName = cell.nameTextField.text
            if Utilities.isNilOrEmpty(groupName) {
                showAlertWithMessage("Hãy nhập tên nhóm")
                return
            }
            
            var members = [Int]()
            for object in users {
                members.append(object.id)
            }
            
            showProgress()
            HCService.createNewChat(type: 2, member: members, title: groupName!, image: image, completion: { (id_conversations) in
                self.dismissProgress()
                self.dismiss(animated: true, completion: {
                    self.completion?(id_conversations)
                })

            }) { (error) in
                self.showError(error)
            }
        }
    }

    @objc func doSelectAvatar() {
        doSelectPhoto(allowEdit: true)
    }
    
    override func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        picker.dismiss(animated: true, completion: nil)
        if let _image = info[UIImagePickerControllerEditedImage] as? UIImage {
            self.image = _image
        }
    }
    
}

extension TaoNhomMoiHoanTatViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {return 1}
        return users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TaoNhomHoanTatTableViewCell", for: indexPath) as! TaoNhomHoanTatTableViewCell
            
            if image != nil {
                cell.avatarImageView.image = image
                cell.avatarImageView.contentMode = .scaleAspectFill
            } else {
                cell.avatarImageView.image = #imageLiteral(resourceName: "icon_media")
                cell.avatarImageView.contentMode = .center
            }
            cell.avatarImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(doSelectAvatar)))
            
            
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseCellIdentifier, for: indexPath) as! ContactTableViewCell
        
        cell.avatarImageView.contentMode = .scaleAspectFill
        let item = users[indexPath.row]
        
        cell.avatarImageView.sd_setImage(with: URL(string: item.image), placeholderImage: #imageLiteral(resourceName: "icon_default_avatar"))
        cell.nameLabel.text = item.name
        cell.typeLabel.text = item.type_text + " - " + item.phone
        
        return cell
    }
}

extension TaoNhomMoiHoanTatViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 1 {
            return 30
        }
        return CGFloat.leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 1 {
            return "Thành viên (\(users.count))"
        }
        return nil
    }
}
