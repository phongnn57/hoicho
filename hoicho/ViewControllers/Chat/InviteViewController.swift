//
//  InviteViewController.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 6/8/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit
import Contacts
import MessageUI

class InviteViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var searchBar: UISearchBar!
    
    var contactStore = CNContactStore()
    var datasource = [ContactStruct]() {
        didSet {
            tableView?.reloadData()
        }
    }
    
    var filterContacts = [ContactStruct]() {
        didSet {
            tableView?.reloadData()
        }
    }
    var isSearching = false {
        didSet {
            tableView?.reloadData()
            if !isSearching {
                filterContacts.removeAll()
            }
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
    }

    func setupUI() {
        
        title = "Mời tham gia"
        
        tableView.tableFooterView = UIView()
        
        searchBar = UISearchBar()
        searchBar.placeholder = "Tìm theo tên"
        searchBar.searchBarStyle = .minimal
        searchBar.sizeToFit()
        searchBar.delegate = self
        searchBar.tintColor = UIColor.HCColor.appColor
        tableView.tableHeaderView = searchBar
        
        contactStore.requestAccess(for: .contacts) { (success, error) in

            if success {
                self.fetchContacts()
            } else {
                self.showAlertWithMessage("Vui lòng cấp quyền truy cập danh bạ cho ứng dụng này. Mở cài đặt", completion: {
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!)
                    } else {
                        UIApplication.shared.openURL(URL(string: UIApplicationOpenSettingsURLString)!)
                    }
                })
            }
        }
    }
    
    func fetchContacts() {
        
        let key = [CNContactGivenNameKey, CNContactFamilyNameKey, CNContactPhoneNumbersKey] as [CNKeyDescriptor]
        let request = CNContactFetchRequest(keysToFetch: key)
        
        var temp = [ContactStruct]()
        
        do {
            try contactStore.enumerateContacts(with: request) { (contact, stoppint) in
                let name = contact.givenName
                let familyName = contact.familyName
                let number = contact.phoneNumbers.first?.value.stringValue
                let fullName = "\(name) \(familyName)"
                var alphabetName = ""
                if fullName.count > 0 {
                    alphabetName = fullName.substring(to: 1)
                }
                if number != nil && !number!.isEmpty {
                    let contactToAppend = ContactStruct(givenName: name, familyName: familyName, phoneNumber: number!, alphabetName: alphabetName)
                    temp.append(contactToAppend)
                }
                
            }
            DispatchQueue.main.async {
                self.datasource = temp.sorted { (ct1, ct2) -> Bool in
                    return ct1.alphabetName.localizedCompare(ct2.alphabetName) == .orderedAscending
                }
            }

        } catch  _ {
            self.showAlertWithMessage("Có lỗi xảy ra, hãy chắc chắn rằng bạn cho phép ứng dụng truy cập danh bạ của bạn")
        }
        
    }

    @objc func doInviteButton(_ sender: UIButton) {
        if MFMessageComposeViewController.canSendText() {
            let appName = Bundle.main.infoDictionary![kCFBundleNameKey as String] as! String
            let messageController = MFMessageComposeViewController()
            messageController.messageComposeDelegate = self
            if isSearching {
                messageController.recipients = [filterContacts[sender.tag].phoneNumber]
            } else {
                messageController.recipients = [datasource[sender.tag].phoneNumber]
            }
            
            messageController.body = "Hãy tải ứng dụng \(appName) để cùng tham quan hội chợ và mua sắm: \(Constant.webUrl)"
            self.present(messageController, animated: true, completion: nil)
        } else {
            showAlertWithMessage("Không thể gửi tin nhắn, hãy kiểm tra lại thiết bị")
        }
    }
    
}

extension InviteViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearching {return filterContacts.count}
        return datasource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "InviteTableViewCell", for: indexPath) as! InviteTableViewCell
        
        var item: ContactStruct!
        if isSearching {
            item = filterContacts[indexPath.row]
        } else {
            item = datasource[indexPath.row]
        }
    
        let name = (item.givenName + " " + item.familyName).trimpSpace()
        
        cell.nameLabel.text = name
        cell.phoneLabel.text = item.phoneNumber
        
        if name.count > 0 {
            let firstCharacter = name.substring(to: 1)
            cell.avatarImageView.image = UIImage(color: UIColor.HCColor.appColor, size: CGSize(width: 40, height: 40))?.textToImage(drawText: firstCharacter, fontSize: 30)
        } else {
            cell.nameLabel.text = "Không có tên"
            cell.avatarImageView.image = #imageLiteral(resourceName: "icon_default_avatar")
        }
        
        cell.inviteButton.tag = indexPath.row
        cell.inviteButton.addTarget(self, action: #selector(doInviteButton(_:)), for: .touchUpInside)
        
        return cell
    }
}

extension InviteViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
}

extension InviteViewController: MFMessageComposeViewControllerDelegate {
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        controller.dismiss(animated: true, completion: nil)
        if result == .sent {
            showAlertWithMessage("Đã gửi link mời tham gia")
        }
    }
}

extension InviteViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        searchBar?.resignFirstResponder()
    }
}

extension InviteViewController: UISearchBarDelegate {
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        isSearching = true
        searchBar.showsCancelButton = true
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = nil
        searchBar.resignFirstResponder()
        isSearching = false
        searchBar.showsCancelButton = false
        
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = true
        isSearching = true
    }
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            filterContacts = datasource
        } else {
            let predicate = NSPredicate(format: "SELF contains[c] %@", searchText)
            
            filterContacts = datasource.filter {predicate.evaluate(with: ($0.givenName + " " + $0.familyName))}
        }
        
    }
}
