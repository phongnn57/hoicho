//
//  TaoNhomMoiViewController.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 6/5/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

private let reuseCellIdentifier = "ContactTableViewCell"

class TaoNhomMoiViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var refreshControl: UIRefreshControl!
    var loadMoreIndicator: UIActivityIndicatorView!
    
    var nextItem: UIBarButtonItem!
    
    var offset = 0
    var can_load_more = false
    var datasource = [ContactModel]() {
        didSet {
            tableView?.reloadData()
        }
    }
    var selectedUsers = [ContactModel]() {
        didSet {
            tableView?.reloadData()
            if selectedUsers.count > 0 && nextItem != nil {
                navigationItem.rightBarButtonItem = nextItem
            } else {
                navigationItem.rightBarButtonItem = nil
            }
        }
    }
    var completion: ((String) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        loadData()
    }

    func setupUI() {
        title = "Tạo nhóm mới"
        nextItem = UIBarButtonItem(title: "Tiếp", style: .plain, target: self, action: #selector(doNextItem))
        tableView.tableFooterView = UIView()
        tableView.register(UINib(nibName: reuseCellIdentifier, bundle: nil), forCellReuseIdentifier: reuseCellIdentifier)
        tableView.register(UINib(nibName: "TaoNhomTopTableViewCell", bundle: nil), forCellReuseIdentifier: "TaoNhomTopTableViewCell")
        refreshControl = initRefreshControl()
        loadMoreIndicator = initLoadMoreIndicator()
        if #available(iOS 10, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
    }
    
    @objc func doNextItem() {
        guard let hoanTatViewController = TaoNhomMoiHoanTatViewController.viewController(.chat) as? TaoNhomMoiHoanTatViewController else {return}
        hoanTatViewController.users = selectedUsers
        hoanTatViewController.completion = completion
        navigationController?.pushViewController(hoanTatViewController, animated: true)
    }
    
    func loadData(refresh: Bool = false, next: Bool = false) {
        if !refresh && !next {
            showProgress()
        } else if !refresh && next {
            tableView.tableFooterView = loadMoreIndicator
        }
        
        HCService.getContacts(offset: offset, completion: { [weak self](objects) in
            self?.refreshControl.endRefreshing()
            self?.dismissProgress()
            self?.tableView.tableFooterView = UIView()
            if self?.offset == 0 {
                self?.datasource = objects
            } else {
                self?.datasource.append(contentsOf: objects)
            }
            if objects.count == Constant.limit {
                self?.can_load_more = true
                self?.offset += Constant.limit
            } else {
                self?.can_load_more = false
            }
        }) { (error) in
            self.refreshControl.endRefreshing()
            self.tableView.tableFooterView = UIView()
            self.showError(error)
        }
    }
    
    override func doRefresh() {
        offset  = 0
        loadData()
    }

}

extension TaoNhomMoiViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return selectedUsers.count == 0 ? 0 : 1
        }
        return datasource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TaoNhomTopTableViewCell", for: indexPath) as! TaoNhomTopTableViewCell
            
            cell.users = selectedUsers
            cell.didRemoveAtIndex = {[weak self](index) in
                if let strongSelf = self {
                    let item = strongSelf.selectedUsers[index]
                    strongSelf.datasource.insert(item, at: 0)
                    strongSelf.selectedUsers.remove(at: index)
                }
                
            }
            
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseCellIdentifier, for: indexPath) as! ContactTableViewCell
        
        cell.avatarImageView.contentMode = .scaleAspectFill
        let item = datasource[indexPath.row]
        
        cell.avatarImageView.sd_setImage(with: URL(string: item.image), placeholderImage: #imageLiteral(resourceName: "icon_default_avatar"))
        cell.nameLabel.text = item.name
        cell.typeLabel.text = item.type_text + " - " + item.phone
        
        if can_load_more && datasource.count - 1 == indexPath.row {
            loadData(next: true)
        }
        
        return cell
    }
    
}

extension TaoNhomMoiViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 1 {
            selectedUsers.insert(datasource[indexPath.row], at: 0)
            datasource.remove(at: indexPath.row)
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 100
        }
        return 56
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 1 {
            return 30
        }
        return CGFloat.leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 1 {
            return "Gợi ý thành viên"
        }
        return nil
    }
}
