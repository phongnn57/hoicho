//
//  HCMessengerViewController.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 6/5/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit
import AsyncDisplayKit
import SwiftyJSON

class HCMessengerViewController: MessengerViewController {
    
    var product: ProductModel!
    
    var topLabel: UILabel!
    var bottomLabel: UILabel!
    var rightImageView: UIImageView!
    var callButton: UIButton!
    
    
    var lastMessageGroup:MessageGroup? = nil
    var firstMessageGroup: MessageGroup?
    
    var users: [UserModel]!
    var conversation: ConversationModel!
    var last_id: Int!
    var preAddedDatasource = [ChatModel]()
    
    var shouldOpenOtherConversation: ((String) -> Void)?
    var shouldReloadConversation: (() -> Void)?

    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        loadData()
    }
    
    func setupUI() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(didReceivedNewChatMessage(_:)), name: .didReceivedNewChatMessage, object: nil)
        khoAnhViewController.completion = {[weak self](urls) in
            self?.sendMultipleImages(urls: urls)
        }
    }
    
    func setupTopView() {
        topLabel = UILabel()
        if conversation != nil && conversation.info != nil {
            topLabel.text = conversation.info!.title
        }
        topLabel.textAlignment = .center
        topLabel.font = UIFont.systemFont(ofSize: 16, weight: .semibold)
        topLabel.textColor = UIColor(hex: "#333333")
        topLabel.isUserInteractionEnabled = true
        topLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(topViewTapped)))
        topLabel.sizeToFit()
        navigationItem.titleView = topLabel
        
        rightImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        rightImageView.contentMode = .scaleAspectFill
        rightImageView.clipsToBounds = true
        rightImageView.layer.cornerRadius = 15
        rightImageView.layer.borderWidth = 1
        rightImageView.layer.borderColor = UIColor.HCColor.appColor.cgColor
        rightImageView.isUserInteractionEnabled = true
        rightImageView.widthAnchor.constraint(equalToConstant: 30).isActive = true
        rightImageView.heightAnchor.constraint(equalToConstant: 30).isActive = true
        if conversation != nil && conversation.info != nil {
            rightImageView.sd_setImage(with: URL(string: conversation.info!.image), placeholderImage: #imageLiteral(resourceName: "icon_default_avatar"))
        } else {
            rightImageView.image = #imageLiteral(resourceName: "icon_default_avatar")
        }
        rightImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(topViewTapped)))
        
        let avatarItem = UIBarButtonItem(customView: rightImageView)
        
        if conversation != nil && conversation.info != nil {
            if conversation.info!.type == 1 {
                callButton = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
                callButton.setImage(#imageLiteral(resourceName: "icon_call_circle"), for: .normal)
                callButton.addTarget(self, action: #selector(doCallItem), for: .touchUpInside)
                let callItem = UIBarButtonItem(customView: callButton)
                
                navigationItem.rightBarButtonItems = [callItem, avatarItem]
            } else {
                navigationItem.rightBarButtonItem = avatarItem
            }
        }
        
        
        

    }
    
    @objc func doCallItem() {
        if let user = self.users.first {
            self.doCall(user.phone)
        }
    }
    
    @objc func topViewTapped() {
        if users.count == 1 {
            guard let thongTinCaNhanViewController = ThongTinCaNhanViewController.viewController(.setting) as? ThongTinCaNhanViewController else {return}
            thongTinCaNhanViewController.hidesBottomBarWhenPushed = true
            thongTinCaNhanViewController.user_id = users[0].id
            navigationController?.pushViewController(thongTinCaNhanViewController, animated: true)
        } else {
            if conversation == nil {return}
            guard let groupProfileViewController = GroupProfileViewController.viewController(.chat) as? GroupProfileViewController else {return}
            groupProfileViewController.info = conversation.info
            groupProfileViewController.id_conversation = conversation.id_conversations
            groupProfileViewController.completion = {
                self.getChatInfo()
                self.shouldReloadConversation?()
            }
            navigationController?.pushViewController(groupProfileViewController, animated: true)
        }
        
    }
    
    func createConversation() {
        
        var temp = [UserModel.shared.id]
        for usr in self.users {
            temp.append(usr.id)
        }
        HCService.createNewChat(type: 1, member: temp, image: nil, completion: { (conversation_id) in
            
            self.conversation = ConversationModel()
            self.conversation.id_conversations = conversation_id
            self.conversation.name = self.users[0].name
            self.loadData()
            
        }, failure: { (error) in
            self.showAlertWithMessage("Có lỗi khi tạo cuộc hội thoại")
        })
    }
    
    func loadData(loadPrev: Bool = false) {
        
        if conversation == nil && users != nil {
            
            createConversation()
            
        } else {
            
            getChatInfo()
            
            HCService.getChatHistories(conversation_id: conversation.id_conversations, last_id: self.last_id, completion: { (objects) in
                
                DispatchQueue.main.async {
                    self.addMessages(objects, isPrevMessage: loadPrev)
                }
                
                if objects.count > 0 {
                    self.last_id = objects[0].id
                }
                
                if objects.count == Constant.limit {
                    self.messengerView.doesBatchFetch = true
                } else {
                    self.messengerView.doesBatchFetch = false
                }
                
                if !loadPrev {
                    self.requestSendProduct()
                }
                
            }) { (error) in
                
            }
        }
        
    }
    
    func requestSendProduct() {
        if product != nil {
            let alertController = HCAlertController.init(title: nil, message: "Gửi sản phẩm \(product.name) vào cuộc hội thoại này?", completion: { (_, index) in
                if index == 0 {
                    self.product = nil
                } else {
                    self.sendProduct()
                }
            }, cancelButtonTitle: "Bỏ qua", otherButtonTitle: "Gửi")
            alertController.show(viewController: self)
        }
    }
    
    func sendProduct() {
        if product == nil {return}
        let productContent = ProductContentNode(product: product, isIncomming: false, bubbleConfiguration: self.sharedBubbleConfiguration)
        productContent.currentViewController = self
        let newMessage = MessageNode(content: productContent)
        newMessage.currentViewController = self
        
        let statusNode = ASTextNode()
        statusNode.attributedText = NSAttributedString(string: "không gửi được", attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 9), NSAttributedStringKey.foregroundColor: UIColor.red])
        
        let ui_id = NSUUID().uuidString
        let message = ChatModel(content: "", ui_id: ui_id, id_conversation: conversation.id_conversations, image: nil)
        self.preAddedDatasource.append(message)
        
        if conversation != nil {
            self.postNewMessage(newMessage, isIncomingMessage: false)
            
            HCService.sendChat(conversation_id: conversation.id_conversations, ui_id: ui_id, message: product.name, image: nil, images: nil, product_id: product.id, completion: { (chat) in
                self.product = nil
            }) { (error) in
                self.product = nil
                newMessage.footerNode = statusNode
            }
        }
    }
    
    func getChatInfo() {
        HCService.getChatGroupInfo(id_conversation: conversation.id_conversations, completion: { (object) in
            self.conversation.info = object
            self.users = object.listMember
            self.setupTopView()
            self.shouldReloadConversation?()
        }) { (error) in
            
        }
    }
    
    func generateMessageNode(_ object: ChatModel) -> MessageNode {
        
        var message: MessageNode!
        
        
        if object.type == 1 {
            let systemContent = TextContentNode(textMessageString: object.content, currentViewController: self, bubbleConfiguration: self.sharedBubbleConfiguration)
            message = MessageNode(content: systemContent)
            message.isSystemMessage = true
            return message
        } else if let _product = object.product, _product.id != 0 {
            let productContent = ProductContentNode(product: _product, isIncomming: object.from != UserModel.shared.id, bubbleConfiguration: self.sharedBubbleConfiguration)
            productContent.currentViewController = self
            message = MessageNode(content: productContent)
            message.currentViewController = self
            
            return message
        }
        
        if object.images.count == 0 {

            let textContent = TextContentNode(textMessageString: object.content.trimpSpace(), currentViewController: self, bubbleConfiguration: self.sharedBubbleConfiguration)
            message = MessageNode(content: textContent)
            message.cellPadding = messagePadding
            
        } else {
            
            if object.images.count == 1 {
                
                message = MessageNode(content: NetworkImageContentNode(imageURL: object.images[0], bubbleConfiguration: self.sharedBubbleConfiguration))
                message.currentViewController = self
                
            } else {
                
                var numberOfRows: CGFloat = 0
                if object.images.count % 3 == 0 {
                    numberOfRows = CGFloat(Int(object.images.count/3))
                } else {
                    numberOfRows = CGFloat(Int(object.images.count/3) + 1)
                }
                var itemSize: CGFloat = 0
                if numberOfRows > 1 {
                    itemSize = ((UIScreen.main.bounds.width - 10) * 2/3 - 8)/3 - 1
                } else {
                    itemSize = ((UIScreen.main.bounds.width - 10) * 2/3 - 8) / CGFloat(object.images.count)
                }
                
                var nodes = [ASImageNode]()
                for imageurl in object.images {
                    let image = ASNetworkImageNode()
                    image.url = URL(string: imageurl)
                    //image.backgroundColor = UIColor.red
                    image.defaultImage = #imageLiteral(resourceName: "icon_no_image")
                    image.forceUpscaling = true
                    image.style.preferredSize = CGSize(width: itemSize, height: itemSize)
                    nodes.append(image)
                }
                
                let isIncomingMessage = object.from != UserModel.shared.id
                
                let collectionViewContent = CollectionViewContentNode(withCustomNodes: nodes, andNumberOfRows: numberOfRows, bubbleConfiguration: self.sharedBubbleConfiguration)
                collectionViewContent.images = object.images
                collectionViewContent.currentViewController = self
                message = MessageNode(content: collectionViewContent)
                message.cellPadding = messagePadding
                message.currentViewController = self
                message.isIncomingMessage = isIncomingMessage
            }
        }
        
        return message
    }
    
    func addMessages(_ objects: [ChatModel], isPrevMessage: Bool? = false) {
        var messageGroups = [MessageGroup]()
        for object in objects {
            
            let isIncomingMessage = object.from != UserModel.shared.id
            let message = generateMessageNode(object)
            
            if messageGroups.last == nil || messageGroups.last?.isIncomingMessage == !isIncomingMessage || message.isSystemMessage || (messageGroups.last != nil && messageGroups.last!.isSystemMessagegroup) || (messageGroups.last != nil && messageGroups.last!.fromId != object.from) {
                let newMessageGroup = self.createMessageGroup()
                newMessageGroup.fromId = object.from
                if message.isSystemMessage {
                    newMessageGroup.isSystemMessagegroup = true
                }
                
                if isIncomingMessage && !message.isSystemMessage {
                    newMessageGroup.avatarNode = self.createAvatar(object.account_avatar)
                }
                newMessageGroup.isIncomingMessage = isIncomingMessage
                newMessageGroup.addMessageToGroup(message, completion: nil)
                messageGroups.append(newMessageGroup)
                
            } else {
                messageGroups.last?.addMessageToGroup(message, completion: nil)
            }
        }
        
        if isPrevMessage! {
            self.messengerView.endBatchFetchWithMessages(messageGroups)
        } else {
            self.messengerView.addMessages(messageGroups, scrollsToMessage: false)
            self.messengerView.scrollToLastMessage(animated: false)
            self.lastMessageGroup = messageGroups.last
            
        }
        
    }
    
    @objc func didReceivedNewChatMessage(_ notification: NSNotification) {
        
        if let objects = notification.userInfo as? [String: JSON], let json = objects["value"] {
            let object = ChatModel(json)
            
            if object.type == 1 {
                self.getChatInfo()
            }
            
            if object.id_conversation != self.conversation.id_conversations && object.from != UserModel.shared.id {
                
                 if let image = SDImageCache.shared().imageFromCache(forKey: object.account_avatar) {
                    HDNotificationView.show(with: image, title: object.account_name, message: object.content.trimpSpace(), isAutoHide: true) {
                        HDNotificationView.hide()
                        self.shouldOpenOtherConversation?(object.id_conversation)
                    }
                    
                 } else {
                    HDNotificationView.show(with: #imageLiteral(resourceName: "icon_default_avatar"), title: object.account_name, message: object.content.trimpSpace(), isAutoHide: true) {
                        HDNotificationView.hide()
                        self.shouldOpenOtherConversation?(object.id_conversation)
                    }
                 }
 
            } else {
                let newMessage = self.generateMessageNode(object)
                
                if object.ui_id.isEmpty {

                    self.postNewMessage(newMessage, isIncomingMessage: object.from != UserModel.shared.id, avatarUrl: object.account_avatar)

                    
                } else {
                    let items = self.preAddedDatasource.filter({ (obj) -> Bool in
                        return obj.ui_id == object.ui_id
                    })
                    if items.count == 0 {
                        self.postNewMessage(newMessage, isIncomingMessage: object.from != UserModel.shared.id, avatarUrl: object.account_avatar)
                    }
                }
            }
        }
    }
    
    // load more previous message
    func batchFetchContent() {
        self.loadData(loadPrev: true)
    }
    
    override func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        picker.dismiss(animated: true, completion: nil)
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            let _ = sendImage(image, isIncomingMessage: false)
        }
    }
    
    // open product
    func openProducts(product: ProductModel) {
        guard let chiTietSanPhamViewController = ChiTietSanPhamViewController.viewController(.home) as? ChiTietSanPhamViewController else {return}
        chiTietSanPhamViewController.hidesBottomBarWhenPushed = true
        chiTietSanPhamViewController.product_id = product.id
        navigationController?.pushViewController(chiTietSanPhamViewController, animated: true)
    }
    
    // Gửi text
    override func sendText(_ text: String, isIncomingMessage: Bool) -> GeneralMessengerCell {
        
        let textContent = TextContentNode(textMessageString: text.trimpSpace(), currentViewController: self, bubbleConfiguration: self.sharedBubbleConfiguration)
        let newMessage = MessageNode(content: textContent)
        newMessage.cellPadding = messagePadding
        newMessage.currentViewController = self
        
        let statusNode = ASTextNode()
        statusNode.attributedText = NSAttributedString(string: "không gửi được", attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 9), NSAttributedStringKey.foregroundColor: UIColor.red])
        
        let ui_id = NSUUID().uuidString
        let message = ChatModel(content: text, ui_id: ui_id, id_conversation: conversation.id_conversations, image: nil)
        self.preAddedDatasource.append(message)
        
        if conversation != nil {
            self.postNewMessage(newMessage, isIncomingMessage: isIncomingMessage)
            
            HCService.sendChat(conversation_id: conversation.id_conversations, ui_id: ui_id, message: text.trimpSpace(), image: nil, images: nil, product_id: nil, completion: { (chat) in
                self.product = nil
            }) { (error) in
                newMessage.footerNode = statusNode
            }
        }
        
        return newMessage
    }
    
    func postNewMessage(_ message: MessageNode, isIncomingMessage: Bool, avatarUrl: String?=nil) {
        if self.lastMessageGroup == nil || self.lastMessageGroup!.isIncomingMessage == !isIncomingMessage || (self.lastMessageGroup != nil && self.lastMessageGroup!.isSystemMessagegroup) {
            self.lastMessageGroup = self.createMessageGroup()
            if message.isSystemMessage {
                self.lastMessageGroup?.isSystemMessagegroup = true
            }
            
            //add avatar if incoming message
            if isIncomingMessage && avatarUrl != nil {
                self.lastMessageGroup!.avatarNode = self.createAvatar(avatarUrl!)
            }
            
            self.lastMessageGroup!.isIncomingMessage = isIncomingMessage
            
            self.messengerView.addMessageToMessageGroup(message, messageGroup: self.lastMessageGroup!, scrollsToLastMessage: false)
            self.messengerView.addMessage(self.lastMessageGroup!, scrollsToMessage: true, withAnimation: isIncomingMessage ? .left : .right)
            self.messengerView.scrollToLastMessage(animated: true)
            
        } else {
            self.messengerView.addMessageToMessageGroup(message, messageGroup: self.lastMessageGroup!, scrollsToLastMessage: true)
        }
    }
    
    // gửi ảnh
    override func sendImage(_ image: UIImage, isIncomingMessage: Bool) -> GeneralMessengerCell {
        let imageContent = ImageContentNode(image: image, bubbleConfiguration: self.sharedBubbleConfiguration)
        let newMessage = MessageNode(content: imageContent)
        newMessage.cellPadding = messagePadding
        newMessage.currentViewController = self
//        newMessage.delegate = self
        
        let statusNode = ASTextNode()
        statusNode.attributedText = NSAttributedString(string: "không gửi được", attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 9), NSAttributedStringKey.foregroundColor: UIColor.red])
        
        let ui_id = NSUUID().uuidString
        let message = ChatModel(content: "", ui_id: ui_id, id_conversation: conversation.id_conversations, image: image)
        self.preAddedDatasource.append(message)
        
        if conversation != nil {

            self.postNewMessage(newMessage, isIncomingMessage: isIncomingMessage)
            
            HCService.sendChat(conversation_id: conversation.id_conversations, ui_id: ui_id, message: nil, image: image, images: nil, product_id: nil, completion: { (chat) in
                self.product = nil
            }) { (error) in
                newMessage.footerNode = statusNode
            }
        }
        return newMessage
    }
    
    func sendMultipleImages(urls: [String]) {
        
        
        var numberOfRows: CGFloat = 0
        if urls.count % 3 == 0 {
            numberOfRows = CGFloat(Int(urls.count/3))
        } else {
            numberOfRows = CGFloat(Int(urls.count/3) + 1)
        }
        var itemSize: CGFloat = 0
        if numberOfRows > 1 {
            itemSize = ((UIScreen.main.bounds.width - 10) * 2/3 - 8)/3 - 1
        } else {
            itemSize = ((UIScreen.main.bounds.width - 10) * 2/3 - 8) / CGFloat(urls.count)
        }
        
        var nodes = [ASImageNode]()
        for imageurl in urls {
            let image = ASNetworkImageNode()
            image.url = URL(string: imageurl)
            //image.backgroundColor = UIColor.red
            image.defaultImage = #imageLiteral(resourceName: "icon_no_image")
            image.forceUpscaling = true
            image.style.preferredSize = CGSize(width: itemSize, height: itemSize)
            nodes.append(image)
        }

        let collectionViewContent = CollectionViewContentNode(withCustomNodes: nodes, andNumberOfRows: numberOfRows, bubbleConfiguration: self.sharedBubbleConfiguration)
        collectionViewContent.images = urls
        collectionViewContent.currentViewController = self
        let newMessage = MessageNode(content: collectionViewContent)
        newMessage.cellPadding = messagePadding
        newMessage.currentViewController = self
        newMessage.isIncomingMessage = false
        
        let statusNode = ASTextNode()
        statusNode.attributedText = NSAttributedString(string: "không gửi được", attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 9), NSAttributedStringKey.foregroundColor: UIColor.red])
        
        let ui_id = NSUUID().uuidString
        let message = ChatModel(content: "", ui_id: ui_id, id_conversation: conversation.id_conversations, image: nil)
        self.preAddedDatasource.append(message)
        
        if conversation != nil {
            self.postNewMessage(newMessage, isIncomingMessage: false)
            
            HCService.sendChat(conversation_id: conversation.id_conversations, ui_id: ui_id, message: nil, image: nil, images: urls, product_id: nil, completion: { (chat) in
                self.product = nil
            }) { (error) in
                newMessage.footerNode = statusNode
            }
        }
    }

    

}
