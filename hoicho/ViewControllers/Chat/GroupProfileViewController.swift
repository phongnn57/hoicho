//
//  GroupProfileViewController.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 6/6/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

private let reuseHeaderIdentifier = "GroupProfileHeaderView"


class GroupProfileViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var info: ConversationInfo! {
        didSet {
            tableView?.reloadData()
        }
    }
    var id_conversation: String!
    var completion: (() -> Void)?
    var showLessUser = true {
        didSet {
            tableView?.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        loadInfo()
    }

    func setupUI() {
        title = "Cài đặt nhóm"
        
        tableView.register(UINib(nibName: "ContactTableViewCell", bundle: nil), forCellReuseIdentifier: "ContactTableViewCell")
        tableView.register(UINib(nibName: reuseHeaderIdentifier, bundle: nil), forHeaderFooterViewReuseIdentifier: reuseHeaderIdentifier)
        tableView.tableFooterView = UIView()
        
    }
    
    func loadInfo() {
        if id_conversation == nil {return}
        showProgress()
        HCService.getChatGroupInfo(id_conversation: id_conversation, completion: { [weak self](object) in
            if let strongSelf = self {
                strongSelf.dismissProgress()
                strongSelf.info = object
                strongSelf.completion?()
            }
        }) { (error) in
            self.showError(error)
        }
    }
    
    @objc func doViewAllButton() {
        showLessUser = !showLessUser
    }
    
    @objc func doEditPhoto() {
        doSelectPhoto(allowEdit: true)
    }

    @objc func doEditName() {
        let alert = UIAlertController(title: nil, message: "Nhập tên nhóm", preferredStyle: .alert)
        
        alert.addTextField { (textfield) in
            textfield.placeholder = "Nhập tên nhóm"
            textfield.text = self.info.title
        }
        
        alert.addAction(UIAlertAction(title: "Huỷ", style: UIAlertActionStyle.destructive, handler:nil))
        alert.addAction(UIAlertAction(title: "Cập nhật", style: UIAlertActionStyle.default, handler:{ (UIAlertAction)in
            if let changedName = alert.textFields?.first?.text {
                if Utilities.isNilOrEmpty(changedName) {
                    self.showAlertWithMessage("Hãy nhập tên nhóm")
                } else {
                    self.updateName(new_name: changedName)
                }
            }
        }))
        self.present(alert, animated: true, completion: nil)

    }
    
    func updateName(new_name: String) {
        if id_conversation == nil {return}
        showProgress()
        HCService.updateGroupChat(id: id_conversation, title: new_name, image: nil, completion: {
            self.dismissProgress()
            self.info.title = new_name
            self.tableView.reloadData()
            self.completion?()
        }) { (error) in
            self.showError(error)
        }
    }
    
    override func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        picker.dismiss(animated: true, completion: nil)
        if let newImage = info[UIImagePickerControllerEditedImage] as? UIImage {
            if id_conversation == nil {return}
            showProgress()
            HCService.updateGroupChat(id: id_conversation, title: self.info.title, image: newImage, completion: {
                self.dismissProgress()
                self.loadInfo()
            }) { (error) in
                self.showError(error)
            }
        }
    }
    
    @objc func switchChanged(_ switch: UISwitch) {
        showAlertWithMessage("Đang phát triển")
    }
    
}

extension GroupProfileViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        if info == nil {return 0}
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {return 1}
        if section == 1 {
            if showLessUser {
                if info.listMember.count <= 1 {
                    return info.listMember.count + 1
                }
                return 3
            } else {
                return info.listMember.count + 1
            }

        }
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "GroupInfoTopTableViewCell", for: indexPath) as! GroupInfoTopTableViewCell
            
            cell.avatarImageView.sd_setImage(with: URL(string: info.image), placeholderImage: #imageLiteral(resourceName: "icon_default_avatar"))
            cell.nameLabel.text = info.title
            
            cell.editImageButton.addTarget(self, action: #selector(doEditPhoto), for: .touchUpInside)
            cell.editNameLabel.addTarget(self, action: #selector(doEditName), for: .touchUpInside)
            
            return cell
        } else if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ContactTableViewCell", for: indexPath) as! ContactTableViewCell
            
            if indexPath.row == 0 {
                cell.nameLabel.text = "Thêm thành viên mới"
                cell.avatarImageView.image = #imageLiteral(resourceName: "icon_contact_invite")
                cell.avatarImageView.contentMode = .center
                cell.typeLabel.text = nil
            } else {
                let item = info.listMember[indexPath.row - 1]
                cell.nameLabel.text = item.name
                cell.typeLabel.text = nil
                cell.avatarImageView.contentMode = .scaleAspectFill
                cell.avatarImageView.sd_setImage(with: URL(string: item.image), placeholderImage: #imageLiteral(resourceName: "icon_default_avatar"))
            }
            
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "GroupProfileBottomTableViewCell", for: indexPath) as! GroupProfileBottomTableViewCell
            
            if indexPath.row == 0 {
                cell.titleLabel.text = "Thông báo tin nhắn mới"
                cell.titleLabel.textColor = UIColor(hex: "#333333")
                cell.switchControl.isHidden = false
            } else if indexPath.row == 1 {
                cell.titleLabel.text = "Rời nhóm"
                cell.titleLabel.textColor = UIColor.red
                cell.switchControl.isHidden = true
            }
            
            cell.switchControl.tag = indexPath.row
            cell.switchControl.addTarget(self, action: #selector(switchChanged(_:)), for: .valueChanged)
            
            return cell
        }
        
    }
}

extension GroupProfileViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1 {
            if indexPath.row == 0  {
                guard let themNguoiViewController = GroupChatAddMemberViewController.viewController(.chat) as? GroupChatAddMemberViewController else {return}
                themNguoiViewController.exceptUsers = info.listMember
                themNguoiViewController.id_conversation = id_conversation
                themNguoiViewController.didFinishAdd = {
                    self.loadInfo()
                    self.completion?()
                }
                self.present(HCNavigationController(rootViewController: themNguoiViewController), animated: true, completion: nil)
            } else {
                let user = info.listMember[indexPath.row - 1]
                guard let thongTinCaNhanViewController = ThongTinCaNhanViewController.viewController(.setting) as? ThongTinCaNhanViewController else {return}
                thongTinCaNhanViewController.hidesBottomBarWhenPushed = true
                thongTinCaNhanViewController.user_id = user.id
                navigationController?.pushViewController(thongTinCaNhanViewController, animated: true)
            }
            
        } else if indexPath.section == 2 {
            if indexPath.row == 1 {
                showAlertWithMessage("Đang phát triển")
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 100
        } else if indexPath.section == 1 {
            return 56
        } else {
            return 44
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 1 {
            let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: reuseHeaderIdentifier) as! GroupProfileHeaderView
            
            headerView.titleLabel.text = "Thành viên (\(info.listMember.count))"
            headerView.viewAllButton.addTarget(self, action: #selector(doViewAllButton), for: .touchUpInside)
            
            if showLessUser {
                headerView.viewAllButton.setTitle("Tất cả", for: .normal)
            } else {
                headerView.viewAllButton.setTitle("Thu gọn", for: .normal)
            }
            
            return headerView
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 1 {
            return 36
        } else if section == 2 {
            return 25
        }
        return CGFloat.leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }

}
