//
//  ChatViewController.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 4/22/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit
import SwiftyJSON
import LoremIpsum

class ChatViewController: UIViewController {
    
    
    
    @IBOutlet weak var inputBar: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var inputTextView: UITextView!
    @IBOutlet weak var inputViewHeight: NSLayoutConstraint!
    
    var loadMoreIndicator: UIActivityIndicatorView!
    
    var topLabel: UILabel!
    var bottomLabel: UILabel!
    
    let INPUT_TEXTVIEW_WIDTH: CGFloat = UIScreen.main.bounds.width - 120
    
    var datasource = [ChatModel]() {
        didSet {
            tableView?.reloadData()
        }
    }
    var users = [UserModel]() {
        didSet {
            
        }
    }
    var conversation: ConversationModel!
    var last_id: Int!
    var canLoadMore = false
    var preAddedDatasource = [ChatModel]()
    

    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        loadData()
    }

    func setupUI() {
        
        setupTopView()
        
        let moreItem = UIBarButtonItem(image: #imageLiteral(resourceName: "icon_more_vertical"), style: .plain, target: self, action: #selector(doMoreItem))
        navigationItem.rightBarButtonItem = moreItem
        
        
        self.tableView.estimatedRowHeight = 1000
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.contentInset.bottom = 52
        self.tableView.scrollIndicatorInsets.bottom = 52
        
        loadMoreIndicator = initLoadMoreIndicator()
        inputTextView.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(didReceivedNewChatMessage(_:)), name: .didReceivedNewChatMessage, object: nil)
    }
    
    func setupTopView() {
        let topView = UIView(frame: CGRect(x: 0, y: 0, width: 200, height: 44))
        topLabel = UILabel(frame: CGRect(x: 0, y: 4, width: 200, height: 25))
        if conversation != nil {
            topLabel.text = conversation.name
        }
        //        topLabel.text = "Nam Phong"
        topLabel.textAlignment = .center
        topLabel.font = UIFont.systemFont(ofSize: 14, weight: .semibold)
        topLabel.textColor = UIColor(hex: "#333333")
        topView.addSubview(topLabel)
        
        bottomLabel = UILabel(frame: CGRect(x: 0, y: 26, width: 200, height: 14))
        bottomLabel.text = "Đang online"
        bottomLabel.textAlignment = .center
        bottomLabel.font = UIFont.systemFont(ofSize: 10, weight: .medium)
        bottomLabel.textColor = UIColor(hex: "#666666")
        topView.addSubview(bottomLabel)
        
        topView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(topViewTapped)))
        
        navigationItem.titleView = topView
    }
    
    @objc func topViewTapped() {
        if users.count > 0 {
            guard let thongTinCaNhanViewController = ThongTinCaNhanViewController.viewController(.setting) as? ThongTinCaNhanViewController else {return}
            thongTinCaNhanViewController.hidesBottomBarWhenPushed = true
            thongTinCaNhanViewController.user_id = users[0].id
            navigationController?.pushViewController(thongTinCaNhanViewController, animated: true)
        }
        
    }
    
    @objc func doMoreItem() {
        let actionSheetController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        actionSheetController.addAction(UIAlertAction(title: "Xem profile", style: .default, handler: { (_) in
            self.topViewTapped()
        }))
        actionSheetController.addAction(UIAlertAction(title: "Huỷ", style: .cancel, handler: { (_) in
            
        }))
        if let popoverController = actionSheetController.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.height, width: 0, height: 0)
        }
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    func createConversation() {
        
        var temp = [UserModel.shared.id]
        for usr in self.users {
            temp.append(usr.id)
        }

        HCService.createNewChat(type: 1, member: temp, image: nil, completion: { (conversation_id) in
            
            self.conversation = ConversationModel()
            self.conversation.id_conversations = conversation_id
            self.conversation.name = self.users[0].name
            self.setupTopView()
            self.loadData()
            
        }, failure: { (error) in
            self.showAlertWithMessage("Có lỗi khi tạo cuộc hội thoại")
        })
    }
    
    func loadData(loadPrev: Bool = false) {
        
        if conversation == nil && users.count > 0 {
            // tao cuoc hoi thoai moi
            createConversation()
            
        } else {
            if loadPrev {
                tableView.tableHeaderView = loadMoreIndicator
            }
            
            HCService.getChatHistories(conversation_id: conversation.id_conversations, last_id: self.last_id, completion: { [weak weakSelf = self](objects) in
                weakSelf?.tableView.tableHeaderView = UIView()
                
                if loadPrev {
                    self.datasource.insert(contentsOf: objects, at: 0)
                } else {
                    self.datasource = objects
                }
                DispatchQueue.main.async {
                    if let state = weakSelf?.datasource.isEmpty, state == false {
                        weakSelf?.tableView.reloadData()
                        if !loadPrev {
                            weakSelf?.tableView.scrollToRow(at: IndexPath.init(row: self.datasource.count - 1, section: 0), at: .bottom, animated: false)
                        } else {
                            if objects.count > 0 {
                                weakSelf?.tableView.scrollToRow(at: IndexPath.init(row: objects.count - 1, section: 0), at: .top, animated: false)
                            }
                            
                        }
                        
                    }
                }
                
                if objects.count > 0 && objects.count == Constant.limit {
                    weakSelf?.last_id = objects[0].id
                    weakSelf?.canLoadMore = true
                } else {
                    weakSelf?.canLoadMore = false
                }
            }) { [weak weakSelf = self](error) in
                weakSelf?.tableView.tableHeaderView = UIView()
            }
        }
        
        // todo: đánh dấu tin nhắn đã đọc
        
    }
    
    @objc func didReceivedNewChatMessage(_ notification: NSNotification) {
        if let objects = notification.userInfo as? [String: JSON], let json = objects["value"] {
            let object = ChatModel(json)
            
            if object.id_conversation == self.conversation.id_conversations {
                if object.ui_id.isEmpty {
                    self.datasource.append(object)
                    DispatchQueue.main.async {
                        if !self.datasource.isEmpty {
                            self.tableView.reloadData()
                            self.tableView.scrollToRow(at: IndexPath.init(row: self.datasource.count - 1, section: 0), at: .bottom, animated: false)
                        }
                    }
                } else {
                    let items = self.preAddedDatasource.filter({ (obj) -> Bool in
                        return obj.ui_id == object.ui_id
                    })
                    if items.count == 0 {
                        self.datasource.append(object)
                        DispatchQueue.main.async {
                            if !self.datasource.isEmpty {
                                self.tableView.reloadData()
                                self.tableView.scrollToRow(at: IndexPath.init(row: self.datasource.count - 1, section: 0), at: .bottom, animated: false)
                            }
                            
                        }
                    }
                }
            }
        }
    }
    
    @IBAction func doSendButton(_ sender: Any) {
        if Utilities.isNilOrEmpty(inputTextView.text) {return}
        let ui_id = NSUUID().uuidString
        
        let newMessage = ChatModel(content: inputTextView.text.trimpSpace(), ui_id: ui_id, id_conversation: conversation.id_conversations, image: nil)
        self.preAddedDatasource.append(newMessage)
        self.datasource.append(newMessage)
        DispatchQueue.main.async {
            if !self.datasource.isEmpty {
                self.tableView.reloadData()
                self.tableView.scrollToRow(at: IndexPath.init(row: self.datasource.count - 1, section: 0), at: .bottom, animated: false)
            }
        }
        HCService.sendChat(conversation_id: self.conversation.id_conversations, ui_id: ui_id, message: inputTextView.text.trimpSpace(), image: nil, images: nil, product_id: nil, completion: { (object) in
            // do nothing
        }) { (error) in
            // todo: show alert fail
        }
        
        self.inputTextView.text = nil
        inputViewHeight.constant = self.calculateInputViewHeight(textInput: "")
        
    }
    
    func calculateInputViewHeight(textInput: String) -> CGFloat {
        let textView = UITextView(frame: CGRect(x: 0, y: 0, width: INPUT_TEXTVIEW_WIDTH, height: CGFloat.greatestFiniteMagnitude))
        textView.font = UIFont.systemFont(ofSize: 14)
        textView.isScrollEnabled = false
        textView.text = textInput
        textView.sizeToFit()
        return (textView.frame.size.height <= 36 ? 36 : textView.frame.size.height)
    }

    @IBAction func doMediaButton(_ sender: Any) {
        doSelectPhoto()
    }
    
    override func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        picker.dismiss(animated: true, completion: nil)
        if let mediaImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            sendImage(mediaImage)
        }
    }
    
    func sendImage(_ image: UIImage) {
        let ui_id = NSUUID().uuidString
        
        let newMessage = ChatModel(content: inputTextView.text.trimpSpace(), ui_id: ui_id, id_conversation: conversation.id_conversations, image: image)
        self.preAddedDatasource.append(newMessage)
        self.datasource.append(newMessage)
        DispatchQueue.main.async {
            if !self.datasource.isEmpty {
                self.tableView.reloadData()
                self.tableView.scrollToRow(at: IndexPath.init(row: self.datasource.count - 1, section: 0), at: .bottom, animated: false)
            }
        }
        HCService.sendChat(conversation_id: self.conversation.id_conversations, ui_id: ui_id, message: nil, image: image, images: nil, product_id: nil, completion: { (object) in
            
        }) { (error) in
            
        }
    }
}

extension ChatViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datasource.count
    }
    
//    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        if tableView.isDragging {
//            cell.transform = CGAffineTransform.init(scaleX: 0.5, y: 0.5)
//            UIView.animate(withDuration: 0.3, animations: {
//                cell.transform = CGAffineTransform.identity
//            })
//        }
//    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let item = datasource[indexPath.row]
        
        
        if item.from == UserModel.shared.id {
            
            if item.images.count == 0 && item.image == nil {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ChatOutgoingTableViewCell", for: indexPath) as! ChatOutgoingTableViewCell
                
                
                cell.contentTextView.text = item.content

                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ChatOutgoingMediaTableViewCell", for: indexPath) as! ChatOutgoingMediaTableViewCell

                if item.image != nil {
                    cell.mediaImageView.image = item.image
                } else {
                    cell.mediaImageView.sd_setImage(with: URL(string: item.images[0]), placeholderImage: #imageLiteral(resourceName: "icon_no_image"))
                }
                
                return cell
            }

        } else {
            
            if item.images.count == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ChatIncommingTableViewCell", for: indexPath) as! ChatIncommingTableViewCell
                
                cell.contentTextView.text = item.content
                cell.avatarImageView.sd_setImage(with: URL(string: item.account_avatar), placeholderImage: #imageLiteral(resourceName: "icon_default_avatar"))
                
                if indexPath.row < datasource.count - 1 {
                    let nextItem = datasource[indexPath.row + 1]
                    if nextItem.from == item.from {
                        cell.avatarImageView.isHidden = true
                    } else {
                        cell.avatarImageView.isHidden = false
                    }
                } else {
                    cell.avatarImageView.isHidden = false
                }
                

                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ChatIncommingMediaTableViewCell", for: indexPath) as! ChatIncommingMediaTableViewCell
                
                cell.avatarImageView.sd_setImage(with: URL(string: item.account_avatar), placeholderImage: #imageLiteral(resourceName: "icon_default_avatar"))
                cell.mediaImageView.sd_setImage(with: URL(string: item.images[0]), placeholderImage: #imageLiteral(resourceName: "icon_no_image"))
                
                if indexPath.row < datasource.count - 1 {
                    let nextItem = datasource[indexPath.row + 1]
                    if nextItem.from == item.from {
                        cell.avatarImageView.isHidden = true
                    } else {
                        cell.avatarImageView.isHidden = false
                    }
                } else {
                    cell.avatarImageView.isHidden = false
                }
                

                return cell
            }

        }
 
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if tableView.isDragging {
            if canLoadMore && tableView.scrollIndicatorInsets.top == 0 && datasource.count > 0 {
                loadData(loadPrev: true)
            }
        }
        
    }
    
}

extension ChatViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        inputTextView.resignFirstResponder()
    }
}

//extension ChatViewController: GrowingTextViewDelegate {
//    func textViewDidChangeHeight(_ textView: GrowingTextView, height: CGFloat) {
//        UIView.animate(withDuration: 0.3) {
//            self.view.layoutIfNeeded()
//        }
//    }
//}

extension ChatViewController: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        let newHeight = calculateInputViewHeight(textInput: textView.text)
        inputViewHeight.constant = newHeight
    }
}



