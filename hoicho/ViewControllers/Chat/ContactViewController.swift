//
//  ContactViewController.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 4/18/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit
import Contacts

class ContactViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var refreshControl: UIRefreshControl!
    var loadMoreIndicator: UIActivityIndicatorView!
    
    var contactStore = CNContactStore()
    
    var offset = 0
    var can_load_more = false
    var datasource = [ContactModel]() {
        didSet {
            tableView?.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        loadData()
    }

    func setupUI() {
        tableView.tableFooterView = UIView()
        tableView.register(UINib(nibName: "ContactTableViewCell", bundle: nil), forCellReuseIdentifier: "ContactTableViewCell")
        refreshControl = initRefreshControl()
        loadMoreIndicator = initLoadMoreIndicator()
        if #available(iOS 10, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
    }

    func loadData(refresh: Bool = false, next: Bool = false) {
        if !refresh && !next {
            showProgress()
        } else if !refresh && next {
            tableView.tableFooterView = loadMoreIndicator
        }
        
        HCService.getFriends(offset: offset, completion: { [weak self](objects) in
            self?.refreshControl.endRefreshing()
            self?.dismissProgress()
            self?.tableView.tableFooterView = UIView()
            if self?.offset == 0 {
                self?.datasource = objects
            } else {
                self?.datasource.append(contentsOf: objects)
            }
            if objects.count == Constant.limit {
                self?.can_load_more = true
                self?.offset += Constant.limit
            } else {
                self?.can_load_more = false
            }
        }) { (error) in
            self.refreshControl.endRefreshing()
            self.tableView.tableFooterView = UIView()
            self.showError(error)
        }
    }
    
    override func doRefresh() {
        offset = 0
        loadData(refresh: true)
    }
    
    func checkContactPermission() {
        contactStore.requestAccess(for: .contacts) { (success, error) in
            
            if success {
                self.fetchContacts()
            } else {
                self.showAlertWithMessage("Vui lòng cấp quyền truy cập danh bạ cho ứng dụng này. Mở cài đặt", completion: {
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!)
                    } else {
                        UIApplication.shared.openURL(URL(string: UIApplicationOpenSettingsURLString)!)
                    }
                })
            }
        }
    }
    
    func fetchContacts() {
        
        let key = [CNContactGivenNameKey, CNContactFamilyNameKey, CNContactPhoneNumbersKey] as [CNKeyDescriptor]
        let request = CNContactFetchRequest(keysToFetch: key)
        
        var temp = [String]()
        
        do {
            try contactStore.enumerateContacts(with: request) { (contact, stoppint) in
                let number = contact.phoneNumbers.first?.value.stringValue
                if number != nil && !number!.isEmpty {
                    temp.append(number!)
                }
            }

            showProgress()
            HCService.syncContacts(phones: temp, completion: {
                self.showAlertWithMessage("Đã đồng bộ danh bạ", completion: {
                    self.doRefresh()
                })
            }) { (error) in
                self.showError(error)
            }
            
        } catch  _ {
            self.showAlertWithMessage("Có lỗi xảy ra, hãy chắc chắn rằng bạn cho phép ứng dụng truy cập danh bạ của bạn")
        }
        
    }
}
extension ContactViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {return 2}
        return datasource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ContactTableViewCell", for: indexPath) as! ContactTableViewCell
            
            cell.avatarImageView.contentMode = .center
            if indexPath.row == 0 {
                cell.nameLabel.text = "Đồng bộ danh bạ"
                cell.avatarImageView.image = #imageLiteral(resourceName: "icon_contact_sync")
                
            } else {
                cell.nameLabel.text = "Mời bạn tham gia"
                cell.avatarImageView.image = #imageLiteral(resourceName: "icon_contact_invite")
            }
            cell.typeLabel.text = nil
            
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContactTableViewCell", for: indexPath) as! ContactTableViewCell
        cell.avatarImageView.contentMode = .scaleAspectFill
        let item = datasource[indexPath.row]
        
        cell.avatarImageView.sd_setImage(with: URL(string: item.image), placeholderImage: #imageLiteral(resourceName: "icon_default_avatar"))
        cell.nameLabel.text = item.name
        cell.typeLabel.text = item.type_text + " - " + item.phone
        
        if can_load_more && datasource.count - 1 == indexPath.row {
            loadData(next: true)
        }
        return cell
    }
}

extension ContactViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                self.checkContactPermission()
            } else if indexPath.row == 1 {
                guard let inviteViewController = InviteViewController.viewController(.chat) as? InviteViewController else {return}
                self.present(HCNavigationController(rootViewController: inviteViewController), animated: true, completion: nil)
            }
        } else {
            guard let chatViewController = HCMessengerViewController.viewController(.chat) as? HCMessengerViewController else {return}
            chatViewController.hidesBottomBarWhenPushed = true
            let item = datasource[indexPath.row]
            let user = UserModel()
            user.id = item.id
            user.name = item.name
            user.image = item.image
            chatViewController.users = [user]
            navigationController?.pushViewController(chatViewController, animated: true)
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 56
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 1 {
            return 30
        }
        return CGFloat.leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 1 {
            return "Bạn bè trên Expo 360"
        }
        return nil
    }
}
