//
//  ChatRootViewController.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 4/18/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit
import CarbonKit

class ChatRootViewController: UIViewController {
    
    var carbonTabSwipeNavigation: CarbonTabSwipeNavigation!

    lazy var conversationViewController: ConversationViewController = {
        let conversationViewController = ConversationViewController.viewController(.chat) as! ConversationViewController
        return conversationViewController
    }()
    lazy var contactViewController: ContactViewController = {
        let contactViewController = ContactViewController.viewController(.chat) as! ContactViewController
        return contactViewController
    }()
    var defaultIndex = 0 {
        didSet {
            carbonTabSwipeNavigation?.setCurrentTabIndex(UInt(defaultIndex), withAnimation: false)
        }
    }
    var avatarImageView: UIImageView!
    var avatarItem: UIBarButtonItem!
    var addItem: UIBarButtonItem!
    var searchBar: UISearchBar!
    var searchItem: UIBarButtonItem!
    
    var content = "" {
        didSet {
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
    }

    func setupUI() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(userLoggedIn(_:)), name: NSNotification.Name.UserLoggedIn, object: nil)
        
        carbonTabSwipeNavigation = CarbonTabSwipeNavigation.init(items: ["Tin nhắn", "Danh bạ"], delegate: self)
        carbonTabSwipeNavigation.setNormalColor(UIColor.init(hex: "#333333"), font: UIFont.boldSystemFont(ofSize: 14))
        carbonTabSwipeNavigation.setSelectedColor(UIColor.HCColor.appColor, font: UIFont.boldSystemFont(ofSize: 14))
        carbonTabSwipeNavigation.setIndicatorColor(UIColor.HCColor.appColor)
        carbonTabSwipeNavigation.carbonSegmentedControl?.setWidth(UIScreen.main.bounds.width/2, forSegmentAt: 0)
        carbonTabSwipeNavigation.carbonSegmentedControl?.setWidth(UIScreen.main.bounds.width/2, forSegmentAt: 1)
        carbonTabSwipeNavigation.insert(intoRootViewController: self)
        
        conversationViewController.didSelectConversation = {[weak self](conversation) in
            if let strongSelf = self {
                strongSelf.openMessengerWithConversation(conversation: conversation)
            }
        }
        
        searchBar = UISearchBar()
        searchBar.searchBarStyle = .minimal
        searchBar.sizeToFit()
        searchBar.delegate = self
        searchBar.placeholder = "Tìm kiếm"
        navigationItem.titleView = searchBar
        
        addItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(doAddItem))
        navigationItem.rightBarButtonItem = addItem
        
        setupAvatar()
        
        carbonTabSwipeNavigation.setCurrentTabIndex(UInt(defaultIndex), withAnimation: false)

    }
    
    func setupAvatar() {
        avatarImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        avatarImageView.isUserInteractionEnabled = true
        avatarImageView.contentMode = .scaleAspectFill
        avatarImageView.layer.borderWidth = 1
        avatarImageView.layer.borderColor = UIColor.HCColor.appColor.cgColor
        avatarImageView.clipsToBounds = true
        avatarImageView.cornerRadius = 15
        avatarImageView.sd_setImage(with: URL(string: UserModel.shared.image), placeholderImage: #imageLiteral(resourceName: "icon_default_avatar"))
        avatarImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(avatarTapped)))
        avatarImageView.widthAnchor.constraint(equalToConstant: 30).isActive = true
        avatarImageView.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        avatarItem = UIBarButtonItem(customView: avatarImageView)
        navigationItem.leftBarButtonItem = avatarItem
    }
    
    @objc func userLoggedIn(_ notification: NSNotification) {
        if conversationViewController.tableView != nil {
            conversationViewController.doRefresh()
        }
        
        if contactViewController.tableView != nil {
            contactViewController.doRefresh()
        }
        setupAvatar()
    }
    
    @objc func doAddItem() {
        guard let taoNhomMoiViewController = TaoNhomMoiViewController.viewController(.chat) as? TaoNhomMoiViewController else {return}
        taoNhomMoiViewController.completion = {[weak self](id_conversation) in
            if let strongSelf = self {
                let conversation = ConversationModel()
                conversation.id_conversations = id_conversation
                strongSelf.openMessengerWithConversation(conversation: conversation)
            }
        }
        self.present(HCNavigationController(rootViewController: taoNhomMoiViewController), animated: true, completion: nil)
    }
    
    @objc func avatarTapped() {
        guard let thongTinCaNhanViewController = ThongTinCaNhanViewController.viewController(.setting) as? ThongTinCaNhanViewController else {return}
        thongTinCaNhanViewController.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(thongTinCaNhanViewController, animated: true)
    }
    
    func openMessengerWithConversation (conversation: ConversationModel) {
        guard let messengerViewController = HCMessengerViewController.viewController(.chat) as? HCMessengerViewController else {return}
        messengerViewController.hidesBottomBarWhenPushed = true
        messengerViewController.conversation = conversation
        messengerViewController.shouldOpenOtherConversation = {[weak self](id_conversation) in
            if let strongSelf = self {
                if let targetConversation = strongSelf.conversationViewController.datasource.filter({ (object) -> Bool in
                    return object.id_conversations == id_conversation
                }).first {
                    strongSelf.openMessengerWithConversation(conversation: targetConversation)
                }
            }
        }
        messengerViewController.shouldReloadConversation = {
            self.conversationViewController.doRefresh()
        }
        self.navigationController?.popToRootViewController(animated: false)
        self.navigationController?.pushViewController(messengerViewController, animated: true)
    }

    @objc func openSearch() {
        navigationItem.rightBarButtonItem = nil
        navigationItem.titleView = searchBar
        searchBar.becomeFirstResponder()
    }
    
    override func doRefresh() {
        conversationViewController.doRefresh()
        contactViewController.doRefresh()
    }

}

extension ChatRootViewController: CarbonTabSwipeNavigationDelegate {
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
        if index == 0 {
            return conversationViewController
        }
        return contactViewController
    }
}

extension ChatRootViewController: UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = true
        navigationItem.rightBarButtonItem = nil
        navigationItem.leftBarButtonItem = nil
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        self.content = ""
        title = "Tin nhắn"
        navigationItem.rightBarButtonItem = addItem
        navigationItem.leftBarButtonItem = avatarItem
        self.dismiss(animated: false, completion: nil)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.content = searchBar.text ?? ""
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.content = searchBar.text ?? ""
    }
}
