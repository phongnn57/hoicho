//
//  GroupChatAddMemberViewController.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 6/6/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

class GroupChatAddMemberViewController: TaoNhomMoiViewController {

    var exceptUsers = [UserModel]()
    var id_conversation = ""
    var didFinishAdd: (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        nextItem = UIBarButtonItem(title: "Thêm", style: .plain, target: self, action: #selector(doNextItem))
        loadData()
    }

    override func loadData(refresh: Bool = false, next: Bool = false) {
        if !refresh && !next {
            showProgress()
        } else if !refresh && next {
            tableView.tableFooterView = loadMoreIndicator
        }
        
        HCService.getContacts(offset: offset, completion: { [weak self](objects) in
            self?.refreshControl.endRefreshing()
            self?.dismissProgress()
            self?.tableView.tableFooterView = UIView()
            
            var newObjects = [ContactModel]()
            for object in objects {
                if self?.exceptUsers.filter({ (user) -> Bool in
                    return user.id == object.id
                }).count == 0 {
                    newObjects.append(object)
                }
            }
            
            if self?.offset == 0 {
                self?.datasource = newObjects
            } else {
                self?.datasource.append(contentsOf: newObjects)
            }
            if objects.count == Constant.limit {
                self?.can_load_more = true
                self?.offset += Constant.limit
            } else {
                self?.can_load_more = false
            }
            
            self?.tableView.reloadData()
        }) { (error) in
            self.refreshControl.endRefreshing()
            self.tableView.tableFooterView = UIView()
            self.showError(error)
        }
    }
    
    override func doNextItem() {
        showProgress()
        var members = [Int]()
        for user in selectedUsers {
            members.append(user.id)
        }
        HCService.addMembersToChat(id: id_conversation, members: members, completion: {
            self.dismissProgress()
            self.dismiss(animated: true, completion: {
                self.didFinishAdd?()
            })
        }) { (error) in
            self.showError(error)
        }
    }


}
