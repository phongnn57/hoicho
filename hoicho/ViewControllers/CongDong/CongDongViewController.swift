//
//  CongDongViewController.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 4/18/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit
import Social
import FBSDKShareKit

private let reuseCellIdentifier = "CongDongTableViewCell"

class CongDongViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var refreshControl: UIRefreshControl!
    var loadMoreIndicator: UIActivityIndicatorView!
    var searchBar: UISearchBar!
    var searchItem: UIBarButtonItem!
    var notificationItem: UIBarButtonItem!
    var notificationLabel: UILabel!
    var offset = 0
    var can_load_more = true
    var datasource = [CommunityModel]() {
        didSet {
            tableView?.reloadData()
        }
    }
    var content = "" {
        didSet {
            doRefresh()
        }
    }
    
    var productSharingIndex: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        loadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        getUnreadNotification()
    }

    func setupUI() {
        title = "Cộng đồng"
        tableView.tableFooterView = UIView()
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 1000
        tableView.register(UINib(nibName: reuseCellIdentifier, bundle: nil), forCellReuseIdentifier: reuseCellIdentifier)
        refreshControl = initRefreshControl()
        loadMoreIndicator = initLoadMoreIndicator()
        if #available(iOS 10, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        searchBar = UISearchBar()
        searchBar.searchBarStyle = .minimal
        searchBar.sizeToFit()
        searchBar.delegate = self
        searchBar.placeholder = "Tìm bài viết"
        
        searchItem = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(openSearch))
        navigationItem.rightBarButtonItem = searchItem
        
        
        notificationLabel = UILabel(frame: CGRect(x: 14, y: 0, width: 16, height: 16))
        notificationLabel.clipsToBounds = true
        notificationLabel.textAlignment = .center
        notificationLabel.textColor = .white
        notificationLabel.font = UIFont.systemFont(ofSize: 10, weight: .medium)
        notificationLabel.minimumScaleFactor = 0.5
        notificationLabel.cornerRadius = 8
        notificationLabel.isHidden = true
        notificationLabel.backgroundColor = UIColor.red
        
        let notificationView = UIView(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        
        let notificationButton = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        notificationButton.setImage(#imageLiteral(resourceName: "icon_notification"), for: .normal)
        notificationButton.tintColor = UIColor.HCColor.appColor
        notificationButton.addTarget(self, action: #selector(doNotificationItem), for: .touchUpInside)
        notificationView.addSubview(notificationButton)
        notificationView.addSubview(notificationLabel)
        
        notificationItem = UIBarButtonItem(customView: notificationView)
        navigationItem.leftBarButtonItem = notificationItem
        
        NotificationCenter.default.addObserver(self, selector: #selector(userLoggedIn(_:)), name: NSNotification.Name.UserLoggedIn, object: nil)
        
    }
    
    @objc func userLoggedIn(_ notification: NSNotification) {
        self.doRefresh()
    }
    
    func getUnreadNotification() {
        if !UserModel.shared.token.isEmpty && UserModel.shared.id > 0 {
            HCService.countUnreadNotification(completion: { [weak self](count) in
                if let strongSelf = self, strongSelf.notificationLabel != nil {
                    if count > 0 {
                        strongSelf.notificationLabel.isHidden = false
                        if count > 99 {
                            strongSelf.notificationLabel.text = "99+"
                        } else {
                            strongSelf.notificationLabel.text = "\(count)"
                        }
                        
                        strongSelf.navigationController?.tabBarItem.badgeValue = "\(count)"
                        
                    } else {
                        strongSelf.notificationLabel.isHidden = true
                        strongSelf.navigationController?.tabBarItem.badgeValue = nil
                    }

                }
            }) { (_) in
                
            }
        }
    }
    
    @objc func doNotificationItem() {
        self.requestLoginWithCompletion {
            guard let thongBaoViewController = ThongBaoViewController.viewController(.common) as? ThongBaoViewController else {return}
            thongBaoViewController.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(thongBaoViewController, animated: true)
        }
    }
    
    @objc func openSearch() {
        navigationItem.rightBarButtonItem = nil
        navigationItem.leftBarButtonItem = nil
        navigationItem.titleView = searchBar
        searchBar.becomeFirstResponder()
    }
    
    func loadData(refresh: Bool = false, next: Bool = false) {
        if !refresh && !next {
            showProgress()
        } else if !refresh && next {
            tableView.tableFooterView = loadMoreIndicator
        }
        
        HCService.getCommunities(content: content, offset: offset, completion: { [weak self](objects) in
            if let strongSelf = self {
                strongSelf.refreshControl.endRefreshing()
                strongSelf.tableView.tableFooterView = UIView()
                strongSelf.dismissProgress()
                if strongSelf.offset == 0 {
                    strongSelf.datasource = objects
                } else {
                    strongSelf.datasource.append(contentsOf: objects)
                }
                if objects.count == Constant.limit {
                    strongSelf.offset += Constant.limit
                    strongSelf.can_load_more = true
                } else {
                    strongSelf.can_load_more = false
                }
            }
        }) { (error) in
            self.refreshControl.endRefreshing()
            self.tableView.tableFooterView = UIView()
            self.showError(error)
        }
    }
    
    override func doRefresh() {
        offset = 0
        loadData(refresh: true)
    }
    
    @objc func openProduct(_ sender: UITapGestureRecognizer) {
        if let senderView = sender.view, let product = datasource[senderView.tag].product {
            guard let chiTietSanPhamViewController = ChiTietSanPhamViewController.viewController(.home) as? ChiTietSanPhamViewController else {return}
            chiTietSanPhamViewController.hidesBottomBarWhenPushed = true
            chiTietSanPhamViewController.product_id = product.id
            navigationController?.pushViewController(chiTietSanPhamViewController, animated: true)
        }
    }
    
    @objc func openProfile(_ sender: UIButton) {
        guard let thongTinCaNhanViewController = ThongTinCaNhanViewController.viewController(.setting) as? ThongTinCaNhanViewController else {return}
        thongTinCaNhanViewController.hidesBottomBarWhenPushed = true
        thongTinCaNhanViewController.user_id = datasource[sender.tag].account_id
        navigationController?.pushViewController(thongTinCaNhanViewController, animated: true)
    }
    
    @objc func doShareButton(_ sender: UIButton) {
        productSharingIndex = sender.tag
        
        let height: CGFloat = 180
        
        
        var items = [CustomizableActionSheetItem]()
        
        let shareView = ShareProductView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width - 30, height: height))
        
        let shareViewItem = CustomizableActionSheetItem()
        shareViewItem.type = .view
        shareViewItem.view = shareView
        shareViewItem.height = height
        items.append(shareViewItem)
        
        // Setup button
        let closeItem = CustomizableActionSheetItem()
        closeItem.type = .button
        closeItem.label = "Đóng"
        closeItem.selectAction = { (actionSheet: CustomizableActionSheet) -> Void in
            actionSheet.dismiss()
        }
        items.append(closeItem)
        
        // Show
        let actionSheet = CustomizableActionSheet()
        actionSheet.showInView(self.view, items: items)
        
        shareView.doShareFacebookProduct = {
            
            actionSheet.dismiss()
            
            if let link = self.datasource[sender.tag].product?.link_affiliate {
                let shareContent = FBSDKShareLinkContent()
                shareContent.contentURL = URL(string: link)
                FBSDKShareDialog.show(from: self, with: shareContent, delegate: self)
            } else if self.datasource[sender.tag].images.count > 0 {
                let shareContent = FBSDKSharePhotoContent()
                var temp = [FBSDKSharePhoto]()
                for imageURL in self.datasource[sender.tag].images {
                    temp.append(FBSDKSharePhoto(imageURL: URL(string: imageURL), userGenerated: true))
                }
                shareContent.photos = temp
                FBSDKShareDialog.show(from: self, with: shareContent, delegate: self)
            }

        }
        
        shareView.doShareOthersProduct =  {
            
            actionSheet.dismiss()
            var url: URL!
            if let link = self.datasource[sender.tag].product?.link_affiliate {
                url = URL(string: link)
            } else if let link = self.datasource[sender.tag].images.first {
                url = URL(string: link)
            }
            
            let shareItems = [url!]
            let activityController = UIActivityViewController(activityItems: shareItems, applicationActivities: nil)
            activityController.excludedActivityTypes = [.print, .postToWeibo, .copyToPasteboard, .addToReadingList, .postToVimeo, .postToTwitter, .assignToContact, .saveToCameraRoll, .postToFlickr, .postToTencentWeibo, .airDrop, .openInIBooks]
            activityController.completionWithItemsHandler = {(activityType, completed, returnedItems, error) in
                if completed {
                    if let product = self.datasource[sender.tag].product {
                        HCService.shareProductCount(id: product.id, completion: { [weak self](count) in
                            if let strongSelf = self {
                                strongSelf.datasource[strongSelf.productSharingIndex].share_count += 1
                                strongSelf.tableView.reloadData()
                            }
                        }) { (error) in
                            
                        }
                    }
                    
                }
            }
            
            self.present(activityController, animated: true, completion: nil)
        }
        
        
        
//        guard let product = datasource[sender.tag].product else {return}
//
//        if product.link_affiliate.length > 0 {
//
//
//
//        } else {
//            showAlertWithMessage("Link sản phẩm bị trống, không thể chia sẻ")
//        }
    }
    
    @objc func doLikeButton(_ sender: UIButton) {
        self.requestLoginWithCompletion {
            self.showProgress()
            HCService.likePost(post_id: self.datasource[sender.tag].id, completion: {(like_count) in
                self.dismissProgress()
                self.datasource[sender.tag].like_count = like_count
                self.datasource[sender.tag].liked = !self.datasource[sender.tag].liked
                
                self.tableView.reloadRows(at: [IndexPath(item: sender.tag, section: 1)], with: .none)
            }) { (error) in
                self.showError(error)
            }
        }
        
    }

}

extension CongDongViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }
        return datasource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CongDongTopTableViewCell", for: indexPath) as! CongDongTopTableViewCell
            
            cell.avatarImageView.sd_setImage(with: URL(string: UserModel.shared.image), placeholderImage: #imageLiteral(resourceName: "icon_default_avatar"))
            
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseCellIdentifier, for: indexPath) as! CongDongTableViewCell
        
        cell.community = datasource[indexPath.row]
        cell.didSelectImageAt = {[weak self](index) in
            if let strongSelf = self {
                strongSelf.openMedia(images: strongSelf.datasource[indexPath.row].images, index: index)
            }
        }
        
        cell.productView.tag = indexPath.row
        cell.profileButton.tag = indexPath.row
        cell.shareButton.tag = indexPath.row
        cell.likeButton.tag = indexPath.row
        cell.productView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openProduct(_:))))
        cell.profileButton.addTarget(self, action: #selector(openProfile(_:)), for: .touchUpInside)
        cell.shareButton.addTarget(self, action: #selector(doShareButton(_:)), for: .touchUpInside)
        cell.likeButton.addTarget(self, action: #selector(doLikeButton(_:)), for: .touchUpInside)
        
        if can_load_more && datasource.count - 1 == indexPath.row {
            loadData(next: true)
        }
        
        return cell
        
    }
}

extension CongDongViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            
            self.requestLoginWithCompletion {
                guard let themBaiVietViewController = ThemBaiVietViewController.viewController(.congdong) as? ThemBaiVietViewController else {return}
                themBaiVietViewController.completion = {[weak self](newPost) in
                    if let strongSelf = self {
                        strongSelf.datasource.insert(newPost, at: 0)
                        strongSelf.tableView.reloadData()
                    }
                }
                self.present(HCNavigationController(rootViewController: themBaiVietViewController), animated: true, completion: nil)
            }
            
        } else {
            let item = datasource[indexPath.row]
            if item.product != nil {
                guard let chiTietSanPhamViewController = ChiTietSanPhamViewController.viewController(.home) as? ChiTietSanPhamViewController else {return}
                chiTietSanPhamViewController.hidesBottomBarWhenPushed = true
                chiTietSanPhamViewController.product_id = item.product!.id
                navigationController?.pushViewController(chiTietSanPhamViewController, animated: true)
            } else {
                guard let chiTietBaiVietViewController = ChiTietCongDongViewController.viewController(.congdong) as? ChiTietCongDongViewController else {return}
                chiTietBaiVietViewController.hidesBottomBarWhenPushed = true
                chiTietBaiVietViewController.id = datasource[indexPath.row].id
                chiTietBaiVietViewController.completion = {(liked, like_count, comment_count, share_count) in
                    self.datasource[indexPath.row].liked = liked
                    self.datasource[indexPath.row].like_count = like_count
                    self.datasource[indexPath.row].comment_count = comment_count
                    self.datasource[indexPath.row].share_count = share_count
                    self.tableView.reloadRows(at: [indexPath], with: .none)
                }
                navigationController?.pushViewController(chiTietBaiVietViewController, animated: true)
            }
            
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 70
        }
        return UITableViewAutomaticDimension
    }
    
}

extension CongDongViewController: UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        self.content = ""
        title = "Cộng đồng"
        navigationItem.rightBarButtonItem = searchItem
        navigationItem.leftBarButtonItem = notificationItem
        navigationItem.titleView = nil
        self.dismiss(animated: false, completion: nil)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.content = searchBar.text ?? ""
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.content = searchBar.text ?? ""
    }
}

extension CongDongViewController: FBSDKSharingDelegate {
    func sharer(_ sharer: FBSDKSharing!, didCompleteWithResults results: [AnyHashable : Any]!) {
        if let _ = self.datasource[productSharingIndex].product {
            HCService.shareProductCount(id: self.datasource[productSharingIndex].product!.id, completion: { [weak self](count) in
                if let strongSelf = self {
                    strongSelf.datasource[strongSelf.productSharingIndex].share_count += 1
                    strongSelf.tableView.reloadData()
                }
            }) { (error) in
                
            }
        }
        
    }
    
    func sharer(_ sharer: FBSDKSharing!, didFailWithError error: Error!) {
        
    }
    
    func sharerDidCancel(_ sharer: FBSDKSharing!) {
        
    }
}
