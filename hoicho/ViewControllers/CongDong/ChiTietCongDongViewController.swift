//
//  ChiTietCongDongViewController.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 4/26/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit
import DKImagePickerController

private let reuseCellIdentifier = "CongDongTableViewCell"
private let reuseHeaderIdentifier = "CommentHeaderView"
private let reuseCellCommentIdentifier = "CommentTableViewCell"
private let reuseCellMediaIdentifier = "RemoveableMediaCollectionViewCell"

class ChiTietCongDongViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var inputTextView: UITextView!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var mediaButton: UIButton!
    @IBOutlet weak var commentViewHeight: NSLayoutConstraint!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var replyLabel: UILabel!
    @IBOutlet weak var replyBottomView: NSLayoutConstraint!
    
    
    var id: Int!
    var datasource: CommunityModel! {
        didSet {
            tableView?.reloadData()
        }
    }
    
    var images = [UIImage]() {
        didSet {
            collectionView?.reloadData()
            if images.count == 0 {
                commentViewHeight.constant = 50
                lineView.isHidden = true
                collectionView.isHidden = true
            } else {
                commentViewHeight.constant = 110
                lineView.isHidden = false
                collectionView.isHidden = false
            }
        }
    }
    var comments = [CommentModel]() {
        didSet {
            tableView?.reloadData()
        }
    }
    
    var replyIndex: Int = 0
    var replyComment: CommentModel! {
        didSet {
            if replyComment == nil {
                replyBottomView.constant = -30
                UIView.animate(withDuration: 0.3) {
                    self.view.layoutIfNeeded()
                }
            } else {
                replyBottomView.constant = 0
                replyLabel.text = "Trả lời bình luận của \(replyComment.account_name)"
                UIView.animate(withDuration: 0.3) {
                    self.view.layoutIfNeeded()
                }
            }
        }
    }
    
    var last_id: Int!
    var can_load_more = true
    var completion: ((Bool, Int, Int, Int) -> Void)?
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        loadData()
    }
    
    func setupUI() {
        title = "Chi tiết bài viết"
        tableView.register(UINib(nibName: reuseCellIdentifier, bundle: nil), forCellReuseIdentifier: reuseCellIdentifier)
        tableView.register(UINib(nibName: reuseHeaderIdentifier, bundle: nil), forHeaderFooterViewReuseIdentifier: reuseHeaderIdentifier)
        tableView.register(UINib(nibName: reuseCellCommentIdentifier, bundle: nil), forCellReuseIdentifier: reuseCellCommentIdentifier)
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 1000
        
        collectionView.register(UINib(nibName: reuseCellMediaIdentifier, bundle: nil), forCellWithReuseIdentifier: reuseCellMediaIdentifier)
        collectionView.delegate = self
        collectionView.dataSource = self
        images = []
    }
    
    func loadData() {
        if id == nil {return}
        showProgress()
        HCService.getCongDongById(id, completion: { [weak self](object) in
            
            self?.datasource = object
            HCService.getComments(last_id: self!.last_id, self!.id, nil, completion: { [weak self](objects) in
                if let strongSelf = self {
                    strongSelf.dismissProgress()
                    if strongSelf.last_id == nil {
                        strongSelf.datasource.comments = objects.reversed()
                    } else {
                        strongSelf.datasource.comments.insert(contentsOf: objects.reversed(), at: 0)
                    }
                    strongSelf.tableView.reloadData()
                    if objects.count == Constant.limit {
                        strongSelf.last_id = objects.first?.id
                        strongSelf.can_load_more = true
                    } else {
                        strongSelf.can_load_more = false
                    }
                }
            }, failure: { (error) in
                self?.showAlertWithMessage("Không thể lấy bình luận")
            })
        }) { (error) in
            self.showError(error)
        }
    }
    
    @IBAction func doMediaButton(_ sender: Any) {
        inputTextView.resignFirstResponder()
        let pickerController = DKImagePickerController()
        pickerController.maxSelectableCount = 5
        pickerController.didSelectAssets = {(assets: [DKAsset]) in
            for asset in assets {
                asset.fetchFullScreenImage(true, completeBlock: { (image: UIImage?, option:[AnyHashable : Any]?) in
                    self.images.append(image!)
                })
                
            }
        }
        
        present(pickerController, animated: true, completion: nil)
    }
    
    @IBAction func doSendButton(_ sender: Any) {
        inputTextView.resignFirstResponder()
        
        if Utilities.isNilOrEmpty(inputTextView.text) && images.count == 0 {
            showAlertWithMessage("Hãy viết bình luận hoặc gửi ảnh")
            return
        }
        
        showProgress()
        HCService.addComment(id: id, parent_comment: replyComment, content: inputTextView.text ?? "", images: images, completion: { [weak self](object) in
            if let strongSelf = self {
                
                strongSelf.datasource.comment_count += 1
                if strongSelf.replyComment == nil {
                    DispatchQueue.main.async {
                        strongSelf.datasource.comments.insert(object, at: 0)
                        strongSelf.tableView.reloadData()
                        strongSelf.tableView.scrollToRow(at: IndexPath.init(row: 0, section: 0), at: .top, animated: true)
                    }
                } else {
                    
                    DispatchQueue.main.async {
                        if strongSelf.datasource.comments[strongSelf.replyIndex].expand {
                            strongSelf.datasource.comments[strongSelf.replyIndex].comments.append(object)
                        } else {
                            strongSelf.datasource.comments[strongSelf.replyIndex].comments = [object]
                        }
                        
                        strongSelf.datasource.comments[strongSelf.replyIndex].comment_count += 1
                        strongSelf.tableView.reloadSections(IndexSet.init(integer: strongSelf.replyIndex + 1), with: .none)
                    }
                    
                }
                strongSelf.dismissProgress()
                strongSelf.inputTextView.text = nil
                strongSelf.replyComment = nil
                strongSelf.images = []
                strongSelf.datasource.comment_count += 1
                strongSelf.tableView.reloadSections(IndexSet.init(integer: 0), with: .none)
                strongSelf.completion?(strongSelf.datasource.liked, strongSelf.datasource.like_count, strongSelf.datasource.comment_count, strongSelf.datasource.comment_count)
            }
        }) { (error) in
            self.showError(error)
        }
    }
    
    @objc func doCloseButton(_ sender: UIButton) {
        self.images.remove(at: sender.tag)
    }
    
    @IBAction func doCloseReplyButton(_ sender: Any) {
        replyComment = nil
    }
    
    @objc func doReplyButton(_ sender: UIButton) {
        replyComment = datasource.comments[sender.tag - 1]
        replyIndex = sender.tag - 1
        inputTextView.becomeFirstResponder()
    }
    
    @objc func doViewMoreButton(_ sender: UIButton) {
        inputTextView.resignFirstResponder()
        
        let item = datasource.comments[sender.tag - 1]
        let currentContentOffset = tableView.contentOffset
        showProgress()
        HCService.getComments(last_id: item.comments.last?.id, id, item.id, completion: { [weak self](objects) in
            if let strongSelf = self {
                strongSelf.dismissProgress()
                strongSelf.datasource.comments[sender.tag - 1].expand = true
                DispatchQueue.main.async {
                    strongSelf.datasource.comments[sender.tag - 1].comments.insert(contentsOf: objects.reversed(), at: 0)
                    strongSelf.tableView.reloadSections(IndexSet.init(integer: sender.tag), with: .none)
                    strongSelf.tableView.setContentOffset(currentContentOffset, animated: false)
                }
            }
        }) { (error) in
            self.showError(error)
        }
    }
    
    @objc func doLikeButton() {
        self.requestLoginWithCompletion {
            self.showProgress()
            HCService.likePost(post_id: self.datasource.id, completion: {(like_count) in
                self.dismissProgress()
                self.datasource.like_count = like_count
                self.datasource.liked = !self.datasource.liked
                self.tableView.reloadData()
                self.completion?(self.datasource.liked, self.datasource.like_count, self.datasource.comment_count, self.datasource.comment_count)
            }) { (error) in
                self.showError(error)
            }
        }
    }
    
    func openProfile(_ id: Int) {
        guard let thongTinCaNhanViewController = ThongTinCaNhanViewController.viewController(.setting) as? ThongTinCaNhanViewController else {return}
        thongTinCaNhanViewController.hidesBottomBarWhenPushed = true
        thongTinCaNhanViewController.user_id = id
        self.navigationController?.pushViewController(thongTinCaNhanViewController, animated: true)
    }
    
    @objc func doProfileButton() {
        self.openProfile(datasource.account_id)
    }
    
    
    
}

extension ChiTietCongDongViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        if datasource == nil {return 0}
        return datasource.comments.count + 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }
        return datasource.comments[section - 1].comments.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: reuseCellIdentifier, for: indexPath) as! CongDongTableViewCell
            
            cell.community = datasource
            cell.lineView.isHidden = false
            
            cell.likeButton.addTarget(self, action: #selector(doLikeButton), for: .touchUpInside)
            
            cell.profileButton.addTarget(self, action: #selector(doProfileButton), for: .touchUpInside)
            
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseCellCommentIdentifier, for: indexPath) as! CommentTableViewCell
        
        let item = datasource.comments[indexPath.section - 1].comments[indexPath.row]
        cell.comment = item
        
        if datasource.comments[indexPath.section - 1].comment_count > datasource.comments[indexPath.section - 1].comments.count {
            cell.viewMoreButtonHeight.constant = 30
            cell.viewMoreButton.isHidden = false
        } else {
            cell.viewMoreButtonHeight.constant = 0
            cell.viewMoreButton.isHidden = true
        }
        cell.viewMoreButton.tag = indexPath.section
        cell.viewMoreButton.addTarget(self, action: #selector(doViewMoreButton(_:)), for: .touchUpInside)
        cell.replyButton.tag = indexPath.section
        cell.replyButton.addTarget(self, action: #selector(doReplyButton(_:)), for: .touchUpInside)
        
        
        cell.openProfile = {
            self.openProfile(item.account_id)
        }
        
        
        return cell
    }
}

extension ChiTietCongDongViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {return nil}
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: reuseHeaderIdentifier) as! CommentHeaderView
        
        headerView.comment = datasource.comments[section - 1]
        headerView.replyButton.tag = section
        headerView.replyButton.addTarget(self, action: #selector(doReplyButton(_:)), for: .touchUpInside)
        headerView.openProfile = {
            self.openProfile(self.datasource.comments[section - 1].account_id)
        }
        return headerView
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return 1000
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return CGFloat.leastNormalMagnitude
        }
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
}

extension ChiTietCongDongViewController: GrowingTextViewDelegate {
    func textViewDidChangeHeight(_ textView: GrowingTextView, height: CGFloat) {
        self.commentViewHeight.constant = height + 16
    }
}

extension ChiTietCongDongViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseCellMediaIdentifier, for: indexPath) as! RemoveableMediaCollectionViewCell
        
        cell.mediaImageView.image = images[indexPath.row]
        cell.closeButton.tag = indexPath.row
        cell.closeButton.addTarget(self, action: #selector(doCloseButton(_:)), for: .touchUpInside)
        cell.closeButtonHeight.constant = 20
        return cell
    }
}

extension ChiTietCongDongViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: 60, height: 60)
    }
}


