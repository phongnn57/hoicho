//
//  ThemBaiVietViewController.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 4/19/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit
import DKImagePickerController

private let reuseCellIdentifier = "RemoveableMediaCollectionViewCell"

class ThemBaiVietViewController: UIViewController {

    @IBOutlet weak var contentTextView: GrowingTextView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var images = [UIImage]() {
        didSet {
            collectionView?.reloadData()
        }
    }
    var completion: ((CommunityModel) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
    }

    func setupUI() {
        title = "Viết bài mới"
        collectionView.register(UINib(nibName: reuseCellIdentifier, bundle: nil), forCellWithReuseIdentifier: reuseCellIdentifier)
        
        let toolbar = UIToolbar.init(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 44))
        toolbar.tintColor = UIColor(hex: "#666666")
        let leftItem = UIBarButtonItem(title: "Thêm ảnh vào bài đăng", style: .plain, target: self, action: nil)
        let spaceItem = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        let imageItem = UIBarButtonItem(image: #imageLiteral(resourceName: "icon_media"), style: .plain, target: self, action: #selector(openImages))
        toolbar.items = [leftItem, spaceItem, imageItem]
        contentTextView.inputAccessoryView = toolbar
        contentTextView.becomeFirstResponder()
        
        let sendItem = UIBarButtonItem(image: #imageLiteral(resourceName: "icon_send"), style: .plain, target: self, action: #selector(doSendItem))
        navigationItem.rightBarButtonItem = sendItem
    }
    
    @objc func openImages() {
        contentTextView.resignFirstResponder()
        let pickerController = DKImagePickerController()
        pickerController.maxSelectableCount = 5
        pickerController.didSelectAssets = {(assets: [DKAsset]) in
            for asset in assets {
                asset.fetchFullScreenImage(true, completeBlock: { (image: UIImage?, option:[AnyHashable : Any]?) in
                    self.images.append(image!)
                })
                
            }
        }
        
        present(pickerController, animated: true, completion: nil)
    }
    
    @objc func doCloseButton(_ sender: UIButton) {
        self.images.remove(at: sender.tag)
    }
    
    @objc func doSendItem() {
        contentTextView.resignFirstResponder()
        
        if Utilities.isNilOrEmpty(contentTextView.text) {
            showAlertWithMessage("Vui lòng nhập nội dung")
            return
        }
        
        showProgress()
        HCService.post(images: images, content: contentTextView.text, completion: { (object) in
            self.dismissProgress()
            self.dismiss(animated: true, completion: {
                self.completion?(object)
            })
        }) { (error) in
            self.showError(error)
        }
    }

}

extension ThemBaiVietViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseCellIdentifier, for: indexPath) as! RemoveableMediaCollectionViewCell
        
        cell.mediaImageView.image = images[indexPath.row]
        cell.closeButton.tag = indexPath.row
        cell.closeButton.addTarget(self, action: #selector(doCloseButton(_:)), for: .touchUpInside)
        
        
        return cell
    }
}

extension ThemBaiVietViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: 128, height: 120)
    }
}

extension ThemBaiVietViewController: GrowingTextViewDelegate {
    func textViewDidChangeHeight(_ textView: GrowingTextView, height: CGFloat) {
        UIView.animate(withDuration: 0.2) {
            self.view.layoutIfNeeded()
        }
    }
}
