//
//  ScanViewController.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 4/18/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit
import ZBarSDK
import FirebaseDynamicLinks
import AVFoundation

extension ZBarSymbolSet: Sequence {
    
    public typealias Iterator = NSFastEnumerationIterator
    
    public func makeIterator() -> NSFastEnumerationIterator {
        return NSFastEnumerationIterator(self)
    }
}

class ScanViewController: ZBarReaderViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.showsZBarControls = false
        self.supportedOrientationsMask = 0
        self.videoQuality = .typeHigh
        self.tracksSymbols = true
        self.scanner.setSymbology(ZBAR_I25, config: ZBAR_CFG_ENABLE, to: 0)
        self.readerView.zoom = 1.0
        self.readerDelegate = self
        
        
        var tempFrame = self.readerView.frame
        tempFrame.size.height += 55
        self.readerView.frame = tempFrame
        
        let overlayView = CameraOverlayView.loadFromNibNamed() as! CameraOverlayView
        overlayView.frame = self.view.bounds
//        overlayView.frame.origin.y = 64
        self.cameraOverlayView = overlayView
        
        title = "Quét mã"
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if AVCaptureDevice.authorizationStatus(for: .video) ==  .authorized {
            //already authorized
        } else {
            AVCaptureDevice.requestAccess(for: .video, completionHandler: { (granted: Bool) in
                if granted {
                    //access allowed
                } else {
                    self.showAlertWithMessage("Vui lòng cấp quyền truy cập máy ảnh", completion: {
                        if #available(iOS 10.0, *) {
                            UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!)
                        } else {
                            UIApplication.shared.openURL(URL(string: UIApplicationOpenSettingsURLString)!)
                        }
                    })
                }
            })
        }
    }
    
    @objc func cancelAction() {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension ScanViewController: ZBarReaderDelegate {
    override func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        let result = info[ZBarReaderControllerResults] as! ZBarSymbolSet
        
        var symbol: ZBarSymbol! = nil
        
        for item in result {
            symbol = item as! ZBarSymbol
            break
        }
        
        if symbol != nil && symbol.data != nil {
            
            let resultString = symbol.data.replacingOccurrences(of: "\r\n", with: "")
            
            let _ = DynamicLinks.dynamicLinks()?.handleUniversalLink(URL(string: resultString)!) { (dynamiclink, error) in
                if let _dynamicLink = dynamiclink {
                    Constant.appDelegate.handleDynamicLink(_dynamicLink)
                } else {
                    self.showAlertWithMessage("Mã không hợp lệ")
                }
            }
            
        } else {
            showAlertWithMessage("Có lỗi xảy ra, không thể nhận dạng được mã")
        }
    }

}

