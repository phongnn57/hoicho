//
//  BaseService.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 4/18/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class BaseService: NSObject {
    class func createHeaderRequest() -> [String:String]? {
        return ["Authorization": "Bearer \(UserModel.shared.token)", "Content-Type": "application/json"]
        
    }

    class func sendRequest(_ method: HTTPMethod, path: String, parameters: Parameters?, completion: @escaping ( _ response: JSON) -> Void, failure:@escaping (_ error: Error) -> Void) {
        
        if Reachability.isInternetAvailable() {
            // get
            if method != .post {
            
                let request = Alamofire.request(Constant.Api.baseUrl + path, method: method, parameters: parameters, encoding: URLEncoding(destination: .methodDependent), headers: createHeaderRequest())
                request.responseJSON { (response: DataResponse<Any>) in
                    processResponse(response, completion: completion, failure: { (error) in
                        if (error as NSError).code == AppErrorCode.unAuthorized.rawValue {
                            reLoginWithToken(completion: { () in
                                sendRequest(method, path: path, parameters: parameters, completion: completion, failure: failure)
                            }, failure: failure)
                        } else {
                            failure(error)
                        }
                        
                    })
                }
                
            } else {
                // post
                var request = URLRequest(url: URL(string: Constant.Api.baseUrl + path)!)
                request.httpMethod = "POST"
                
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                request.setValue("Bearer \(UserModel.shared.token)", forHTTPHeaderField: "Authorization")
                
                if parameters != nil {
                    do {
                        request.httpBody = try JSONSerialization.data(withJSONObject: parameters!, options: JSONSerialization.WritingOptions.prettyPrinted)
                    } catch {
                        request.httpBody = try? JSON(parameters!).rawData()
                    }
                }
                
                Alamofire.request(request).responseJSON { (response: DataResponse<Any>) in
                    processResponse(response, completion: completion, failure: { (error) in
                        if (error as NSError).code == AppErrorCode.unAuthorized.rawValue {
                            reLoginWithToken(completion: { () in
                                sendRequest(method, path: path, parameters: parameters, completion: completion, failure: failure)
                            }, failure: failure)
                        } else {
                            failure(error)
                        }
                        
                    })
                }
            }
            
        } else {
            failure(AppError.networkError())
        }
        
    }
    
    class func sendRawRequest(path: String, parameters: Any, completion: @escaping ( _ response: JSON) -> Void, failure:@escaping (_ error: Error) -> Void) {
        
        if Reachability.isInternetAvailable() {
            var request = URLRequest(url: URL(string: Constant.Api.baseUrl + path)!)
            request.httpMethod = "POST"
            
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.setValue("Bearer \(UserModel.shared.token)", forHTTPHeaderField: "Authorization")
            
            request.httpBody = try! JSONSerialization.data(withJSONObject: parameters, options: JSONSerialization.WritingOptions.prettyPrinted)
            
            Alamofire.request(request).responseJSON { (response: DataResponse<Any>) in
                processResponse(response, completion: completion, failure: failure)
                
            }
            
        } else {
            failure(AppError.networkError())
        }
        
    }
    
    class func reLoginWithToken(completion: @escaping () -> Void, failure:@escaping (_ error: Error) -> Void) {
        if Reachability.isInternetAvailable() {
            var request = URLRequest(url: URL(string: Constant.Api.baseUrl + "/api/v1/expo/refresh-token")!)
            request.httpMethod = "POST"
            
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.setValue("Bearer \(UserModel.shared.token)", forHTTPHeaderField: "Authorization")
            
            
            Alamofire.request(request).responseJSON { (response: DataResponse<Any>) in
                if let statusCode = response.response?.statusCode, statusCode == AppErrorCode.unAuthorized.rawValue {
                    failure(AppError.unAuthorizedError())
                }else if let statusCode = response.response?.statusCode, statusCode == AppErrorCode.serverError.rawValue {
                    failure(AppError.serverError())
                } else if let error = response.result.error,(error as NSError).code == AppErrorCode.timeout.rawValue {
                    failure(AppError.timeoutError())
                } else if let error = response.result.error,(error as NSError).code == AppErrorCode.serverConnection.rawValue {
                    failure(AppError.serverConnection())
                } else if let _ = response.result.error {
                    failure(AppError.commonError())
                } else {
                    if response.result.isSuccess {
                        if let value = response.result.value {
                            let json = JSON(value)
                            
                            //print(json)
                            
                            if json["status"].intValue == 1 {
                                let token = json["data"]["token"].stringValue
                                UserModel.shared.updateToken(token: token)
                                completion()
                            } else {
                                let error = NSError(domain: "com.app.ISG", code: 0, userInfo: [NSLocalizedDescriptionKey: json["message"].stringValue])
                                failure(error)
                            }
                            
                        } else {
                            failure(AppError.serverConnection())
                        }
                    } else {
                        if let error = response.result.error {
                            failure(error)
                        } else {
                            failure(AppError.serverConnection())
                        }
                    }
                }
            }
            
        } else {
            failure(AppError.networkError())
        }
    }
    
    class func processResponse(_ response: DataResponse<Any>, completion: @escaping (_ responseObject: JSON) -> Void, failure:@escaping (_ error: Error) -> Void) {
        
        if let statusCode = response.response?.statusCode, statusCode == AppErrorCode.unAuthorized.rawValue {
            failure(AppError.unAuthorizedError())
        }else if let statusCode = response.response?.statusCode, statusCode == AppErrorCode.serverError.rawValue {
            failure(AppError.serverError())
        } else if let error = response.result.error,(error as NSError).code == AppErrorCode.timeout.rawValue {
            failure(AppError.timeoutError())
        } else if let error = response.result.error,(error as NSError).code == AppErrorCode.serverConnection.rawValue {
            failure(AppError.serverConnection())
        } else if let _ = response.result.error {
            failure(AppError.commonError())
        } else {
            if response.result.isSuccess {
                if let value = response.result.value {
                    let json = JSON(value)
                    print("---------START RESPONSE--------")
                    print(json["data"])
                    print("---------END RESPONSE--------")
                    
                    if json["status"].intValue == 1 || json["status"].intValue == 200 {
                        if json["status"].intValue == 200 {
                            if json["data"]["isClone"].boolValue {
                                let error = NSError(domain: "com.app.ISG", code: 0, userInfo: [NSLocalizedDescriptionKey: "Không có thông tin sản phẩm trên hệ thống"])
                                failure(error)
                            } else {
                                completion(json["data"])
                            }
                        } else {
                            if json["data"].exists() {
                                completion(json["data"])
                            } else {
                                completion(json["message"])
                            }
                            
                        }
                        
                    } else {
                        let error = NSError(domain: "com.app.ISG", code: 0, userInfo: [NSLocalizedDescriptionKey: json["message"].stringValue])
                        failure(error)
                    }
                    
                } else {
                    failure(AppError.serverConnection())
                }
            } else {
                if let error = response.result.error {
                    failure(error)
                } else {
                    failure(AppError.serverConnection())
                }
            }
        }
    }
    
    
}
