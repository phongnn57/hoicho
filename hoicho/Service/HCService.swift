//
//  HCService.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 4/18/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Firebase
import FirebaseInstanceID
import CoreLocation

typealias HCFailure = (Error) -> Void

class HCService: BaseService {
    // đăng nhập
    static func login(phone: String, password: String, completion: @escaping () -> Void, failure: @escaping HCFailure) {
        var params = ["phone": phone, "password": password, "id_app": Constant.idApp]
        if let firebase_token = InstanceID.instanceID().token() {
            params["device_token_ios"] = firebase_token
        }

        sendRequest(.post, path: "/api/v1/expo/login", parameters: params, completion: { (response) in
            
            let userModel = UserModel(response)
            UserModel.shared = userModel
            UserModel.shared.save(response)
            completion()
            NotificationCenter.default.post(name: .UserLoggedIn, object: nil)
            
        }, failure: failure)
    }
    
    // đăng xuất tài khoản
    static func logout(completion: @escaping() -> Void, failure: @escaping HCFailure) {
        sendRequest(.post, path: "/api/v1/expo/logout", parameters: ["type": "ios"], completion: { (_) in
            completion()
        }, failure: failure)
    }
    
    // đăng ký tài khoản
    class func register(type: UserType, image: UIImage, phone: String, name: String, email: String, company: String?, birthday: Date?, region: RegionModel?, address: String, password: String, completion: @escaping() -> Void, failure: @escaping (_ error: Error) -> Void) {
        
        Alamofire.upload(multipartFormData: { (formdata: MultipartFormData) in
            
            formdata.append(Constant.idApp.data(using: String.Encoding.utf8)!, withName: "id_app")
            formdata.append("\(type.rawValue)".data(using: String.Encoding.utf8)!, withName: "type")
            formdata.append(phone.data(using: String.Encoding.utf8)!, withName: "phone")
            formdata.append(name.data(using: String.Encoding.utf8)!, withName: "name")
            formdata.append(email.data(using: String.Encoding.utf8)!, withName: "email")
            if company != nil {
                formdata.append(company!.data(using: String.Encoding.utf8)!, withName: "company_store")
            }
            if birthday != nil {
                formdata.append(birthday!.toString().data(using: String.Encoding.utf8)!, withName: "birthday")
            }
            if region != nil {
                formdata.append(region!.name.data(using: String.Encoding.utf8)!, withName: "region")
            }
            formdata.append(address.data(using: String.Encoding.utf8)!, withName: "address")
            formdata.append(password.data(using: String.Encoding.utf8)!, withName: "password")
            formdata.append(image.dataForUpload(), withName: "image", fileName: "avatar.jpg", mimeType: "image/jpeg")
            
        }, usingThreshold: 999999, to: Constant.Api.baseUrl + "/api/v1/expo/register", method: .post, headers: createHeaderRequest()) { (result: SessionManager.MultipartFormDataEncodingResult) in
            switch result {
                
            case .success(let upload, _, _):
                
                upload.responseJSON { (response: DataResponse<Any>) in
                    
                    processResponse(response, completion: { (object: JSON) in
                        completion()
                    }, failure: failure)
                    
                }
                break
                
            case .failure(let encodingError):
                failure(encodingError)
                break
                
            }
        }
    }
    
    // lấy danh mục sản phẩm
    static func getCategories(completion: @escaping ([CategoryModel]) -> Void, failure: @escaping HCFailure) {
        sendRequest(.get, path: "/api/v1/expo/categories", parameters: ["id_app": Constant.idApp], completion: { (response) in
            
            completion(CategoryModel.fromJSON(response))
            
        }, failure: failure)
    }
    
    // lấy danh sách các danh mục con và các sản phẩm của 1 danh mục
    static func getSubCateAndProducts(category_id: Int, offset: Int, sort: ProductSortType, completion: @escaping([ProductModel], [CategoryModel], [CategoryModel]) -> Void, failure: @escaping HCFailure) {
        sendRequest(.get, path: "/api/v1/category-products", parameters: ["category_id": category_id, "account_id": UserModel.shared.id, "limit": Constant.limit, "offset": offset, "sort": sort.sort.rawValue, "field": sort.key, "id_app": Constant.idApp], completion: { (response) in
            completion(ProductModel.fromJSON(response["product"]), CategoryModel.fromJSON(response["subs"]), CategoryModel.fromJSON(response["breakCum"]))
        }, failure: failure)
    }
    
    static func getProductCategory(category_id: Int, offset: Int, sort: ProductSortType, completion: @escaping([ProductModel]) -> Void, failure: @escaping HCFailure) {
        sendRequest(.get, path: "/api/v1/expo/get-product-category", parameters: ["category_id": category_id, "account_id": UserModel.shared.id, "limit": Constant.limit, "offset": offset, "sort": sort.sort.rawValue, "field": sort.key, "id_app": Constant.idApp], completion: { (response) in
            completion(ProductModel.fromJSON(response))
        }, failure: failure)
    }
    
    static func getSubCategory(category_id: Int, completion: @escaping ([CategoryModel]) -> Void, failure: @escaping HCFailure) {
        sendRequest(.get, path: "/api/v1/expo/sub-categories", parameters: ["category_id": category_id, "id_app": Constant.idApp], completion: { (response) in
            completion(CategoryModel.fromJSON(response))
        }, failure: failure)
    }
    
    // lâý danh sách sản phẩm của 1 thương hiệu
    static func getBrandProducts(brand_id: Int, offset: Int, sort: ProductSortType, completion: @escaping([ProductModel]) -> Void, failure: @escaping HCFailure) {
        sendRequest(.get, path: "/api/v1/expo/brand-products/\(brand_id)", parameters: ["limit": Constant.limit, "offset": offset, "sort": sort.sort.rawValue, "field": sort.key], completion: { (response) in
            completion(ProductModel.fromJSON(response))
        }, failure: failure)
    }
    
    // lấy danh sách cộng đồng
    static func getCommunities(account_id: Int! = nil, content: String, offset: Int, completion: @escaping([CommunityModel]) -> Void, failure: @escaping HCFailure) {
        var params = ["content": content, "id_app": Constant.idApp, "limit": Constant.limit, "offset": offset] as [String : Any]
        if account_id != nil {
            params["account_id"] = account_id!
        }
        sendRequest(.get, path: "/api/v1/expo/community", parameters: params, completion: { (response) in
            completion(CommunityModel.fromJSON(response["post"]))
        }, failure: failure)
    }
    
    // like bai viet
    static func likePost(post_id: Int, completion: @escaping (_ like_count: Int) -> Void, failure: @escaping (_ error: Error) -> Void) {

        sendRequest(.post, path: "/api/v1/expo/community/like-post/\(post_id)", parameters: ["id_app": Constant.idApp], completion: { (response) in
            completion(response["count"].intValue)
        }, failure: failure)
    }
    
    // lấy thông tin chi tiết sản phẩm
    static func getProductDetail(id: Int, completion: @escaping(ProductModel) -> Void, failure: @escaping HCFailure) {
        sendRequest(.get, path: "/api/v1/expo/product/\(id)", parameters: ["id_app": Constant.idApp], completion: { (response) in
            completion(ProductModel(response))
        }, failure: failure)
    }
    
    // lấy thông tin cá nhân của 1 tài khoản
    static func getUserInfomation(user_id: Int! = UserModel.shared.id, completion: @escaping (UserModel) -> Void, failure: @escaping HCFailure) {
        sendRequest(.get, path: "/api/v1/expo/profile", parameters: ["account_id": user_id], completion: { (response) in
            completion(UserModel(response))
        }, failure: failure)
    }
    
    // lấy danh sách khu vực
    static func getRegions(completion: @escaping([RegionModel]) -> Void, failure: @escaping HCFailure) {
        sendRequest(.get, path: "/api/v1/get-cities", parameters: nil, completion: { (response) in
            completion(RegionModel.fromJSON(response: response))
        }, failure: failure)
    }
    
    // lấy danh sách quận huyện
    static func getDistricts(id: String, name: String, completion: @escaping([DistrictModel]) -> Void, failure: @escaping HCFailure) {
        var params = [String: Any]()
        if !id.isEmpty {
            params["province_id"] = id
        }
        if !name.isEmpty {
            params["province_name"] = name
        }
        sendRequest(.get, path: "/api/v1/get-districts", parameters: ["province_id": id, "province_name": name], completion: { (response) in
            completion(DistrictModel.fromJSON(response: response))
        }, failure: failure)
    }
    
    // cập nhật thông tin cá nhân
    static func updateProfile(id: Int = UserModel.shared.id, image: UIImage?, name: String, email: String, birthday: Date?, company: String?, region: RegionModel?, address: String, intro: String, completion: @escaping() -> Void, failure: @escaping HCFailure) {
        Alamofire.upload(multipartFormData: { (formdata: MultipartFormData) in
            
            formdata.append("\(id)".data(using: String.Encoding.utf8)!, withName: "account_id")
            formdata.append(name.data(using: String.Encoding.utf8)!, withName: "name")
            formdata.append(email.data(using: String.Encoding.utf8)!, withName: "email")
            if company != nil {
                formdata.append(company!.data(using: String.Encoding.utf8)!, withName: "company_store")
            }
            if birthday != nil {
                formdata.append(birthday!.toString().data(using: String.Encoding.utf8)!, withName: "birthday")
            }
            if region != nil {
                formdata.append(region!.name.data(using: String.Encoding.utf8)!, withName: "region")
            }
            formdata.append(address.data(using: String.Encoding.utf8)!, withName: "address")
            formdata.append(intro.data(using: String.Encoding.utf8)!, withName: "introduction")
            if image != nil {
                formdata.append(image!.dataForUpload(), withName: "image", fileName: "avatar.jpg", mimeType: "image/jpeg")
            }
            
        }, usingThreshold: 999999, to: Constant.Api.baseUrl + "/api/v1/update-profile", method: .post, headers: createHeaderRequest()) { (result: SessionManager.MultipartFormDataEncodingResult) in
            switch result {
                
            case .success(let upload, _, _):
                
                upload.responseJSON { (response: DataResponse<Any>) in
                    
                    processResponse(response, completion: { (object: JSON) in
                        if UserModel.shared.id == id {
                            let token = UserModel.shared.token
                            UserModel.shared = UserModel(object)
                            UserModel.shared.token = token
                            UserModel.shared.save(object, token)
                        }

                        completion()
                    }, failure: failure)
                    
                }
                break
                
            case .failure(let encodingError):
                failure(encodingError)
                break
                
            }
        }
    }
    
    // lấy danh sách thương hiệu
    static func getBrands(completion: @escaping([BrandModel]) -> Void, failure: @escaping HCFailure) {
        sendRequest(.get, path: "/api/v1/brands", parameters: nil, completion: { (response) in
            completion(BrandModel.fromJSON(response))
        }, failure: failure)
    }
    
    // cập nhật thương hiệu
    static func updateBrand(id: Int, name: String? = nil , image: UIImage? = nil , feature: Bool? = nil, completion: @escaping() -> Void, failure: @escaping HCFailure) {
        Alamofire.upload(multipartFormData: { (formdata: MultipartFormData) in
            
            if name != nil {
                formdata.append(name!.data(using: String.Encoding.utf8)!, withName: "name")
            }
            if image != nil {
                formdata.append(image!.dataForUpload(), withName: "logo", fileName: "logo.jpg", mimeType: "image/jpeg")
            }
            if feature != nil {
                formdata.append((feature! == true ? "1": "0").data(using: String.Encoding.utf8)!, withName: "is_featured")
            }

        }, usingThreshold: 999999, to: Constant.Api.baseUrl + "/api/v1/update-brands/\(id)", method: .post, headers: createHeaderRequest()) { (result: SessionManager.MultipartFormDataEncodingResult) in
            switch result {
                
            case .success(let upload, _, _):
                
                upload.responseJSON { (response: DataResponse<Any>) in
                    
                    processResponse(response, completion: { (object: JSON) in
                        completion()
                    }, failure: failure)
                    
                }
                break
                
            case .failure(let encodingError):
                failure(encodingError)
                break
                
            }
        }
    }
    
    // lấy danh sách chat
    static func getChatConversations(offset: Int, completion: @escaping([ConversationModel]) -> Void, failure: @escaping HCFailure) {
        sendRequest(.get, path: "/api/v1/chat/get-inbox", parameters: ["limit": Constant.limit, "offset": offset], completion: { (response) in
            
            var temp = [ConversationModel]()
            for json in response.arrayValue {
                temp.append(ConversationModel(json))
            }
            completion(ConversationModel.fromJSON(response: response))
            
            
        }, failure: failure)
    }
    
    // lấy danh bạ chat
    static func getContacts(offset: Int, completion: @escaping([ContactModel]) -> Void, failure: @escaping HCFailure) {
        
        let params = ["limit": Constant.limit, "offset": offset] as [String: Any]
        
        sendRequest(.get, path: "/api/v1/chat/get-contact", parameters: params, completion: { (response) in
            
            completion(ContactModel.fromJSON(response: response))
            
        }, failure: failure)
        
    }
    
    // lấy danh sách thành viên
    static func getMembers(_ offset: Int, start_time: Date?, end_time: Date?, phone: String? = "", name: String? = "", region: RegionModel?, completion: @escaping([UserModel], Int) -> Void, failure: @escaping HCFailure) {
        var params = ["limit": Constant.limit, "offset": offset] as [String : Any]
        if phone != nil {
            params["phone"] = phone
        }
        if name != nil {
            params["name"] = name
        }
        if region != nil && !region!.id.isEmpty {
            params["region"] = region!.name
        }
        if start_time != nil {
            params["start_time"] = start_time!.toString()
        }
        if end_time != nil {
            params["end_time"] = end_time!.toString()
        }
        
        sendRequest(.get, path: "/api/v1/expo/members", parameters: params, completion: { (response) in
            
            var temp: [UserModel] = []
            for json in response["member"].arrayValue {
                temp.append(UserModel(json))
            }
            
            let total = response["total"].intValue
            completion(temp, total)
        }, failure: failure)
    }
    
    // lấy danh sách thành viên
    static func khachThamQuan(_ offset: Int, start_time: Date?, end_time: Date?, phone: String? = "", name: String? = "", region: RegionModel?, completion: @escaping([UserModel], Int) -> Void, failure: @escaping HCFailure) {
        var params = ["limit": Constant.limit, "offset": offset] as [String : Any]
        if phone != nil {
            params["phone"] = phone
        }
        if name != nil {
            params["name"] = name
        }
        if region != nil && !region!.id.isEmpty {
            params["region"] = region!.name
        }
        if start_time != nil {
            params["start_time"] = start_time!.toString()
        }
        if end_time != nil {
            params["end_time"] = end_time!.toString()
        }
        
        sendRequest(.get, path: "/api/v1/expo/customer-visit", parameters: params, completion: { (response) in
            
            var temp: [UserModel] = []
            for json in response["visit"].arrayValue {
                temp.append(UserModel(json))
            }
            
            let total = response["total"].intValue
            completion(temp, total)
        }, failure: failure)
    }
    
    // đăng bài viết mới trên cộng đồng
    static func post(type: SocialType? = .congdong, images: [UIImage], content: String, completion: @escaping (CommunityModel) -> Void, failure: @escaping HCFailure) {
        
        Alamofire.upload(multipartFormData: { (formdata: MultipartFormData) in
            
            formdata.append(content.data(using: String.Encoding.utf8)!, withName: "content")
            formdata.append(String(UserModel.shared.id).data(using: String.Encoding.utf8)!, withName: "account_id")
            formdata.append(String(type!.rawValue).data(using: String.Encoding.utf8)!, withName: "type")
            for (index, image) in images.enumerated() {
                formdata.append(image.dataForUpload(), withName: "images[]", fileName: "community\(index + 1).jpg", mimeType: "image/jpeg")
            }
            
        }, usingThreshold: 999999, to: Constant.Api.baseUrl + "/api/v1/create-community", method: .post, headers: createHeaderRequest()) { (result: SessionManager.MultipartFormDataEncodingResult) in
            switch result {
                
            case .success(let upload, _, _):
                
                upload.responseJSON { (response: DataResponse<Any>) in
                    
                    processResponse(response, completion: { (object: JSON) in
                        completion(CommunityModel(object))
                    }, failure: failure)
                    
                }
                break
                
            case .failure(let encodingError):
                failure(encodingError)
                break
                
            }
        }
    }
    
    // lấy chi tiết cộng đồng
    static func getCongDongById(_ id: Int, completion: @escaping(CommunityModel) -> Void, failure: @escaping(Error) -> Void) {
        
        sendRequest(.get, path: "/api/v1/community/\(id)", parameters: nil, completion: { (response) in
            completion(CommunityModel(response))
        }, failure: failure)
        
    }
    
    // lấy danh sách comment
    class func getComments(last_id: Int!, _ id: Int, _ parent_id: Int!, completion: @escaping ([CommentModel]) -> Void, failure: @escaping HCFailure) {
        
        var params = ["limit": Constant.limit]
        if parent_id != nil {
            params["parent_id"] = parent_id
        }
        if last_id != nil {
            params["last_id"] = last_id
        }

        sendRequest(.get, path: "/api/v1/expo/community/list-comment/\(id)", parameters: params, completion: { (response) in
            
            var temp = [CommentModel]()
            for json in response.arrayValue {
                temp.append(CommentModel(json))
            }
            
            completion(temp.reversed())
            
        }, failure: failure)
    }
    
    // Thêm comment
    static func addComment(id: Int, parent_comment: CommentModel!, content: String, images: [UIImage], completion: @escaping (_ object: CommentModel) -> Void, failure: @escaping (_ error: Error) -> Void) {

        Alamofire.upload(multipartFormData: { (formdata: MultipartFormData) in
            
            formdata.append("\(id)".data(using: String.Encoding.utf8)!, withName: "social_post_id")
            formdata.append("\(id)".data(using: String.Encoding.utf8)!, withName: "post_id")
            formdata.append(content.data(using: String.Encoding.utf8)!, withName: "content")
            if parent_comment != nil {
                formdata.append("\(parent_comment!.id)".data(using: String.Encoding.utf8)!, withName: "parent_id")
            }
            
            for image in images {
                formdata.append(image.dataForUpload(), withName: "images[]", fileName: "comment.jpg", mimeType: "image/jpeg")
            }
            
            
        }, usingThreshold: 999999, to: Constant.Api.baseUrl + "/api/v1/add-comment-community-feedback", method: .post, headers: createHeaderRequest()) { (result: SessionManager.MultipartFormDataEncodingResult) in
            switch result {
                
            case .success(let upload, _, _):
                
                upload.responseJSON { (response: DataResponse<Any>) in
                    
                    processResponse(response, completion: { (object: JSON) in
                        completion(CommentModel(object))
                    }, failure: failure)
                    
                }
                break
                
            case .failure(let encodingError):
                failure(encodingError)
                break
                
            }
        }
    }
    
    ///////////////////////////////////////////////////
    // lấy danh sách banner
    static func getBanners(completion: @escaping([BannerModel]) -> Void, failure: @escaping HCFailure) {
        sendRequest(.get, path: "/api/v1/expo/get-banners", parameters: ["id_app": Constant.idApp], completion: { (response) in
            completion(BannerModel.fromJSON(response))
        }, failure: failure)
    }
    
    // lấy danh sách thương hiệu nổi bật
    static func getHighlightBrands(offset: Int = 0, completion: @escaping([BrandModel]) -> Void, failure: @escaping HCFailure) {
        sendRequest(.get, path: "/api/v1/expo/highlight-brands", parameters: ["id_app": Constant.idApp, "offset": offset, "limit": Constant.limit], completion: { (response) in
            completion(BrandModel.fromJSON(response))
        }, failure: failure)
    }
    
    // lấy danh sách sản phẩm nổi bật
    static func getHighlightProducts(offset: Int = 0, completion: @escaping([ProductModel]) -> Void, failure: @escaping HCFailure) {
        sendRequest(.get, path: "/api/v1/expo/highlight-products", parameters: ["offset": offset, "limit": Constant.limit, "id_app": Constant.idApp], completion: { (response) in
            completion(ProductModel.fromJSON(response))
        }, failure: failure)
    }
    
    // lấy danh sách sản phẩm gợi ý
    static func getSuggestProducts(offset: Int = 0, completion: @escaping([ProductModel]) -> Void, failure: @escaping HCFailure) {
        sendRequest(.get, path: "/api/v1/expo/suggested-products", parameters: ["id_app": Constant.idApp, "limit": Constant.limit, "offset": offset], completion: { (response) in
            completion(ProductModel.fromJSON(response))
        }, failure: failure)
    }
    
    // láy danh sách sản phẩm yêu thích
    static func getFavoriteProducts(offset: Int = 0, completion: @escaping([ProductModel]) -> Void, failure: @escaping HCFailure) {
        sendRequest(.get, path: "/api/v1/expo/favorite-products", parameters: ["limit": Constant.limit, "offset": offset], completion: { (response) in
            completion(ProductModel.fromJSON(response))
        }, failure: failure)
    }
    
    // láy danh sách sản phẩm đã xem
    static func getViewedProducts(product_id: Int! = nil, offset: Int = 0, completion: @escaping([ProductModel]) -> Void, failure: @escaping HCFailure) {
        var params = ["limit": Constant.limit, "offset": offset]
        if product_id != nil {
            params["product_id"] = product_id
        }
        sendRequest(.get, path: "/api/v1/expo/viewed-products", parameters: params, completion: { (response) in
            completion(ProductModel.fromJSON(response))
        }, failure: failure)
    }
    
    // lấy danh sách sản phẩm cùng gian hàng
    static func getSanPhamCungGianHang(offset: Int = 0, product_id: Int!, completion: @escaping ([ProductModel]) -> Void, failure: @escaping HCFailure) {
        var params = ["id_app": Constant.idApp, "offset": offset, "limit": Constant.limit] as [String : Any]
        if product_id != nil {
            params["product_id"] = product_id
        }
        sendRequest(.get, path: "/api/v1/expo/relate-products", parameters: params, completion: { (response) in
            completion(ProductModel.fromJSON(response))
        }, failure: failure)
    }
    
    // lấy mã otp quên mật khẩu
    static func getOTP(phone: String, completion: @escaping () -> Void, failure: @escaping HCFailure) {
        sendRequest(.get, path: "/api/v1/expo/get-otp", parameters: ["phone": phone, "id_app": Constant.idApp], completion: { (response) in
            completion()
        }, failure: failure)
    }
    
    // cập nhật mật khẩu mới trong quên mật khẩu
    static func createNewPassword(phone: String, password: String, otp: String, completion: @escaping () -> Void, failure: @escaping HCFailure) {
        sendRequest(.post, path: "/api/v1/expo/change-password", parameters: ["phone": phone, "otp": otp, "password": password, "id_app": Constant.idApp], completion: { (response) in
            completion()
        }, failure: failure)
    }
    
    //  đổi mật khẩu
    static func changePassword(current: String, new: String, completion: @escaping () -> Void, failure: @escaping HCFailure) {
        sendRequest(.post, path: "/api/v1/new-password", parameters: ["old_password": current, "new_password": new], completion: { (response) in
            completion()
        }, failure: failure)
    }
    
    // quan tâm sản phẩm
    static func followProduct(id: Int, completion: @escaping() -> Void, failure: @escaping HCFailure) {
        sendRequest(.post, path: "/api/v1/expo/like/product/\(id)", parameters: nil, completion: { (response) in
            completion()
        }, failure: failure)
    }
    
    // thêm điểm bán
    static func themDiemBan(id: Int, price: Int, phone: String, name: String, city: String, district: String, address: String, location: CLLocationCoordinate2D?, completion: @escaping() -> Void, failure: @escaping HCFailure) {
        
        var params = ["product_id": id, "price": price, "phone": phone, "name": name, "city": city, "district": district, "address": address] as [String: Any]
        if location != nil {
            params["latitude"] = location!.latitude
            params["longitude"] = location!.longitude
            params["lat"] = location!.latitude
            params["lng"] = location!.longitude
        }
        
        sendRequest(.post, path: "/api/v1/expo/add-sale-point", parameters: params, completion: { (response) in
            completion()
        }, failure: failure)
    }
    
    // them diem ban gian hang
    static func themDiemBanGianHang(phone: String, name: String, city: String, district: String, address: String, location: CLLocationCoordinate2D?, completion: @escaping() -> Void, failure: @escaping HCFailure) {
        
        var params = ["phone": phone, "name": name, "city": city, "district": district, "address": address] as [String: Any]
        if location != nil {
            params["latitude"] = location!.latitude
            params["longitude"] = location!.longitude
            params["lat"] = location!.latitude
            params["lng"] = location!.longitude
        }
        
        sendRequest(.post, path: "/api/v1/expo/booth/add-sale-point", parameters: params, completion: { (response) in
            completion()
        }, failure: failure)
    }
    
    // thêm comment vào sản phẩm
    static func commentProduct(product_id: Int, parent_comment_id: Int!, content: String, images: [UIImage], rate: Double, completion: @escaping (CommentModel) -> Void, failure: @escaping HCFailure) {
        Alamofire.upload(multipartFormData: { (formdata: MultipartFormData) in
            
            formdata.append(content.data(using: String.Encoding.utf8)!, withName: "content")
            formdata.append("\(rate)".data(using: String.Encoding.utf8)!, withName: "rate")
            if parent_comment_id != nil {
                formdata.append("\(parent_comment_id!)".data(using: String.Encoding.utf8)!, withName: "parent_id")
            }
            for (index, image) in images.enumerated() {
                formdata.append(image.dataForUpload(), withName: "images[]", fileName: "community\(index + 1).jpg", mimeType: "image/jpeg")
            }
            
        }, usingThreshold: 999999, to: Constant.Api.baseUrl + "/api/v1/expo/comment-product/\(product_id)", method: .post, headers: createHeaderRequest()) { (result: SessionManager.MultipartFormDataEncodingResult) in
            switch result {
                
            case .success(let upload, _, _):
                
                upload.responseJSON { (response: DataResponse<Any>) in
                    
                    processResponse(response, completion: { (object: JSON) in
                        completion(CommentModel(object))
                    }, failure: failure)
                    
                }
                break
                
            case .failure(let encodingError):
                failure(encodingError)
                break
                
            }
        }
    }
    
    // lấy cấu hình gian hang
    static func getCauHinhGianHang(completion: @escaping (GianHangModel) -> Void, failure: @escaping HCFailure) {
        sendRequest(.get, path: "/api/v1/expo/config", parameters: nil, completion: { (response) in
            completion(GianHangModel(response))
        }, failure: failure)
    }
    
    //  cập nhật cấu hình gian hàng
    static func updateCauHinhGianHang(name: String, hotline: String, info: String, intro: String, banner: UIImage?, address: RegionModel?, completion: @escaping() -> Void, failure: @escaping HCFailure) {
        Alamofire.upload(multipartFormData: { (formdata: MultipartFormData) in
            
            formdata.append(name.data(using: String.Encoding.utf8)!, withName: "name")
            formdata.append(hotline.data(using: String.Encoding.utf8)!, withName: "hotline")
            formdata.append(info.data(using: String.Encoding.utf8)!, withName: "info")
            formdata.append(intro.data(using: String.Encoding.utf8)!, withName: "introduction")
            if address != nil {
                formdata.append(address!.name.data(using: String.Encoding.utf8)!, withName: "address")
            }
            if let image = banner {
                formdata.append(image.dataForUpload(), withName: "banner", fileName: "banner.jpg", mimeType: "image/jpeg")
            }
            
        }, usingThreshold: 999999, to: Constant.Api.baseUrl + "/api/v1/expo/config/update", method: .post, headers: createHeaderRequest()) { (result: SessionManager.MultipartFormDataEncodingResult) in
            switch result {
                
            case .success(let upload, _, _):
                
                upload.responseJSON { (response: DataResponse<Any>) in
                    
                    processResponse(response, completion: { (object: JSON) in
                        completion()
                    }, failure: failure)
                    
                }
                break
                
            case .failure(let encodingError):
                failure(encodingError)
                break
                
            }
        }
    }
    
    // lấy danh sách điểm bán của sản phẩm
    static func getSalePoint(offset: Int = 0, product_id: Int, completion: @escaping([DiemBanModel]) -> Void, failure: @escaping HCFailure) {
        sendRequest(.get, path: "/api/v1/expo/get-sale-point/\(product_id)", parameters: ["id_app": Constant.idApp, "offset": offset, "limit": Constant.limit], completion: { (response) in
            completion(DiemBanModel.fromJSON(response))
        }, failure: failure)
    }
    
    // lấy danh sách comment sản phẩm
    static func getCommentProduct(product_id: Int, last_id: Int!, limit: Int = Constant.limit, completion: @escaping ([CommentModel]) -> Void, failure: @escaping HCFailure) {
        var params = ["limit": limit]
        if last_id != nil {
            params["last_id"] = last_id
        }
        sendRequest(.get, path: "/api/v1/expo/comment-product/\(product_id)", parameters: params, completion: { (response) in
            completion(CommentModel.fromJSON(response))
        }, failure: failure)
    }
    
    // lấy danh sách gian hàng
    static func getBooths(offset: Int = 0, limit: Int = Constant.limit, is_follow: Bool = false, name: String = "", completion: @escaping ([GianHangModel]) -> Void, failure: @escaping HCFailure) {
        var path = "/api/v1/expo/booths"
        if is_follow {
            path = "/api/v1/expo/list-follow-booth"
        }
        sendRequest(.get, path: path, parameters: ["limit": limit, "offset": offset, "name": name], completion: { (response) in
            completion(GianHangModel.fromJSON(response))
        }, failure: failure)
    }
    
    // danh sách điểm bán của gian hàng
    static func getSalePointOfBooth(offset: Int = 0, completion: @escaping([DiemBanModel]) -> Void, failure: @escaping HCFailure) {
        sendRequest(.get, path: "/api/v1/expo/booth/get-sale-point", parameters: ["id_app": Constant.idApp, "offset": offset, "limit": Constant.limit], completion: { (response) in
            completion(DiemBanModel.fromJSON(response))
        }, failure: failure)
    }
    
    // lấy thông tin chi tiết gian hàng
    static func getBoothDetail(id: Int, completion: @escaping(GianHangModel) -> Void, failure: @escaping HCFailure) {
        sendRequest(.get, path: "/api/v1/expo/shop/\(id)", parameters: ["id_app": Constant.idApp], completion: { (response) in
            completion(GianHangModel(response["booth"]))
        }, failure: failure)
    }
    
    // đánh giá gian hàng
    static func danhGiaGianHang(id: Int, content: String, rate: Double, completion: @escaping (RateModel) -> Void, failure: @escaping HCFailure) {
        sendRequest(.post, path: "/api/v1/expo/shop/\(id)/rate", parameters: ["content": content, "rate_point": rate], completion: { (response) in
            completion(RateModel(response))
        }, failure: failure)
    }
    
    // danh sách đánh giá gian hàng
    static func getBoothRates(id: Int, offset: Int = 0, completion: @escaping ([RateModel]) -> Void, failure: @escaping HCFailure) {
        sendRequest(.get, path: "/api/v1/expo/shop/\(id)/rates", parameters: ["offset": offset, "limit": Constant.limit], completion: { (response) in
            completion(RateModel.fromJSON(response))
        }, failure: failure)
    }
    
    // Tăng lượt chia sẻ sản phẩm
    static func shareProductCount(id: Int, completion: @escaping (Int) -> Void, failure: @escaping HCFailure) {
        sendRequest(.post, path: "/api/v1/expo/share-product/\(id)", parameters: nil, completion: { (response) in
            completion(response["share"].intValue)
        }, failure: failure)
    }
    
    // tìm sản phẩm hoặc gian hàng
    static func searchProducts(text: String, offset: Int, completion: @escaping ([ProductModel]) -> Void, failure: @escaping HCFailure) {
        sendRequest(.get, path: "/api/v1/expo/search-product", parameters: ["q": text, "limit": Constant.limit, "offset": offset, "id_app": Constant.idApp], completion: { (response) in
            completion(ProductModel.fromJSON(response["product"]))
        }, failure: failure)
    }
    
    // tìm kiếm gian hàng
    static func searchBooths(text: String, offset: Int, completion: @escaping ([GianHangModel]) -> Void, failure: @escaping HCFailure) {
        sendRequest(.get, path: "/api/v1/expo/search-booth", parameters: ["q": text, "limit": Constant.limit, "offset": offset, "id_app": Constant.idApp], completion: { (response) in
            completion(GianHangModel.fromJSON(response["booth"]))
        }, failure: failure)
    }
    
    // tim kiem diem ban
    static func searchSalePoint(text: String, offset: Int, completion: @escaping ([DiemBanModel]) -> Void, failure: @escaping HCFailure) {
        sendRequest(.get, path: "/api/v1/expo/search-sale-point", parameters: ["q": text, "limit": Constant.limit, "offset": offset, "id_app": Constant.idApp], completion: { (response) in
            completion(DiemBanModel.fromJSON(response["salePoint"]))
        }, failure: failure)
    }
    
    // lấy danh mục gian hàng
    static func getDanhMucGianHang(id: Int, completion: @escaping ([CategoryModel]) -> Void, failure: @escaping HCFailure) {
        sendRequest(.get, path: "/api/v1/expo/booth/get-category/\(id)", parameters: nil, completion: { (response) in
            completion(CategoryModel.fromJSON(response))
        }, failure: failure)
    }
    
    // them san pham
    static func themSanPham(mainImage: UIImage?, images: [UIImage], name: String, code: String, price: Int, showBanSi: Bool, banSiTu: Int, banSiDen: Int, soSanPhamToiThieu: Int, dvt: String, provider_id: Int?, department_id: Int?, madeIn: String, description_text: String, status: Int, featured: Int, categories: [CategoryModel], completion: @escaping() -> Void, failure: @escaping(Error) -> Void) {
        
        Alamofire.upload(multipartFormData: { (formdata: MultipartFormData) in
            
            if mainImage != nil {
                formdata.append(mainImage!.dataForUpload(fileSize: 1), withName: "image", fileName: "avt.jpg", mimeType: "image/jpeg")
            }
            for image in images {
                formdata.append(image.dataForUpload(fileSize: 1), withName: "images[]", fileName: "avt.jpg", mimeType: "image/jpeg")
            }
            
            formdata.append(name.data(using: String.Encoding.utf8)!, withName: "name")
            
            formdata.append(code.data(using: String.Encoding.utf8)!, withName: "code")
            
            // ban si
            formdata.append("\(showBanSi == true ? 1 : 0)".data(using: String.Encoding.utf8)!, withName: "view_wholesale")
            if showBanSi {
                if banSiTu > 0 {
                    formdata.append("\(banSiTu)".data(using: String.Encoding.utf8)!, withName: "wholesale_price_from")
                }
                if banSiDen > 0 {
                    formdata.append("\(banSiDen)".data(using: String.Encoding.utf8)!, withName: "wholesale_price_to")
                }
                if soSanPhamToiThieu > 0 {
                    formdata.append("\(soSanPhamToiThieu)".data(using: String.Encoding.utf8)!, withName: "wholesale_count_product")
                }
            }
            
            formdata.append("\(price)".data(using: String.Encoding.utf8)!, withName: "price")
            
            formdata.append(dvt.data(using: String.Encoding.utf8)!, withName: "dvt")
            if provider_id != nil {
                formdata.append("\(provider_id!)".data(using: String.Encoding.utf8)!, withName: "provider_id")
            }
            if department_id != nil {
                formdata.append("\(department_id!)".data(using: String.Encoding.utf8)!, withName: "department_id")
            }
            
            formdata.append(madeIn.data(using: String.Encoding.utf8)!, withName: "madeIn")
            formdata.append(description_text.data(using: String.Encoding.utf8)!, withName: "description")
            formdata.append("\(status)".data(using: String.Encoding.utf8)!, withName: "status")
            formdata.append("\(featured)".data(using: String.Encoding.utf8)!, withName: "is_featured")

            for category in categories {
                formdata.append("\(category.id)".data(using: String.Encoding.utf8)!, withName: "categories[]")
            }

        }, usingThreshold: 999999, to: Constant.Api.baseUrl + "/api/v1/add-product", method: .post, headers: createHeaderRequest()) { (result: SessionManager.MultipartFormDataEncodingResult) in
            switch result {
                
            case .success(let upload, _, _):
                
                upload.responseJSON { (response: DataResponse<Any>) in
                    
                    processResponse(response, completion: { (object: JSON) in
                        
                        completion()
                    }, failure: failure)
                }
                break
            case .failure(let encodingError):
                failure(encodingError)
                break
                
            }
        }
        
    }
    
    // danh sacsh ve tham quan
    static func getTickets(offset: Int = 0, name: String = "", completion: @escaping ([TicketModel]) -> Void, failure: @escaping HCFailure) {
        sendRequest(.get, path: "/api/v1/expo/ticket", parameters: ["limit": Constant.limit, "offset": offset, "name": name], completion: { (response) in
            completion(TicketModel.fromJSON(response))
        }, failure: failure)
    }
    
    // quan ly tong hop
    static func getBaiVietTongHop(type: TongHopType, offset: Int, category_id: Int?, name: String?, status: Bool, completion: @escaping([TongHopModel], Int, Bool) -> Void, failure: @escaping(Error) -> Void) {
        var params = ["limit": Constant.limit, "offset": offset, "type": type.rawValue, "id_app": Constant.idApp] as [String: Any]
        if category_id != nil {
            params["category_id"] = category_id!
        }
        if name != nil {
            if type == .hoidap {
                params["key_search"] = name!
            } else {
                params["name"] = name!
            }
        }
        var path = "/api/v1/expo/general-info/get-posts"
        if type == .hoidap {
            params["status"] = status
            path = "/api/v1/expo/question/get-posts"
        }
        sendRequest(.get, path: path, parameters: params, completion: { (response) in
            
            var temp = [TongHopModel]()
            if response["objects"].exists() {
                for json in response["objects"].arrayValue {
                    temp.append(TongHopModel(json))
                }
            } else if response["object"].exists() {
                for json in response["object"].arrayValue {
                    temp.append(TongHopModel(json))
                }
            } else if response["posts"].exists() {
                for json in response["posts"].arrayValue {
                    temp.append(TongHopModel(json))
                }
            }
            
            var total = 0
            if response["total"].exists() {
                total = response["total"].intValue
            } else if response["total_post"].exists() {
                total = response["total_post"].intValue
            }
            
            completion(temp, total, response["subcribe_isg"].boolValue)
            
        }, failure: failure)
    }
    
    static func getChiTietBaiViet(type: TongHopType, id: Int, completion: @escaping(String) -> Void, failure: @escaping(Error) -> Void) {
        
        var path = "/api/v1/expo/general-info/get-posts/\(id)"
        if type == .hoidap {
            path = "/api/v1/expo/question/get-posts/\(id)"
        }
        sendRequest(.get, path: path, parameters: ["id_app": Constant.idApp], completion: { (response) in
            if type == .hoidap {
                completion(response["answer"].stringValue)
            } else {
                completion(response["content"].stringValue)
            }
            
        }, failure: failure)
    }
    
    static func getDanhMucTongHop(type: TongHopType, offset: Int, completion: @escaping([DanhMucModel]) -> Void, failure: @escaping(Error) -> Void) {
        var path = "/api/v1/question/get-categories"
        if type != .hoidap {
            path = "/api/v1/general-info/get-categories"
        }
        sendRequest(.get, path: path, parameters: ["limit": Constant.limit, "offset": offset, "type": type.rawValue], completion: { (response) in
            var temp = [DanhMucModel]()
            for json in response.arrayValue {
                temp.append(DanhMucModel(json))
            }
            completion(temp)
        }, failure: failure)
    }
    
    static func xoaDanhMuc(type: TongHopType, id: Int, completion: @escaping() -> Void, failure: @escaping(Error) -> Void) {
        
        var path = "/api/v1/general-info/delete-categories/\(id)"
        if type == .hoidap {
            path = "/api/v1/question/delete-categories/\(id)"
        }
        
        sendRequest(.delete, path: path, parameters: nil, completion: { (_) in
            completion()
        }, failure: failure)
    }
    
    static func taoDanhMuc(id: Int?, type: TongHopType, name: String, title: String, desc: String, meta_keyword: String, meta_description: String, completion: @escaping() -> Void, failure: @escaping(Error) -> Void) {
        
        var path = "/api/v1/general-info/create-categories"
        if type == .hoidap {
            path = "/api/v1/question/create-categories"
        }
        if id != nil {
            path = "/api/v1/general-info/update-categories/\(id!)"
            if type == .hoidap {
                path = "/api/v1/question/update-categories/\(id!)"
            }
        }
        
        sendRequest(.post, path: path, parameters: ["name": name, "type": type.rawValue, "title": title, "description": desc, "meta_keyword": meta_keyword, "meta_description": meta_description], completion: { (response) in
            completion()
        }, failure: failure)
    }
    
    //////////////////////////////////////////////////////////
    
    static func countUnreadInbox(completion: @escaping(Int) -> Void, failure: @escaping (Error) -> Void) {
        sendRequest(.get, path: "/api/v1/chat/count-unread-inbox", parameters: nil, completion: { (response) in
            completion(response.intValue)
            NotificationCenter.default.post(name: NSNotification.Name.countUnreadNotification, object: nil)
        }, failure: failure)
    }
    
    static func countUnreadPushNotification(completion: @escaping(Int) -> Void, failure: @escaping (Error) -> Void) {
        sendRequest(.get, path: "/api/v1/count-all", parameters: nil, completion: { (response) in
            completion(response["count"].intValue)
        }, failure: failure)
    }
    
    static func createNewChat(type: Int, member: [Int], title: String = "", image: UIImage?, completion: @escaping(String) -> Void, failure: @escaping(Error) -> Void) {
        
        Alamofire.upload(multipartFormData: { (formdata: MultipartFormData) in
            
            formdata.append("\(type)".data(using: String.Encoding.utf8)!, withName: "type")
            formdata.append(title.data(using: String.Encoding.utf8)!, withName: "title")
            for member_id in member {
                formdata.append("\(member_id)".data(using: String.Encoding.utf8)!, withName: "member[]")
            }
            if let _image = image {
                formdata.append(_image.dataForUpload(), withName: "image", fileName: "image", mimeType: "image/jpeg")
            }
        }, usingThreshold: 999999, to: Constant.Api.baseUrl + "/api/v1/chat/create-new-chat", method: .post, headers: createHeaderRequest()) { (result: SessionManager.MultipartFormDataEncodingResult) in
            switch result {
                
            case .success(let upload, _, _):
                
                upload.responseJSON { (response: DataResponse<Any>) in
                    
                    processResponse(response, completion: { (object: JSON) in
                        completion(object["code"].stringValue)
                    }, failure: failure)
                    
                }
                break
                
            case .failure(let encodingError):
                failure(encodingError)
                break
                
            }
        }
        
    }
    
    // cap nhat thong tin chat
    static func updateGroupChat(id: String, title: String, image: UIImage?, completion: @escaping () -> Void, failure: @escaping HCFailure) {
        Alamofire.upload(multipartFormData: { (formdata: MultipartFormData) in
            
            formdata.append(id.data(using: String.Encoding.utf8)!, withName: "conver")
            formdata.append(title.data(using: String.Encoding.utf8)!, withName: "title")
            if let _image = image {
                formdata.append(_image.dataForUpload(), withName: "image", fileName: "image", mimeType: "image/jpeg")
            }
        }, usingThreshold: 999999, to: Constant.Api.baseUrl + "/api/v1/chat/update-info-group", method: .post, headers: createHeaderRequest()) { (result: SessionManager.MultipartFormDataEncodingResult) in
            switch result {
                
            case .success(let upload, _, _):
                
                upload.responseJSON { (response: DataResponse<Any>) in
                    
                    processResponse(response, completion: { (object: JSON) in
                        completion()
                    }, failure: failure)
                    
                }
                break
                
            case .failure(let encodingError):
                failure(encodingError)
                break
                
            }
        }
    }
    
    static func addMembersToChat(id: String, members: [Int], completion: @escaping () -> Void, failure: @escaping HCFailure) {
        Alamofire.upload(multipartFormData: { (formdata: MultipartFormData) in
            
            formdata.append(id.data(using: String.Encoding.utf8)!, withName: "conver")
            for member_id in members {
                formdata.append("\(member_id)".data(using: String.Encoding.utf8)!, withName: "member[]")
            }
        }, usingThreshold: 999999, to: Constant.Api.baseUrl + "/api/v1/chat/add-member-group", method: .post, headers: createHeaderRequest()) { (result: SessionManager.MultipartFormDataEncodingResult) in
            switch result {
                
            case .success(let upload, _, _):
                
                upload.responseJSON { (response: DataResponse<Any>) in
                    
                    processResponse(response, completion: { (object: JSON) in
                        completion()
                    }, failure: failure)
                    
                }
                break
                
            case .failure(let encodingError):
                failure(encodingError)
                break
                
            }
        }
 
    }
    
    // lay lich su chat
    static func getChatHistories(conversation_id: String, last_id: Int!, completion: @escaping([ChatModel]) -> Void, failure: @escaping(Error) -> Void) {
        var params = ["id_conversion": conversation_id, "limit": Constant.limit] as [String : Any]
        if last_id != nil {
            params["last_id"] = last_id
        }
        sendRequest(.get, path: "/api/v1/chat/get-message", parameters: params, completion: { (response) in

            completion(ChatModel.fromJSON(response).reversed())
            NotificationCenter.default.post(name: NSNotification.Name.countUnreadInbox, object: nil)

        }, failure: failure)
    }
    
    // gui tin nhan chat
    static func sendChat(conversation_id: String, ui_id: String, message: String?, image: UIImage?, images: [String]?, product_id: Int!, completion: @escaping(ChatModel) -> Void, failure: @escaping(Error) -> Void) {
        
        Alamofire.upload(multipartFormData: { (formdata: MultipartFormData) in
            
            formdata.append(conversation_id.data(using: String.Encoding.utf8)!, withName: "id")
            formdata.append(ui_id.data(using: String.Encoding.utf8)!, withName: "ui_id")
            if let _message = message {
                formdata.append(_message.trimpSpace().data(using: String.Encoding.utf8)!, withName: "message")
            }
            if let _image = image {
                formdata.append(_image.dataForUpload(), withName: "image", fileName: "chatimage", mimeType: "image/jpeg")
            }
            if let _images = images {
                for object in _images {
                    formdata.append(object.data(using: String.Encoding.utf8)!, withName: "listImages[]")
                }
            }
            if product_id != nil {
                formdata.append("\(product_id!)".data(using: String.Encoding.utf8)!, withName: "product_id")
            }
            
            if let socket_id = UserDefaults.standard.string(forKey: "socket_id") {
                formdata.append(socket_id.data(using: String.Encoding.utf8)!, withName: "socket_id")
            }
            
        }, usingThreshold: 999999, to: Constant.Api.baseUrl + "/api/v1/chat/send-chat", method: .post, headers: createHeaderRequest()) { (result: SessionManager.MultipartFormDataEncodingResult) in
            switch result {
                
            case .success(let upload, _, _):
                
                upload.responseJSON { (response: DataResponse<Any>) in
                    
                    processResponse(response, completion: { (object: JSON) in
                        completion(ChatModel(object["dataMessage"]))
                    }, failure: failure)
                    
                }
                break
                
            case .failure(let encodingError):
                failure(encodingError)
                break
                
            }
        }
    }
    
    // follow gian hang
    static func followGianHang(_ id: Int, completion: @escaping () -> Void, failure: @escaping HCFailure) {
        sendRequest(.post, path: "/api/v1/expo/follow", parameters: ["id": id], completion: { (response) in
            completion()
        }, failure: failure)
    }
    
    // diem ban gian hang
    static func diemBanGianHang(_ id: Int, offset: Int, completion: @escaping ([DiemBanModel]) -> Void, failure: @escaping HCFailure) {
        sendRequest(.get, path: "/api/v1/expo/booth/list-sale-point/\(id)", parameters: ["offset": offset, "limit": Constant.limit, "id_app": Constant.idApp], completion: { (response) in
            completion(DiemBanModel.fromJSON(response))
        }, failure: failure)
    }
    
    // san pham cua 1 gian hang
    static func getProductBooth(booth_id: Int, category_id: Int!, offset: Int, completion: @escaping ([ProductModel]) -> Void, failure: @escaping HCFailure) {
        var params = ["limit": Constant.limit, "offset": offset, "id_app": Constant.idApp] as [String : Any]
        if category_id != nil {
            params["category_id"] = category_id!
        }
        sendRequest(.get, path: "/api/v1/expo/get-product-booth/\(booth_id)", parameters: params, completion: { (response) in
            completion(ProductModel.fromJSON(response))
        }, failure: failure)
    }
    
    // chi tiet diem ban
    static func getDetailSalePoint(phone: String, product_id: Int!, offset: Int, completion: @escaping ([ProductModel], DiemBanModel) -> Void, failure: @escaping HCFailure) {
        var params = ["phone": phone, "id_app": Constant.idApp, "offset": offset, "limit": Constant.limit] as [String: Any]
        if product_id != nil {
            params["product_id"] = product_id!
        }
        sendRequest(.get, path: "/api/v1/expo/booth/detail-sale-point", parameters: params, completion: { (response) in
            completion(ProductModel.fromJSON(response["products"]["data"]), DiemBanModel(response["salePoint"]))
        }, failure: failure)
    }
    
    // thong tin group chat
    static func getChatGroupInfo(id_conversation: String, completion: @escaping (ConversationInfo) -> Void, failure: @escaping (Error) -> Void) {
        sendRequest(.get, path: "/api/v1/chat/get-info-group", parameters: ["conver": id_conversation], completion: { (response) in
            completion(ConversationInfo(response))
        }, failure: failure)
    }
    
    // kho anh
    static func getKhoAnh(offset: Int, completion: @escaping([KhoAnhModel]) -> Void, failure: @escaping(Error) -> Void) {
        sendRequest(.get, path: "/api/v1/chat/get-image-warehouse", parameters: ["limit": Constant.limit, "offset": offset], completion: { (response) in
            var temp = [KhoAnhModel]()
            for json in response.arrayValue {
                temp.append(KhoAnhModel(json))
            }
            completion(temp)
        }, failure: failure)
    }
    
    // xoa kho anh
    static func xoaKhoAnh(id: Int, completion: @escaping() -> Void, failure: @escaping(Error) -> Void) {
        sendRequest(.delete, path: "/api/v1/chat/delete-image-warehouse/\(id)", parameters: nil, completion: { (response) in
            completion()
        }, failure: failure)
    }
    
    //////////////////////////////////////////////////thong bao/////////////////////////////////////////////////////
    static func getNotifications(_ offset: Int, completion: @escaping (_ objects: [NotificationModel]) -> Void, failure: @escaping (_ error: Error) -> Void) {
        sendRequest(.get, path: "/api/v1/notifications", parameters: ["account_id": UserModel.shared.id, "limit": Constant.limit, "offset": offset], completion: { (response) in
            
            var temp = [NotificationModel]()
            for json in response.arrayValue {
                temp.append(NotificationModel(json))
            }
            completion(temp)
            
        }, failure: failure)
    }
    
    static func readNotification(_ notificationId: Int?, completion: @escaping () -> Void, failure: @escaping (_ error: Error) -> Void) {
        var params = [String: Any]()
        if notificationId != nil {
            params["ids[]"] = notificationId!
            params["all"] = 0
        } else {
            params["all"] = 1
        }
        
        Alamofire.upload(multipartFormData: { (formdata: MultipartFormData) in
            if notificationId != nil {
                formdata.append("\(notificationId!)".data(using: String.Encoding.utf8)!, withName: "ids[]")
            } else {
                formdata.append("\(1)".data(using: String.Encoding.utf8)!, withName: "all")
            }
            
        }, usingThreshold: 999999, to: Constant.Api.baseUrl + "/api/v1/read-notifications", method: .post, headers: createHeaderRequest()) { (result: SessionManager.MultipartFormDataEncodingResult) in
            switch result {
                
            case .success(let upload, _, _):
                
                upload.responseJSON { (response: DataResponse<Any>) in
                    
                    processResponse(response, completion: { (object: JSON) in
                        completion()
                    }, failure: failure)
                    
                }
                break
                
            case .failure(let encodingError):
                failure(encodingError)
                break
                
            }
        }
        
    }
    
    class func countUnreadNotification(completion: @escaping (_ count: Int) -> Void, failure: @escaping (_ error: Error) -> Void) {
        sendRequest(.get, path: "/api/v1/count-notification", parameters: ["account_id": UserModel.shared.id], completion: { (response) in
            completion(response.intValue)
            NotificationCenter.default.post(name: NSNotification.Name.countUnreadNotification, object: nil)
        }, failure: failure)
    }
    
    // đồng bộ danh bạ
    static func syncContacts(phones: [String], completion: @escaping () -> Void, failure: @escaping HCFailure) {
        
        Alamofire.upload(multipartFormData: { (formdata: MultipartFormData) in
            for phone in phones {
                formdata.append(phone.data(using: String.Encoding.utf8)!, withName: "phones[]")
            }
            
        }, usingThreshold: 999999, to: Constant.Api.baseUrl + "/api/v1/expo/friend/sync-contact", method: .post, headers: createHeaderRequest()) { (result: SessionManager.MultipartFormDataEncodingResult) in
            switch result {
                
            case .success(let upload, _, _):
                
                upload.responseJSON { (response: DataResponse<Any>) in
                    
                    processResponse(response, completion: { (object: JSON) in
                        completion()
                    }, failure: failure)
                    
                }
                break
                
            case .failure(let encodingError):
                failure(encodingError)
                break
                
            }
        }

    }

    // check friend status
    static func checkFriendStatus(id: Int, completion: @escaping (FriendStatus) -> Void, failure: @escaping HCFailure) {
        sendRequest(.get, path: "/api/v1/expo/friend/check-status/\(id)", parameters: nil, completion: { (response) in
            completion(FriendStatus(rawValue: response["status"].intValue) ?? .not_friend)
        }, failure: failure)
    }
    
    // request ket ban
    static func addFriend(id: Int, completion: @escaping () -> Void, failure: @escaping HCFailure) {
        sendRequest(.post, path: "/api/v1/expo/friend/add/\(id)", parameters: nil, completion: { (response) in
            completion()
        }, failure: failure)
    }
    
    // danh sach ban be
    static func getFriends(offset: Int, completion: @escaping ([ContactModel]) -> Void, failure: @escaping HCFailure) {
        sendRequest(.get, path: "/api/v1/expo/friend/list", parameters: ["limit": Constant.limit, "offset": offset], completion: { (response) in
            completion(ContactModel.fromJSON(response: response))
        }, failure: failure)
    }
    
    // chap nhan, huy ket ban
    static func responseFriendRequest(id: Int, accept: Bool, completion: @escaping() -> Void, failure: @escaping HCFailure) {
        sendRequest(.post, path: "/api/v1/expo/friend/action", parameters: ["type": accept == true ? 1 : 2, "friendId": id], completion: { (response) in
            completion()
        }, failure: failure)
    }
    
    static func removeFriend(id: Int, completion: @escaping () -> Void, failure: @escaping HCFailure) {
        sendRequest(.post, path: "/api/v1/expo/friend/remove/\(id)", parameters: nil, completion: { (response) in
            completion()
        }, failure: failure)
    }
    
    // danh sach request
    static func getFriendRequest(offset: Int, completion: @escaping ([UserModel]) -> Void, failure: @escaping HCFailure) {
        sendRequest(.get, path: "/api/v1/expo/friend/list-request", parameters: ["limit": Constant.limit, "offset": offset], completion: { (response) in
            var temp = [UserModel]()
            for json in response.arrayValue {
                temp.append(UserModel(json))
            }
            completion(temp)
        }, failure: failure)
    }
    
    // lay bai khao sat
    static func getSurvey(completion: @escaping (SurveyModel) -> Void, failure: @escaping HCFailure) {
        sendRequest(.get, path: "/api/v1/expo/survey", parameters: nil, completion: { (response) in
            completion(SurveyModel.init(response))
        }, failure: failure)
    }
    
    // kiem tra xem da lam khao sat hay chua
    static func checkSurvey(completion: @escaping (Bool) -> Void, failure: @escaping HCFailure) {
        sendRequest(.get, path: "/api/v1/expo/survey/check-survey", parameters: nil, completion: { (response) in
            
        }, failure: failure)
    }
    
    // nop bai khao sat
    static func finishSurvey(survey: SurveyModel, completion: @escaping () -> Void, failure: @escaping HCFailure) {
        
        var temp = [[String: Any]]()
        for question in survey.questions {
            var dict = [String: Any]()
            dict["idQuestion"] = question.id
            if let answer = question.answers.filter({ (object) -> Bool in
                return object.selected == true
            }).first {
                dict["idAnswer"] = answer.id
                dict["content"] = answer.answer_content
            }
            
            temp.append(dict)
        }
        
        sendRequest(.post, path: "/api/v1/expo/survey/add-survey-account/\(survey.id)", parameters: ["survey": temp], completion: { (response) in
            completion()
        }, failure: failure)

    }
    
    // tim nguoi tren bigdata
    static func findUser(_ phone: String, completion: @escaping (_ object: UserModel) -> Void, failure: @escaping (_ error: Error) -> Void) {
        sendRequest(.get, path: "/api/v1/find-info", parameters: ["phone": phone], completion: { (response) in
            completion(UserModel(response))
        }, failure: failure)
    }
    
    // danh sach quan tri vien
    
    static func getAllAdministrator(offset: Int, completion: @escaping([UserModel]) -> Void, failure: @escaping(Error) -> Void) {
        sendRequest(.get, path: "/api/v1/list-administrators", parameters: ["limit": Constant.limit, "offset": offset], completion: { (response) in
            
            var temp = [UserModel]()
            for json in response.arrayValue {
                temp.append(UserModel(json))
            }
            completion(temp)
            
        }, failure: failure)
    }
    
    // danh sach hoi cho
    static func getFair(offset: Int = 0, completion: @escaping([ExpoModel]) -> Void, failure: @escaping HCFailure) {
        sendRequest(.get, path: "/api/v1/expo/fair", parameters: ["id_app": Constant.idApp, "limit": Constant.limit, "offset": offset], completion: { (response) in
            completion(ExpoModel.fromJSON(response))
        }, failure: failure)
    }
    
    // chi tiet hoi cho
    static func detailExpo(id: Int, completion: @escaping (ExpoModel) -> Void, failure: @escaping HCFailure) {
        sendRequest(.get, path: "/api/v1/expo/fair/detail/\(id)", parameters: ["id_app": Constant.idApp], completion: { (response) in
            completion(ExpoModel(response))
        }, failure: failure)
    }
    
    // vi tri gian hang
    static func mapList(id: Int, offset: Int, name: String, completion: @escaping ([ExpoLocationModel]) -> Void, failure: @escaping HCFailure) {
        sendRequest(.get, path: "/api/v1/expo/map/list/\(id)", parameters: ["name": name, "offset": offset, "limit": Constant.limit, "id_app": Constant.idApp], completion: { (response) in
            completion(ExpoLocationModel.fromJSON(response))
        }, failure: failure)
    }
    
    // tạo hội chợ
    static func addOrUpdateExpo(id: Int! = nil, image: UIImage?, name: String, start_time: Date, end_time: Date, address: String, description: String, completion: @escaping () -> Void, failure: @escaping HCFailure) {
        
        var path = "/api/v1/expo/fair/add"
        if id != nil {
            path = "/api/v1/expo/fair/update/\(id!)"
        }
        
        Alamofire.upload(multipartFormData: { (formdata: MultipartFormData) in
            formdata.append(name.data(using: String.Encoding.utf8)!, withName: "name")
            formdata.append(start_time.toString(format: "HH:mm dd-MM-yyyy").data(using: String.Encoding.utf8)!, withName: "start_time")
            formdata.append(end_time.toString(format: "HH:mm dd-MM-yyyy").data(using: String.Encoding.utf8)!, withName: "end_time")
            formdata.append(address.data(using: String.Encoding.utf8)!, withName: "address")
            formdata.append(description.data(using: String.Encoding.utf8)!, withName: "description")
            if image != nil {
                formdata.append(image!.dataForUpload(), withName: "image", fileName: "image", mimeType: "image/jpeg")
            }
 
        }, usingThreshold: 999999, to: Constant.Api.baseUrl + path, method: .post, headers: createHeaderRequest()) { (result: SessionManager.MultipartFormDataEncodingResult) in
            switch result {
                
            case .success(let upload, _, _):
                
                upload.responseJSON { (response: DataResponse<Any>) in
                    
                    processResponse(response, completion: { (object: JSON) in
                        completion()
                    }, failure: failure)
                    
                }
                break
                
            case .failure(let encodingError):
                failure(encodingError)
                break
                
            }
        }
        
    }
    
    // xoa hoi cho
    static func deleteExpo(id: Int, completion: @escaping () -> Void, failure: @escaping HCFailure) {
        sendRequest(.delete, path: "/api/v1/expo/fair/delete/\(id)", parameters: nil, completion: { (response) in
            completion()
        }, failure: failure)
    }
    
    static func setupMapExpo(id: Int, count: Int, image: UIImage?, completion: @escaping () -> Void, failure: @escaping HCFailure) {
        Alamofire.upload(multipartFormData: { (formdata: MultipartFormData) in
            formdata.append("\(count)".data(using: String.Encoding.utf8)!, withName: "count")
            if image != nil {
                formdata.append(image!.dataForUpload(), withName: "image", fileName: "image", mimeType: "image/jpeg")
            }
            
        }, usingThreshold: 999999, to: Constant.Api.baseUrl + "/api/v1/expo/fair/setup-map/\(id)", method: .post, headers: createHeaderRequest()) { (result: SessionManager.MultipartFormDataEncodingResult) in
            switch result {
                
            case .success(let upload, _, _):
                
                upload.responseJSON { (response: DataResponse<Any>) in
                    
                    processResponse(response, completion: { (object: JSON) in
                        completion()
                    }, failure: failure)
                    
                }
                break
                
            case .failure(let encodingError):
                failure(encodingError)
                break
                
            }
        }
    }
    
    // them gian hang vao ban do
    static func addBoothToMap(id: Int, id_booth: Int, completion: @escaping () -> Void, failure: @escaping HCFailure) {
        sendRequest(.post, path: "/api/v1/expo/map/choose-booth/\(id)", parameters: ["id_booth": id_booth], completion: { (response) in
            completion()
        }, failure: failure)
    }
    
    static func removeBoothFromMap(id: Int, id_booth: Int,  completion: @escaping () -> Void, failure: @escaping HCFailure) {
        sendRequest(.post, path: "/api/v1/expo/map/delete-booth/\(id)", parameters: ["id_booth": id_booth], completion: { (response) in
            completion()
        }, failure: failure)
    }
    
    ///////////////////////////////////
    static func getAllPermission(completion: @escaping([PermissionModel]) -> Void, failure: @escaping(Error) -> Void) {
        sendRequest(.get, path: "/api/v1/expo/account-permissions", parameters: nil, completion: { (response) in
            
            var temp = [PermissionModel]()
            for json in response.arrayValue {
                temp.append(PermissionModel(json))
            }
            completion(temp)
            
        }, failure: failure)
    }
    
    static func getMemberPermission(id: Int, completion: @escaping([String]) -> Void, failure: @escaping(Error) -> Void) {
        sendRequest(.get, path: "/api/v1/expo/get-permissions", parameters: ["account_id": id], completion: { (response) in
            
            var temp = [String]()
            for json in response.arrayValue {
                temp.append(json.stringValue)
            }
            completion(temp)
            
        }, failure: failure)
    }
    
    static func addQuanTriVien(id: Int!, phone: String, permissions: [String], completion: @escaping() -> Void, failure: @escaping(Error) -> Void) {
        
        var path = "/api/v1/expo/add-administrators"
        if id != nil {
            path = "/api/v1/expo/edit-administrators/\(id!)"
        }
        
        Alamofire.upload(multipartFormData: { (formdata: MultipartFormData) in
            if id == nil {
                formdata.append(phone.data(using: String.Encoding.utf8)!, withName: "phone")
            }
            
            
            for permission in permissions {
                formdata.append(permission.data(using: String.Encoding.utf8)!, withName: "permissions[]")
            }
            
            
        }, usingThreshold: 999999, to: Constant.Api.baseUrl + path, method: .post, headers: createHeaderRequest()) { (result: SessionManager.MultipartFormDataEncodingResult) in
            switch result {
                
            case .success(let upload, _, _):
                
                upload.responseJSON { (response: DataResponse<Any>) in
                    
                    processResponse(response, completion: { (object: JSON) in
                        completion()
                    }, failure: failure)
                    
                }
                break
                
            case .failure(let encodingError):
                failure(encodingError)
                break
                
            }
        }
    }
    
    static func deleteAdmin(id: Int, completion: @escaping() -> Void, failure: @escaping(Error) -> Void) {
        sendRequest(.delete, path: "/api/v1/expo/delete-administrators/\(id)", parameters: nil, completion: { (response) in
            completion()
        }, failure: failure)
    }
    
    static func quyenQuanTriVien(completion: @escaping ([SettingModel]) -> Void, failure: @escaping HCFailure) {
        
        var temp = [SettingModel]()
        if let array = NSArray(contentsOfFile:Bundle.main.path(forResource: "ChuHeThong", ofType: "plist")!) {
            for item in array {
                let settingModel = SettingModel(JSON(item))
                if settingModel.enable {
                    temp.append(settingModel)
                }
            }
        }
        
        
        HCService.getMemberPermission(id: UserModel.shared.id, completion: { (permissions) in
            
            var availabelObject = [SettingModel]()
            for object in temp {
                let common = object.permissions.filter {permissions.contains($0)}
                if common.count > 0 {
                    object.permissions = common
                    availabelObject.append(object)
                }
            }
            completion(availabelObject)
            
        }, failure: failure)

    }
}
