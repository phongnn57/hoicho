//
//  DanhSachDiemBanHeaderView.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 4/28/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

class DanhSachDiemBanHeaderView: UITableViewHeaderFooterView {

    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var productPriceLabel: UILabel!
    


}
