//
//  ShareProductView.swift
//  IShopGo
//
//  Created by Nguyen Van Quyen on 10/24/17.
//  Copyright © 2017 Mua Đồ Tốt. All rights reserved.
//

import UIKit

enum ShareType: Int, EnumEnumerable {
    
    case facebook
    case zalo
    case googlePlus
    case messenger
}

class ShareProductView: UIView {

    @IBOutlet var mainView: UIView!
    @IBOutlet weak var shareProductTitle: UILabel!

    @IBOutlet weak var shareTitleHeightContraint: NSLayoutConstraint!
    
    var doShareFacebookProduct: (() -> Void)?
    var doShareOthersProduct: (() -> Void)?

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        Bundle.main.loadNibNamed("ShareProductView", owner: self, options: nil)
        self.addSubview(mainView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        mainView.frame = self.bounds
    }
    
    @IBAction func doShareFacebook(_ sender: Any) {
        
        if doShareFacebookProduct != nil {
            self.doShareFacebookProduct!()
        }
    }
    
    @IBAction func doShareOthers(_ sender: Any) {
        
        if doShareOthersProduct != nil {
            self.doShareOthersProduct!()
        }
    }
}
