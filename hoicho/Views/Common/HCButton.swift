//
//  HCButton.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 4/18/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

class HCButton: UIButton {

    @IBInspectable internal var is_cancel_button: Bool = false
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupUI()
    }
    
    func setupUI() {
        self.titleLabel?.font = UIFont.systemFont(ofSize: 16)
        if !is_cancel_button {
            self.backgroundColor = UIColor.HCColor.appColor
        } else {
            self.backgroundColor = UIColor.HCColor.cancelButtonColor
        }
        self.setTitleColor(.white, for: .normal)
        self.layer.cornerRadius = 3.0
    }

}
