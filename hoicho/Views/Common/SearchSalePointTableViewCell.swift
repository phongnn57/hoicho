//
//  SearchSalePointTableViewCell.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 5/28/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

class SearchSalePointTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
