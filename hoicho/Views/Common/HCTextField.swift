//
//  HCTextField.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 4/18/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

class HCTextField: UITextField {
    @IBInspectable internal var is_currency_format = false
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupUI()
    }
    
    func setupUI() {
        self.borderStyle = .none
        self.layer.cornerRadius = 3.0
        self.layer.borderWidth = 0.5
        self.layer.borderColor = UIColor(hex: "#b2b2b2").cgColor
        self.textColor = UIColor(hex: "#333333")
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.origin.x + 10, y: bounds.origin.y, width: bounds.width - 10, height: bounds.height)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.origin.x + 10, y: bounds.origin.y, width: bounds.width - 10, height: bounds.height)
    }
    
    func isNilOrEmpty() -> Bool {
        return Utilities.isNilOrEmpty(self.text)
    }
}
