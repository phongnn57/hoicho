//
//  RemoveableMediaCollectionViewCell.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 4/20/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

class RemoveableMediaCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var mediaImageView: UIImageView!
    @IBOutlet weak var closeButtonHeight: NSLayoutConstraint!
    @IBOutlet weak var closeButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
