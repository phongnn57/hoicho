//
//  ThemDanhGiaView.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 4/20/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

class ThemDanhGiaView: CommonPopupView {

    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var commentTextView: UITextView!
    
    var completion: ((String, Double) -> Void)?

    @IBAction func doReview(_ sender: Any) {
        if Utilities.isNilOrEmpty(commentTextView.text) {
            return
        }
        completion?(commentTextView.text, ratingView.rating)
        dismissPopup()
    }
    
    @IBAction func doCancel(_ sender: Any) {
        dismissPopup()
    }
}
