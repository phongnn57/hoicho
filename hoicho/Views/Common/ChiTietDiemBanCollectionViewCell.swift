//
//  ChiTietDiemBanCollectionViewCell.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 5/30/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

class ChiTietDiemBanCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    
    
}
