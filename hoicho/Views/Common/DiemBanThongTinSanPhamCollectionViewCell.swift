//
//  DiemBanThongTinSanPhamCollectionViewCell.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 6/1/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

class DiemBanThongTinSanPhamCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var codeLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    
    
}
