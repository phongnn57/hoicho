//
//  ISGUpdateAppView.swift
//  IShopGo
//
//  Created by Phong Nguyen Nam on 11/2/17.
//  Copyright © 2017 Mua Đồ Tốt. All rights reserved.
//

import UIKit

class ISGUpdateAppView: CommonPopupView {

    @IBOutlet weak var currentVersion: UILabel!
    @IBOutlet weak var storeVersion: UILabel!
    
    @IBAction func doUpdateButton(_ sender: Any) {
        UIApplication.shared.openURL(URL(string: "itms-apps://itunes.apple.com/app/id1384337919")!)
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        
    }

}
