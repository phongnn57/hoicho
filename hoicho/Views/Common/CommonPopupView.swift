//
//  CommonPopupView.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 4/20/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

class CommonPopupView: UIView {
    
    @IBOutlet weak var contentView: UIView!
    
    func openPopup() {
        
        contentView.transform = CGAffineTransform.identity.scaledBy(x: 0.2, y: 0.2)
        
        UIView.animate(withDuration: 0.2/1.5, animations: { () -> Void in
            self.contentView.transform = CGAffineTransform.identity.scaledBy(x: 1.1, y: 1.1)
        }, completion: {
            finished in
            UIView.animate(withDuration: 0.2/2, animations: { () -> Void in
                self.contentView.transform = CGAffineTransform.identity.scaledBy(x: 0.9, y: 0.9)
            }, completion: {
                finished in
                UIView.animate(withDuration: 0.2/2, animations: { () -> Void in
                    self.contentView.transform = CGAffineTransform.identity
                })
            })
        })
    }
    
    
    
    func dismissPopup() {
        UIView.animate(withDuration: 0.3, animations: {
            self.contentView.transform = CGAffineTransform.identity.scaledBy(x: 0.01, y: 0.01)
        }) { (finished: Bool) in
            self.removeFromSuperview()
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            let point = touch.location(in: self.contentView)
            if(point.x < 0 || point.y < 0 || point.x > self.contentView.frame.width || point.y > self.contentView.frame.height){
                self.dismissPopup()
            }
        }
    }
}
