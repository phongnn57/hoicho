//
//  SanPhamNoiBatCollectionViewCell.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 4/19/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit
private let reuseCellIdentifier = "ProductCollectionViewCell"
private let reuseCellNewsIdentifier = "TinTucHoiChoCollectionViewCell"

class SanPhamNoiBatCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var viewMoreButton: UIButton!
    
    var datasource = [ProductModel]() {
        didSet {
            collectionView?.reloadData()
        }
    }
    var news = [TongHopModel]() {
        didSet {
            collectionView?.reloadData()
        }
    }
    var showNews = false
    
    var didSelectAtIndex: ((Int) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib(nibName: reuseCellIdentifier, bundle: nil), forCellWithReuseIdentifier: reuseCellIdentifier)
        collectionView.register(UINib(nibName: reuseCellNewsIdentifier, bundle: nil), forCellWithReuseIdentifier: reuseCellNewsIdentifier)
        
    }
    
}

extension SanPhamNoiBatCollectionViewCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if showNews {
            return news.count
        }
        return datasource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if showNews {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseCellNewsIdentifier, for: indexPath) as! TinTucHoiChoCollectionViewCell
            
            let item = news[indexPath.row]
            
            cell.avatarImageView.sd_setImage(with: URL(string: item.image))
            cell.nameLabel.text = item.name
            cell.timeLabel.text = item.created_at?.toString()
            if let _attributedString = item.short_content.html2AttributedString {
                cell.contentLabel.attributedText = _attributedString
            } else {
                cell.contentLabel.text = item.short_content
            }
            cell.contentLabel.textColor = UIColor(hex: "#666666")
            cell.contentLabel.font = UIFont.systemFont(ofSize: 12, weight: .medium)
            
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseCellIdentifier, for: indexPath) as! ProductCollectionViewCell
            
            
            cell.leftConstraint.constant = 8
            cell.rightConstraint.constant = 4
            
            cell.productNameLabel.font = UIFont.systemFont(ofSize: 12, weight: .semibold)
            cell.productPriceLabel.font = UIFont.systemFont(ofSize: 12, weight: .semibold)
            
            let item = datasource[indexPath.row]
            
            cell.productNameLabel.text = item.name
            cell.productPriceLabel.text = item.price.priceString()
            cell.productImageView.sd_setImage(with: URL(string: item.image), placeholderImage: #imageLiteral(resourceName: "icon_no_image"))
            
            return cell
        }
        
        
    }
}

extension SanPhamNoiBatCollectionViewCell: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        didSelectAtIndex?(indexPath.row)
    }
}

extension SanPhamNoiBatCollectionViewCell: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if showNews {
            let itemWidth = UIScreen.main.bounds.width/2
            let imageWidth = itemWidth - 20
            let itemHeight = imageWidth * 2/3 + 125
            return CGSize.init(width: itemWidth, height: itemHeight)
        } else {
            let itemWidth = UIScreen.main.bounds.width/3
            let imageWidth = itemWidth - 20
            let itemHeight = 6 + 4 + imageWidth + 4 + 33.5 + 4 + 17 + 4 + 6
            return CGSize(width: itemWidth, height: itemHeight)
        }

    }
}
