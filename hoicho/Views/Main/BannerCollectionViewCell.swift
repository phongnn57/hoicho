//
//  BannerCollectionViewCell.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 4/18/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit
import iCarousel

class BannerCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var bannerView: iCarousel!
    @IBOutlet weak var pageControl: UIPageControl!
    
    var completion: ((Int) -> Void)?
    var banners = [BannerModel]() {
        didSet {
            bannerView?.reloadData()
            if banners.count > 1 {
                bannerView?.isScrollEnabled = true
                if bannerTimer != nil {
                    bannerTimer.invalidate()
                    bannerTimer = nil
                }
                bannerTimer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(doTimer), userInfo: nil, repeats: true)
            } else {
                bannerView?.isScrollEnabled = false
            }
            pageControl.numberOfPages = banners.count
        }
    }
    
    var bannerTimer: Timer!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        bannerView.delegate = self
        bannerView.dataSource = self
        bannerView.type = .linear
        bannerView.isPagingEnabled = true
    }
    
    @objc func doTimer() {
        var nextIndex = bannerView.currentItemIndex + 1
        if nextIndex >= banners.count {
            nextIndex = 0
        }
        bannerView.scrollToItem(at: nextIndex, animated: true)
    }
}

extension BannerCollectionViewCell: iCarouselDataSource {
    func numberOfItems(in carousel: iCarousel) -> Int {
        return banners.count
    }
    
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
        if let imageView = view as? UIImageView {
            imageView.sd_setImage(with: URL(string: banners[index].image))
            return imageView
        } else {
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.width / 3))
            imageView.contentMode = .scaleAspectFill
            imageView.clipsToBounds = true
            imageView.sd_setImage(with: URL(string: banners[index].image))
            
            return imageView
        }
    }
}

extension BannerCollectionViewCell: iCarouselDelegate {
    func carousel(_ carousel: iCarousel, didSelectItemAt index: Int) {
        completion?(index)
    }
    
    func carousel(_ carousel: iCarousel, valueFor option: iCarouselOption, withDefault value: CGFloat) -> CGFloat {
        if option == .wrap {
            return 1
        }
        return value
    }
    
    func carouselCurrentItemIndexDidChange(_ carousel: iCarousel) {
        pageControl.currentPage = carousel.currentItemIndex
    }
}
