//
//  SoDoChiTietTableViewCell.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 6/18/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

class SoDoChiTietTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var phoneButton: UIButton!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var positionLabel: UILabel!
    @IBOutlet weak var boothStackView: UIStackView!
    @IBOutlet weak var selectButton: UIButton!
    @IBOutlet weak var trashButton: UIButton!
    
}
