//
//  ProductCollectionViewCell.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 4/19/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

class ProductCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var leftConstraint: NSLayoutConstraint!
    @IBOutlet weak var rightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var productPriceLabel: UILabel!
    
    var product: ProductModel! {
        didSet {
            setupUI()
        }
    }
    
    func setupUI() {
        if product == nil {return}
        productImageView.sd_setImage(with: URL(string: product.image))
        productNameLabel.text = product.name
        productPriceLabel.text = product.price.priceString()
    }

}
