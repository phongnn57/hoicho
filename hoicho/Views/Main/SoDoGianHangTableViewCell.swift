//
//  SoDoGianHangTableViewCell.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 6/18/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

class SoDoGianHangTableViewCell: UITableViewCell {

    @IBOutlet weak var bgImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var moreButton: UIButton!
    
    var expo: ExpoModel! {
        didSet {
            setupUI()
        }
    }
    
    func setupUI() {
        if expo == nil {return}
        
        bgImageView.sd_setImage(with: URL(string: expo.image))
        nameLabel.text = expo.name.uppercased()
        timeLabel.text = "Thời gian: Từ \(expo.start_time?.toString(format: "HH:mm dd/MM/yyyy") ?? "...") đến \(expo.end_time?.toString(format: "HH:mm dd/MM/yyyy") ?? "...")"
        addressLabel.text = "Địa chỉ: \(expo.address)"
    }

}
