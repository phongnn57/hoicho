//
//  DanhSachDiemBanTableViewCell.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 4/27/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

private let reuseCellIdentifier = "DiemBanTableViewCell"

class DanhSachDiemBanTableViewCell: UITableViewCell {

    @IBOutlet weak var themDiemBanButton: UIButton!
    @IBOutlet weak var xemThemDiemBanButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var xemThemButtonHeight: NSLayoutConstraint!
    
    var didSelectItemAt: ((Int) -> Void)?
    
    var datasource = [DiemBanModel]() {
        didSet {
            if datasource.count == 0 {
                tableViewHeight.constant = 0
                xemThemButtonHeight.constant = 0
                xemThemDiemBanButton.isHidden = true
            } else {
                if datasource.count > 3 {
                    tableViewHeight.constant = 300
                } else {
                    tableViewHeight.constant = CGFloat(100 * datasource.count)
                }
                
                xemThemButtonHeight.constant = 40
                xemThemDiemBanButton.isHidden = false
            }
            tableView?.reloadData()
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        tableView.register(UINib(nibName: reuseCellIdentifier, bundle: nil), forCellReuseIdentifier: reuseCellIdentifier)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension DanhSachDiemBanTableViewCell: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if datasource.count > 3 {
            return 3
        } else {
            return datasource.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseCellIdentifier, for: indexPath) as! DiemBanTableViewCell
        
        let item = datasource[indexPath.row]
        cell.nameLabel.text = item.name
        cell.addressLabel.text = "\(item.address), \(item.district), \(item.city)"
        cell.phoneButton.setTitle(item.phone, for: .normal)
        cell.priceLabel.text = item.price.priceString()
        
        return cell
    }
}

extension DanhSachDiemBanTableViewCell: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        didSelectItemAt?(indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
}
