//
//  ThongTinGianHangTableViewCell.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 5/5/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

class ThongTinGianHangTableViewCell: UITableViewCell {
    
    @IBOutlet weak var tenGianHangLabel: UILabel!
    @IBOutlet weak var khuVucLabel: UILabel!
    @IBOutlet weak var soSanPhamLabel: UILabel!
    @IBOutlet weak var danhGiaLabel: UILabel!
    @IBOutlet weak var phoneButton: UIButton!
    
    var didSelectButton: ((Int) -> Void)?

    var booth: GianHangModel? {
        didSet {
            setupUI()
        }
    }
    
    func setupUI() {
        if let gianhang = booth {
            phoneButton.setTitle(gianhang.hotline.isEmpty == true ? "Chưa xác định":gianhang.hotline, for: .normal)
            tenGianHangLabel.text =  (gianhang.name.isEmpty == true ? "Chưa có tên" : gianhang.name)
            khuVucLabel.text =  (gianhang.address.isEmpty == true ? "Chưa rõ": gianhang.address)
            soSanPhamLabel.text = "\(gianhang.product_count)"
            danhGiaLabel.text = "\(gianhang.rate_count)"
        }
    }

    @IBAction func doCallButton(_ sender: Any) {
        didSelectButton?(2)
    }
    
    @IBAction func doViewGianHang(_ sender: Any) {
        didSelectButton?(1)
    }

}
