//
//  SanPhamLienQuanTableViewCell.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 4/19/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

private let reuseCellIdentifier = "ProductCollectionViewCell"

class SanPhamLienQuanTableViewCell: UITableViewCell {

    @IBOutlet weak var viewMoreButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var didSelectAtIndex: ((Int) -> Void)?
    
    var datasource = [ProductModel]() {
        didSet {
            collectionView?.reloadData()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib(nibName: reuseCellIdentifier, bundle: nil), forCellWithReuseIdentifier: reuseCellIdentifier)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension SanPhamLienQuanTableViewCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return datasource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseCellIdentifier, for: indexPath) as! ProductCollectionViewCell
        
        
        cell.leftConstraint.constant = 8
        cell.rightConstraint.constant = 4
        
        cell.productNameLabel.font = UIFont.systemFont(ofSize: 12, weight: .semibold)
        cell.productPriceLabel.font = UIFont.systemFont(ofSize: 12, weight: .semibold)
        
        cell.product = datasource[indexPath.row]
        
        return cell
    }
}

extension SanPhamLienQuanTableViewCell: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        didSelectAtIndex?(indexPath.row)
    }
}

extension SanPhamLienQuanTableViewCell: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemWidth = UIScreen.main.bounds.width/3
        let imageWidth = itemWidth - 20
        let itemHeight = 6 + 4 + imageWidth + 4 + 33.5 + 4 + 17 + 4 + 6
        return CGSize(width: itemWidth, height: itemHeight)
    }
}

