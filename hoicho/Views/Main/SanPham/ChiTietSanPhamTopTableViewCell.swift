//
//  ChiTietSanPhamTopTableViewCell.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 4/19/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit
import iCarousel

class ChiTietSanPhamTopTableViewCell: UITableViewCell {

    @IBOutlet weak var mediaView: iCarousel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var viewDescriptionButton: UIButton!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var priceSiLabel: UILabel!
    @IBOutlet weak var priceSiImageView: UIImageView!
    @IBOutlet weak var priceSiConditionLabel: UILabel!
    @IBOutlet weak var brandButton: UIButton!
    @IBOutlet weak var priceSiStackView: UIStackView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    @IBOutlet weak var shareButton: UIButton!
    
    var didSelectImageAt: ((Int) -> Void)?
    var didSelectButton: ((Int) -> Void)?
    
    var product: ProductModel! {
        didSet {
            setupUI()
        }
    }
    
    var mediaTimer: Timer!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        mediaView.delegate = self
        mediaView.dataSource = self
        mediaView.type = .linear
        mediaView.isPagingEnabled = true
        pageControl.hidesForSinglePage = true

    }
    
    @objc func doTimer() {
        var nextItem = mediaView.currentItemIndex + 1
        if nextItem >= product.images.count {
            nextItem = 0
        }
        mediaView.scrollToItem(at: nextItem, animated: true)
    }

    func setupUI() {
        if product == nil {return}
        mediaView.reloadData()
        if product.images.count > 1 {
            mediaView.isScrollEnabled = true
            if mediaTimer != nil {
                mediaTimer.invalidate()
                mediaTimer = nil
            }
            mediaTimer = Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(doTimer), userInfo: nil, repeats: true)
        } else {
            mediaView.isScrollEnabled = false
        }
        
        pageControl.numberOfPages = product.images.count
        nameLabel.text = product.name
        
        webView.loadHTMLString(product.description_text.addCssStyle(), baseURL: nil)
        webView.scrollView.isScrollEnabled = false
        
        if let brand = product.brand {
            brandButton.setTitle(brand.name, for: .normal)
        } else {
            brandButton.setTitle("Chưa rõ thương hiệu", for: .normal)
        }
        
        let attributedBanLe = NSMutableAttributedString()
        attributedBanLe.append(NSAttributedString(string: "Giá bán lẻ: ", attributes: [NSAttributedStringKey.foregroundColor: UIColor(hex: "#333333"), NSAttributedStringKey.font: UIFont.systemFont(ofSize: 16, weight: .medium)]))
        attributedBanLe.append(NSAttributedString(string: product.price.priceString(), attributes: [NSAttributedStringKey.foregroundColor: UIColor(hex: "#27AE60"), NSAttributedStringKey.font: UIFont.systemFont(ofSize: 16, weight: .bold)]))
        priceLabel.attributedText = attributedBanLe
        
        if product.view_wholesale {
            priceSiStackView.isHidden = false
            
            let attributedPriceSi = NSMutableAttributedString()
            attributedPriceSi.append(NSAttributedString(string: "Giá bán sỉ: Từ ", attributes: [NSAttributedStringKey.foregroundColor: UIColor(hex: "#333333"), NSAttributedStringKey.font: UIFont.systemFont(ofSize: 16, weight: .medium)]))
            attributedPriceSi.append(NSAttributedString(string: product.wholesale_price_from.priceString(), attributes: [NSAttributedStringKey.foregroundColor: UIColor(hex: "#FF2851"), NSAttributedStringKey.font: UIFont.systemFont(ofSize: 16, weight: .bold)]))
            attributedPriceSi.append(NSAttributedString(string: " đến ", attributes: [NSAttributedStringKey.foregroundColor: UIColor(hex: "#333333"), NSAttributedStringKey.font: UIFont.systemFont(ofSize: 16, weight: .medium)]))
            attributedPriceSi.append(NSAttributedString(string: product.wholesale_price_to.priceString(), attributes: [NSAttributedStringKey.foregroundColor: UIColor(hex: "#FF2851"), NSAttributedStringKey.font: UIFont.systemFont(ofSize: 16, weight: .bold)]))
            
            priceSiLabel.attributedText = attributedPriceSi
            
            if product.wholesale_count_product > 0 {
                let attributedConditionSi = NSMutableAttributedString()
                attributedConditionSi.append(NSAttributedString(string: "Mua tối thiểu ", attributes: [NSAttributedStringKey.foregroundColor: UIColor(hex: "#666666"), NSAttributedStringKey.font: UIFont.systemFont(ofSize: 16, weight: .regular)]))
                attributedConditionSi.append(NSAttributedString(string: "\(product.wholesale_count_product)", attributes: [NSAttributedStringKey.foregroundColor: UIColor(hex: "#FF2851"), NSAttributedStringKey.font: UIFont.systemFont(ofSize: 16, weight: .bold)]))
                attributedConditionSi.append(NSAttributedString(string: " sản phẩm", attributes: [NSAttributedStringKey.foregroundColor: UIColor(hex: "#666666"), NSAttributedStringKey.font: UIFont.systemFont(ofSize: 16, weight: .regular)]))
                priceSiConditionLabel.attributedText = attributedConditionSi
            } else {
                priceSiConditionLabel.text = "Không yêu cầu mua tối thiểu bao nhiêu sản phẩm"
            }
            
        } else {
            priceSiStackView.isHidden = true
        }
        
    }
    
    @IBAction func doViewDanhSachSanPham(_ sender: Any) {
        didSelectButton?(4)
    }
    

    
    @IBAction func doViewDescriptionButton(_ sender: Any) {
        didSelectButton?(0)
    }
}

extension ChiTietSanPhamTopTableViewCell: iCarouselDataSource {
    func numberOfItems(in carousel: iCarousel) -> Int {
        if product == nil {return 0}
        return product.images.count
    }
    
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
        if let imageView = view as? UIImageView {
            imageView.sd_setImage(with: URL(string: product.images[index]), placeholderImage: #imageLiteral(resourceName: "icon_no_image"))
            return imageView
        } else {
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.width))
            imageView.contentMode = .scaleAspectFill
            imageView.clipsToBounds = true
            imageView.sd_setImage(with: URL(string: product.images[index]), placeholderImage: #imageLiteral(resourceName: "icon_no_image"))
            
            return imageView
        }
    }
}

extension ChiTietSanPhamTopTableViewCell: iCarouselDelegate {
    func carousel(_ carousel: iCarousel, didSelectItemAt index: Int) {
        didSelectImageAt?(index)
    }
    
    func carousel(_ carousel: iCarousel, valueFor option: iCarouselOption, withDefault value: CGFloat) -> CGFloat {
        if option == .wrap {
            return 1
        }
        return value
    }
    
    func carouselCurrentItemIndexDidChange(_ carousel: iCarousel) {
        pageControl.currentPage = carousel.currentItemIndex
    }
}

