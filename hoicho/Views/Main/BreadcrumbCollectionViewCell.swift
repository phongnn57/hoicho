//
//  BreadcrumbCollectionViewCell.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 4/20/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit


private let arrowCharacter = "‣"

private let reuseCellTopIdentifier = "BreadcrumbTableViewCell"
private let reuseCellBottomIdentifier = "BreadcrumbColorTableViewCell"

class BreadcrumbCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var bottomView: UIView!
    
    var topTableView: UITableView!
    var bottomTableView: UITableView!
    
    var category: CategoryModel! {
        didSet {
            topTableView?.reloadData()
            bottomTableView?.reloadData()
        }
    }
    
    var didSelectCategoryAtIndex: ((Int, Bool) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let topFrame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 44)
        topTableView = UITableView(frame: topFrame)
        topTableView.transform = CGAffineTransform.init(rotationAngle: -CGFloat.pi/2)
        topTableView.frame = topFrame
        topTableView.separatorStyle = .none
        topTableView.showsVerticalScrollIndicator = false
        topTableView.showsHorizontalScrollIndicator = false
        topTableView.tableFooterView = UIView()
        topTableView.register(UINib(nibName: reuseCellTopIdentifier, bundle: nil), forCellReuseIdentifier: reuseCellTopIdentifier)
        topTableView.delegate = self
        topTableView.dataSource = self
        topView.addSubview(topTableView)
        
        let bottomFrame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 55)
        bottomTableView = UITableView(frame: bottomFrame)
        bottomTableView.transform = CGAffineTransform.init(rotationAngle: -CGFloat.pi/2)
        bottomTableView.frame = bottomFrame
        bottomTableView.separatorStyle = .none
        bottomTableView.showsVerticalScrollIndicator = false
        bottomTableView.showsHorizontalScrollIndicator = false
        bottomTableView.tableFooterView = UIView()
        bottomTableView.register(UINib(nibName: reuseCellBottomIdentifier, bundle: nil), forCellReuseIdentifier: reuseCellBottomIdentifier)
        bottomTableView.delegate = self
        bottomTableView.dataSource = self
        bottomView.addSubview(bottomTableView)
        
    }
    
    
    
}

extension BreadcrumbCollectionViewCell: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if category == nil {
            return 0
        }
        if tableView.isEqual(topTableView) {
            return category.breadCrumb.count
        }
        return category.subCate.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView.isEqual(topTableView) {
            let cell = tableView.dequeueReusableCell(withIdentifier: reuseCellTopIdentifier, for: indexPath) as! BreadcrumbTableViewCell
            cell.transform = CGAffineTransform.init(rotationAngle: CGFloat.pi/2)
            var font: UIFont!
            var textValue = ""
            
            if indexPath.row == category.breadCrumb.count - 1 {
                textValue = category.breadCrumb[indexPath.row].name
                font = UIFont.boldSystemFont(ofSize: 12)
            } else {
                textValue = category.breadCrumb[indexPath.row].name + "  " + arrowCharacter + "  "
                font = UIFont.systemFont(ofSize: 12, weight: .regular)
            }
            cell.titleLabel.font = font
            cell.titleLabel.text = textValue
            
            if indexPath.row > 0 {
                cell.leftConstraint.constant = 0
            } else {
                cell.leftConstraint.constant = 8
            }
            
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseCellBottomIdentifier, for: indexPath) as! BreadcrumbColorTableViewCell
        cell.transform = CGAffineTransform.init(rotationAngle: CGFloat.pi/2)
        cell.titleLabel.text = category.subCate[indexPath.row].name
        
        cell.colorView.backgroundColor = UIColor(hex: UIColor.HCColor.randomColors[indexPath.row % UIColor.HCColor.randomColors.count])
        
        return cell
        
        
    }
}

extension BreadcrumbCollectionViewCell: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView.isEqual(topTableView) {
            if indexPath.row < category.breadCrumb.count - 1 {
                didSelectCategoryAtIndex?(indexPath.row, true)
            }
        } else {
            didSelectCategoryAtIndex?(indexPath.row, false)
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView.isEqual(topTableView) {
            var font: UIFont!
            var textValue = ""
            
            if indexPath.row == category.breadCrumb.count - 1 {
                textValue = category.breadCrumb[indexPath.row].name
                font = UIFont.boldSystemFont(ofSize: 12)
            } else {
                textValue = category.breadCrumb[indexPath.row].name + "  " + arrowCharacter + "  "
                font = UIFont.systemFont(ofSize: 12, weight: .regular)
            }
            if indexPath.row == 0 {
                return textValue.width(withConstrainedHeight: 15, font: font) + 8
            }
            return textValue.width(withConstrainedHeight: 15, font: font)
        }
        
        return category.subCate[indexPath.row].name.width(withConstrainedHeight: 15, font: UIFont.systemFont(ofSize: 12)) + 32
        
    }
    
    
}
