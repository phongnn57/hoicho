//
//  DanhSachThuongHieuCollectionViewCell.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 4/19/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

class DanhSachThuongHieuCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var collectionView: UICollectionView!
    
    var brands = [BrandModel]() {
        didSet {
            collectionView?.reloadData()
        }
    }
    
    var didSelectItemAt: ((Int) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib(nibName: "ThuongHieuNoiBatCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ThuongHieuNoiBatCollectionViewCell")
    }

}

extension DanhSachThuongHieuCollectionViewCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return brands.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ThuongHieuNoiBatCollectionViewCell", for: indexPath) as! ThuongHieuNoiBatCollectionViewCell
        
        cell.logoImageView.sd_setImage(with: URL(string: brands[indexPath.row].logo))
        
        return cell
    }
}

extension DanhSachThuongHieuCollectionViewCell: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        didSelectItemAt?(indexPath.row)
    }
}

extension DanhSachThuongHieuCollectionViewCell: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if brands.count >= 6 {
            let itemHeight = (UIScreen.main.bounds.width / 2 - 33)/2
            return CGSize(width: itemHeight * 2, height: itemHeight)
        }
        let itemHeight = (UIScreen.main.bounds.width / 2 - 33)
        return CGSize(width: itemHeight * 2, height: itemHeight)
    }

}
