//
//  ThongTinGianHangCollectionViewCell.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 4/20/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

class ThongTinGianHangCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var productCountLabel: UILabel!
    @IBOutlet weak var joinedDateLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var rateLabel: UILabel!
    @IBOutlet weak var clickLabel: UILabel!
    @IBOutlet weak var shareLabel: UILabel!
    @IBOutlet weak var introductionLabel: UILabel!
    @IBOutlet weak var phoneButton: UIButton!
    
}
