//
//  GianHangTopCollectionViewCell.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 4/19/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

class GianHangTopCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var gianHangImageView: UIImageView!
    @IBOutlet weak var thongTinButton: UIButton!
    @IBOutlet weak var sanPhamButton: UIButton!
    @IBOutlet weak var danhMucButton: UIButton!
    @IBOutlet weak var danhGiaButton: UIButton!
    @IBOutlet weak var indicatorLeftConstraint: NSLayoutConstraint!
    
    var didSelectButtonAt:((Int) -> Void)?
    
    @IBAction func doThongTinButton(_ sender: Any) {
        didSelectButtonAt?(0)
    }
    
    @IBAction func doSanPhamButton(_ sender: Any) {
        didSelectButtonAt?(1)
    }
    
    @IBAction func doDanhMucButton(_ sender: Any) {
        didSelectButtonAt?(2)
    }
    
    @IBAction func doDanhGiaButton(_ sender: Any) {
        didSelectButtonAt?(3)
    }
    
    func setIndicatorAtIndex(_ index: Int, animated: Bool) {
        let buttons = [thongTinButton, sanPhamButton, danhMucButton, danhGiaButton]
        for (pos, button) in buttons.enumerated() {
            if pos == index {
                button?.setTitleColor(UIColor.HCColor.appColor, for: .normal)
            } else {
                button?.setTitleColor(UIColor(hex: "#333333"), for: .normal)
            }
        }
        let quaterWidth = UIScreen.main.bounds.width/4
        if animated {
            UIView.animate(withDuration: 0.3) {
                self.indicatorLeftConstraint.constant = CGFloat(index) * quaterWidth
            }
        }
    }
    
}
