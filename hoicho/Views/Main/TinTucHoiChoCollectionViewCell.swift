//
//  TinTucHoiChoCollectionViewCell.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 6/14/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

class TinTucHoiChoCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
