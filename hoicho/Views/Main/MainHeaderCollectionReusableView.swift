//
//  MainHeaderCollectionReusableView.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 4/19/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

class MainHeaderCollectionReusableView: UICollectionReusableView {

    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var goiYView: UIView!
    
    @IBOutlet weak var goiYImageView: UIImageView!
    @IBOutlet weak var goiYLabel: UILabel!

    
    var didSelectType: ((MainViewProductType) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        goiYView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(viewTapped(_:))))
        
    }
    
    @objc func viewTapped(_ sender: UITapGestureRecognizer) {
        if let view = sender.view {
            if view.isEqual(goiYView) {
                didSelectType?(.goiy)
            }
        }
    }
    
    func setCurrentType(type: MainViewProductType) {
        activeAtIndex(index: type.rawValue)
    }
    
    func activeAtIndex(index: Int) {
        let listImages = [goiYImageView]
        let listLabels = [goiYLabel]
        
        for (pos, item) in listImages.enumerated() {
            if pos == index {
                item?.tintColor = UIColor.HCColor.appColor
                listLabels[pos]?.textColor = UIColor.HCColor.appColor
            } else {
                item?.tintColor = UIColor(hex: "#333333")
                listLabels[pos]?.textColor = UIColor(hex: "#333333")
            }
        }
    }
    
}
