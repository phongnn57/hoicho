//
//  DanhMucCollectionViewCell.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 5/28/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

class DanhMucCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var categories = [CategoryModel]() {
        didSet {
            collectionView?.reloadData()
        }
    }
    
    var didSelectItemAt: ((Int) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib(nibName: "ItemDanhMucCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ItemDanhMucCollectionViewCell")
        collectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 8)
    }
    
}

extension DanhMucCollectionViewCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categories.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ItemDanhMucCollectionViewCell", for: indexPath) as! ItemDanhMucCollectionViewCell
        
        cell.titleLabel.text = categories[indexPath.row].name
        cell.iconImageView.sd_setImage(with: URL(string: categories[indexPath.row].image))
        
        return cell
    }
}

extension DanhMucCollectionViewCell: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        didSelectItemAt?(indexPath.row)
    }
}

extension DanhMucCollectionViewCell: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemHeight = UIScreen.main.bounds.width/4 * 1.5
        return CGSize(width: itemHeight * 3/4, height: itemHeight )
    }
    
}
