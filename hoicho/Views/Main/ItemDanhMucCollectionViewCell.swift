//
//  ItemDanhMucCollectionViewCell.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 5/28/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

class ItemDanhMucCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
