//
//  SoDoChiTietHeaderView.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 6/18/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

class SoDoChiTietHeaderView: UITableViewHeaderFooterView {

    @IBOutlet weak var qrcodeImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var bgImageView: UIImageView!
    

}
