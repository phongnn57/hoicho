//
//  MenuFooterView.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 6/14/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

class MenuFooterView: UITableViewHeaderFooterView {

    @IBOutlet weak var newsButton: UIButton!
    @IBOutlet weak var questionButton: UIButton!
    @IBOutlet weak var mapButton: UIButton!
    @IBOutlet weak var infoButton: UIButton!
    

}
