//
//  MenuHeaderView.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 4/20/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

class MenuHeaderView: UITableViewHeaderFooterView {

    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var arrowImageView: UIImageView!
    @IBOutlet weak var actionButton: UIButton!
    
}
