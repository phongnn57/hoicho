//
//  ThongBaoTableViewCell.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 6/7/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

class ThongBaoTableViewCell: UITableViewCell {

    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var senderLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    
    var notification: NotificationModel! {
        didSet {
            setupUI()
        }
    }
    
    func setupUI() {
        if notification == nil {
            return
        }
        timeLabel.text = notification.createdAt?.timeAgoSinceNow
        if notification.short_description.isEmpty {
            if notification.content.isEmpty {
                titleLabel.text = "Không có nội dung"
            } else {
                titleLabel.attributedText = notification.content.html2AttributedString
            }
        } else {
            titleLabel.text = notification.short_description
        }
        
        senderLabel.text = notification.sender
        
        if notification.isRead {
            titleLabel.font = UIFont.systemFont(ofSize: 15)
            titleLabel.textColor = UIColor(hex: "#333333")
        } else {
            titleLabel.font = UIFont.boldSystemFont(ofSize: 15)
            titleLabel.textColor = UIColor.HCColor.appColor
        }
        
        avatarImageView.sd_setImage(with: URL(string: notification.account_image), placeholderImage: #imageLiteral(resourceName: "icon_default_avatar"))
    }

}
