//
//  GroupProfileHeaderView.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 6/6/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

class GroupProfileHeaderView: UITableViewHeaderFooterView {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var viewAllButton: UIButton!
    

}
