//
//  GroupProfileBottomTableViewCell.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 6/6/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

class GroupProfileBottomTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var switchControl: UISwitch!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
