//
//  TaoNhomTopTableViewCell.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 6/6/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

class TaoNhomTopTableViewCell: UITableViewCell {

    @IBOutlet weak var collectionView: UICollectionView!
    
    var users = [ContactModel]() {
        didSet {
            collectionView?.reloadData()
        }
    }
    var didRemoveAtIndex: ((Int) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib(nibName: "TaoNhomCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "TaoNhomCollectionViewCell")
    }

    @objc func doRemoveButton(_ sender: UIButton) {
        didRemoveAtIndex?(sender.tag)
    }

}

extension TaoNhomTopTableViewCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return users.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TaoNhomCollectionViewCell", for: indexPath) as! TaoNhomCollectionViewCell
        
        let item = users[indexPath.row]
        
        let components = item.name.components(separatedBy: CharacterSet(charactersIn: " "))
        if components.count > 0 {
            cell.nameLabel.text = components.last
        } else {
            cell.nameLabel.text = item.name
        }
        
        cell.avatarImageView.sd_setImage(with: URL(string: item.image), placeholderImage: #imageLiteral(resourceName: "icon_default_avatar"))
        cell.removeButton.tag = indexPath.row
        cell.removeButton.addTarget(self, action: #selector(doRemoveButton(_:)), for: .touchUpInside)
        
        return cell
    }
}

extension TaoNhomTopTableViewCell: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 72, height: 100)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}
