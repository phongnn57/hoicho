//
//  ChatIncommingTableViewCell.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 5/28/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

class ChatIncommingTableViewCell: UITableViewCell {

    @IBOutlet weak var avatarImageView: RoundedImageView!
    @IBOutlet weak var contentTextView: UITextView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
