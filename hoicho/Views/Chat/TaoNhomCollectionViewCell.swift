//
//  TaoNhomCollectionViewCell.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 6/6/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

class TaoNhomCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var removeButton: UIButton!
    
}
