//
//  TinTucTableViewCell.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 6/14/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

class TinTucTableViewCell: UITableViewCell {

    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    
    var model: TongHopModel! {
        didSet {
            setupUI()
        }
    }
    
    func setupUI() {
        if model == nil {return}
        avatarImageView.sd_setImage(with: URL(string: model.image))
        nameLabel.text = model.name
        timeLabel.text = model.created_at?.toString()
        categoryLabel.text = model.category_name
        if let _attributedString = model.short_content.html2AttributedString {
            contentLabel.attributedText = _attributedString
            contentLabel.font = UIFont.systemFont(ofSize: 14, weight: .medium)
            contentLabel.textColor = UIColor(hex: "#666666")
        } else {
            contentLabel.text = model.short_content
        }
    }

}
