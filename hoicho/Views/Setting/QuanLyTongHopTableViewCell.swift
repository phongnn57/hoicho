//
//  QuanLyTongHopTableViewCell.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 5/14/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

class QuanLyTongHopTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var categoryLabel: UILabel!
    
    var post: TongHopModel! {
        didSet {
            setupUI()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setupUI() {
        if post == nil {
            return
        }
        
        titleLabel.text = post.name
        let attributedString = NSMutableAttributedString()
        attributedString.append(NSAttributedString(string: (post.created_at?.toString() ?? Date().toString()) + " | Đăng bởi ", attributes: [NSAttributedStringKey.foregroundColor: UIColor(hex: "#9E9E9E"), NSAttributedStringKey.font: UIFont.systemFont(ofSize: 14)]))
        attributedString.append(NSAttributedString(string: post.account_name.isEmpty ? "Không xác định": post.account_name, attributes: [NSAttributedStringKey.foregroundColor: UIColor(hex: "#9E9E9E"), NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 14)]))
        attributedString.append(NSAttributedString(string: " | " + post.status_text, attributes: [NSAttributedStringKey.foregroundColor: UIColor(hex: "#9E9E9E"), NSAttributedStringKey.font: UIFont.systemFont(ofSize: 14)]))
        subTitleLabel.attributedText = attributedString
        categoryLabel.text = post.category_name
        
    }

}
