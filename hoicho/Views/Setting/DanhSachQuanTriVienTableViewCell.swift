//
//  DanhSachQuanTriVienTableViewCell.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 6/12/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

class DanhSachQuanTriVienTableViewCell: UITableViewCell {

    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var phoneLabel: UIButton!
    @IBOutlet weak var moreButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
