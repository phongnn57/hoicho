//
//  MenuConfigHeaderView.swift
//  IShopGo
//
//  Created by Phong Nguyen Nam on 12/17/17.
//  Copyright © 2017 Mua Đồ Tốt. All rights reserved.
//

import UIKit

class MenuConfigHeaderView: UITableViewHeaderFooterView {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var actionButton: UIButton!


}
