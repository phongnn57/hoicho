//
//  FriendRequestTableViewCell.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 6/9/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

class FriendRequestTableViewCell: UITableViewCell {

    @IBOutlet weak var accountImageView: UIImageView!
    @IBOutlet weak var acceptButton: UIButton!
    @IBOutlet weak var rejectButton: UIButton!
    @IBOutlet weak var nameLabel: UILabel!
    

}
