//
//  QuanLyVeThamQuanTableViewCell.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 5/14/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

class QuanLyVeThamQuanTableViewCell: UITableViewCell {
    
    @IBOutlet weak var codeImageView: UIImageView!
    @IBOutlet weak var nameButton: UIButton!
    @IBOutlet weak var phoneButton: UIButton!
    @IBOutlet weak var regionLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var saveButton: UIButton!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
