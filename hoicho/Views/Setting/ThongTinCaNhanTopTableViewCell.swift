//
//  ThongTinCaNhanTopTableViewCell.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 5/28/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

class ThongTinCaNhanTopTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var vietBaiButton: UIButton!
    @IBOutlet weak var nhomButton: UIButton!
    @IBOutlet weak var baiDangButton: UIButton!
    @IBOutlet weak var thietLapButton: UIButton!
    @IBOutlet weak var phoneButton: UIButton!
    @IBOutlet weak var birthdayLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var companyLabel: UILabel!
    @IBOutlet weak var regionLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var joinedDateLabel: UILabel!
    @IBOutlet weak var introLabel: UILabel!
    
    @IBOutlet weak var gianHangDiemBanStackView: UIStackView!
    
    @IBOutlet weak var gianHangDiemBanLabel: UILabel!
    @IBOutlet weak var gianHangDiemBanButton: UIButton!
    
    @IBOutlet weak var danhBaLabel: UILabel!
    @IBOutlet weak var vietBaiView: UIView!
    @IBOutlet weak var nhomView: UIView!
    @IBOutlet weak var baiDangView: UIView!
    @IBOutlet weak var thietLapView: UIView!
    @IBOutlet weak var nhanTinView: UIView!
    
    var user: UserModel! {
        didSet {
            setupUI()
        }
    }
    var status: FriendStatus = .not_friend {
        didSet {
            setupUI()
        }
    }
    var didSelectButtonAtIndex: ((Int) -> Void)?
    var doCallUser: (() -> Void)?
    
    func setupUI() {
        if user == nil {return}
        
        if user.id != UserModel.shared.id {
            vietBaiView.isHidden = true
            if UserModel.shared.type != nil && UserModel.shared.type! == .chu_he_thong {
                thietLapView.isHidden = false
            } else {
                thietLapView.isHidden = true
            }
            if status == .accept {
                danhBaLabel.text = "Chờ kết bạn"
                danhBaLabel.textColor = UIColor.HCColor.appColor
            } else if status == .friend {
                danhBaLabel.text = "Bạn bè"
                danhBaLabel.textColor = UIColor(hex: "#333333")
            } else if status == .not_friend {
                danhBaLabel.text = "Kết bạn"
                danhBaLabel.textColor = UIColor(hex: "#333333")
            } else if status == .request_sent {
                danhBaLabel.text = "Đang kết bạn"
                danhBaLabel.textColor = UIColor(hex: "#333333")
            }
            
            baiDangView.isHidden = false
        } else {
            nhanTinView.isHidden = true
            baiDangView.isHidden = true
            thietLapView.isHidden = false
            vietBaiView.isHidden = false
            nhomView.isHidden = false
            danhBaLabel.text = "Danh bạ"
        }
        
        avatarImageView.sd_setImage(with: URL(string: user.image), placeholderImage: #imageLiteral(resourceName: "icon_default_avatar"))
        nameLabel.text = user.name
        phoneButton.setTitle(user.phone, for: .normal)
        birthdayLabel.text = user.birthday?.toString()
        emailLabel.text = user.email
        companyLabel.text = user.company
        regionLabel.text = user.region
        addressLabel.text = user.address
        typeLabel.text = user.type_text_expo
        joinedDateLabel.text = user.created_at?.toString()
        introLabel.text = user.introduction
        
        phoneButton.addTarget(self, action: #selector(doCall), for: .touchUpInside)
        
        if user.type_text_expo.lowercased() == UserType.gian_hang.rawValue.lowercased() {
            gianHangDiemBanStackView.isHidden = false
            gianHangDiemBanLabel.text = "Gian hàng"
            gianHangDiemBanButton.setTitle(user.booth_name, for: .normal)
        } else if user.type_text_expo.lowercased() == UserType.thanh_vien.rawValue.lowercased() && !user.sale_point_name.isEmpty {
            gianHangDiemBanStackView.isHidden = false
            gianHangDiemBanLabel.text = "Điểm bán"
            gianHangDiemBanButton.setTitle(user.sale_point_name, for: .normal)
        } else {
            gianHangDiemBanStackView.isHidden = true
        }
    }
    
    @objc func doCall() {
        doCallUser?()
    }
    
    @IBAction func doVietBaiButton(_ sender: Any) {
        didSelectButtonAtIndex?(0)
    }
    
    @IBAction func doNhomButton(_ sender: Any) {
        didSelectButtonAtIndex?(1)
    }
    
    @IBAction func doBaiDangButton(_ sender: Any) {
        didSelectButtonAtIndex?(2)
    }
    
    @IBAction func doThietLapButton(_ sender: Any) {
        didSelectButtonAtIndex?(3)
    }
    
    @IBAction func doNhanTinButton(_ sender: Any) {
        didSelectButtonAtIndex?(4)
    }
}
