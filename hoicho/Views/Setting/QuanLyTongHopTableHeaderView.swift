//
//  QuanLyTongHopTableHeaderView.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 5/14/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

class QuanLyTongHopTableHeaderView: UITableViewHeaderFooterView {

    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var subcribeLabel: UILabel!
    @IBOutlet weak var subcribeSwitch: UISwitch!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var stackViewHeight: NSLayoutConstraint!
}
