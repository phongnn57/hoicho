//
//  MenuConfigViewCell.swift
//  IShopGo
//
//  Created by Phong Nguyen Nam on 7/6/17.
//  Copyright © 2017 Mua Đồ Tốt. All rights reserved.
//

import UIKit

class MenuConfigViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var switchControl: UISwitch!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
