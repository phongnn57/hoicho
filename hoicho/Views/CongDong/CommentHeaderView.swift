//
//  CommentHeaderView.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 4/26/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

class CommentHeaderView: UITableViewHeaderFooterView {

    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var mediaView: UIView!
    @IBOutlet weak var mediaHeight: NSLayoutConstraint!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var replyButton: UIButton!
    @IBOutlet weak var bgView: UIView!
    
    var didSelectImageAt: ((Int) -> Void)?
    var openProfile: (() -> Void)?
    
    
    var comment: CommentModel! {
        didSet {
            setupUI()
        }
    }
    
    func setupUI() {
        if comment == nil {return}
        
        avatarImageView.sd_setImage(with: URL(string: comment.account_image), placeholderImage: #imageLiteral(resourceName: "icon_default_avatar"))
        nameLabel.text = comment.account_name
        contentLabel.text = comment.content
        if comment.content.isEmpty {
            self.bgView.backgroundColor = UIColor.white
        } else {
            self.bgView.backgroundColor = UIColor(hex: "#F2F2F2")
        }
        if comment.updated_at != nil {
            timeLabel.text = Date.timeAgo(since: comment.updated_at!)
        } else {
            timeLabel.text = nil
        }
        
        if comment.images.count == 0 {
            mediaView.isHidden = true
            mediaHeight.constant = 0
        } else {
            mediaView.isHidden = false
    
            configMediaLayout()
        }
        avatarImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(avatarTapped)))
    }
    
    
    @objc func avatarTapped() {
        openProfile?()
    }
    
    func configMediaLayout() {
        for subview in mediaView.subviews {
            subview.removeFromSuperview()
        }
        mediaView.isHidden = false
        
        
        let mediaWidth = UIScreen.main.bounds.width - 78
        mediaHeight.constant = mediaWidth
        
        if comment.images.count == 1 {
            self.layoutIfNeeded()
            
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: mediaWidth, height: mediaWidth))
            imageView.contentMode = .scaleAspectFill
            imageView.clipsToBounds = true
            imageView.tag = 0
            imageView.layer.borderWidth = 0.5
            imageView.layer.borderColor = UIColor(hex: "#f0f0f0").cgColor
            imageView.sd_setImage(with: URL(string: comment.images[0]), placeholderImage: #imageLiteral(resourceName: "icon_no_image"))
            imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(doTapImage(_:))))
            imageView.isUserInteractionEnabled = true
            mediaView.addSubview(imageView)
        } else if comment.images.count == 2 {
            self.layoutSubviews()
            
            let imageView1 = UIImageView(frame: CGRect(x: 0, y: 0, width: mediaWidth, height: mediaWidth/2 - 2))
            imageView1.contentMode = .scaleAspectFill
            imageView1.clipsToBounds = true
            imageView1.tag = 0
            imageView1.layer.borderWidth = 0.5
            imageView1.layer.borderColor = UIColor(hex: "#f0f0f0").cgColor
            imageView1.sd_setImage(with: URL(string: comment.images[0]), placeholderImage: #imageLiteral(resourceName: "icon_no_image"))
            imageView1.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(doTapImage(_:))))
            imageView1.isUserInteractionEnabled = true
            mediaView.addSubview(imageView1)
            
            let imageView2 = UIImageView(frame: CGRect(x: 0, y: mediaWidth/2 + 2, width: mediaWidth, height: mediaWidth/2 - 2))
            imageView2.contentMode = .scaleAspectFill
            imageView2.clipsToBounds = true
            imageView2.tag = 1
            imageView2.layer.borderWidth = 0.5
            imageView2.layer.borderColor = UIColor(hex: "#f0f0f0").cgColor
            imageView2.sd_setImage(with: URL(string: comment.images[1]), placeholderImage: #imageLiteral(resourceName: "icon_no_image"))
            imageView2.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(doTapImage(_:))))
            imageView2.isUserInteractionEnabled = true
            mediaView.addSubview(imageView2)
        } else if comment.images.count == 3 {
            self.layoutSubviews()
            
            let imageView1 = UIImageView(frame: CGRect(x: 0, y: 0, width: mediaWidth, height: mediaWidth/2 - 2))
            imageView1.contentMode = .scaleAspectFill
            imageView1.clipsToBounds = true
            imageView1.tag = 0
            imageView1.layer.borderWidth = 0.5
            imageView1.layer.borderColor = UIColor(hex: "#f0f0f0").cgColor
            imageView1.sd_setImage(with: URL(string: comment.images[0]), placeholderImage: #imageLiteral(resourceName: "icon_no_image"))
            imageView1.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(doTapImage(_:))))
            imageView1.isUserInteractionEnabled = true
            mediaView.addSubview(imageView1)
            
            let imageView2 = UIImageView(frame: CGRect(x: 0, y: mediaWidth/2 + 2, width: mediaWidth/2 - 2, height: mediaWidth/2 - 2))
            imageView2.contentMode = .scaleAspectFill
            imageView2.clipsToBounds = true
            imageView2.tag = 1
            imageView2.layer.borderWidth = 0.5
            imageView2.layer.borderColor = UIColor(hex: "#f0f0f0").cgColor
            imageView2.sd_setImage(with: URL(string: comment.images[1]), placeholderImage: #imageLiteral(resourceName: "icon_no_image"))
            imageView2.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(doTapImage(_:))))
            imageView2.isUserInteractionEnabled = true
            mediaView.addSubview(imageView2)
            
            let imageView3 = UIImageView(frame: CGRect(x: mediaWidth/2 + 2, y: mediaWidth/2 + 2, width: mediaWidth/2 - 2, height: mediaWidth/2 - 2))
            imageView3.contentMode = .scaleAspectFill
            imageView3.clipsToBounds = true
            imageView3.layer.borderWidth = 0.5
            imageView3.tag = 2
            imageView3.layer.borderColor = UIColor(hex: "#f0f0f0").cgColor
            imageView3.sd_setImage(with: URL(string: comment.images[2]), placeholderImage: #imageLiteral(resourceName: "icon_no_image"))
            imageView3.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(doTapImage(_:))))
            imageView3.isUserInteractionEnabled = true
            mediaView.addSubview(imageView3)
        } else if comment.images.count >= 4 {
            self.layoutSubviews()
            
            let imageView1 = UIImageView(frame: CGRect(x: 0, y: 0, width: mediaWidth, height: mediaWidth * 2/3 - 1.5))
            imageView1.contentMode = .scaleAspectFill
            imageView1.clipsToBounds = true
            imageView1.layer.borderWidth = 0.5
            imageView1.tag = 0
            imageView1.layer.borderColor = UIColor(hex: "#f0f0f0").cgColor
            imageView1.sd_setImage(with: URL(string: comment.images[0]), placeholderImage: #imageLiteral(resourceName: "icon_no_image"))
            imageView1.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(doTapImage(_:))))
            imageView1.isUserInteractionEnabled = true
            mediaView.addSubview(imageView1)
            
            let bottomImageHeight = mediaWidth - 3 - (mediaWidth * 2/3 - 1.5)
            
            let imageView2 = UIImageView(frame: CGRect(x: 0, y: mediaWidth * 2/3 + 1.5, width: bottomImageHeight, height: bottomImageHeight))
            imageView2.contentMode = .scaleAspectFill
            imageView2.clipsToBounds = true
            imageView2.layer.borderWidth = 0.5
            imageView2.tag = 1
            imageView2.layer.borderColor = UIColor(hex: "#f0f0f0").cgColor
            imageView2.sd_setImage(with: URL(string: comment.images[1]), placeholderImage: #imageLiteral(resourceName: "icon_no_image"))
            imageView2.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(doTapImage(_:))))
            imageView2.isUserInteractionEnabled = true
            mediaView.addSubview(imageView2)
            
            let imageView3 = UIImageView(frame: CGRect(x: bottomImageHeight + 3, y: mediaWidth * 2/3 + 1.5, width: bottomImageHeight, height: bottomImageHeight))
            imageView3.contentMode = .scaleAspectFill
            imageView3.clipsToBounds = true
            imageView3.layer.borderWidth = 0.5
            imageView3.tag = 2
            imageView3.layer.borderColor = UIColor(hex: "#f0f0f0").cgColor
            imageView3.sd_setImage(with: URL(string: comment.images[2]), placeholderImage: #imageLiteral(resourceName: "icon_no_image"))
            imageView3.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(doTapImage(_:))))
            imageView3.isUserInteractionEnabled = true
            mediaView.addSubview(imageView3)
            
            let imageView4 = UIImageView(frame: CGRect(x: bottomImageHeight * 2 + 6, y: mediaWidth * 2/3 + 1.5, width: bottomImageHeight, height: bottomImageHeight))
            imageView4.contentMode = .scaleAspectFill
            imageView4.clipsToBounds = true
            imageView4.layer.borderWidth = 0.5
            imageView4.tag = 3
            imageView4.layer.borderColor = UIColor(hex: "#f0f0f0").cgColor
            imageView4.sd_setImage(with: URL(string: comment.images[3]), placeholderImage: #imageLiteral(resourceName: "icon_no_image"))
            imageView4.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(doTapImage(_:))))
            imageView4.isUserInteractionEnabled = true
            mediaView.addSubview(imageView4)
            
            
            if comment.images.count > 4 {
                let overlayView = UIView(frame: imageView4.frame)
                overlayView.backgroundColor = UIColor.black.withAlphaComponent(0.4)
                let countingLabel = UILabel(frame: overlayView.bounds)
                countingLabel.text = "+ \(comment.images.count - 4)"
                countingLabel.font = UIFont.boldSystemFont(ofSize: 20)
                countingLabel.textColor = UIColor.white
                countingLabel.textAlignment = .center
                countingLabel.isUserInteractionEnabled = true
                countingLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openImages)))
                overlayView.addSubview(countingLabel)
                mediaView.addSubview(overlayView)
                
            }
        }
    }
    
    @objc func openImages() {
        didSelectImageAt?(3)
    }
    
    @objc func doTapImage(_ sender: UITapGestureRecognizer) {
        if let imageView = sender.view as? UIImageView {
            didSelectImageAt?(imageView.tag)
        }
    }
    
}
