//
//  CongDongTableViewCell.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 4/26/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

class CongDongTableViewCell: UITableViewCell {
    
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var contentTextView: UITextView!
    @IBOutlet weak var mediaView: UIView!
    @IBOutlet weak var mediaHeight: NSLayoutConstraint!
    @IBOutlet weak var likeCountLabel: UILabel!
    @IBOutlet weak var commentCountLabel: UILabel!
    @IBOutlet weak var shareCountLabel: UILabel!
    @IBOutlet weak var productView: UIView!
    @IBOutlet weak var productHeight: NSLayoutConstraint!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var likeImageView: UIImageView!
    
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var productPriceLabel: UILabel!
    @IBOutlet weak var profileButton: UIButton!
    
    var community: CommunityModel! {
        didSet {
            setupUI()
        }
    }
    var didSelectImageAt: ((Int) -> Void)?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setupUI() {
        if community == nil {return}
        
        avatarImageView.sd_setImage(with: URL(string: community.account_image), placeholderImage: #imageLiteral(resourceName: "icon_default_avatar"))
        nameLabel.text = community.account_name
        if community.created_at != nil {
            timeLabel.text = Date.timeAgo(since: community.created_at!)
        } else {
            timeLabel.text = nil
        }
        
        contentTextView.text = community.content
        likeCountLabel.text = "\(community.like_count)"
        commentCountLabel.text = "\(community.comment_count)"
        shareCountLabel.text = "\(community.share_count)"
        
        if community.liked {
            likeImageView.image = #imageLiteral(resourceName: "icon_like_count")
        } else {
            likeImageView.image = #imageLiteral(resourceName: "icon_chuaquantam")
        }
        
        if community.images.count == 0 {
            mediaView.isHidden = true
            mediaHeight.constant = 0
        } else {
            configMediaLayout()
        }
        
        if let product = community.product {
            let productWidth = UIScreen.main.bounds.width - 30
            productHeight.constant = productWidth * 2/3
            productView.isHidden = false
            
            productImageView.sd_setImage(with: URL(string: product.image), placeholderImage: #imageLiteral(resourceName: "icon_no_image"))
            productNameLabel.text = product.name
            productPriceLabel.text = product.price.priceString()
            shareButton.isHidden = false
            likeButton.isHidden = true
            
        } else {
            productHeight.constant = 0
            productView.isHidden = true
            shareButton.isHidden = true
            likeButton.isHidden = false
        }
        
        
        
    }
    
    func configMediaLayout() {
        for subview in mediaView.subviews {
            subview.removeFromSuperview()
        }
        mediaView.isHidden = false
        
        let mediaWidth = UIScreen.main.bounds.width - 30
        mediaHeight.constant = mediaWidth
        
        if community.images.count == 1 {
            self.layoutIfNeeded()
            
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: mediaWidth, height: mediaWidth))
            imageView.backgroundColor = UIColor(hex: "#EDEDED")
            imageView.contentMode = .scaleAspectFit
            imageView.clipsToBounds = true
            imageView.tag = 0
            imageView.layer.borderWidth = 0.5
            imageView.layer.borderColor = UIColor(hex: "#f0f0f0").cgColor
            imageView.sd_setImage(with: URL(string: community.images[0]), placeholderImage: #imageLiteral(resourceName: "icon_no_image"))
            imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(doTapImage(_:))))
            imageView.isUserInteractionEnabled = true
            mediaView.addSubview(imageView)
        } else if community.images.count == 2 {
            self.layoutSubviews()
            
            let imageView1 = UIImageView(frame: CGRect(x: 0, y: 0, width: mediaWidth, height: mediaWidth/2 - 2))
            imageView1.backgroundColor = UIColor(hex: "#EDEDED")
            imageView1.contentMode = .scaleAspectFit
            imageView1.clipsToBounds = true
            imageView1.tag = 0
            imageView1.layer.borderWidth = 0.5
            imageView1.layer.borderColor = UIColor(hex: "#f0f0f0").cgColor
            imageView1.sd_setImage(with: URL(string: community.images[0]), placeholderImage: #imageLiteral(resourceName: "icon_no_image"))
            imageView1.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(doTapImage(_:))))
            imageView1.isUserInteractionEnabled = true
            mediaView.addSubview(imageView1)
            
            let imageView2 = UIImageView(frame: CGRect(x: 0, y: mediaWidth/2 + 2, width: mediaWidth, height: mediaWidth/2 - 2))
            imageView2.backgroundColor = UIColor(hex: "#EDEDED")
            imageView2.contentMode = .scaleAspectFit
            imageView2.clipsToBounds = true
            imageView2.tag = 1
            imageView2.layer.borderWidth = 0.5
            imageView2.layer.borderColor = UIColor(hex: "#f0f0f0").cgColor
            imageView2.sd_setImage(with: URL(string: community.images[1]), placeholderImage: #imageLiteral(resourceName: "icon_no_image"))
            imageView2.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(doTapImage(_:))))
            imageView2.isUserInteractionEnabled = true
            mediaView.addSubview(imageView2)
        } else if community.images.count == 3 {
            self.layoutSubviews()
            
            let imageView1 = UIImageView(frame: CGRect(x: 0, y: 0, width: mediaWidth, height: mediaWidth/2 - 2))
            imageView1.backgroundColor = UIColor(hex: "#EDEDED")
            imageView1.contentMode = .scaleAspectFit
            imageView1.clipsToBounds = true
            imageView1.tag = 0
            imageView1.layer.borderWidth = 0.5
            imageView1.layer.borderColor = UIColor(hex: "#f0f0f0").cgColor
            imageView1.sd_setImage(with: URL(string: community.images[0]), placeholderImage: #imageLiteral(resourceName: "icon_no_image"))
            imageView1.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(doTapImage(_:))))
            imageView1.isUserInteractionEnabled = true
            mediaView.addSubview(imageView1)
            
            let imageView2 = UIImageView(frame: CGRect(x: 0, y: mediaWidth/2 + 2, width: mediaWidth/2 - 2, height: mediaWidth/2 - 2))
            imageView2.backgroundColor = UIColor(hex: "#EDEDED")
            imageView2.contentMode = .scaleAspectFit
            imageView2.clipsToBounds = true
            imageView2.tag = 1
            imageView2.layer.borderWidth = 0.5
            imageView2.layer.borderColor = UIColor(hex: "#f0f0f0").cgColor
            imageView2.sd_setImage(with: URL(string: community.images[1]), placeholderImage: #imageLiteral(resourceName: "icon_no_image"))
            imageView2.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(doTapImage(_:))))
            imageView2.isUserInteractionEnabled = true
            mediaView.addSubview(imageView2)
            
            let imageView3 = UIImageView(frame: CGRect(x: mediaWidth/2 + 2, y: mediaWidth/2 + 2, width: mediaWidth/2 - 2, height: mediaWidth/2 - 2))
            imageView3.backgroundColor = UIColor(hex: "#EDEDED")
            imageView3.contentMode = .scaleAspectFit
            imageView3.clipsToBounds = true
            imageView3.layer.borderWidth = 0.5
            imageView3.tag = 2
            imageView3.layer.borderColor = UIColor(hex: "#f0f0f0").cgColor
            imageView3.sd_setImage(with: URL(string: community.images[2]), placeholderImage: #imageLiteral(resourceName: "icon_no_image"))
            imageView3.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(doTapImage(_:))))
            imageView3.isUserInteractionEnabled = true
            mediaView.addSubview(imageView3)
        } else if community.images.count >= 4 {
            self.layoutSubviews()
            
            let imageView1 = UIImageView(frame: CGRect(x: 0, y: 0, width: mediaWidth, height: mediaWidth * 2/3 - 1.5))
            imageView1.backgroundColor = UIColor(hex: "#EDEDED")
            imageView1.contentMode = .scaleAspectFit
            imageView1.clipsToBounds = true
            imageView1.layer.borderWidth = 0.5
            imageView1.tag = 0
            imageView1.layer.borderColor = UIColor(hex: "#f0f0f0").cgColor
            imageView1.sd_setImage(with: URL(string: community.images[0]), placeholderImage: #imageLiteral(resourceName: "icon_no_image"))
            imageView1.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(doTapImage(_:))))
            imageView1.isUserInteractionEnabled = true
            mediaView.addSubview(imageView1)
            
            let bottomImageHeight = mediaWidth - 3 - (mediaWidth * 2/3 - 1.5)
            
            let imageView2 = UIImageView(frame: CGRect(x: 0, y: mediaWidth * 2/3 + 1.5, width: bottomImageHeight, height: bottomImageHeight))
            imageView2.backgroundColor = UIColor(hex: "#EDEDED")
            imageView2.contentMode = .scaleAspectFit
            imageView2.clipsToBounds = true
            imageView2.layer.borderWidth = 0.5
            imageView2.tag = 1
            imageView2.layer.borderColor = UIColor(hex: "#f0f0f0").cgColor
            imageView2.sd_setImage(with: URL(string: community.images[1]), placeholderImage: #imageLiteral(resourceName: "icon_no_image"))
            imageView2.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(doTapImage(_:))))
            imageView2.isUserInteractionEnabled = true
            mediaView.addSubview(imageView2)
            
            let imageView3 = UIImageView(frame: CGRect(x: bottomImageHeight + 3, y: mediaWidth * 2/3 + 1.5, width: bottomImageHeight, height: bottomImageHeight))
            imageView3.backgroundColor = UIColor(hex: "#EDEDED")
            imageView3.contentMode = .scaleAspectFit
            imageView3.clipsToBounds = true
            imageView3.layer.borderWidth = 0.5
            imageView3.tag = 2
            imageView3.layer.borderColor = UIColor(hex: "#f0f0f0").cgColor
            imageView3.sd_setImage(with: URL(string: community.images[2]), placeholderImage: #imageLiteral(resourceName: "icon_no_image"))
            imageView3.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(doTapImage(_:))))
            imageView3.isUserInteractionEnabled = true
            mediaView.addSubview(imageView3)
            
            let imageView4 = UIImageView(frame: CGRect(x: bottomImageHeight * 2 + 6, y: mediaWidth * 2/3 + 1.5, width: bottomImageHeight, height: bottomImageHeight))
            imageView4.backgroundColor = UIColor(hex: "#EDEDED")
            imageView4.contentMode = .scaleAspectFit
            imageView4.clipsToBounds = true
            imageView4.layer.borderWidth = 0.5
            imageView4.tag = 3
            imageView4.layer.borderColor = UIColor(hex: "#f0f0f0").cgColor
            imageView4.sd_setImage(with: URL(string: community.images[3]), placeholderImage: #imageLiteral(resourceName: "icon_no_image"))
            imageView4.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(doTapImage(_:))))
            imageView4.isUserInteractionEnabled = true
            mediaView.addSubview(imageView4)
            
            if community.images.count > 4 {
                let overlayView = UIView(frame: imageView4.frame)
                overlayView.backgroundColor = UIColor.black.withAlphaComponent(0.4)
                let countingLabel = UILabel(frame: overlayView.bounds)
                countingLabel.text = "+ \(community.images.count - 4)"
                countingLabel.font = UIFont.boldSystemFont(ofSize: 20)
                countingLabel.textColor = UIColor.white
                countingLabel.textAlignment = .center
                countingLabel.isUserInteractionEnabled = true
                countingLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openImages)))
                overlayView.addSubview(countingLabel)
                mediaView.addSubview(overlayView)
            }
        }
    }
    
    @objc func openImages() {
        didSelectImageAt?(3)
    }
    
    @objc func doTapImage(_ sender: UITapGestureRecognizer) {
        if let imageView = sender.view as? UIImageView {
            didSelectImageAt?(imageView.tag)
        }
    }
    
}
