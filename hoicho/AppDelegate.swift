//
//  AppDelegate.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 4/18/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import FBSDKCoreKit
import PusherSwift
import UserNotifications
import Firebase
import Fabric
import Crashlytics
import GoogleMaps
import FirebaseDynamicLinks
import AVFoundation
import SwiftyJSON

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var pusher: Pusher!
    var payload: PayloadModel!
    
    var player: AVAudioPlayer?
    
    func playSound() {
        guard let url = Bundle.main.url(forResource: "new_message_sound", withExtension: "mp3") else { return }
        
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
            try AVAudioSession.sharedInstance().setActive(true)
            
            
            /* The following line is required for the player to work on iOS 11. Change the file type accordingly*/
            player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileType.mp3.rawValue)
            
            /* iOS 10 and earlier require the following line:
             player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileTypeMPEGLayer3) */
            
            guard let player = player else { return }
            
            player.play()
            
        } catch let error {
            print(error.localizedDescription)
        }
    }


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        setupPusher()
        setupUI()
        configFirebase(application, didFinishLaunchingWithOptions: launchOptions)
        Fabric.with([Crashlytics.self])
        GMSServices.provideAPIKey("AIzaSyC_8FOm0HcXwTtD3YkzB01nLnEiXwMQB54")
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
        window?.rootViewController = HCTabBarViewController.viewController(.main)
        
        Database.database().reference().child("expo").child("app_version").child("ios").observe(.value, with: { (snapshot) in
            if snapshot.exists() {
                
                let json = JSON(snapshot.value ?? "")
                let store_version = json["ver"].stringValue
                let current_version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String
                let numberFormatter = NumberFormatter()
                numberFormatter.numberStyle = .decimal
                
                let storeVersionNumber = numberFormatter.number(from: store_version.replacingOccurrences(of: ".", with: ""))
                
                let currentVersionNumber = numberFormatter.number(from: current_version.replacingOccurrences(of: ".", with: ""))
                
                if storeVersionNumber!.intValue > currentVersionNumber!.intValue {
                    
                    if let updateAppView = ISGUpdateAppView.loadFromNibNamed() as? ISGUpdateAppView, self.window?.rootViewController != nil {
                        updateAppView.frame = UIScreen.main.bounds
                        updateAppView.currentVersion.text = current_version
                        updateAppView.storeVersion.text = store_version
                        
                        self.window?.rootViewController?.view.addSubview(updateAppView)
                        updateAppView.openPopup()
                        
                    }
                }
            }
        })
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
    }

    func applicationWillTerminate(_ application: UIApplication) {
    }

    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {

//        let sourceApplication: String? = options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String
//        return FBSDKApplicationDelegate.sharedInstance().application(app, open: url, sourceApplication: sourceApplication, annotation: nil)
        
        return application(app, open: url,
                           sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String,
                           annotation: "")
    }

    
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
//        return FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
        if let dynamicLink = DynamicLinks.dynamicLinks()?.dynamicLink(fromCustomSchemeURL: url) {
            handleDynamicLink(dynamicLink)
            return true
        }
        
        return false
    }
    
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([Any]?) -> Void) -> Bool {
        let handled = DynamicLinks.dynamicLinks()?.handleUniversalLink(userActivity.webpageURL!) { (dynamiclink, error) in
            if let _dynamicLink = dynamiclink {
                self.handleDynamicLink(_dynamicLink)
            }
            
        }
        return handled ?? false
    }
    
    

}

/////////////////////////DEEP LINK/////////////////////////////////
extension AppDelegate {
    func handleDynamicLink(_ dynamicLink: DynamicLink) {
        
        guard let handleUrl = dynamicLink.url else {return}
        
        if handleUrl.host != nil && Constant.webUrl.contains(handleUrl.host!) {
            guard let urlString = dynamicLink.url?.absoluteString else {return}

            if urlString.contains("san-pham") {
                let components = urlString.components(separatedBy: "-")
                guard let product_id = components.last else {return}
                
                self.openProductDetail(id: Int(product_id)!)
            } else if urlString.contains("gian-hang") {
                let components = urlString.components(separatedBy: "-")
                guard let booth_id = components.last else {return}
                if let finalId = booth_id.components(separatedBy: "?").first {
                    self.openBooth(id: Int(finalId) ?? 0)
                }
            }
        }
        
    }
}

////////////////////////////////////////////////////////////////////

// Giao diện
extension AppDelegate {
    func setupUI() {
        window?.backgroundColor = UIColor.white
        window?.makeKeyAndVisible()
        
        UINavigationBar.appearance().tintColor = UIColor.HCColor.appColor
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.HCColor.appColor]
        
        UITabBar.appearance().tintColor = UIColor.HCColor.appColor
        UITabBar.appearance().isTranslucent = false
        
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.toolbarDoneBarButtonItemImage = #imageLiteral(resourceName: "icon_hide_keyboard")
        IQKeyboardManager.shared.toolbarDoneBarButtonItemText = "Đóng"
        IQKeyboardManager.shared.disabledToolbarClasses = [MessengerViewController.self]
        
        if UserDefaults.standard.runCount() == 0 {
            UserModel.shared.delete()
            UserModel.deleteCurrentUserPhone()
        }
        UserDefaults.standard.updateRuncount()
        
        UserModel.loadData()

    }
}

extension AppDelegate: PusherDelegate {
    func setupPusher() {
        let authMethod = AuthMethod.endpoint(authEndpoint: Constant.Pusher.end_point)
        let host = PusherHost.cluster(Constant.Pusher.app_cluster)
        
        let optionsWithEndpoint = PusherClientOptions.init(authMethod: authMethod, attemptToReturnJSONObject: false, autoReconnect: true, host: host, port: nil, encrypted: false)
        pusher = Pusher(key: Constant.Pusher.app_key, options: optionsWithEndpoint)
        
        pusher.delegate = self
        
        pusher.connect()
    }
    
    func changedConnectionState(from old: ConnectionState, to new: ConnectionState) {
        print("old: \(old.stringValue()) -> new: \(new.stringValue())")
    }
    
    func subscribedToChannel(name: String) {
        print("Subscribed to \(name)")
    }
    
    func debugLog(message: String) {
        print(message)
    }
    
}

// firebase
extension AppDelegate {
    func configFirebase(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) {
        FirebaseOptions.defaultOptions()?.deepLinkURLScheme = Constant.customURLScheme
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        
        if let userInfo = launchOptions?[UIApplicationLaunchOptionsKey.remoteNotification] {
            
            let json = JSON(userInfo)
            
            let payloadModel = PayloadModel(json)
            
            self.payload = payloadModel
        }
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        
        print(userInfo)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        print(userInfo)
        
        completionHandler(UIBackgroundFetchResult.newData)
    }
}

@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        
        let json = JSON(userInfo)
        
        print("USER INFO: \(json)")
        
        let title = json["aps"]["alert"]["title"].stringValue
        let body = json["aps"]["alert"]["body"].stringValue
        
//        let badge = json["aps"]["badge"].intValue
        
        
        let payloadModel = PayloadModel(json)
        
        var shouldShow = true
        if let topViewController = window?.visibleViewController() {
            if topViewController.isKind(of: HCMessengerViewController.self) {
                if let currentConversationId = (topViewController as! HCMessengerViewController).conversation?.id_conversations, currentConversationId == payloadModel.idConversion {
                    shouldShow = false
                }
            }
        }
        
        if shouldShow {
            HDNotificationView.show(with: #imageLiteral(resourceName: "icon_default_avatar"), title: payloadModel.domain + ": " + title, message: body, isAutoHide: true) {
                // TO DO
                
                HDNotificationView.hide(onComplete: {
                    self.handlePayload(payloadModel)
                })
                
            }
        }
        
        completionHandler([])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        
        let json = JSON(userInfo)
        print(json)
        
        let payloadModel = PayloadModel(json)
        self.handlePayload(payloadModel)
        
        completionHandler()
    }
}

extension AppDelegate : MessagingDelegate {
    
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        
    }
    
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Received data message: \(remoteMessage.appData)")
    }
    // [END ios_10_data_message]
}

/////////////////////// xử lý thông báo nhận về qua push////////////////////////////
extension AppDelegate {
    
    func handlePayload(_ payload: PayloadModel) {
        
        if payload.type == nil || UserModel.shared.token.isEmpty {
//            openThongBao()
            return
        }
        
//        ISGNotificationService.readNotification(payload.notificationId, completion: {
//
//        }) { (_) in
//
//        }
//
        guard let _ = window?.rootViewController as? HCTabBarViewController else {
            self.payload = payload
            
            return
        }
        
        if payload.type == .don_hang {

        } else if payload.type == .tin_tuc {

        } else if payload.type == .cong_dong {
            openCongDong(payload)
        } else if payload.type == .phan_hoi {

        } else if payload.type == .dao_tao {

        } else if payload.type == .chinh_sach {

        } else if payload.type == .cong_viec {

        } else if payload.type == .tuyen_dung {

        } else if payload.type == .san_pham {
            openProduct(payload)
        } else if payload.type == .bao_loi {

        } else if payload.type == .chat {
            doOpenChat(payload)
        } else {

        }
        
    }
    
    func doOpenChat(_ payload: PayloadModel) {
        guard let rootViewController = window?.rootViewController as? HCTabBarViewController else {
            return
        }
        
        guard let chatNavigationController = rootViewController.viewControllers?[3] as? HCNavigationController else {
            return
        }
        
        chatNavigationController.popToRootViewController(animated: false)
        rootViewController.selectedIndex = 3
        
        let conversationModel = ConversationModel()
        conversationModel.id_conversations = payload.idConversion
        
        
        let chatViewController = HCMessengerViewController.viewController(.chat) as! HCMessengerViewController
        chatViewController.hidesBottomBarWhenPushed = true
        chatViewController.conversation = conversationModel
        chatNavigationController.pushViewController(chatViewController, animated: true)
        
    }
    
    func openProduct(_ payload: PayloadModel) {
        guard let rootViewController = window?.rootViewController as? HCTabBarViewController else {
            return
        }
        
        guard let mainNavigationController = rootViewController.viewControllers?[0] as? HCNavigationController else {
            return
        }
        
        mainNavigationController.popToRootViewController(animated: false)
        rootViewController.selectedIndex = 0
        
        guard let chiTietSanPhamViewController = ChiTietSanPhamViewController.viewController(.home) as? ChiTietSanPhamViewController else {return}
        chiTietSanPhamViewController.hidesBottomBarWhenPushed = true
        chiTietSanPhamViewController.product_id = payload.id
        mainNavigationController.pushViewController(chiTietSanPhamViewController, animated: true)
    }
    
    func openCongDong(_ payload: PayloadModel) {
        guard let rootViewController = window?.rootViewController as? HCTabBarViewController else {
            return
        }
        
        guard let congDongNavigationController = rootViewController.viewControllers?[1] as? HCNavigationController else {
            return
        }
        
        congDongNavigationController.popToRootViewController(animated: false)
        rootViewController.selectedIndex = 1
        
        guard let chiTietBaiVietViewController = ChiTietCongDongViewController.viewController(.congdong) as? ChiTietCongDongViewController else {return}
        chiTietBaiVietViewController.hidesBottomBarWhenPushed = true
        chiTietBaiVietViewController.id = payload.id
        congDongNavigationController.pushViewController(chiTietBaiVietViewController, animated: true)
    }
    
    func openDanhBa() {
        guard let rootViewController = window?.rootViewController as? HCTabBarViewController else {
            return
        }
        
        guard let chatNavigationController = rootViewController.viewControllers?[3] as? HCNavigationController else {
            return
        }
        
        chatNavigationController.popToRootViewController(animated: false)
        rootViewController.selectedIndex = 3
        
        if let chatRootViewController = chatNavigationController.viewControllers.first as? ChatRootViewController {
            chatRootViewController.defaultIndex = 1
        }
        
    }
    
    func openProductDetail(id: Int) {
        guard let rootViewController = window?.rootViewController as? HCTabBarViewController else {
            return
        }
        
        guard let mainNavigationController = rootViewController.viewControllers?[0] as? HCNavigationController else {
            return
        }
        
        mainNavigationController.popToRootViewController(animated: false)
        rootViewController.selectedIndex = 0
        
        guard let chiTietSanPhamViewController = ChiTietSanPhamViewController.viewController(.home) as? ChiTietSanPhamViewController else {return}
        chiTietSanPhamViewController.hidesBottomBarWhenPushed = true
        chiTietSanPhamViewController.product_id = id
        mainNavigationController.pushViewController(chiTietSanPhamViewController, animated: true)
    }
    
    func openBooth(id: Int) {
        
        guard let rootViewController = window?.rootViewController as? HCTabBarViewController else {
            return
        }
        
        guard let mainNavigationController = rootViewController.viewControllers?[0] as? HCNavigationController else {
            return
        }
        
        mainNavigationController.popToRootViewController(animated: false)
        rootViewController.selectedIndex = 0
        
        guard let gianHangViewController = GianHangViewController.viewController(.home) as? GianHangViewController else {return}
        gianHangViewController.hidesBottomBarWhenPushed = true
        gianHangViewController.booth_id = id
        gianHangViewController.followImediately = true
        mainNavigationController.pushViewController(gianHangViewController, animated: true)
    }
}


