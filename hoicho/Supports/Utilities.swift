//
//  Utilities.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 4/18/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import SystemConfiguration

public class Reachability {
    
    class func isInternetAvailable() -> Bool {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }
}


class Utilities: NSObject {
    
    
    
    class func isNilOrEmpty(_ object: Any?) -> Bool {
        if(object == nil) {
            return true
        }
        if(object is String && (object as! String).isEmpty){
            return true
        }
        if(object is Array<Any> && (object as! Array<Any>).count == 0){
            return true
        }
        if(object is Dictionary<String, Any> && (object as! Dictionary<String, Any>).count == 0){
            return true
        }
        return false
    }
    
    class func formatPriceNumber(_ price: NSNumber) -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .decimal
        numberFormatter.groupingSize = 3
        numberFormatter.groupingSeparator = "."
        
        return numberFormatter.string(from: price)!
    }
    
    class func isEmail(_ email: String) -> Bool {
        do {
            let regex = try NSRegularExpression(pattern: "^[A-Z0-9._%+-]+@(?:[A-Z0-9-]+\\.)+[A-Z]{2,4}$", options: NSRegularExpression.Options.caseInsensitive)
            let matches = regex.matches(in: email, options: NSRegularExpression.MatchingOptions.reportCompletion, range: NSMakeRange(0, email.count))
            return matches.count > 0
            
        } catch {
            return false
        }
    }
    
    class func isPhone(_ value: String) -> Bool {
        let phoneRegex = "^((\\+84)|(84)|(0))[0-9]{9,10}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", phoneRegex)
        return phoneTest.evaluate(with: value)
    }
    
    
}
