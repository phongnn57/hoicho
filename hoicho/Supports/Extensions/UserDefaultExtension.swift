//
//  UserDefaultExtension.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 4/18/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

extension UserDefaults {
    
    fileprivate enum Constants: String {
        case run_count
    }
    
    func runCount() -> Int {
        return integer(forKey: Constants.run_count.rawValue)
    }
    
    func updateRuncount() {
        set(runCount() + 1, forKey: Constants.run_count.rawValue)
    }
    
    
}
