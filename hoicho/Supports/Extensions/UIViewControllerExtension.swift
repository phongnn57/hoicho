//
//  UIViewControllerExtension.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 4/18/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit
import AVFoundation
import NVActivityIndicatorView


extension UIViewController {
    
    static func viewController(_ storyboard: StoryBoard) -> UIViewController? {
        let className = NSStringFromClass(self).components(separatedBy: ".").last!
        let storyboard = UIStoryboard(name: storyboard.rawValue, bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: className)
    }
    

    func findHairlineImageViewUnder(_ view: UIView) -> UIImageView? {
        if view.isKind(of: UIImageView.self) && view.bounds.size.height <= 1.0 {
            return view as? UIImageView
        }
        
        for subview in view.subviews {
            if let imgView = self.findHairlineImageViewUnder(subview){
                return imgView
            }
        }
        return nil
    }
    
    func showProgress() {
        let activityData = ActivityData(type: .ballScaleMultiple)
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
    }
    
    func dismissProgress() {
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
    }
    
    func showError(_ error: Error) {
        
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
        
        DispatchQueue.main.async {
            let errorCode = (error as NSError).code
            if errorCode == AppErrorCode.unAuthorized.rawValue {
                
                HCAlertController.showAlert(message: error.localizedDescription, leftButton: nil, rightButton: "Đăng nhập", viewController: self, completion: { (index) in
                    Constant.appDelegate.window?.rootViewController = HCNavigationController(rootViewController: LoginViewController.viewController(.auth)!)
                })
                
            } else {
                
                HCAlertController.showAlertWithMessage(message: error.localizedDescription, viewController: self)
                
            }
            
        }
        
    }
    
    
    func showAlertWithMessage(_ message: String, completion: (() -> Void)? = nil) {
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
        
        HCAlertController.showAlert(message: message, leftButton: nil, rightButton: "Đóng", viewController: self) { (index) in
            completion?()
        }

    }
    
    func showAlertTwoButtonWithMessage(_ message: String, completion: (() -> Void)? = nil) {
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
        HCAlertController.showAlert(message: message, leftButton: "Huỷ", rightButton: "Đồng ý", viewController: self) { (index) in
            if index == 1 {
                completion?()
            }
            
        }
    }
    
    
    func initLoadMoreIndicator() -> UIActivityIndicatorView {
        let indicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        indicator.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 60)
        indicator.startAnimating()
        return indicator
    }
    
    func initRefreshControl() -> UIRefreshControl {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(doRefresh), for: .valueChanged)
        return refreshControl
    }
    
    @objc func doRefresh() {
        
    }
    
    func doCall(_ phone: String) {
        if phone.trimpSpace().isEmpty {
            showAlertWithMessage("Không có số điện thoại")
            return
        }
        guard let url = URL(string: "tel://\(phone.trimpSpace())") else {
            return
        }
        
        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.openURL(url)
        } else {
            showAlertWithMessage("Thiết bị không hỗ trợ chức năng này")
        }
        
    }
    
    func doSelectPhoto(allowEdit: Bool = false){
        let actionSheet = UIAlertController(title: "Chọn ảnh", message: nil, preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Thư viện ảnh", style: .default, handler: { (action: UIAlertAction) in
            self.doSelectMediaPhotoWithType(.photoLibrary, allowEdit)
            
        }))
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            actionSheet.addAction(UIAlertAction(title: "Chụp ảnh", style: .default, handler: { (action: UIAlertAction) in
                self.doSelectMediaPhotoWithType(.camera, allowEdit)
            }))
        }
        
        actionSheet.addAction(UIAlertAction(title: "Huỷ", style: .destructive, handler: nil))
        if let popoverController = actionSheet.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.height, width: 0, height: 0)
        }
        present(actionSheet, animated: true, completion: nil)
    }
    
    func doSelectMediaPhotoWithType(_ sourceType: UIImagePickerControllerSourceType, _ allowEdit: Bool = false) {
        let imagePickerController = UIImagePickerController()
        imagePickerController.sourceType = sourceType
        imagePickerController.delegate = self
        imagePickerController.allowsEditing = allowEdit
        present(imagePickerController, animated: true, completion: nil)
    }
    
    func initDatePicker() -> UIDatePicker {
        let datePicker = UIDatePicker(frame: .zero)
        datePicker.datePickerMode = .date
        datePicker.locale = Locale(identifier: "vi_VN")
        datePicker.calendar = Calendar(identifier: .gregorian)
        
        return datePicker
    }
    
    func initToolbar() -> UIToolbar {
        let toolBar = UIToolbar(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 44))
        let cancelItem = UIBarButtonItem(title: "Huỷ", style: .plain, target: self, action: #selector(doCancelItem))
        let spaceItem = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneItem = UIBarButtonItem(title: "Chọn", style: .plain, target: self, action: #selector(doDoneItem))
        toolBar.setItems([cancelItem, spaceItem, doneItem], animated: false)
        toolBar.tintColor = UIColor.HCColor.appColor
        
        return toolBar
    }
    
    @objc func doCancelItem() {
        self.view.endEditing(true)
    }
    
    @objc func doDoneItem() {
        
    }
    
    func openMedia(images: [String], index: Int = 0) {
        guard let mediaViewController = MediaViewController.viewController(.common) as? MediaViewController else {return}
        mediaViewController.images = images
        mediaViewController.defaultIndex = index
        self.present(mediaViewController, animated: true, completion: nil)
    }
    
    func openWebViewWith(html: String? = "", url: String? = "", webTitle: String?) {
        guard let webViewController = HCWebViewViewController.viewController(.common) as? HCWebViewViewController else {return}
        webViewController.title = webTitle
        webViewController.url = url!
        webViewController.html = html!
        self.present(HCNavigationController(rootViewController: webViewController), animated: true, completion: nil)
    }
    
    func requestLoginWithCompletion(_ forceLogin: Bool = false, _ completion: (() -> Void)?) {
        if UserModel.shared.token.isEmpty || forceLogin {
            guard let loginViewController = LoginViewController.viewController(.auth) as? LoginViewController else {return}
            loginViewController.completion = completion
            self.present(HCNavigationController(rootViewController: loginViewController), animated: true, completion: nil)
            
        } else {
            completion?()
        }
    }
    
}


extension UIViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
    }
}




