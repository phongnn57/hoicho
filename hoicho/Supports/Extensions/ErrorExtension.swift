//
//  ErrorExtension.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 4/18/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

enum AppErrorCode: Int {
    case network = 999
    case unAuthorized = 401
    case timeout = -1001
    case serverConnection = -1004
    case serverError = 500
}

class AppError: Error {
    
    static func networkError() -> Error {
        
        return NSError(domain: "com.ishopgo.hoicho", code: AppErrorCode.network.rawValue, userInfo: [NSLocalizedDescriptionKey : "Không có kết nối mạng. Vui lòng thử lại sau"])
        
    }
    
    static func unAuthorizedError() -> Error {
        return NSError(domain: "com.ishopgo.hoicho", code: AppErrorCode.unAuthorized.rawValue, userInfo: [NSLocalizedDescriptionKey: "Yêu cầu đăng nhập lại"])
    }
    
    static func timeoutError() -> Error {
        return NSError(domain: "com.ishopgo.hoicho", code: AppErrorCode.timeout.rawValue, userInfo: [NSLocalizedDescriptionKey: "Quá thời gian thực thi. Vui lòng thử lại sau"])
    }
    
    static func serverConnection() -> Error {
        return NSError(domain: "com.ishopgo.hoicho", code: AppErrorCode.serverConnection.rawValue, userInfo: [NSLocalizedDescriptionKey: "Không thể kết nối máy chủ"])
    }
    
    static func serverError() -> Error {
        return NSError(domain: "com.ishopgo.hoicho", code: AppErrorCode.serverConnection.rawValue, userInfo: [NSLocalizedDescriptionKey: "Hệ thống xảy ra lỗi. Vui lòng thử lại sau"])
    }
    
    static func commonError() -> Error {
        return NSError(domain: "com.ishopgo.hoicho", code: AppErrorCode.serverConnection.rawValue, userInfo: [NSLocalizedDescriptionKey: "Có lỗi xảy ra. Vui lòng thử lại sau"])
    }
}
