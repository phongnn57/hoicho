//
//  StringExtension.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 4/18/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit
import Foundation


extension String {
    
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: font], context: nil)
        
        return ceil(boundingBox.height)
    }
    
    func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: font], context: nil)
        
        return ceil(boundingBox.width)
    }
    
    var html2AttributedString: NSAttributedString? {
        guard
            let data = data(using: String.Encoding.utf8)
            else { return nil }
        do {
            let attString = try NSAttributedString(data: data, options: [NSAttributedString.DocumentReadingOptionKey.documentType : NSAttributedString.DocumentType.html, NSAttributedString.DocumentReadingOptionKey.characterEncoding:  String.Encoding.utf8.rawValue], documentAttributes: nil)
            
            let attFont = [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 14)]
            let mutableString = NSMutableAttributedString(attributedString: attString)
            let nsrange = NSRange(location: 0, length: attString.length)
            mutableString.addAttributes(attFont, range: nsrange)
            
            
            return mutableString
            
        } catch let error as NSError {
            print(error.localizedDescription)
            return  nil
        }
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
    
    var string2Date: NSDate?{
        let dateFormat:DateFormatter = DateFormatter()
        dateFormat.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        
        let dateFM = dateFormat.date(from: self)
        
        if(dateFM == nil){
            return NSDate()
        }
        
        return dateFM as NSDate?
    }
    
    func toDate(_ format: String?="dd/MM/yyyy") -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        
        return dateFormatter.date(from: self)
    }
    
    func toDateTimeFormat() -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy/MM/dd HH:mm:ss"
        
        return dateFormatter.date(from: self)
    }
    
    var length: Int {
        return self.count
    }
    
    subscript (i: Int) -> String {
        return self[Range(i ..< i + 1)]
    }
    
    func substring(from: Int) -> String {
        return self[Range(min(from, length) ..< length)]
    }
    
    func substring(to: Int) -> String {
        return self[Range(0 ..< max(0, to))]
    }
    
    subscript (r: Range<Int>) -> String {
        let range = Range(uncheckedBounds: (lower: max(0, min(length, r.lowerBound)),
                                            upper: min(length, max(0, r.upperBound))))
        let start = index(startIndex, offsetBy: range.lowerBound)
        let end = index(start, offsetBy: range.upperBound - range.lowerBound)
        return String(self[Range(start ..< end)])
    }
    
    func trimpSpace() -> String {
        var object = self.trimmingCharacters(in: .whitespacesAndNewlines)
        while let rangeToReplace = object.range(of: "\n\n") {
            object.replaceSubrange(rangeToReplace, with: "\n")
        }
        return object
    }
    
    func addCssStyle() -> String {
        do {
            let cssString = try String(contentsOf: Bundle.main.url(forResource: "WebViewStyle", withExtension: "css")!, encoding: .utf8)
            
            return String(format: "<html><head><meta name=\"viewport\" content=\"user-scalable=no\"/><style>%@</style></head><body style='margin:8; padding:8; font-size: 16; font-family: HelveticaNeue; color:#333333'>%@</body></html>",cssString, self)
        } catch _ {
            return self
        }
    }
    
    func currencyInputFormatting() -> String {
        
        var number: NSNumber!
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        //        formatter.locale = Locale(identifier: "vi_VN")
        formatter.currencySymbol = "đ"
        formatter.maximumFractionDigits = 0
        formatter.positiveFormat = "#,##0 ¤"
        formatter.negativeFormat = "-#,##0 ¤"
        formatter.groupingSeparator = "."
        formatter.currencyGroupingSeparator = "."
        
        var amountWithPrefix = self
        
        if amountWithPrefix.substring(from: amountWithPrefix.length - 1) == " " {
            let regex = try! NSRegularExpression(pattern: "[^0-9]", options: .caseInsensitive)
            amountWithPrefix = regex.stringByReplacingMatches(in: amountWithPrefix, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, self.count), withTemplate: "")
            
            let double = (amountWithPrefix as NSString).doubleValue
            number = NSNumber(value: Int(double/10))
            
            // if first number is 0 or all numbers were deleted
            guard number.intValue != 0 as Int else {
                return ""
            }
            
            return formatter.string(from: number)!
        } else {
            // remove from String: "$", ".", ","
            let regex = try! NSRegularExpression(pattern: "[^0-9]", options: .caseInsensitive)
            amountWithPrefix = regex.stringByReplacingMatches(in: amountWithPrefix, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, self.count), withTemplate: "")
            
            let double = (amountWithPrefix as NSString).doubleValue
            number = NSNumber(value: double)
            
            // if first number is 0 or all numbers were deleted
            guard number != 0 as NSNumber else {
                return ""
            }
            
            return formatter.string(from: number)!
        }
        
        
    }
    
    func moneyString() -> String {
        do {
            let regex = try NSRegularExpression(pattern: "[^0-9]", options: .caseInsensitive)
            return regex.stringByReplacingMatches(in: self, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, self.count), withTemplate: "")
        } catch {
            return "0"
        }
        
    }
    
    func amountNumber() -> NSNumber {
        do {
            let regex = try NSRegularExpression(pattern: "[^0-9]", options: .caseInsensitive)
            let value = regex.stringByReplacingMatches(in: self, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, self.count), withTemplate: "")
            let numberFormatter = NumberFormatter()
            return numberFormatter.number(from: value) ?? 0
        } catch {
            return 0
        }
        
    }
    
    var decodeEmoji: String{
        let data = self.data(using: String.Encoding.utf8);
        let decodedStr = NSString(data: data!, encoding: String.Encoding.nonLossyASCII.rawValue)
        if let str = decodedStr{
            return str as String
        }
        return self
    }
    
    var encodeEmoji: String{
        if let encodeStr = NSString(cString: self.cString(using: .nonLossyASCII)!, encoding: String.Encoding.utf8.rawValue){
            return encodeStr as String
        }
        return self
    }
    
    static func normalAndBoldText(normal: String, bold: String) -> NSAttributedString {
        let attributedString = NSMutableAttributedString()
        attributedString.append(NSAttributedString(string: normal, attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 16), NSAttributedStringKey.foregroundColor: UIColor(hex: "#333333")]))
        attributedString.append(NSAttributedString(string: bold, attributes: [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 16), NSAttributedStringKey.foregroundColor: UIColor(hex: "#333333")]))
        
        return attributedString
    }
    
    
}

