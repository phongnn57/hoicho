//
//  UIButtonExtension.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 6/8/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

extension UIButton {
    
    func shake() {
        let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
        animation.duration = 0.6
        animation.values = [-20.0, 20.0, -20.0, 20.0, -10.0, 10.0, -5.0, 5.0, 0.0 ]
        layer.add(animation, forKey: "shake")
    }
    
}
