//
//  NSNumberExtension.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 4/18/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

extension NSNumber {
    
    func priceString(_ enterLine: Bool? = false) -> String {
        if enterLine! {
            return Utilities.formatPriceNumber(self) + "\nđ"
        }
        return Utilities.formatPriceNumber(self) + " đ"
    }
    
    func numberString() -> String {
        return Utilities.formatPriceNumber(self)
    }
    
    func priceWithoutSuffix(_ enterLine: Bool? = false) -> String {
        if enterLine! {
            return Utilities.formatPriceNumber(self)
        }
        return Utilities.formatPriceNumber(self)
    }
    
}
