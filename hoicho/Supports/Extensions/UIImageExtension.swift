//
//  UIImageExtension.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 4/18/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

extension UIImage {
    
    convenience init(view: UIView) {
        let scale = UIScreen.main.scale
        UIGraphicsBeginImageContextWithOptions(view.frame.size, false, scale)
        view.layer.render(in:UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        self.init(cgImage: image!.cgImage!)
    }
    
    func dataForUpload(fileSize: CGFloat = 5) -> Data {
        
        var imageData = UIImageJPEGRepresentation(self, 1.0)
        var compressionRate: CGFloat = 10
        
        while imageData!.count > Int(1024 * fileSize) {
            if compressionRate > 0.5 {
                compressionRate -= 0.5
                imageData = UIImageJPEGRepresentation(self, compressionRate/10)
            } else {
                return imageData!
            }
        }
        
        return imageData!
        
    }
    
    func textToImage(drawText text: String, fontSize: CGFloat?=22) -> UIImage {
        
        let textColor = UIColor.white
        
        let paragraphstyle = NSMutableParagraphStyle()
        paragraphstyle.alignment = .center
        
        let scale = UIScreen.main.scale
        UIGraphicsBeginImageContextWithOptions(self.size, false, scale)
        
        let textFontAttributes = [
            NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: fontSize!),
            NSAttributedStringKey.foregroundColor: textColor
        ]
        
        
        let textRect = text.boundingRect(with: CGSize.zero, options: .usesLineFragmentOrigin, attributes: textFontAttributes, context: nil)
        
        
        self.draw(in: CGRect(origin: CGPoint.zero, size: self.size))
        
        let rect = CGRect(origin: CGPoint(x: self.size.width/2 - textRect.size.width/2, y: self.size.height/2 - textRect.size.height/2), size: self.size)
        
        text.draw(in: rect, withAttributes: textFontAttributes)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    public convenience init?(color: UIColor, size: CGSize = CGSize(width: 1, height: 1)) {
        let rect = CGRect(origin: .zero, size: size)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
        color.setFill()
        UIRectFill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        guard let cgImage = image?.cgImage else { return nil }
        self.init(cgImage: cgImage)
    }
}

