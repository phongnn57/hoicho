//
//  URLExtension.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 6/9/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

extension URL {
    
    public var queryParameters: [String: String]? {
        guard let components = URLComponents(url: self, resolvingAgainstBaseURL: true), let queryItems = components.queryItems else {
            return nil
        }
        
        var parameters = [String: String]()
        for item in queryItems {
            parameters[item.name] = item.value
        }
        
        return parameters
    }
}
