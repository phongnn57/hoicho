//
//  UIViewExtension.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 4/18/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

extension UIView {
    
    func addDashedLine(color: UIColor = .lightGray, vertical: Bool = false) {
        let _ = layer.sublayers?.filter({ $0.name == "DashedTopLine" }).map({ $0.removeFromSuperlayer() })
        backgroundColor = .clear
        
        let shapeLayer = CAShapeLayer()
        shapeLayer.name = "DashedTopLine"
        shapeLayer.bounds = bounds
        shapeLayer.position = CGPoint(x: frame.width / 2, y: frame.height / 2)
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = color.cgColor
        shapeLayer.lineWidth = 1
        shapeLayer.lineJoin = kCALineJoinRound
        shapeLayer.lineDashPattern = [4, 4]
        
        let path = CGMutablePath()
        path.move(to: CGPoint.zero)
        if vertical {
            path.addLine(to: CGPoint(x: 0, y: frame.height))
        } else {
            path.addLine(to: CGPoint(x: frame.width, y: 0))
        }
        
        shapeLayer.path = path
        
        layer.addSublayer(shapeLayer)
    }
    
    class func loadFromNibNamed(_ nibNamed: String?="") -> UIView? {
        
        let className = NSStringFromClass(self).components(separatedBy: ".").last!
        
        return UINib(
            nibName: className,
            bundle: nil
            ).instantiate(withOwner: self, options: nil)[0] as? UIView
    }
    
    
    @IBInspectable var borderColor: UIColor {
        get {
            return UIColor.clear
        }
        set (color) {
            self.layer.borderColor = color.cgColor
        }
    }
    
    @IBInspectable var borderWidth: CGFloat {
        get {
            return 0
        }
        set (width) {
            self.layer.borderWidth = width
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return 0
        }
        set (width) {
            self.layer.cornerRadius = width
            self.layer.masksToBounds = true
        }
    }
}


