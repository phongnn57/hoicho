//
//  NotificationNameExtension.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 4/27/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

extension Notification.Name {
    static let UserLoggedIn = Notification.Name.init("user-logged-in")
    static let didReceivedNewChatMessage = Notification.Name.init("did-receive-new-chat-message")
    static let countUnreadInbox = Notification.Name.init("count-unread-inbox")
    static let countUnreadNotification = Notification.Name.init("count-unread-notification")
}
