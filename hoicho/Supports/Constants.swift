//
//  Constants.swift
//  hoicho
//
//  Created by Nam Phong Nguyen on 4/18/18.
//  Copyright © 2018 Mua Do Tot. All rights reserved.
//

import UIKit

enum StoryBoard: String {
    case auth = "Auth"
    case main = "Main"
    case common = "Common"
    case chat = "Chat"
    case home = "Home"
    case congdong = "CongDong"
    case setting = "Setting"
}

enum SocialType: Int, EnumEnumerable {
    case congdong = 0
    case other = 1
}

enum SortType: String {
    case ascending = "asc"
    case descending = "desc"
}

enum ProductSortType: Int, EnumEnumerable {
    case lowestPrice, highestPrice, aName, zName
    
    var title: String {
        switch self {
        case .lowestPrice: return "Giá từ thấp đến cao"
        case .highestPrice: return "Giá từ cao đến thấp"
        case .aName: return "Tên sản phẩm từ A - Z"
        case .zName: return "Tên sản phẩm từ Z - A"
        }
    }
    
    var key: String {
        switch self {
        case .lowestPrice: return "price"
        case .highestPrice: return "price"
        case .aName: return "name"
        case .zName: return "name"
        }
    }
    
    var sort: SortType {
        switch self {
        case .lowestPrice: return .ascending
        case .highestPrice: return .descending
        case .aName: return .ascending
        case .zName: return .descending
        }
    }
    
}

struct Constant {
    static let idApp = "hoichone"
    static let webUrl = "http://hangviet360.com"
    static let customURLScheme = "expo360"
    static let appDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
    static let limit = 20
    static let arrowCharacter = "‣"
    
    struct Pusher {
        static let app_key = "23b43a03aec7a3b476d6"
        static let app_id = 110053
        static let app_cluster = "ap1"
        static let end_point = "http://ishopgo.com/api/v1/chat/pusher-auth"
        
        static let channel = "my-channel"
        static let event = "new-chat"
    }
    
    struct Api {
        static let baseUrl = "http://ishopgo.expo360.vn"
    }
    
}



